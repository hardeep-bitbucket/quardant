<?php

/**
 * Partners Controller
 * 
 * 
 * @created    28/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Sonia
 */
class PartnersController extends AppController {

    var $uses = "User";

    /*
     * @Before Filter
     */

    public function beforeFilter() 
    {
        parent::beforeFilter();
        
        $this->Auth->allow(array(            
            "ajaxHomeCampaigns"
        ));
        
        $group_id = PARTNER_GROUP_ID;
        $this->set(compact("group_id"));
    }

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() 
    {
        $this->layout = "admin_inner";
        $this->Redirect->urlToNamed();
        $conditions = $this->getSearchConditions(array(
            array('model' => 'User', 'field' => 'username', 'type' => 'string', 'view_field' => 'username'),
            array('model' => 'User', 'field' => 'name', 'type' => 'string', 'view_field' => 'name'),
            array('model' => 'User', 'field' => 'type_id', 'type' => 'integer', 'view_field' => 'type_id')
        ));
        $conditions["User.group_id"] = PARTNER_GROUP_ID;

        $this->{$this->modelClass}->contain(array(
            "Campaign" => array(
                "fields" => array("id")
            ),
            "Type"
        ));
        
        $records = $this->paginate('User', $conditions);        
        $this->set(compact('records'));
    }

    /**
     * add method
     * @return void
     */
    public function admin_add() {
        parent::add();

        $heading = $title_for_layout = 'Add Partner';

        $this->set(compact('heading', 'title_for_layout'));
        $this->render('admin_form');
    }

    /*
     * @Edit Screen
     */
    public function admin_edit($id = null, $redirect = array("action" => "index")) 
    {
        $this->{$this->modelClass}->recursive = -1;
        parent::edit($id, $redirect);
        
        $heading = $title_for_layout = 'Edit Partner';
        
        $this->set(compact('heading', 'title_for_layout'));
        $this->render('admin_form');
    }    
    
    /**
     * 
     * @param int $id
     */
    public function admin_edit_profile($id = null)
    {
        $this->admin_edit($id, false);
    }
    
     /**
     * Toggles the status of is_active field
     * 
     * @param Integer $id
     * @param Integer $status
     */
    public function admin_toggleStatus($id, $status) 
    {
        $this->{$this->modelClass}->id = $id;
        $result = $this->{$this->modelClass}->saveField('is_active', !(int) $status, FALSE);        
        echo (int) $result; exit;
    }
        
    public function admin_home()
    {   
        $user_id = $this->user['id'];
        
        $location_list = $this->{$this->modelClass}->Campaign->AdLocation->Location->find("list", array(
            "fields" => array("id", "name"),
            "conditions" => array("partner_id", $this->user['id']),
            "recursive" => "-1"
        )); 
        
        $this->set(compact('location_list'));
        $this->set('title_for_layout', "Home");
    }
    
    public function ajaxHomeCampaigns($location_id = 0)
    {
        $location_id = $location_id ? $location_id : "NULL";
        $partner_id = $this->user['id'];
        
        /**
         * calling procedure
         */
        $records = $this->{$this->modelClass}->query("CALL sp_get_campaigns($location_id, $partner_id, NULL, NULL);");        
        
        $campaigns["total"] = $campaigns["active"] = $campaigns["submitted"] = $campaigns["completed"] = 0;
        $campaigns["total_spend"] = $campaigns["partner_spend"] = $campaigns["total_future_spend"] = $campaigns["partner_future_spend"] = 0;  
        
        $campaign_list = array();
        foreach ($records as $key => $record)
        {
            if (!isset($campaign_list[$record['C']['id']]))
            {
                $campaign_list[$record['C']['id']] = $record['C']['name'];
                
                //calculating current spend
                $temp = ($record["AL"]['reached_imperssions'] * $record["L"]['price_per_impression']);                
                $campaigns["total_spend"] += $temp;                
                $campaigns["partner_spend"] += (($temp * $record['L']['partner_revenue_share_percentage']) / 100);
                
                //calculating future spend
                $temp = (($record["AL"]['target_imperssions'] - $record["AL"]['reached_imperssions']) * $record["L"]['price_per_impression']);                
                $campaigns["total_future_spend"] += $temp;                
                $campaigns["partner_future_spend"] += (($temp * $record['L']['partner_revenue_share_percentage']) / 100);
                
                
                $campaigns["total"]++;
                switch($record["C"]['status_type_id'])
                {
                    case CAMPAIGN_ACTIVE_ID:
                        $campaigns["active"]++;                    
                    break;

                    case CAMPAIGN_SUBMITTED_ID:
                        $campaigns["submitted"]++;
                    break;

                    case CAMPAIGN_COMPLETED_ID:
                        $campaigns["completed"]++;
                    break;            
                }     
            }
        }
        
        $this->set(compact("campaign_list" , 'campaigns'));
    }
    
}
