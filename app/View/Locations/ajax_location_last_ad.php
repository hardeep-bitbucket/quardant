<?php
/**
 * Locations Controller
 * 
 * 
 * @created    30/03/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

?>

<style>
#ajax-loader
{
    width : 100%;     
    color : #000;
}

#ajax-loader-faliure{
    display: none;
}

#ajax-loader  .ajax-loader-content{
    width : 176px;    
    padding : 5px 30px;
    background-color: #C5EFB3;
    border: 1px solid #58a039;
    margin : 0 auto 10px;
}

#ajax-loader-faliure  .ajax-loader-content{
    background-color: #F79896;
    border: 1px solid #D11814;    
}

.location-name{
    font-size: 1em;
}

.location-name span{
    color : #447DAD;
    bold : true;
    font-size: 1em;
}
</style>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&sensor=false&libraries=geometry"></script>


<div class="row" style="height : 40px;">
    <div class="col-md-3">
        
    </div>
    <div class="col-md-4" style="height : 20px;">
        <div id="ajax-loader">
            <div class="ajax-loader-content">
                <?php
                echo $this->Html->image("admin/ajax-loader-green.gif");
                ?>
                <span style="margin:10px;">Loading.....</span>
            </div>
        </div>
    </div>
    <div class="col-md-1"></div>
    <div class="col-md-4" style="height : 20px;">        
        <span class="location-name">
            Last Ad shown on location : 
            <span id="last-location-name"></span>
        </span>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div id="map-canvas"></div>
    </div>
</div>



<script type="text/javascript">
    
    $("#map-canvas").height(window.innerHeight - 70);
    
    $(function()
    {
        var map, latlngbounds, marker = null;


        function placeMarker(point, latlngset)
        {
            if (marker)
            {
                marker.setMap(null);
            }
            
            marker = new google.maps.Marker({
                map: map,
                position: latlngset,
                icon: mapConfig.marker.icon.default         
            });

            if (typeof point['content'] != "undefined")
            {
                var content = "<div class='map-marker-window'>" + point['content'] + "</div>";

                var infoWindow = new google.maps.InfoWindow();
                infoWindow.setContent(content);

                /**
                 * adding Listeners for inflate window
                 */
                //google.maps.event.addListener(marker, 'mouseover', infoWindowOpen(infoWindow, marker));
                //google.maps.event.addListener(marker, 'mouseout', infoWindowClose(infoWindow, marker));
            }
            
            infoWindow.open(map, marker);
        }
        
        function get_last_location()
        {
            $("#ajax-loader, .ajax-loader").show();
            
            var country_id = $("#LocationCountryId").val();
            
            if (!country_id)
            {
                country_id = 0;
            }
            
            $.get('<?php echo SITE_URL; ?>Locations/ajaxGetLocationOnLastAdPlayed/' + country_id, function(data, status)
            {
                if (status == "success")
                {
                    data = JSON.parse(data);                    
                    if (typeof data.Location != "undefined")
                    {
//                        var content = "<div style='width:250px;'> <b>Location</b> : " + data["Location"]["name"] + "<br/>";
//                        content += "<b>Price Per Impression</b> : $" + data["Location"]["price_per_impression"] + "<br/>";                        
//                        content += "<b>Add. : </b>  " + data["Location"]["address"] + "<br/>";
//                        content += "<b>Play Time : </b> " + data["AdPlayedLog"]["created_on"] + "<br/>";
//                        content += "<b>Automobile : </b>  " + data["Automobile"]["name"] + "<br/>";
//                        content += "</div>";
                        
                        var content = "<div style='width:250px;'>";
                        content += "<p>An ad has been shown to a <b>" + data["Automobile"]["name"] + "</b> at <b>" + data["Location"]["address"] + "</b>";
                        content += "</div>";
                        
                        var latlng = new google.maps.LatLng(data.Location.latitude, data.Location.longitude);
                        map.panTo(latlng);
                        
                        placeMarker({ content : content}, latlng);  
                        
                        $("#last-location-name").html(data["Location"]["name"]);
                    }
                    
                    $("#ajax-loader, .ajax-loader").fadeOut(700);
                }
            });
        }

        function infoWindowOpen(infoWindow, marker)
        {
            return function() {
                infoWindow.open(map, marker);
            };
        }
        function infoWindowClose(infoWindow, marker)
        {
            return function() {
                infoWindow.close(map, marker);
            };
        }

        function initialize()
        {
            var home = new google.maps.LatLng('<?php echo MAP_HOME_LAT ?>', '<?php echo MAP_HOME_LNG ?>');

            var mapOptions = {
                zoom: 5,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: home
            };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            
            setInterval(get_last_location, 10000);
            get_last_location();
        }


        $(document).ready(function()
        {
            google.maps.event.addDomListener(window, 'load', initialize);
            
//            $("#LocationCountryId").change(function ()
//            {
//                map.setZoom(5);
//                get_last_location();
//            });
        });

    });
</script> 

