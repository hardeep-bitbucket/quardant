<!DOCTYPE html>
<html class="no-js">
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $meta_title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">       
        <link href="<?php echo SITE_URL; ?>/app/webroot/favicon.ico" type="image/x-icon" rel="icon" /><link href="<?php echo SITE_URL; ?>/app/webroot/favicon.ico" type="image/x-icon" rel="shortcut icon" />
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700%7COpen+Sans:300italic,400italic,600italic,700italic,800italic,300,400,600,700">
        <?php
        echo $this->Html->css(array('frontend/vendor', 'frontend/bootstrap', 'frontend/main', 'frontend/theme_sangria', 'frontend/custom'));
        echo $this->Html->script(array('jquery-1.7.2.min', 'frontend/vendor/modernizr', "admin/map", "admin/ajax", "common"));        
        ?>
        <noscript>
        <link rel="stylesheet" href="/frontend/styleNoJs.css">
        </noscript>
       
    </head>

    <body>
        <div id="load"></div><!--[if lt IE 9]>
<p class="browsehappy">You are using an strong outdated browser. <br>Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
        <div class="page">
            <!-- Start body Section-->
            <?php echo $this->fetch('content'); ?>
        </div>
        <!-- Load JS -->
        <?php
        echo $this->Html->script(array(
            'frontend/vendor/plugin', 
            'frontend/vendor/bootstrap',
            'frontend/vendor/bootstrap-extend', 
            'frontend/main',            
            ));
        ?>
    </body>
</html>
