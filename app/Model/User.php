<?php

/**
 * User Model
 *
 * @property Group $Group
 * @property Post $Post
 */
class User extends AppModel {

    public $actsAs = array('Acl' => array('type' => 'requester', 'enabled' => false));
    
    public $virtualFields = array(
        "full_name" => 'TRIM(CONCAT(User.name, " ", User.subname))'
    );

    public function beforeSave($options = array())
    {
        parent::beforeSave($options);
        if (isset($this->data['User']['password']) && $this->data['User']['password'])
        {
            $this->data['User']['tmp_password'] = $this->data['User']['password'];
            $this->data['User']['password'] = AuthComponent::password(
                $this->data['User']['password']
            );
        }
        
        return true;
    } 

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'username' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty',
                'message' => USERNAME_REQUIRED_MESSAGE
            ),
            "isUnique" => array(
                "rule" => "isUnique", 'allowEmpty' => true, "message" => "Email already exists"
            ),
            'email' => array(
                'rule' => array('email', true),
                'message' => 'Please Enter valid email'
            )
        ),       
        'password' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty',
                'message' => PASSWORD_REQUIRED_MESSAGE
            )
        ),
        'name' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty',
                'message' => NAME_REQUIRED_MESSAGE
            )
        ),
        'landline_no' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty',
                'message' => MOBILE_REQUIRED_MESSAGE
            ),
            'numeric' => array(
                'rule' => array('numeric'), 
                'required' => false,
                "message" => "Landline / Mobile Number sould be Numeric only, Don't use -"
            ),
            'maxLength' => array(
                'rule' => array('maxLength', 11),
                'required' => false,
                'message' => "Landline / Mobile Number should be upto %d characters.",
            ),
            'minLength' => array(
                'rule' => array('minLength', 10),
                'required' => false,
                'message' => "Landline / Mobile Number should be upto %d characters.",
            ),
        ),
        'type_id' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty',
                'message' => INDUSTRY_REQUIRED_MESSAGE
            )
        ),
        'business_name' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty',
                'message' => BUSINESS_NAME_REQUIRED_MESSAGE
            )
        ),
        'group_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            ),
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Type' => array(
            'className' => 'Type',
            'foreignKey' => 'type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        "Location" => array(
            'className' => 'Location',
            'foreignKey' => 'partner_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        "PartnerTransaction" => array(
            'className' => 'PartnerTransaction',
            'foreignKey' => 'partner_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['group_id'])) {
            $groupId = $this->data['User']['group_id'];
        } else {
            $groupId = $this->field('group_id');
        }
        if (!$groupId) {
            return null;
        } else {
            return array('Group' => array('id' => $groupId));
        }
    }
    
    public function bindNode($user) {
        return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
    }
    
    public function getUserList($conditions = array())
    {
        return $this->find("list", array(
            "fields" => array("id", "full_name"),
            "conditions" => $conditions
        ));
    }
    
}
