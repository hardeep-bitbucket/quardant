<?php
/**
 * Form Area
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Sonia
 */
$model = Inflector::classify($this->params['controller']);
$controller = $this->params['controller'];
$action = $this->params['action'];
?>
<aside class="body_rt"> 
    <!--Feedback head start-->
    <div class="page_head">
        <h1>Enquiry Manager</h1>
        <div class="back">
            <?php echo $this->Html->link($this->Html->image('images/back.png'), array('action' => 'admin_index'), array('escape' => false, 'title' => BACK_TITLE)); ?>
        </div>
    </div>
    <!--Feedback head end--> 

    <!--Blog warp start-->
    <section class="bolg_warp">
        <div class="sub_head"><?php echo $heading; ?></div>
        <div class="three_column">
            <?php echo $this->Form->create($model, array('type' => 'file')); ?>
            <h1>General Details</h1>
            <ul>
                <h3>
                    <?php echo $this->Form->hidden('id', array('label' => false, 'class' => 'text_field')); ?>

                </h3>
                <li>
                    <h2>Name:</h2>
                    <h3>
                        <?php
                        echo $this->Form->input('name', array('type' => 'text', 'label' => false,  'escape' => false, 'class' => 'text_field',  'disabled' => 'disabled'));
                        ?> 
                    </h3>
                </li>
               
            </ul>
            <h1>Contact Details</h1>
            <ul>
                <li>
                    <h2>Email</h2>
                    <h3>
                        <?php
                        echo $this->Form->input('email', array('type' => 'email', 'label' => false,  'class' => 'text_field', 'disabled' => 'disabled'));
                        ?>    
                    </h3>
                </li>
                <li>
                    <h2>Mobile:</h2>
                    <h3>
                        <?php echo $this->Form->input('mobile', array('label' => false, 'class' => 'text_field', 'disabled' => 'disabled')); ?>
                    </h3>
                </li>
                <li>
                    <h2>Type:</h2>
                    <h3>
                        <?php echo $this->Form->input('type_id', array('label' => false, 'class' => 'text_field', 'disabled' => 'disabled', 'type' => 'text', 'value' => $this->request->data["Type"]["value"])); ?>
                    </h3>
                </li>
                <li>
                    <h2>Interested IN:</h2>
                    <h3>
                        <?php echo $this->Form->input('interested_in', array('type' => 'text', 'label' => false, 'class' => 'text_field', 'disabled' => 'disabled')); ?>
                    </h3>
                </li>
                
            </ul>
            <h1>Enquiry Details</h1>
            <ul>	
                <li>
                    <h2>Date:</h2>
                    <h3>
                        <?php
                        $this->request->data["Enquiry"]["created_on"] = date(DEFAULT_DATE_FORMAT, strtotime($this->request->data["Enquiry"]["created_on"]));
                        echo $this->Form->input('created_on', array('type' => 'text', 'label' => false, 'escape' => false, 'class' => 'text_field', 'disabled' => 'disabled'));
                        ?> 
                    </h3>
                </li>
                
                <li>
                    <h2>Comments:</h2>
                    <h3>
                        <?php
                        echo $this->Form->input('comments', array('type' => 'textarea', 'label' => false, 'escape' => false, 'class' => 'text_area', 'disabled' => 'disabled'));
                        ?> 
                    </h3>
                </li>
            </ul>
        </div>
        <?php echo $this->Form->end(); ?>
    </section>
    <!--Blog warp end--> 

</aside>