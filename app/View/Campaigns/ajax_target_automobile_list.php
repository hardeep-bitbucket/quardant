<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
echo $this->Form->input($name, array(
    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
    'options' => $automobile_list, "selected" => $selected,
    "class" => "form-control chosen",
    'multiple' => 'multiple',
    'data-placeholder' => $title
));
