<?php
/**
 * flash for success
 * 
 * 
 * @created    19/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<div class="Metronic-alerts alert alert-success fade in flash-message">
     <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>    
     Success! <?php echo h($message); ?>
</div>


