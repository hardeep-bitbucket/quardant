<?php
/**
 * Enquiry Controller
 * 
 * 
 * @created    24/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h3 class="page-title">
              <?php echo $title_for_layout; ?>
            </h3>
        </div>
    </div>
</div>

<?php
    //echo $this->element("admin/breadcrumb"); 
    echo $this->Session->flash();
?>


<div class="portlet light">   
    <div class="portlet-body">        
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet green-meadow box">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-desktop"></i><?php echo $heading; ?>
                        </div>    
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>                            
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row static-info">
                            <div class="col-md-5 name">Full Name</div>
                            <div class="col-md-7 value"><?php echo $record['Enquiry']['fullname']; ?>
                            </div>
                        </div>                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Company Name</div>
                            <div class="col-md-7 value"><?php echo $record['Enquiry']['company_name']; ?>
                            </div>
                        </div>                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Company Web site</div>
                            <div class="col-md-7 value"><?php echo $record['Enquiry']['company_url']; ?>
                            </div>
                        </div>                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Post Code</div>
                            <div class="col-md-7 value"><?php echo $record['Enquiry']['postcode']; ?>
                            </div>
                        </div>                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Email Address</div>
                            <div class="col-md-7 value"><?php echo $record['Enquiry']['email']; ?>
                            </div>
                        </div>                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Phone No</div>
                            <div class="col-md-7 value"><?php echo $record['Enquiry']['phone']; ?>
                            </div>
                        </div>                           
                         <div class="row static-info">
                            <div class="col-md-5 name">Address</div>
                            <div class="col-md-7 value"><?php echo $record['Enquiry']['address']; ?>
                            </div>
                        </div>   
                         <div class="row static-info">
                            <div class="col-md-5 name">Your Spot</div>
                            <div class="col-md-7 value"><?php echo $record['Enquiry']['spot']; ?>
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
