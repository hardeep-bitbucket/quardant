<div class="modal fade" id="about-us-video-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Video</h4>
          </div>
          <div class="modal-body video-frame">
            <iframe style="width: 100%;" id="video_iframe" src="https://www.youtube.com/embed/sJnfSYOVt3A" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
    </div>
</div> 


<div class="modal fade" id="how-it-works-video-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Video</h4>
          </div>
          <div class="modal-body video-frame">
           <iframe style="width: 100%; height : 100%;" id="video_iframe" src="https://www.youtube.com/embed/U4-ag3ZfjmM" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
    </div>
</div> 


<div class="modal fade" id="ANPRMotion-map-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Last Played Ads</h4>            
            </div>
          <div class="modal-body video-frame">
           <iframe style="width: 100%; height : 100%;" id="video_iframe" src="" frameborder="0" allowfullscreen></iframe>
          </div>
        </div>
    </div>
</div> 

