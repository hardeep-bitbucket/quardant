<?php

/**
 * Page Model
 * 
 * 
 * @created    18/12/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Sonia
 */
class Location extends AppModel 
{
    var $displayField = 'name';

    // function for set location id.
    public function beforeSave($options = Array()) 
    {
        parent::beforeSave($options = Array());
        //debug($this->data['Location']); exit;
        if (isset($this->data['Location']['name']) && empty($this->data['Location']['location_id'])) 
        {
            $this->data['Location']['security_code'] = AuthComponent::password($this->data['Location']['security_code']);
            
            $name = substr($this->data['Location']['name'], 0, 6);            
            $strLength = 8;
            $this->data['Location']['location_id'] = "ANPR_" . str_pad($name, 6, "A", STR_PAD_RIGHT) . "_" . substr(uniqid(), 0, $strLength);            
        }        
        
        return true;
    }

    /**
     * Behaviours
     */
    //public  $actsAs = array('Uuid' => array('field' =>'location_uuid'));
    
    /**
    *Associations
    */
    var $belongsTo = array(
        'State' => array(
            'className' => 'Place',
            'foreignKey' => 'state_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Country' => array(
            'className' => 'Place',
            'foreignKey' => 'country_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'City' => array(
            'className' => 'Place',
            'foreignKey' => 'city_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        "User" => array(
            'className' => 'User',
            'foreignKey' => 'partner_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
    
    var $hasMany = array(
        'AdLocation' => array(
            'className' => 'AdLocation',
            'foreignKey' => 'location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'AdPlayedLog' => array(
            'className' => 'AdPlayedLog',
            'foreignKey' => 'location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        "AutomobileLocation" => array(
            'className' => 'AutomobileLocation',
            'foreignKey' => 'location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

    /**
     * Valiadtions
     */
    public $validate = array(              
        'username' => array(
            'notEmpty' => array('rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD)
        ),
        'password' => array(
            'notEmpty' => array('rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD)
        ),
        'name' => array(
            'notEmpty' => array('rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD),
            "isUnique" => array(
                "rule" => "isUnique", 'allowEmpty' => true, "message" => UNIQUE_FIELD
            ),
        ),
        'activation_key' => array(
            'notEmpty' => array( 'rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD)
        ),
        'security_code' => array(
            'notEmpty' => array( 'rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD)
        ),
        'country_id' => array(
            'notEmpty' => array( 'rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD)
        ),
        'state_id' => array(
            'notEmpty' => array( 'rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD)
        ),
        'city_id' => array(
            'notEmpty' => array( 'rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD)
        ),
        'price_per_impression' => array(
            'notEmpty' => array( 'rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD),            
            'numeric' => array(
                'rule' => array('numeric'), 'allowEmpty' => true, 'message' => NUMERIC_FIELD,
            ),
        ),     
        'setup_cost' => array(
           'notEmpty' => array( 'rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD),
           'numeric' => array(
                'rule' => array('numeric'), 'allowEmpty' => true, 'message' => NUMERIC_FIELD,
            ),
        ),
        'latitude' => array(
            'notEmpty' => array( 'rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD)
        ),        
        'longitude' => array(
            'notEmpty' => array( 'rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD)
        ),
        "partner_id" => array(
            'notEmpty' => array( 'rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD)
        ),
        "partner_revenue_share_percentage" => array(
            'notEmpty' => array( 'rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD),            
        ),
        'ws_no_of_ads' => array(
           'notEmpty' => array( 'rule' => array('notEmpty'), 'message' =>  REQUIRED_FIELD),
           'numeric' => array(
                'rule' => array('numeric'), 'allowEmpty' => true, 'message' => NUMERIC_FIELD,
            ),
            'checkZero' => array( 'rule' => array('checkZero'), 'message' =>  "Please Enter greater than 0")
        ),
    );
      
    public function checkZero()
    {
        if (isset($this->data['Location']['ws_no_of_ads']))
        {
            if (!$this->data['Location']['ws_no_of_ads'])
            {
                return false;
            }
        }
        return true;
    }
}

?>