<div id="ANPRMotion-advertiser-terms" style="display: none;">
   <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>            
</div> 

<div class="col-md-10 col-md-offset-1">
    <div class="contact-form sep-top">
            <div style="display:none" class="successMessage alert alert-success text-center">
                    <p><i class="fa fa-check-circle fa-2x"></i></p>
                    <p>Thanks.</p>
            </div>
            <div  style="display:none" class="failureMessage alert alert-danger text-center">
                <p><i class="fa fa-times-circle fa-2x"></i></p>
                <p>There was a problem. Please, try again.</p>
            </div>
            <div style="display:none" class="incompleteMessage alert alert-warning text-center">
                <p><i class="fa fa-exclamation-triangle fa-2x"></i></p>
                <p>Please complete all the mandatory fields in the form.</p>
            </div>
            <?php 
                echo $this->Form->create("User", array("controller" => "users", "action" => "add", 'type' => 'POST', 'class' => 'validate-form'));
                echo $this->Form->hidden("group_id", array("label" => false, "value" => ADVERTISER_GROUP_ID));
            ?>
            <div class="row">
                <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label class="upper" for="name">First Name  <i class="required-lbl">*</i></label>                        
                        <?php
                            echo $this->Form->input("name", array(
                                "type" => "text", "label" => false, "div" => false, "escape" => false,
                                "class" => "form-control input-lg required", 
                                "aria-required" => "true", "placeholder" => "First Name"
                            ));
                        ?>
                    </div>
                </div>
                <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label class="upper" for="subname">Last Name  <i class="required-lbl">*</i></label>                        
                        <?php
                            echo $this->Form->input("subname", array(
                                "type" => "text", "label" => false, "div" => false, "escape" => false,
                                "class" => "form-control input-lg required", 
                                "aria-required" => "true", "placeholder" => "Last Name"
                            ));
                        ?>
                    </div>
                </div>
            </div>
           <div class="row">
                <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label class="upper" for="username">Email <i class="required-lbl">*</i></label>                        
                        <?php
                            echo $this->Form->input("username", array(
                                "type" => "text", "label" => false, "div" => false, "escape" => false,
                                "class" => "form-control input-lg required", 
                                "aria-required" => "true", "placeholder" => "Email"
                            ));
                        ?>
                    </div>
                </div>
                <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label class="upper" for="password">Password  <i class="required-lbl">*</i></label>                        
                        <?php
                            echo $this->Form->input("password", array(
                                "type" => "password", "label" => false, "div" => false, "escape" => false,
                                "class" => "form-control input-lg required", 
                                "aria-required" => "true", "placeholder" => "Password"
                            ));
                        ?>
                    </div>
                </div>
            </div>
        
            <div class="row">                
                <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label class="upper" for="confirm_password">Confirm Password  <i class="required-lbl">*</i></label>                        
                        <?php
                            echo $this->Form->input("confirm_password", array(
                                "type" => "password", "label" => false, "div" => false, "escape" => false,
                                "class" => "form-control input-lg required", 
                                "aria-required" => "true", "placeholder" => "Confirm Password"
                            ));
                        ?>
                    </div>
                </div>
            </div>
           
             <div class="row">
                <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label class="upper" for="business_name">Business Name  <i class="required-lbl">*</i></label>                        
                        <?php
                            echo $this->Form->input("business_name", array(
                                "type" => "text", "label" => false, "div" => false, "escape" => false,
                                "class" => "form-control input-lg required", 
                                "aria-required" => "true", "placeholder" => "Business Name"
                            ));
                        ?>
                    </div>
                </div>
                  <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label class="upper" for="type_id">Industry  <i class="required-lbl">*</i></label>
                        <?php
                            echo $this->Form->input("type_id", array("class" => "form-control input-lg required", 
                                "id" => "type_id", "aria-required" => "true", "label" => false,
                                "div" => false, "empty" => "Select Industry", 
                                "type" => "select", "options" => $industry_types)
                            );
                        ?>                       
                    </div>
                </div> 
                
            </div>
            <div class="row">
                 <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label class="upper" for="address">Business Address  <i class="required-lbl">*</i></label>                        
                         <?php
                            echo $this->Form->textarea("address", array(
                                "type" => "text", "label" => false, "div" => false, "escape" => false,
                                "rows" => 4, 
                                "class" => "form-control input-lg required", 
                                "aria-required" => "true", "placeholder" => "Business Address"
                            ));
                        ?>
                    </div>
                </div>
                <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label class="upper" for="landline_no">Landline / Mobile Number  <i class="required-lbl">*</i></label>                        
                        <?php
                            echo $this->Form->input("landline_no", array(
                                "type" => "text", "label" => false, "div" => false, "escape" => false,
                                "class" => "form-control input-lg required", 
                                "aria-required" => "true", "placeholder" => "Landline / Mobile Number"
                            ));
                        ?>
                    </div>
                </div>               
            </div>
            <div class="row sep-top-xs">
               
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="upper" for="reference_source">How did u hear about us ?</label>
                        <?php
                            echo $this->Form->input("reference_source", array("class" => "form-control input-lg", 
                                "id" => "spont", "aria-required" => "true", "label" => false,
                                "div" => false, "empty" => "Select Answer", 
                                "type" => "select", 
                                "options" => array('1'=>'Existing Partner','2'=>'Google','3'=>'Email mailshot','4'=>'Brochure'))
                            );
                        ?>
                        
                    </div>
                </div>
                <div class="col-md-6" style="margin-top: -35px;">
                    <div class="captcha">
                        <img id="captcha_code" /> 
                        <a href="javascript:void(0);" onClick="refreshCaptcha();">
                            <img src="http://www.captchacreator.com/images/captcha/icon.jpg" alt=""></a>
                        <br/>                        
                    </div>
                    <div style="margin-top : 5px;">
                        <label class="upper">Confirmation Code</label>
                        <div class="form-group">
                            <input type="text" name="captcha" id="captcha" class="form-control input-lg required" placeholder = "Captcha">
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="row">
                <div class="col-md-6 " style="margin-top:15px;">
                    <label class="upper"></label>
                    <div class="form-group">
                        <input type="checkbox" name="is_accept" required> &nbsp; 
                        I accept the <i><b>
                           <a href="#ANPRMotion-advertiser-terms" id="ANPRMotion-advertiser-terms-link">Terms & Conditions </a></b></i>  <i class="required-lbl">*</i>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 sep-top-xs">
                    <div class="form-group text-center">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Submit</button>
                    </div>
                </div>
            </div>        
        <div class="hidden"></div>
    </div>
</div>


<script>
    
    $("#ANPRMotion-advertiser-terms-link").fancybox({
        'transitionIn'	:	'elastic',
        'transitionOut'	:	'elastic',
        'speedIn'		:	600, 
        'speedOut'		:	200, 
        'overlayShow'	:	false
    });
    
    refreshCaptcha();
</script>