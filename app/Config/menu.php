<?php
/**
 * Menu Configuration
 * 
 * @created    20/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
Class AdminMenu
{
    public static $adminMenuConfig = array(
        
        array(
            'title' => 'Home',
            'icon-class' => "icon-home",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID, ADVERTISER_GROUP_ID, PARTNER_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Home',
                    'icon-class' => "icon-home",
                    'link' => array(
                        "controller" => "users", "action" => "admin_home"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ), 
                array(
                    'text' => 'Dashboard',
                    'icon-class' => "fa fa-bars",
                    'link' => array(
                        "controller" => "users", "action" => "admin_dashboard"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),   
                array(
                    'text' => 'Settings',
                    'icon-class' => "fa fa-cogs",
                    'link' => array(
                        "controller" => "AdminSettings", "action" => "admin_edit"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID) //Group IDs
                ),   
                array(
                    'text' => 'Home',
                    'icon-class' => "icon-home",
                    'link' => array(
                        "controller" => "advertisers", "action" => "admin_home"
                    ),
                    'permissions' => array(ADVERTISER_GROUP_ID) //Group IDs
                ),   
                 array(
                    'text' => 'Dashboard',
                    'icon-class' => "fa fa-bars",
                    'link' => array(
                        "controller" => "advertisers", "action" => "admin_dashboard"
                    ),
                    'permissions' => array(ADVERTISER_GROUP_ID) //Group IDs
                ),                   
                //partner
                array(
                    'text' => 'Home',
                    'icon-class' => "icon-home",
                    'link' => array(
                        "controller" => "partners", "action" => "admin_home"
                    ),
                    'permissions' => array(PARTNER_GROUP_ID) //Group IDs
                ),   
               
            )
        ),
        array(
            'title' => 'Page',
            'icon-class' => "fa fa-paper-plane-o",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "pages", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add Page',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "pages", "action" => "admin_add"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        array(
            'title' => 'Graphic',
            'icon-class' => "fa fa-pencil-square-o",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "graphics", "action" => "admin_index"
                    ), 
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add Graphic',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "graphics", "action" => "admin_add"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        array(
            'title' => 'Module',
            'icon-class' => "fa fa-indent",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "modules", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add Module',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "modules", "action" => "admin_add"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        array(
            'title' => 'Latest Update',
            'icon-class' => "fa fa-download",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "updates", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add Latest Update',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "updates", "action" => "admin_add"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        array(
            'title' => 'Place',
            'icon-class' => "fa fa-indent",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "places", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add Place',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "places", "action" => "admin_add"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        array(
            'title' => 'Automobiles',
            'icon-class' => "fa fa-taxi",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "automobiles", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add Automobile',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "automobiles", "action" => "admin_add"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => "Import",
                    'icon-class' => "fa fa-cloud-upload",
                    'link' => array(
                        "controller" => "automobiles", "action" => "admin_import"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => "Location's Automobiles",
                    'icon-class' => "fa fa-cloud-upload",
                    'link' => array(
                        "controller" => "automobiles", "action" => "admin_locationAutomobile"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        array(
            'title' => 'Car',
            'icon-class' => "fa fa-automobile",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "cars", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add Car',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "cars", "action" => "admin_add"
                    ),                    
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Import Car',
                    'icon-class' => "icon-home",
                    'link' => array(
                        "controller" => "cars", "action" => "admin_import"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        array(
            'title' => 'Location',
            'icon-class' => "fa fa-delicious",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "locations", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add Location',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "locations", "action" => "admin_add"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        array(
            'title' => 'Log',
            'icon-class' => "fa fa-life-ring",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Log Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "import_logs", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Campaign Version Log',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "CampaignVersionLogs", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Web Service Log', 
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "web_service_logs", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Location Alert Summary', 
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "LocationWebServiceAlerts", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Cron Job Log', 
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "cron_job_logs", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        
        array(
            'title' => 'Discount',
            'icon-class' => "fa fa-money",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "discounts", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add Discount',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "discounts", "action" => "admin_add"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
         array(
            'title' => 'Invoices',
            'icon-class' => "fa fa-money",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID, ADVERTISER_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "invoices", "action" => "admin_index"
                    ),
                    'actions' => array("admin_form"),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID, ADVERTISER_GROUP_ID) //Group IDs
                ),  
            )
        ),
        array(
            'title' => 'Transaction',
            'icon-class' => "fa fa-money",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID, ADVERTISER_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "transactions", "action" => "admin_index"
                    ),
                    'actions' => array("admin_view"),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID, ADVERTISER_GROUP_ID) //Group IDs
                ), 
                array(
                    'text' => 'Recharge Wallet',
                    'icon-class' => "fa fa-money",
                    'link' => array(
                        "controller" => "transactions", "action" => "admin_advertiser_recharge"
                    ),
                    'actions' => array("admin_view"),
                    'permissions' => array(ADVERTISER_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Cash Back',
                    'icon-class' => "fa fa-money",
                    'link' => array(
                        "controller" => "transactions", "action" => "admin_advertiser_refund"
                    ),
                    'actions' => array("admin_view"),
                    'permissions' => array(ADVERTISER_GROUP_ID) //Group IDs
                ), 
                array(
                    'text' => 'Recharge Wallet',
                    'icon-class' => "fa fa-money",
                    'link' => array(
                        "controller" => "transactions", "action" => "admin_recharge_wallet"
                    ),
                    'actions' => array("admin_view"),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ), 
                array(
                    'text' => 'Cash Back Wallet',
                    'icon-class' => "fa fa-money",
                    'link' => array(
                        "controller" => "transactions", "action" => "admin_refund_wallet"
                    ),
                    'actions' => array("admin_view"),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),                 
            )
        ),
        array(
            'title' => 'Enquiry',
            'icon-class' => "icon-grid",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(               
                array(
                    'text' => 'Parnter',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "enquiries", "action" => "admin_partner_index"
                    ),
                    'actions' => array("admin_partner_view"),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Request A Demo',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "enquiries", "action" => "admin_request_demo_index"
                    ),
                    'actions' => array("admin_request_demo_view"),                    
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        array(
            'title' => 'Campaign',
            'icon-class' => "fa fa-tasks",
            'permissions' => array(ADMIN_GROUP_ID, ADVERTISER_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "campaigns", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, ADVERTISER_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add Campaign',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "campaigns", "action" => "admin_add"
                     ),
                    'permissions' => array(ADMIN_GROUP_ID, ADVERTISER_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                 array(
                    'text' => 'Efficiency Report',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "campaigns", "action" => "admin_efficiency_report"
                     ),
                    'permissions' => array(ADMIN_GROUP_ID, ADVERTISER_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        array(
            'title' => 'User',
            'icon-class' => "fa fa-users",
            'permissions' => array(ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "users", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add User',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "users", "action" => "admin_add"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        array(
            'title' => 'Advertiser',
            'icon-class' => "fa fa-user",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "advertisers", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add Advertiser',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "advertisers", "action" => "admin_add"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
        array(
            'title' => 'Partner',
            'icon-class' => "fa fa-user",
            'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID),
            'links' => array(
                array(
                    'text' => 'Summary',
                    'icon-class' => "fa fa-th",
                    'link' => array(
                        "controller" => "partners", "action" => "admin_index"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                ),
                array(
                    'text' => 'Add Partner',
                    'icon-class' => "fa fa-plus",
                    'link' => array(
                        "controller" => "partners", "action" => "admin_add"
                    ),
                    'permissions' => array(ADMIN_GROUP_ID, SUB_ADMIN_GROUP_ID) //Group IDs
                )
            )
        ),
    );
}