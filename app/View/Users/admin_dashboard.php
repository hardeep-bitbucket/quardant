<?php

/**
 * dashboard User 
 * 
 * 
 * @created    14/03/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

?>
<h3>Dashboard</h3>
<div class="row margin-top-10"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">Campaigns</span>
                </div>
                <div class="pull-right">
                    <div class="caption caption-md">
                        <?php
                           echo $this->Form->input("country", array(
                                "type" => "select", "div" => false, "label" => false, "escape" => false,
                                "options" => $country_list, "empty" => "All",
                                "class" => "form-control"
                            ));
                        ?>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div id="campaign-block">
                </div>   
            </div>
         </div>
    </div>
</div>



<div class="row margin-top-10"> 
    <div class="col-md-6">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">Campaigns Completed in</span>
                </div>
                <div class="pull-right">
                    <div class="caption caption-md">
                       <?php
                            $options = array(
                                "-7" => "Last 7 Days",
                                "-15" => "Last 15 Days",
                                "-30" => "Last 30 Days",                                                  
                            );
                         echo $this->Form->input("completed_days", array(
                              "type" => "select", "div" => false, "label" => false, "escape" => false,
                              "options" => $options,
                              "class" => "form-control"
                          ));
                      ?>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div id="campaign-completed-block">
                </div>   
            </div>
         </div>
    </div>
    <div class="col-md-6">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">Campaigns Will Complete in</span>
                </div>
                <div class="pull-right">
                    <div class="caption caption-md">
                       <?php
                            $options = array(
                                "7" => "Next 7 Days",
                                "15" => "Next 15 Days",
                                "30" => "Next 30 Days",                                                  
                            );
                         echo $this->Form->input("will_complete_days", array(
                              "type" => "select", "div" => false, "label" => false, "escape" => false,
                              "options" => $options,
                              "class" => "form-control"
                          ));
                      ?>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div id="campaign-will-complete-block">
                </div>   
            </div>
         </div>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#country").change(function()
        {
            var v = $(this).val();
            
            $("#campaign-block").load("<?php echo SITE_URL.$controller; ?>/ajaxHomeDetails/" + v, function (data, status)
            {
                $("#completed_days, #will_complete_days").trigger("change");
            });
        });
        
        $("#will_complete_days").change(function()
        {
            var country_id = $("#country").val();
            country_id = country_id ? country_id : 0;
            var v = $(this).val();
            
            $("#campaign-will-complete-block").load("<?php echo SITE_URL.$controller; ?>/ajaxGetCompleteCampaigns/" + country_id + "/" + v, function (data, status)
            {
            });
        });
        
        $("#completed_days").change(function()
        {
            var country_id = $("#country").val();
            country_id = country_id ? country_id : 0;
            var v = $(this).val();
            
            $("#campaign-completed-block").load("<?php echo SITE_URL.$controller; ?>/ajaxGetCompleteCampaigns/" + country_id + "/" + v, function (data, status)
            {
            });
        });
        
        $("#country").trigger('change');      
    });
</script>

