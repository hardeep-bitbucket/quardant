<?php
/**
 * home Advertiser
 * 
 * 
 * @created    03/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<div class="row">
    <div class="col-md-5">
        <div style="width: 100%;" class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light">                
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <span class="font-size-24">Hi, </span>
                    <span class="profile-usertitle-name">
                         <?php echo $auth_user["full_name"]; ?>
                    </span>                    
                </div>    
                
                <div class="margin-top-15">                    
                    <div class="margin-5">
                        <span class="profile-desc-text highlighted"> Your Industry : <b><?php echo $auth_user["Type"]["value"]; ?></b> </span>
                    </div>                    
                    <div class="margin-5">
                        <span class="profile-desc-text highlighted"> Your Business : <b><?php echo $auth_user["business_name"]; ?></b> </span>
                    </div>  
                    <?php if ($auth_user["last_login"]): ?>
                    <div class="margin-5">
                        <span class="profile-desc-text highlighted"> Last Login : <b><?php echo DateUtility::getFormatDateFromString($auth_user["last_login"], DEFAULT_DATETIME_FORMAT); ?></b> </span>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="margin-top-15">
                    <div class="profile-userbuttons">                        
                        <?php
                            echo $this->Html->link("<b> Profile </b>", 
                                array("controller" => "advertisers", "action" => "admin_edit_profile", $auth_user['id']), 
                                array("escape" => false, "class" => "btn btn-circle green-soft btn-sm")
                            );
                        ?>
                        
                        <a href="mailto:<?php echo FROM_EMAIL; ?>">
                            <button class="btn btn-circle blue-soft btn-sm" type="button">Contact Admin</button>
                        </a>
                    </div>
                </div>                  
                <hr>
                <div class="margin-top-10"> 
                    <?php if ($pending_invoice_amount): ?>
                        <div class="margin-5">
                            <span class="profile-desc-text highlighted">Payment Due :  <span  style="color: #CC4444; font-size : 17px;"> <?php echo CURRENCY_SYMBOL; ?><b><?php echo $pending_invoice_amount; ?></b> </span> </span>
                        </div>
                        <hr>
                    <?php endif; ?>
                    
                    <?php if ($auth_user['wallet_amount']): ?>
                        <div class="margin-5">
                            <span class="profile-desc-text highlighted"> Your Wallet Amount  : <span  style="color: #447DAD; font-size : 17px;"> <?php echo CURRENCY_SYMBOL; ?><b style="color: #447DAD; font-size : 17px;"><?php echo $auth_user['wallet_amount']; ?></b> </span> </span>                        
                        </div>
                    <?php endif; ?>
                </div>
                <div class="margin-top-15">
                    <div class="profile-userbuttons">  
                        <a href="#wallet-recharge-form" id="wallet-recharge">
                            <button class="btn btn-circle green-soft btn-sm" type="button">Recharge</button>
                        </a>                       
                        <?php 
                            echo $this->Html->link('<button class="btn btn-circle blue-soft btn-sm" type="button">Cash Back</button>', 
                                    array("controller" => "transactions", "action" => "refund", "admin" => true),
                                    array("escape" => false)
                            );
                        ?>
                    </div>
                </div>  
                              
            </div> 

        </div>
    </div>
    
    <div class="col-md-7">
        <!-- BEGIN PORTLET -->
        <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">                    
                    <i class="icon-bell"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">Notifications</span>                    
                    
                    <span class="badge badge-default" id="notification-count"> 0 </span>                    
                </div>
            </div>
            <div class="portlet-body">
                <div style="height : 208px; overflow-y: auto;">
                <ul class="feeds" id="notification-list">                    
                </ul>
                </div>
                
                <hr>
                <div style="text-align: center;">
                    <a href="#">
                        <span id="load-notification">Load old Notifications</span>
                    </a>
                </div>
            </div>
        </div>
        <!-- END PORTLET -->
    </div>
</div>

<div class="row margin-top-10"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">Campaigns</span>
                </div>
                <div class="pull-right">
                    <div class="caption caption-md">
                        <?php
                           echo $this->Form->input("country", array(
                                "type" => "select", "div" => false, "label" => false, "escape" => false,
                                "options" => $country_list, "empty" => "All",
                                "class" => "form-control"
                            ));
                        ?>
                    </div>
                </div>
            </div>
            <div class="portlet-body" id="campaign-block">
                
            </div>
         </div>
    </div>
</div>
<div id="temp-list" style="display: none;"></div>

<div id="wallet-recharge-form" style="display: none;">
    <div style="width : 500px;">
        <div class="portlet light">
           <div class="portlet-title tabbable-line">
               <div class="caption caption-md">
                   <i class="icon-globe theme-font hide"></i>
                   <span class="caption-subject font-blue-madison bold uppercase">Recharge Wallet</span>
               </div>                    
           </div>
           <div class="portlet-body" id="campaign-block">
               <form action='<?php echo PAYPAL_URL; ?>' method='post' name='frmPayPal1' id='paypal-recharge-form' class="form-horizontal">
                       <input type='hidden' name='business' value='<?php echo $adminSetting['paypal_id'];?>'>
                       <input type='hidden' name='cmd' value='_xclick'>

                       <input type='hidden' name='item_name' value='ANPRMotion Wallet Recharge'>
                       <input type='hidden' name='item_number' value='<?php echo $auth_user['id'];?>'>
                       <input type='hidden' name='no_shipping' value='1'>
                       <input type='hidden' name='currency_code' value='<?php echo PAYPAL_CURRENCY; ?>'>
                       <input type='hidden' name='handling' value='0'>
                       <input type='hidden' name='cancel_return' value='<?php echo SITE_URL . "transactions/payPalFailure" ?>'>
                       <input type='hidden' name='return' value='<?php echo SITE_URL. "transactions/payPalSuccess" ?>'> 
                       <input type="hidden" value="2" name="rm">
                       
                       <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Amount(<?php echo CURRENCY_SYMBOL; ?>)</label>
                                    <div class="col-md-8">  
                                        <input name='amount' value='0' class="amount form-control  numeric postive" required="required"> 
                                        <span id="amount-validation-msg" class="required"></span>
                                    </div>
                                </div>
                            </div>
                           
                           <div class="col-md-4" style="text-align: right;">
                                <?php
                                    echo $this->Form->button("Recharge Now", array("class" => "btn blue btn-save", 'id' => "pay-now", "type" => "submit", 'label' => false, 'div' => false,)); 
                                ?>
                           </div>
                       </div>
                   </form>
           </div>
        </div>
   </div>
</div>

<div id="wallet-refund-form" style="display: none;">
    <div class="row margin-top-10"> 
        <div class="col-md-12">
             <div class="portlet light">
                <div class="portlet-title tabbable-line">
                    <div class="caption caption-md">
                        <i class="icon-globe theme-font hide"></i>
                        <span class="caption-subject font-blue-madison bold uppercase">Refund Wallet</span>
                    </div>                    
                </div>
                <div class="portlet-body" id="campaign-block">

                </div>
             </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#country").change(function()
        {
            var v = $(this).val();
            
            $("#campaign-block").load("<?php echo SITE_URL.$controller; ?>/ajaxHomeCampaigns/" + v, function (data, status)
            {
            });
        });
        
        $("#country").trigger('change');
        
        //notifications
        
        $("#load-notification").click(function()
        {
            var id = $("#notification-list li:last").attr("id");
            if (!id)
            {
                id = 0;
            }
            
            $("#temp-list").load('<?php echo SITE_URL ?>Notifications/ajaxGetNotification/' + id, function(data, status)
            {
                 $("#notification-list").append($("#temp-list").html());  
                 
                 $("#notification-count").html($("#notification-list li").length);
            });
        });
        
        
        $("#wallet-recharge").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'speedIn'		:	600, 
            'speedOut'		:	200, 
            'overlayShow'	:	false
        });
        
        $("#paypal-recharge-form").submit(function()
        {
            var v = $("#paypal-recharge-form .amount").val();
            
            if (v)
            {
                v = parseFloat(v);
                if (v)
                {
                    return true;
                }
            }
            $("#amount-validation-msg").html("Enter Valid Amount").show();
            
            return false;
        });
        
        $("#load-notification").trigger("click");
    });
</script>