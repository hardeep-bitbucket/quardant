<?php
/**
 * @package    Date Time Format
 * 
 * 
 * @created    25/03/2015
 * @package    ANPR
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class DateTimeFormatBehavior extends ModelBehavior 
{
	/**
	 * Initiate setup
	 * 
	 * @param Model $model
	 * @param Array $config
	 */
    public function setup(Model $model, $config = array()) 
	{
        foreach ($config as $field => $arr)
        {
            if (!isset($arr["out_format"]))
            {
                $config[$field]["out_format"] = DEFAULT_DATETIME_FORMAT;
            }
            if (!isset($arr["in_format"]))
            {
                $config[$field]["in_format"] = DEFAULT_SQL_DATETIME_FORMAT;
            }
        }
        
        $this->settings[$model->alias] = $config;        
    }

	/**
	 * Sets date in dd-mmm-yyyy format
	 * 
	 * @param \Model $model
	 * @param Array $results
	 * @param boolean $primary
	 */
	public function afterFind(Model $model, $results, $primary = false)
	{
		parent::afterFind($model, $results, $primary);
		
		foreach($results as $key => $record)
		{
			foreach($this->settings[$model->alias] as $field => $config)
			{
				if (isset($record[$model->alias]) && isset($results[$key][$model->alias][$field]) && $record[$model->alias][$field] && $config["out_format"])
				{
                    //$results[$key][$model->alias][$field] = DateUtility::getClientDateString($record[$model->alias][$field], $config["out_format"]);
                    $results[$key][$model->alias][$field] = DateUtility::getFormatDateFromString($record[$model->alias][$field], $config["out_format"]);
				}
			}
		}
		
		return $results;
	}
	
	/**
	 * Sets date in yyyy-mm-dd format
	 * 
	 * @param \Model $model
	 * @param Array $options
	 */
	public function beforeSave(Model $model, $options = array())
	{
		foreach($this->settings[$model->alias] as $field => $config)
		{
			if (isset($model->data[$model->alias][$field]) && $config["in_format"])
			{
                $model->data[$model->alias][$field] = DateUtility::getFormatDateFromString($model->data[$model->alias][$field], $config["in_format"]);
			}
		}
		parent::beforeSave($model, $options);
	}

}