<?php

/**
 * @package    Graphic
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */
class Graphic extends AppModel {

    /**
     * associations belogs
     * @var type array
     */
    var $belongsTo = array(
        'Type' => array(
            'className' => 'Type',
            'foreignKey' => 'type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /*
     * Behaviours 
     */
    var $actsAs = array(
        'MeioUpload' => array(
            'image' => array(
                'dir' => GRAPHIC_IMAGES,
                'create_directory' => false,
                'allowed_mime' => array('image/jpeg', 'image/pjpeg', 'image/png'),
                'allowed_ext' => array('.jpg', '.jpeg', '.png'),
                'maxSize' => 1214400,
                'thumbnails' => true,
                'thumbsizes' => array(
                    'resize' => array('width' => 1500, 'height' => 753, 'maxDimension' => '', 'thumbnailQuality' => 100,
                        'zoomCrop' => true)
                ),
                'thumbnailDir' => false,
                'validations' => false
            ),
        'logo' => array(
                'dir' => GRAPHIC_LOGO_IMAGES,
                'create_directory' => false,
                'allowed_mime' => array('image/jpeg', 'image/pjpeg', 'image/png'),
                'allowed_ext' => array('.jpg', '.jpeg', '.png'),
                'maxSize' => 1214400,
                'thumbnails' => true,
                'thumbsizes' => array(
                    'resize' => array('width' => 200, 'height' => 200, 'maxDimension' => '', 'thumbnailQuality' => 100,
                        'zoomCrop' => true)
                ),
                'thumbnailDir' => false,
                'validations' => false
            )
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'type_id' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => TYPE_REQUIRED_MESSAGE)
        ),  
    );

}
