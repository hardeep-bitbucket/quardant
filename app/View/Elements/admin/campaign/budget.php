<?php
/**
 * Campaign Controller
 * 
 * budget tab
 * 
 * @created    06/03/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal', 'id' => 'budget-form', 'enctype' => "multipart/form-data"));
echo $this->Form->hidden('id', array('label' => false)); 
echo $this->Form->hidden('tab_name', array('label' => false)); 
?>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-4">Max Budget (<?php echo CURRENCY_SYMBOL; ?>) <span class="required">*</span></label>
            <div class="col-md-8">                                            
                <?php
                    echo $this->Form->input('budget', array(
                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                        
                        'class' => 'form-control numeric postive',
                    ));
                ?>                                            
            </div>
        </div>
    </div>
    
    <div class="col-md-6">
        
    </div>
</div>

<div class="portlet-body">
    <table class="table table-bordered table-striped table-condensed table-custom">
        <thead>
            <tr class="head" >
                <td width="4%" class="td-center">Sr.</td>
                <td>name</td>
                <td width="12%" class="td-center">Price Per Impression (<?php echo CURRENCY_SYMBOL; ?>)</td>
            <?php  if ($auth_user["group_id"] == ADMIN_GROUP_ID) : ?>
                <td width="12%" class="td-center">Setup Cost (<?php echo CURRENCY_SYMBOL; ?>)</td>
                <td width="5%" class="td-center">Exclusive</td>
            <?php endif; ?>
                <td width="10%" class="td-center">Budget Per Location (%)</td>
                <td width="12%" class="td-center">Budget Per Location (<?php echo CURRENCY_SYMBOL; ?>)</td>
                <td width="8%">Impressions</td>                
                <td width="5%" class="td-center">Action</td>
            </tr>        
        </thead>
        <tbody>
            <?php $i = 0;  foreach ($records as $record) {  $i++; ?>
                <tr>
                    <td class="td-center"><?php echo $i; ?></td>
                    <td><?php echo $record['Location']['name']; ?></td>
                    <td class="td-center">
                        <?php 
                            echo $record['Location']['price_per_impression']; 
                            echo $this->Form->hidden('AdLocation.' . $record["AdLocation"]['id'] . '.price_per_impression', array(
                                'label' => false, "value" => $record['Location']['price_per_impression']
                            ));
                        ?>
                    </td>
                <?php  if ($auth_user["group_id"] == ADMIN_GROUP_ID) : ?>
                    <td class="td-center"><?php echo $record['Location']['setup_cost']; ?></td>   
                    <td class="td-center">
                        <?php
                            $img = $record['Location']['is_exclusive'] == 1 ? "admin/status_1.png" : "admin/status_0.png";
                            echo $this->Html->image($img,array(
                                "alt" => "Active",                                    
                                "class" => "summary-action-icon",
                                "escape" => false
                            ));
                        ?>
                    </td>  
                <?php endif; ?>
                    <td>
                        <?php 
                            echo $this->Form->input('AdLocation.' . $record["AdLocation"]['id'] . '.actual_budget_per', array(
                                'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                'class' => 'my-text actual_budget_per  percentage postive' 
                            ));
                        ?>
                    </td>
                    <td>
                        <?php                            
                            echo $this->Form->input('AdLocation.' . $record["AdLocation"]['id'] . '.actual_budget', array(
                                'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                "class" => "my-text ad_location-budgets numeric postive",
                                'data-price_per_impression' => $record['Location']['price_per_impression'],
                            ));
                        ?>
                    </td>
                    <td>
                        <?php 
                            echo $this->Form->input('AdLocation.' . $record["AdLocation"]['id'] . '.target_imperssions', array(
                                'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                        
                                'class' => 'my-text ad_location-impressions', "readOnly"
                            ));
                        ?>
                    </td>
                    <td class="td-center">
                        <?php
                        if ($i > 1 && $can_save)
                        {
                            echo $this->Html->link(
                                        "link", 
                                        array("controller" => 'AdLocations', 'action' => 'admin_delete', $record['AdLocation']['id']), 
                                        array('escape' => false, 'title' => DELETE_TITLE, "class" => "summary-action-delete-link")
                                    );

                             echo $this->Html->image("admin/summary_delete.png" , array(
                                 "class" => "summary-action-icon summary-action-delete",
                                 "data-confirm_text" => "Are you sure to delete record ?"
                             ));
                        }
                        ?>
                    </td>
                </tr>
            <?php } ?>
                
                <tr style="font-weight: bold;">
                    <?php  if ($auth_user["group_id"] == ADMIN_GROUP_ID) : ?>
                    <td colspan="5"></td>
                    <?php else :?>
                    <td colspan="3"></td>
                    <?php endif;?>
                    <td id="sum-per">                        
                    </td>
                    <td id="sum-total">                        
                    </td>
                    <td id="sum-impression">                        
                    </td>
                    <td>                        
                    </td>
                </tr>
        </tbody>
    </table>
</div>

<?php echo $this->element("admin/tab_save_btn"); ?>

<script>

$(document).ready(function()
{
    function get_total_budget(warning)
    {
       var v = $("#CampaignBudget").val();
       
       if (v != "")
       {
           v = parseFloat(v);
       }       
       else
       {
           v = 0;
       }
       
       if (v == 0 && warning)
       {
           warn("Please Enter total Budget");
       }
       
       return v;
    }
    
    function get_grand_total()
    {
        var v = 0;
        $(".ad_location-budgets").each(function()
        {
            v += parseFloat($(this).val());
        });
        return v;
    }
    
    function load_budget_per()
    {
        var total = get_total_budget();
        if (total > 0)
        {
            $(".ad_location-budgets").each (function()
            {
                var price = $(this).val();
                $(this).parents("tr").find(".actual_budget_per").val((price * 100/total).toFixed(2)).trigger("blur");
            });
        }
    }
    
    
    $("#CampaignBudget").blur(function (e, opt)
    {
        var warning = typeof opt == "undefined" ? true : opt.warning;
        var v = get_total_budget(warning);
        
        if (v == 0)
        {
            $(".actual_budget_per").attr("readOnly", "readOnly");
            $(".actual_budget_per").val(0).trigger("blur");
        }
        else
        {
            var record_count = '<?php echo count($records); ?>';
            if (record_count == 1)
            {
                $(".actual_budget_per").val(100);
            }
            
            $(".actual_budget_per").removeAttr("readonly").trigger("blur");                  
        }
    });
    
    $(".actual_budget_per").blur(function ()
    {
        var total = get_total_budget();
        var v = $(this).val();
        
        var amt = Math.round(total * (v/100));
        $(this).parents("tr").find(".ad_location-budgets").val(amt).trigger("blur");
        
        $(this).val(v);
        
        v = 0;
        $(".actual_budget_per").each(function()
        {
            v += parseFloat($(this).val());
        });
        
        $("#sum-per").html(v + "%");
    });
    
    $(".ad_location-budgets").blur(function ()
    {        
        var total = get_total_budget();
        var price = $(this).data("price_per_impression");
        var val = $(this).val();
        var v = Math.floor(val / price);
        
        $(this).parents("tr").find(".actual_budget_per").val(Math.round(val * 100/ total));
        
        $(this).parents("tr").find(".ad_location-impressions").val(v);
        
        v = 0;
        $(".ad_location-budgets").each(function()
        {
            v += parseFloat($(this).val());
        });
        
        $("#sum-total").html(v);
        
        v = 0;
        $(".actual_budget_per").each(function()
        {
            v += parseFloat($(this).val());
        });
        
        $("#sum-per").html(v + "%");
        
        v = 0;
        $(".ad_location-impressions").each(function()
        {
            v += parseFloat($(this).val());
        });
        
        $("#sum-impression").html(v);
    });
    
    $("form").submit(function()
    {
        var grand = get_grand_total();
        var total = parseFloat($("#CampaignBudget").val());
        
        if (total == 0 || grand == 0)
        {
            warn("Please enter amount");
            return false;
        }
        
        if (grand > total)
        {
            warn("Budget Per Location should be less than or equal to Max Budget");
            return false;
        }
        
        return true;
    });
    
    load_budget_per();
    
    $("#CampaignBudget").trigger("blur", { warning : false });
    
});

</script>
