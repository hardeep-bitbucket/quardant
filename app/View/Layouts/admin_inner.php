<?php

/**
 * Layout Admin inner 
 * 
 * 
 * @created    19/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */

if (!isset($title_for_layout))
{
    $title_for_layout = SITE_NAME;
}

?>

<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title><?php echo $title_for_layout; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="favicon.ico"/>

<?php
    echo $this->Html->css(array(
        "assets/global/plugins/font-awesome/css/font-awesome.min",
        "assets/global/plugins/simple-line-icons/simple-line-icons.min",
        "assets/global/plugins/bootstrap/css/bootstrap.min",
        "assets/global/plugins/uniform/css/uniform.default",
        "assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min",        
        "assets/global/css/components",
        "assets/global/css/plugins",
        "assets/admin/layout2/css/layout",
        "assets/admin/layout2/css/themes/grey",
        "assets/admin/layout2/css/custom",
        "assets/global/plugins/bootstrap-datepicker/css/datepicker",
        // fancy box
        "fancybox/source/jquery.fancybox", "fancybox/source/helpers/jquery.fancybox-buttons", 
        "fancybox/source/helpers/jquery.fancybox-thumbs",        
        
        //sweet alert
        "sweet_alert/sweet-alert", "sweet_alert/ie9",  
        
        //multi select
        "chosen/chosen.min", "chosen/prism",
        
        //tree view
        "assets/global/plugins/jstree/dist/themes/default/style.min",
        
        //timepicker
        "assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min",
        "assets/admin/pages/css/profile-old",
        "assets/admin/pages/css/profile",        
        
        "assets/admin_override",       
        "admin/admin_custom"
    ));   
    
     echo $this->Html->script(array(
        "assets/global/plugins/jquery.min",
        "assets/global/plugins/jquery-migrate.min",
        "assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min",
        "assets/global/plugins/bootstrap/js/bootstrap.min",
        "assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min",
        "assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min",
        "assets/global/plugins/jquery.blockui.min",
        "assets/global/plugins/jquery.cokie.min",
        "assets/global/plugins/uniform/jquery.uniform.min",
        "assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min",
        "assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker",         
        "assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min" ,
         
        "assets/global/scripts/metronic",
        "assets/admin/layout2/scripts/layout",
        "assets/admin/layout2/scripts/demo",
         
        //fancy box
        "fancybox/lib/jquery.mousewheel-3.0.6.pack", "fancybox/source/jquery.fancybox",
        "fancybox/source/helpers/jquery.fancybox-buttons", "fancybox/source/helpers/jquery.fancybox-thumbs",
        "fancybox/source/helpers/jquery.fancybox-media",
        
        //sweet alert
        "sweet_alert/sweet-alert.min",  
         
        //tree view 
        "assets/global/plugins/jstree/dist/jstree.min",
         
         //timepicker
        "assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min",
        "assets/admin/pages/scripts/components-pickers",
         
         //am chart
         "assets/global/plugins/amcharts/amcharts/amcharts",
         "assets/global/plugins/amcharts/amcharts/serial",
         "assets/global/plugins/amcharts/amcharts/pie",
         "assets/global/plugins/amcharts/amcharts/themes/light",         
         "assets/global/plugins/amcharts/amcharts/exporting/amexport",
         "assets/global/plugins/amcharts/amcharts/exporting/canvg",
         "assets/global/plugins/amcharts/amcharts/exporting/rgbcolor",
         "assets/global/plugins/amcharts/amcharts/exporting/filesaver",
         
         "admin/common", "admin/ajax", "admin/map", "admin/chart",
         "common"
    ));
?>
<!-- in this section sets constant variables  required in all js -->
<script>
    var Constants = {
        SUMMARY_ACTIVE_CLASS : '<?php echo SUMMARY_ACTIVE_CLASS; ?>',
        SUMMARY_INACTIVE_CLASS : '<?php echo SUMMARY_INACTIVE_CLASS; ?>'        
    };
    
</script>
</head>

<body class="page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo ">
    <?php echo $this->element("admin/header"); ?>    
    <div class="clearfix"></div>
    
    <div class="container">
        <div class="page-container">
            <?php echo $this->element("admin/sidebar"); ?>
            
            <div class="page-content-wrapper">
                <div class="page-content" id="main-body">   
                    <?php echo $this->fetch('content'); ?>  
                </div>
            </div>
        </div>
        
        <?php echo $this->element("admin/footer"); ?>
    </div>    
</body>
   
<script>
    jQuery(document).ready(function() {       
       // initiate layout and plugins
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
        //ComponentsPickers.init();
    });
    
    /** ------ bootstarp ------------ */
    $(".datepicker").datepicker({
        format: '<?php echo DEFAULT_JS_DATE_FORMAT; ?>', 
        autoclose: true,
    });	
    
</script>
