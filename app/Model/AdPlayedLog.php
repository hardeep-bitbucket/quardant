<?php

/**
 * AdPlayedLog Model
 * 
 * 
 * @created    12/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     HARDEEP
 */
class AdPlayedLog extends AppModel {

    /**
     * Associations
     */
    var $belongsTo = array(
        'AdLocation' => array(
            'className' => 'AdLocation',
            'foreignKey' => 'ad_location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Location' => array(
            'className' => 'Location',
            'foreignKey' => 'location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),   
        'Variant' => array(
            'className' => 'Automobile',
            'foreignKey' => 'automobile_variant_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}

?>