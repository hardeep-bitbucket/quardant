<?php

/**
 * Users Controller
 * 
 * forget password template
 * 
 * 
 * @created    21/04/2015
 * @package    ANPR
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     hardeep
 */
?>
<pre>
Hello <?php echo $User['name'] ?>,

You have received this email because you have requested your password to be reset.

Your new password  : <b><?php echo $User['name'] ?></b>

If you have not requested a password reset please get in touch with us as soon as possible.

Kind Regards,