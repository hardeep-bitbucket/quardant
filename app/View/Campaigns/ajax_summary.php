<?php

/**
 * Campaigns Controller
 * 
 * ajax summary call by dashbaord
 * 
 * @created    12/03/2015
 * @package    ANPR
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>
<div class="portlet box blue-soft ajax-campaign-summary">
    <div class="portlet-title">
        <div class="caption">Campaigns, Location & Transactions</div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">
        <table class="table table-bordered table-striped table-condensed  table-custom nested-table">
            <thead>
               <tr class="head tr-border-blue-madison bold" >
                    <td width="4%" class="td-center"></td>
                    <td width="15%"><?php  echo $this->Paginator->sort("Campaign.name", "Campaign", array('class' => "ajax-page-link")); ?></td>
                    <td width="15%"><?php  echo $this->Paginator->sort("User.name", "Advertiser", array('class' => "ajax-page-link")); ?></td>
                    <td width="10%" class="td-center"><?php  echo $this->Paginator->sort("start_date", "Start Date", array('class' => "ajax-page-link")); ?></td>
                    <td width="10%" class="td-center"><?php  echo $this->Paginator->sort("Campaign.end_date", "End Date", array('class' => "ajax-page-link")); ?></td>
                    <td width="10%" class="td-center"><?php  echo $this->Paginator->sort("Campaign.budget", "Max Budget(" . CURRENCY_SYMBOL . ")", array('class' => "ajax-page-link")); ?></td>
                    <td width="10%" class="td-center"><?php  echo $this->Paginator->sort("CampaignStatus.value", "Status", array('class' => "ajax-page-link")); ?></td>
                    <td width="10%" class="td-center">Completion(%)</td>
                </tr>
            </thead>
            <tbody>
                <?php $a = 0; if ($records)  foreach ($records as $record) { $a++; ?>
                    <tr>
                        <td class="td-center">
                            <?php  if ($record['Invoice'] || $record['AdLocation']) :  ?>
                                    <span class="row-table-childs child-table-toggler  child-table-close" data-tr_id="<?php echo $a; ?>"></span>
                            <?php endif; ?>
                        </td>
                        <td> 
                            <?php 
                                //echo $record['Campaign']['name']; 
                                echo $this->Html->link($record['Campaign']['name'],
                                    array("controller" => "campaigns", "action" => "edit", $record['Campaign']['id'], "admin" => true),
                                    array()
                                );
                            ?>
                        </td>                                
                        <td> <?php echo $record['User']['name'] . " " . $record['User']['subname']; ?></td>
                        <td class="td-center"> <?php echo $record['Campaign']['start_date']; ?></td>
                        <td class="td-center"> <?php echo $record['CampaignDuration']['id'] == CAMPAIGN_DURATION_FIXED_ID ? $record['Campaign']['end_date'] : ""; ?></td>
                        <td class="td-center"> <?php echo $record['Campaign']['budget']; ?></td>
                        <td class="td-center"> <?php echo $record['CampaignStatus']['value']; ?></td>
                        <td class="td-center"> <?php echo $record['Campaign']['target_complete']; ?></td>
                    </tr>
                    <?php if ($record['Invoice'] || $record['AdLocation']) : ?>
                        <tr class="row-child-table hidden" id="<?php echo $a; ?>">
                            <td></td>
                            <td colspan="7">
                                <?php if ($record['Invoice'] ): ?>
                                    <h4>Invoices</h4>
                                    <div>
                                         <table class="table table-bordered table-striped table-condensed  table-custom">
                                            <thead>
                                               <tr class="head tr-border-blue-madison bold" >                                                        
                                                   <td width="8%" class="td-center">Id</td>                                                   
                                                   <td width="12%" class="td-center">Start Date</td>
                                                   <td width="12%" class="td-center">End Date</td>
                                                   <td width="20%" class="td-center">Transaction Id</td>
                                                   <td width="12%" class="td-center">Amount (<?php echo CURRENCY_SYMBOL; ?>)</td>
                                                   <td width="12%" class="td-center">Discount (<?php echo CURRENCY_SYMBOL; ?>)</td>
                                                   <td width="12%" class="td-center">Paid Amount (<?php echo CURRENCY_SYMBOL; ?>)</td>
                                                   <td width="12%" class="td-center">Status</td>
                                                   <td width="12%" class="td-center">Action</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($record['Invoice'] as $arr): ?>
                                                <tr>
                                                    <td class="td-center"><?php echo $arr['id']; ?></td>                                                    
                                                    <td class="td-center"><?php echo $arr['start_date']; ?></td>
                                                    <td class="td-center"><?php echo $arr['end_date']; ?></td>                                                    
                                                    <td class="td-center"><?php echo $arr['transaction_id']; ?></td>
                                                    <td class="td-center"><?php echo $arr['amount']; ?></td>
                                                    <td class="td-center"><?php echo $arr['discount']; ?></td>
                                                    <td class="td-center"><?php echo $arr['net_amount']; ?></td>
                                                    <td class="td-center td-label"><?php echo $this->TSHtml->transactionStatusLabel($arr['status']); ?></td>
                                                    <td class="td-center">
                                                        <div class="summary-action">                                
                                                            <?php 
                                                                if ($auth_user['group_id'] == ADVERTISER_GROUP_ID)
                                                                {
                                                                    if ($arr['status'] == StaticArray::$transaction_status_unpaid)
                                                                    {
                                                                        echo $this->Html->link("Pay", 
                                                                            array("controller" => "Invoices", 'action' => 'edit', $arr['id'], "admin" => true), 
                                                                            array('escape' => false, 'title' => "Pay Now")
                                                                        );
                                                                    }
                                                                    else
                                                                    {
                                                                        echo $this->Html->link(
                                                                            $this->Html->image("admin/summary_view.png", array("class" => "summary-action-icon")), 
                                                                            array("controller" => "Invoices", 'action' => 'edit', $arr['id'], "admin" => true), 
                                                                            array('escape' => false, 'title' => "Pay Now")
                                                                        );
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo $this->Html->link(
                                                                        $this->Html->image("admin/summary_edit.png", array("class" => "summary-action-icon")), 
                                                                        array("controller" => "Invoices", 'action' => 'edit', $arr['id'], "admin" => true), 
                                                                        array('escape' => false, 'title' => "Pay Now")
                                                                    );
                                                                }
                                                                    
                                                            ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                         </table>
                                    </div>
                                <?php endif; ?>

                                <?php if ($record['AdLocation']): ?>
                                    <h4>Location</h4>
                                    <div>
                                         <table class="table table-bordered table-striped table-condensed  table-custom">
                                            <thead>
                                               <tr class="head tr-border-blue-madison bold" >                                                        
                                                    <td width="25%">Location</td>                                                            
                                                    <td width="15%" class="td-center">Target Impressions</td>
                                                    <td width="15%" class="td-center">Reached Impressions</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($record['AdLocation'] as $arr): ?>
                                                <tr>
                                                    <td><?php echo $arr['Location']['name']; ?></td>                                                            
                                                    <td class="td-center"><?php echo $arr['target_imperssions']; ?></td>
                                                    <td class="td-center"><?php echo $arr['reached_imperssions']; ?></td>                                                            
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                         </table>
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php } ?>
            </tbody>                  
        </table>   
        <div>    
            <?php echo $this->element("admin/pagination"); ?>
          </div> 
    </div>
</div>