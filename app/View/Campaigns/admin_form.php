<?php
/**
 * Add & edit Campaign
 * 
 * 
 * @created    25/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("admin/form_header");

echo $this->Html->script(array(
    "jquery.ajaxform.min"
));

?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>
                    
    <div class="portlet-body form">
        <div class="form-wizard">
            <div class="form-body">
                
                <ul class="nav nav-pills nav-justified steps">
                    <?php 
                        $i = 0;  $checked = true; $done_tabs = 0;
                            foreach ($tabs as $key => $tab)
                            {
                                $i++;

                            $link = "#" . $key;
                            $class = "";
                            
                            if ($checked)
                            {
                                $done_tabs++;
                                if (isset($id) && $id)
                                {
                                    $link = SITE_URL . "admin/$controller/edit/$id/$key";
                                }
                            }
                            
                            if ($key == $current_tab)
                            {
                                $class = "active";
                            }
                            
                            if ($key == $last_tab)
                            {
                                $checked = false;                                
                            }
                            
                            
                            
                            
                    ?>
                        <li class="<?php echo $class; ?>">
                            <a href="<?php echo $link; ?>" class="step">
                                <span class="number"><?php echo $i; ?> </span>
                                <div>
                                    <span class="desc">                    
                                        <?php 
                                            if ($checked)
                                            {
                                                echo "<i class='fa fa-check'></i> ";
                                            }
                                            echo $tab; 
                                        ?> 
                                    </span>
                                </div>
                            </a>
                        </li>
                    <?php } ?>    
                </ul>
                
                
                <div id="bar" class="progress progress-striped" role="progressbar">
                    <?php
                        $pb_width = $done_tabs * (100/ count($tabs));
                    ?>
                    <div class="progress-bar progress-bar-success" style="width:<?php echo $pb_width; ?>%;">
                    </div>
                </div>

                <div class="tab-content"> 
                    <div class="tab-pane active">
                        <?php echo $this->element("admin/campaign/". $current_tab); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>

