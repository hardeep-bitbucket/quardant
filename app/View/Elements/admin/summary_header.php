<?php

/**
 * summary header
 * 
 * 
 * @created    17/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */
?>
<div class="pull-left">
    <h3 class="page-title">
      
      <?php 
        if (isset($title))
        {         
            echo $title;
            if (isset($subtitle))
            {
                echo " <small>$subtitle</small>";
            }
        }
        else if (isset($title_for_layout)){
            echo $title_for_layout;
        }
        else
        {
            echo ucwords($model) . " " . "<small>Manager</small>";
        }
       ?>
    </h3>
</div>
<div class="pull-right">
    <div style="margin-top:10px;">
    <?php
        echo $this->Html->link('<i class="fa fa-plus"></i>', array("action" => "admin_add"), array("escape" => false));
    ?>
    </div>
</div>

<div class="clearfix"></div>

<?php
    //echo $this->element("admin/breadcrumb"); 
    echo $this->Session->flash();
?>