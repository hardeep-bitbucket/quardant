<?php 
/**
 * sidebar
 * 
 * 
 * @created    17/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<div class="page-sidebar-wrapper">    
    <div class="page-sidebar navbar-collapse collapse">    
        <div style="width: 100%; background-color: #465460;">
                    <div class="menu-toggler sidebar-toggler" style="height:28; width: 30px; margin: auto;">                           
                </div>
        </div>
        <ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            
            <?php             
                foreach (AdminMenu::$adminMenuConfig as $menu):                  
                   
                    $class = ""; 
                        foreach ($menu["links"] as $submenu)
                        {
                            if ($submenu["link"]["controller"] == $controller && $submenu["link"]["action"] == $action)
                            {
                                $class = "sidebar-active"; 
                            }
                            else if (isset($submenu["actions"]) && in_array($action, $submenu["actions"]))
                            {
                                $class = "sidebar-active"; 
                            }
                                
                        }
                if (in_array($auth_user["group_id"], $menu["permissions"])):
                    $menu_title = " <i class='" . $menu["icon-class"] . "'></i><span class='title'> " . $menu['title'] . "</span><span class='arrow'></span>";                    
             ?>
                  <li class="<?php echo $class; ?>">
                        <a href='#'><?php echo $menu_title; ?></a> 
                    
                        <ul class='sub-menu'>
                            <?php
                            foreach ($menu["links"] as $submenu)
                            {
                                $subtitle = "<i class='" . $submenu["icon-class"] . "'></i> ". $submenu["text"];
                                $class = "";
//                                if ($submenu['ajax'])
//                                {
//                                    $class = "ajax-page-link";
//                                }
                                
                                if (in_array($auth_user["group_id"], $submenu["permissions"]))
                                {
                                    echo "<li>";
                                    echo $this->Html->link($subtitle, $submenu["link"], array("escape" => false, 'class' => $class));
                                    echo "</li>";
                                }
                            }
                            ?>
                        </ul>
                   </li>
             
             <?php endif; endforeach;   ?>
        </ul>       
    </div>
</div>

<script>
    $(document).ready(function()
    {
        /**
         * for ajax only
         */
        $(".sub-menu li, .sub-menu a").click(function ()
        {
            $(".page-sidebar-menu li").removeClass('sidebar-active');
            $(this).parent().parent().addClass('sidebar-active');
        });
    });
</script>
