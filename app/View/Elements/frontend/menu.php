<button type="button" class="navbar-toggle"><i class="fa fa-close fa-lg"></i></button>
<ul id="one-page-menu" role="menu" class="nav navbar-nav navbar-left">
    <?php foreach($pages as $page) { ?>
    <li>
        <?php 
            if($page["Page"]["slug"] == LOCATION_SLUG)
            {
                $link = "/" . $page["Page"]["slug"];
            } 
            else
            {
                $link = "#" . $page["Page"]["slug"];
            }
            
            if($page["Page"]["name"] == "ANPRMotion")
            { ?>
                <style>
                    .navbar-nav > li > a{
                        text-transform: none;
                    }
                </style>
            <?php } 
        ?>
        <a href="<?php echo $link; ?>" title="<?php echo $page["Page"]["name"]; ?>" data-ref = "<?php echo $page["Page"]["slug"]; ?>">
            <?php echo $page["Page"]["name"]; ?>
        </a>
    </li>
    <?php } ?>
    <!--<li><a href="#about" title="About us" data-ref="about">About us</a>
    </li>
    <li><a href="#portfolio" title="Portfolio" data-ref="portfolio">Portfolio</a>
    </li>
    <li><a href="#shop" title="Shop" data-ref="shop">Shop</a>
    </li>
    <li><a href="#blog" title="Blog" data-ref="blog">Blog</a>
    </li>
    <li><a href="#pricing" title="Pricing" data-ref="pricing">Pricing</a>
    </li>
    <li><a href="#team" title="Team" data-ref="team">Team</a>
    </li>
    <li><a href="#services" title="Services" data-ref="services">Services</a>
    </li>
    <li><a href="#contacts" title="Contacts" data-ref="contacts">Contacts</a>
    </li>-->
</ul>