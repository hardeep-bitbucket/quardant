<?php
/**
 * Invoice PDF 
 * 
 * 
 * @created    18/05/2015
 * @package    ANPRMotion
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
echo $this->element('/report/pdf/header');
?>

<div id="content">
    <div class="header-title">
        Campaign
    </div>
    
    <div>
        
        <table class="table-row" style="width : 100%;">
            <tr>
                <td>
                    <span class="label blue">Campaign No. : </span>                
                    <span class="label-value"><?php echo $record["Campaign"]["campaign_no"]; ?></span>
                </td>
                 <td>
                    <span class="label blue">Advertiser : </span>                
                    <span class="label-value"><?php echo $record['User']['name'] . " " . $record['User']['subname']; ?></span>
                </td>                  
            </tr>
            
            <tr>
                <td>
                    <span class="label blue">Campaign Name : </span>                
                    <span class="label-value"><?php echo $record["Campaign"]["name"]; ?></span>
                </td>  
                <td>
                    <span class="label blue">Duration : </span>                
                    <span class=""><?php echo $record['CampaignDuration']['value']; ?></span>
                </td>                
            </tr>
            
            <tr> 
                <td>
                    <span class="label blue">Type : </span>                
                    <span class=""><?php echo $record['CampaignType']['value']; ?></span>
                </td>                
                <td>
                    <span class="label blue">Start Date : </span>                
                    <span class=""><?php echo $record['Campaign']['start_date']; ?></span>
                </td>                
            </tr>
            
            <tr> 
                <td>
                    <span class="label blue">Submit Date : </span>                
                    <span class=""><?php echo $record['Campaign']['submit_datetime'] ? $record['Campaign']['submit_datetime'] : "Not Yet"; ?></span>
                </td>      
                
                <td>
                    <span class="label blue">End Date : </span>
                    <?php if ($record['CampaignDuration']['id'] == CAMPAIGN_DURATION_FIXED_ID): ?>
                        <span class=""><?php echo $record['Campaign']['end_date']; ?></span>                    
                    <?php endif; ?>                
                </td>
            </tr>
            
            <tr> 
                <td>
                    <span class="label blue">Approve Date : </span>                
                    <span class=""><?php echo $record['Campaign']['approve_datetime'] ? $record['Campaign']['approve_datetime'] : "Not Yet"; ?></span>
                </td>                              
                <?php if ($auth_user['group_id'] != ADVERTISER_GROUP_ID) : ?>
                    <td>
                        <span class="label blue">Setup Cost : </span>                
                        <span class=""><?php echo CURRENCY_SYMBOL_CODE; ?><?php echo $record[$model]['setup_cost']; ?></span>
                    </td>    
                 <?php endif; ?>
            </tr>
            
            <tr> 
                <td>
                    <span class="label blue">Activation Date : </span>                
                    <span class=""><?php echo $record['Campaign']['active_datetime'] ? $record['Campaign']['active_datetime'] : "Not Yet"; ?></span>
                </td> 
                <td>
                    <span class="label blue">Budget : </span>                
                    <span class=""><?php echo CURRENCY_SYMBOL_CODE; ?><?php echo $record[$model]['budget']; ?></span>
                </td>   
                
            </tr>
            
            <tr>
                <td>
                    <span class="label blue">Efficiency : </span>                
                    <span class=""><?php echo $efficiency ?>%</span>
                </td>
                <td>
                    <span class="label blue">Spend : </span>                
                    <span class=""><?php echo CURRENCY_SYMBOL_CODE; ?><?php echo $record[$model]['spend']; ?></span>
                </td>                 
            </tr>
            
            <tr>
                <td>
                    <span class="label blue">Purpose : </span>                
                    <span class=""><?php echo $record['Campaign']['purpose']; ?></span>
                </td>
                <td>
                    <?php if ($invoice['pending_amount'] > 0) :?>
                            <span class="label blue">Payment Due : </span>                
                            <span class=""><?php echo CURRENCY_SYMBOL_CODE . $invoice['pending_amount']; ?></span>                    
                    <?php endif; ?>
                </td>
            </tr>
        </table>
        
        <hr>
        
        <div class="sub-title">
            Locations
        </div>
        <table class="summary-table" style="width : 100%;" border="0" cellspacing="0" cellpadding="0">
            <thead>
               <tr class="head" >
                    <th width="5%" class="td-center">Sr.</th>                            
                    <th width="25%">Location</th>
                    <th width="15%" class="td-center">Target Impressions</th>
                    <th width="15%" class="td-center">Reached Impressions</th>                            
                    <th width="20%" class="td-center">Spend (<?php echo CURRENCY_SYMBOL_CODE; ?>)</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 0; foreach ($record["AdLocation"] as $ad_location) : $i++; ?>
                    <tr>
                        <td class="td-center"><?php echo $i; ?></td>
                        <td><?php echo $ad_location['Location']['name']; ?></td>
                        <td class="td-center"><?php echo $ad_location['target_imperssions']; ?></td>
                        <td class="td-center"><?php echo $ad_location['reached_imperssions']; ?></td>
                        <td class="td-center"><?php echo $ad_location['reached_imperssions'] * $ad_location['Location']['price_per_impression']; ?></td>                                
                    </tr>
                <?php  endforeach; ?>
            </tbody>                  
        </table> 
        
        <?php if ($record["Invoice"]) : ?>
            <div style="height : 10px;"></div>
            <hr>        
            <div class="sub-title">
                Invoices
            </div>
            <table class="summary-table" style="width : 100%;" border="0" cellspacing="0" cellpadding="0">
                <thead>
                   <tr class="head" >                           
                        <th width="5%"  class="td-center">ID</th>
                        <th width="10%" class="td-center">Start Date</th>
                        <th width="10%" class="td-center">End Date</th>                            
                        <th width="10%" class="td-center">Amount(<?php echo CURRENCY_SYMBOL; ?>)</th>
                        <th width="12%" class="td-center">Invoice Date</th>                            
                        <th width="8%" class="td-center">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0; foreach ($record["Invoice"] as $arr) { $i++; ?>
                        <tr>                                
                            <td class="td-center"><?php echo $i; ?></td>
                            <td class="td-center"><?php echo $arr['start_date']; ?></td>  
                            <td class="td-center"><?php echo $arr['end_date']; ?></td>                                  
                            <td class="td-center"><?php echo $arr['amount']; ?></td>                                  
                            <td class="td-center"><?php echo DateUtility::getFormatDateFromString($arr['created_on'], DEFAULT_DATETIME_FORMAT); ?></td>
                            <td class="td-center td-label"> 
                                <?php echo StaticArray::$invoice_status_type[$arr['status']]; ?>
                        </td>
                        </tr>
                <?php } ?>
                </tbody>                  
            </table> 
       <?php endif; ?>
    </div>
</div>

<?php 
echo $this->element('/report/pdf/footer');