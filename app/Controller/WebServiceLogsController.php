<?php
/**
 * Web Service Logs Controller
 * 
 * 
 * @created    06/02/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
class WebServiceLogsController extends AppController 
{
    var $layout = 'admin_inner';
    
    /*
     * @Summary Screen
     */    
    public function admin_index()
    {
        $title_for_layout = 'Web Service Log Summary';        
        
        $this->Redirect->urlToNamed();
        
        if (isset($this->params['']['status']) && $this->params[$this->modelClass]['status'] < 0)
        {
            $this->params[$this->modelClass]['status'] = 0;
        }
        
        $conditions = $this->getSearchConditions(array(
                array('model' => 'Location', 'field' => 'name', 'type' => 'string', 'view_field' => 'name'),
                array('model' => $this->modelClass, 'field' => 'status', 'type' => 'integer', 'view_field' => 'status'),
                array('model' => $this->modelClass, 'field' => 'service_id', 'type' => 'integer', 'view_field' => 'service_id'),
                array('model' => $this->modelClass, 'field' => 'created_on', 'type' => 'from_date', 'view_field' => 'from_date'),
                array('model' => $this->modelClass, 'field' => 'created_on', 'type' => 'to_date', 'view_field' => 'to_date'),
            )
        );
        
        if (isset($conditions['WebServiceLog.status']) && $conditions['WebServiceLog.status'] < 0)
        {
            $conditions['WebServiceLog.status'] = 0;
        }
        
        $this->{$this->modelClass}->contain(array(
            "Location" => array(
                "fields" => array("id", "name")
            )
        ));
        
        $records = $this->paginate($this->modelClass, $conditions);
        //debug($records); exit;
        
        $this->set(compact('records', 'title_for_layout'));
    }
}