
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$limits = array(
    "3" => "Last 3",
    "5" => "Last 5",
    "10" => "Last 10",
    "0" => "All",
)
?>
<style>
    .bold{
        font-weight: bold;
    }
</style>

<div class="row">    
    <div class="col-md-6">
        <h3 class="page-title">Dashboard</h3>
    </div>
    <div class="col-md-6">
        <div class="pull-right">
            
            <div style="width : 250px;">
                <?php
//                    echo $this->Form->input("country", array(
//                        "type" => "select", "div" => false, "label" => false, "escape" => false,
//                        "options" => $country_list, "empty" => "All",
//                        "class" => "form-control"
//                    ));
                ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Session->flash(); ?>

<div class="portlet box blue-soft">
    <div class="portlet-title">
        <div class="caption">Active Campaigns</div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">  
        <div class="row">
            <div class="col-md-6">            
            </div>
            <div class="col-md-6">
                <div class="pull-right">
                    <div style="width : 250px;">
                        <?php
                            echo $this->Form->input("active_campaign_limit", array(
                                "type" => "select", "div" => false, "label" => false, "escape" => false,
                                "options" => $limits,
                                "class" => "form-control"
                            ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-top-10">
            <div class="col-md-12" id="active-campaign"></div>
        </div>
    </div>
</div>


<div class="portlet box green-soft">
    <div class="portlet-title">
        <div class="caption">Recently Completed Campaigns</div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">  
        <div class="row">
            <div class="col-md-6">            
            </div>
            <div class="col-md-6">
                <div class="pull-right">
                    <div style="width : 250px;">
                        <?php
                            echo $this->Form->input("completed_campaign_limit", array(
                                "type" => "select", "div" => false, "label" => false, "escape" => false,
                                "options" => $limits,
                                "class" => "form-control"
                            ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-top-10">
            <div class="col-md-12" id="completed-campaign"></div>
        </div>
    </div>
</div>


<div class="portlet box red-soft">
    <div class="portlet-title">
        <div class="caption">Trial Campaigns</div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">  
        <div class="row">
            <div class="col-md-6">            
            </div>
            <div class="col-md-6">
                <div class="pull-right">
                    <div style="width : 250px;">
                        <?php
                            echo $this->Form->input("trial_campaign_limit", array(
                                "type" => "select", "div" => false, "label" => false, "escape" => false,
                                "options" => $limits,
                                "class" => "form-control"
                            ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-top-10">
            <div class="col-md-12" id="trial-campaign"></div>
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function()
    {
        $("#active_campaign_limit").change(function()
        {
            var v = $(this).val();            
            $("#active-campaign").load("<?php echo SITE_URL.$controller ?>/ajaxDashBoardActiveCampaigns/" + v, function(data, status)
            {
                
            });
        });
        
        $("#completed_campaign_limit").change(function()
        {
            var v = $(this).val();            
            $("#completed-campaign").load("<?php echo SITE_URL.$controller ?>/ajaxDashBoardCompletedCampaigns/" + v, function(data, status)
            {
                
            });
        });
        
        $("#trial_campaign_limit").change(function()
        {
            var v = $(this).val();            
            $("#trial-campaign").load("<?php echo SITE_URL.$controller ?>/ajaxDashBoardTrialCampaigns/" + v, function(data, status)
            {
                
            });
        });
        
        $("#completed_campaign_limit, #active_campaign_limit, #trial_campaign_limit").trigger("change");
    });
</script>