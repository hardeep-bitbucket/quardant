<div id="slider" style="height : 87%; width : 100%;  margin-top : 104px;" class="sl-slider-wrapper">
    <div class="sl-slider">
        <!-- start slide-->
        <?php foreach($graphics as $graphic) { ?>
        <div data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2" class="sl-slide">
            <div style="background: url(<?php echo RESIZE_GRAPHIC_IMAGES . $graphic["Graphic"]["image"]; ?>); background-size : 100% auto !important; background-repeat : no-repeat;" class="sl-slide-inner">
                <!--<div class="slide-container">
                    <div class="slide-content text-center">
                            <?php //if($graphic["Graphic"]["logo"]) { ?>
                                <img src="/<?php //echo RESIZE_GRAPHIC_LOGO_IMAGES . $graphic["Graphic"]["logo"]; ?>" alt="Ottavio" class="deco">
                            <?php //} ?>
                        <h2 class="main-title"><?php //echo $graphic["Graphic"]["title"]; ?></h2>
                        <blockquote>
                            <p><?php //echo $graphic["Graphic"]["description"]; ?></p>
                            <a class="btn btn-light btn-bordered btn-lg"><?php //echo $graphic["Graphic"]["link_text"]; ?></a>
                        </blockquote>
                    </div>
                </div>-->
            </div>
        </div>
        <?php } ?>
        <!-- end slide-->
    </div>
    <!--       /sl-slider-->    
    <nav id="nav-arrows" class="nav-arrows"><span class="nav-arrow-prev">Previous</span><span class="nav-arrow-next">Next</span></nav>
    <nav id="nav-dots" class="nav-dots">
        <span class="nav-dot-current"></span>
        <?php for($i = 1; $i < count($graphics); $i++) : ?>
        <span></span>
        <?php endfor; ?>
    </nav>
</div>