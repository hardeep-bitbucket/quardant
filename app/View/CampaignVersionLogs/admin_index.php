<?php
/**
 * web service logs Controller
 * 
 * 
 * @created    22/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */

$action_for_search = str_replace("admin_", '', $action);
?>


<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h3 class="page-title">
              <?php echo $title_for_layout; ?>
            </h3>
        </div>
    </div>
</div>
<?php
    //echo $this->element("admin/breadcrumb"); 
    echo $this->Session->flash();
?>

<div class="row">
    <div class="col-md-12">   
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">Search</div>  
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>

            </div>
            <div class="portlet-body">    
                
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->                    
                    <?php echo $this->Form->create($model, array('type' => 'GET' , 'class' => 'ajax-form form-horizontal', 'data-action' => $action_for_search)); ?>
                     
                        <div class="form-body">                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">From Date</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('CampaignVersionLogSearch.from_date',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $CampaignVersionLogfrom_date, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">To Date</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('CampaignVersionLogSearch.to_date',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $CampaignVersionLogto_date, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Advertiser Name</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('UserSearch.name',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $Username, 
                                                    'class' => 'form-control'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Campaign</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('CampaignSearch.id',array(
                                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $Campaignid, 
                                                    "options" => $country_list, "empty" => "Please Select",
                                                    'class' => 'form-control'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>    
                            </div>                            
                            
                            <!--/row-->
                        </div>
                        <?php echo $this->element("admin/form_search_btn"); ?>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
        
        
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Summary</div>                        
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed table-custom">
                    <thead>
                       <tr class="head" >                           
                            <td width="5%"  class="td-center"><?php echo $this->Paginator->sort('id', 'ID', array('class' => "ajax-page-link")); ?></td>
                            <td width="15%"><?php echo $this->Paginator->sort('Campaign.name', 'Campaign', array('class' => "ajax-page-link")); ?></td>
                            <td width="15%"><?php echo $this->Paginator->sort('User.name', 'Advertiser', array('class' => "ajax-page-link")); ?></td>                            
                            <td width="15%">Activated Version </td>
                            <td width="10%" class="td-center"><?php echo $this->Paginator->sort('Campaign.created_on', 'Datetime', array('class' => "ajax-page-link")); ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $record) { ?>
                            <tr>                                
                                <td class="td-center"><?php echo $record[$model]['id']; ?></td>
                                <td><?php echo $record['Campaign']['name']; ?> </td>                                                 
                                <td><?php echo $record['User']['name']; ?> </td>
                                <td><?php echo $record['HistoryCampaign']['name'] . " #" . $record['HistoryCampaign']['id']; ?> </td>
                                <td class="td-center"><?php echo $record[$model]['created_on']; ?></td>
                            </tr>
                    <?php } ?>
                    </tbody>                  
                </table>                    
                    <div>    
                      <?php echo $this->element("admin/pagination"); ?>
                    </div> 
            </div>
        </div>               

    </div>
</div>
