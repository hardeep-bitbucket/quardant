<?php
/**
 * Frontend Home 
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */
$this->set('title_for_layout', $content["Page"]["meta_title"]);

$top_header_image = "faq.jpg";

    switch($slug)
    {
        case "faq":
            $top_header_image = "faq.jpg";
        break;    
        case "partners":
            $top_header_image = "partners.jpg";
        break;
        case "careers":
            $top_header_image = "career.jpg";
        break;
        case "privacy-policy":
            $top_header_image = "privacy-policy.jpg";
        break; 
        case "register-advertiser":
            $top_header_image = "get-started.jpg";
        break;
        case "register-partner":
            $top_header_image = "partners.jpg";
        break;
    }
?>
<!-- Start header Section-->
<section style="margin-top : 104px;" class="other-page-banner-background">
    <img src="/img/frontend/pages_header/<?php echo $top_header_image; ?>" style="width:100%; height: auto;">
</section>
<!-- Start Skills section-->

<section class="sep-top-1x sep-bottom-lg">
    <div class="container">
        <div class="row">
            <?php 
                $contents = $content["Page"]["contents"]; 
                $contents = str_replace("{{PARTNERS_FORM}}", $this->element("frontend/inner/partners-form"), $contents);
                $contents = str_replace("{{REGISTER_FORM}}", $this->element("frontend/inner/register-form"), $contents);
                echo $contents; 
            ?>
            
        </div>
    </div>
</section>

