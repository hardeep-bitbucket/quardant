<?php

/**
 * @package    TFQ
 * 
 * 
 * @created    23/12/2014
 * @package    iBOND
 * @copyright  Copyright (coffee) 2014
 * @license    Proprietary
 * @author     Sonia
 */
class ImportLog extends AppModel
{
    public $actsAs = array(
        'DateTimeFormat' => array('created_on' => array('in_format' => ''))
    );

    /**
     * Associations
     */
    var $belongsTo = array(
        'Type' => array(
            'className' => 'Type',
            'foreignKey' => 'type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
