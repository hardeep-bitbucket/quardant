<?php

/**
 * @package    Type
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */
class Type extends AppModel {

    var $order = array("Type.value ASC");
    public $displayField = 'value';

    /**
     * associations has many
     * @var type array
     */
    var $hasMany = array(
        'Graphic' => array(
            'className' => 'Graphic',
            'foreignKey' => 'type_id',
            'dependent' => false,
            'conditions' => '',
        ),
        'Enquiry' => array(
            'className' => 'Enquiry',
            'foreignKey' => 'type_id',
            'dependent' => false,
            'conditions' => '',
        ),
        'ImportLog' => array(
            'className' => 'ImportLog',
            'foreignKey' => 'type_id',
            'dependent' => false,
            'conditions' => '',
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'type_id',
            'dependent' => false,
            'conditions' => '',
        )
    );
    
    public function getList($conditions = array())
    {
        return $this->find("list", array(
            "fields" => array("id", "value"),
            "conditions" => $conditions,
            "recursive" => -1
        ));
    }

}
