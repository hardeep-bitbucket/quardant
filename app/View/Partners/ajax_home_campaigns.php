<?php
/**
 * home campaign details
 * 
 * 
 * @created    03/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<style>
    .my-portfolio-info{
        text-transform: initial !important;
        font-size:18px !important;
    }
</style>


<div class="row"> 
    
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <span class="dashboard-stat dashboard-stat-light blue-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-money"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $campaigns['total']; ?></div>
                <div class="desc"><b>Total Campaigns</b></div>                
            </div>
        </span>
    </div>
    
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <span class="dashboard-stat dashboard-stat-light red-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-money"></i>                
            </div>
            <div class="details">                
                <div class="number">$<?php echo $campaigns['partner_spend']; ?></div>
                <div class="desc"><b>Revenue</b></div>
            </div>
        </span>
    </div>
    
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <span class="dashboard-stat dashboard-stat-light purple-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-money"></i>                
            </div>
            <div class="details">                
                <div class="number">$<?php echo $campaigns['partner_future_spend']; ?></div>
                <div class="desc"><b>Forecast Revenue</b></div>
            </div>
        </span>
    </div>    
</div>

<div class="row  margin-top-10">
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <span class="dashboard-stat dashboard-stat-light green-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-tasks"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $campaigns['active']; ?></div>
                <div class="desc"><b>Active Campaigns</b></div>
            </div>
        </span>
    </div>
    
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <span class="dashboard-stat dashboard-stat-light purple-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-tasks"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $campaigns['submitted']; ?></div>
                <div class="desc"><b>Submitted Campaigns</b></div>
            </div>
        </span>
    </div>
    
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <span class="dashboard-stat dashboard-stat-light red-soft round-10  margin-bottom-10">
            <div class="visual" >
                <i class="fa fa-tasks"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $campaigns['completed']; ?></div>
                <div class="desc"><b>Completed Campaigns</b></div>                                
            </div>
        </span>
    </div>
</div>


<div class="row margin-top-10">     
    <div class="col-lg-6 col-sm-6 col-xs-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">Campaign List</span>
                </div>                
            </div>
            <div class="portlet-body">
                <div class="margin-5" style="height : 250px; overflow-y: auto;">
                    <?php if ($campaign_list): 
                            $i = 0;
                            foreach ($campaign_list as $id => $name): 
                                $i++;
                    ?>            
                        <div style="background-color: #FAFAFA; font-size: 15px; padding: 5px" class="margin-5">
                            <?php 
                                echo $i . ". " . $name;
                            ?>
                        </div>        
                    <?php endforeach; else: ?>
                        <span class="required">No Data</span>
                    <?php endif; ?>
                </div>
            </div>
         </div>
    </div>
</div>