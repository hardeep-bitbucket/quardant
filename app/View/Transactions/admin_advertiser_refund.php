<?php 
/**
 * Form refund controller transaction
 * 
 * 
 * @created    19/02/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("admin/form_header");

?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal'));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            ?>
            <div class="form-body">
                <div class="row margin-top-15">
                    <div class="col-md-12">
                        <span class="required">*Note: If your wallet amount less than the <?php echo CURRENCY_SYMBOL; ?><?php echo $adminSetting['advertiser_threshold_wallet_amount']; ?> , then your all campaign will stop</span>
                    </div>
                </div>
                    <div class="row margin-top-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">PayPal ID <span class="required">*</span></label>
                                <div class="col-md-8">                                            
                                    <?php                                
                                        echo $this->Form->input('paypal_id', array(
                                            'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                        
                                            'class'=>'form-control', "required"
                                        ));

                                    ?>                                            
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Refund Amount (<?php echo CURRENCY_SYMBOL; ?>)<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <?php
                                    echo $this->Form->input('refund_amount', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control  numeric postive',  "required"
                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-11">
                    <div class="pull-right">
                        <?php 
                            echo $this->Form->button("<i class='fa fa-check'></i> Refund", array("class" => "btn green", "type" => "submit", 'label' => false, 'div' => false, 'escape' => false)); 
                        ?>
                    </div>
                </div>                                
            </div>  

            <!-- END FORM-->
        </div>
    </div>
</div>