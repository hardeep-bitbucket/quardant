<?php
/**
 * ajax get notification view
 * 
 * 
 * @created    05/04/2015
 * @package    anpr
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<?php if ($records) foreach ($records as $record):  ?>
    <li id="<?php echo $record[$model]['id'] ?>">
        <div class="col1">
            <div class="cont">
                <div class="cont-col1">
                    <div class="label label-sm label-warning">
                        <i class="fa fa-warning"></i>
                    </div>                    
                </div>
                <div class="cont-col2">
                    <div class="desc">
                         <?php echo $record[$model]['content']; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col2">
            <div class="date">
                 <?php echo $record[$model]['created_on']; ?>
            </div>
        </div>
    </li>
    
<?php   endforeach;   ?>
