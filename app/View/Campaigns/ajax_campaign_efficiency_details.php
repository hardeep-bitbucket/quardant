<?php
/**
 * ajax Campaign Efficiency Report
 * 
 * 
 * @created    06/05/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<div class="row"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="fa fa-tasks theme-font"></i>
                    <span class="caption-subject font-blue-madison bold">Campaign Basic Detail</span>
                </div>   
                <div class="tools">                    
                    <?php 
                        echo $this->Html->link(
                            $this->Html->image("admin/pdf.png", array("class" => "tool-link")), 
                            array("controller" => "Campaigns", 'action' => 'admin_export_pdf', "admin" => true, $record["Campaign"]["id"]), 
                            array('escape' => false)
                        );
                     ?>
                    <?php 
                        echo $this->Html->link(
                            $this->Html->image("admin/excel.png", array("class" => "tool-link")), 
                            array("controller" => "Campaigns", 'action' => 'admin_export_excel', "admin" => true, $record["Campaign"]["id"]), 
                            array('escape' => false)
                        );
                     ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row"> 
                    <div class="col-md-5">
                        <div class="row static-info">
                            <div class="col-md-5 name">Name</div>
                            <div class="col-md-7 value"><?php echo $record['Campaign']['name']; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Purpose</div>
                            <div class="col-md-7 value"><?php echo $record['Campaign']['purpose']; ?></div>
                        </div>
                        <?php if ($auth_user['group_id'] != ADVERTISER_GROUP_ID): ?>
                            <div class="row static-info">                        
                                <div class="col-md-5 name">Advertiser</div>
                                <div class="col-md-7 value"><?php echo $record['User']['name'] . " " . $record['User']['subname']; ?></div>
                            </div>                    
                        <?php endif; ?>
                        <div class="row static-info">
                            <div class="col-md-5 name">Type</div>
                            <div class="col-md-7 value"><?php echo $record['CampaignType']['value']; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Duration</div>
                            <div class="col-md-7 value"><?php echo $record['CampaignDuration']['value']; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Date Submitted</div>
                            <div class="col-md-7 value"><?php echo $record[$model]['submit_datetime'] ? $record[$model]['submit_datetime'] : "Not Yet"; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Date Approved</div>
                            <div class="col-md-7 value"><?php echo $record[$model]['approve_datetime'] ? $record[$model]['approve_datetime'] : "Not Yet"; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Date Activated</div>
                            <div class="col-md-7 value"><?php echo $record[$model]['active_datetime'] ? $record[$model]['active_datetime'] : "Not Yet"; ?></div>
                        </div>
                        <?php if ($auth_user['group_id'] != ADVERTISER_GROUP_ID) : ?>
                        <div class="row static-info">
                            <div class="col-md-5 name">Setup Cost</div>
                            <div class="col-md-7 value"><?php echo CURRENCY_SYMBOL; ?><?php echo $record[$model]['setup_cost']; ?></div>
                        </div>
                        <?php endif; ?>
                        <div class="row static-info">
                            <div class="col-md-5 name">Budget</div>
                            <div class="col-md-7 value"><?php echo CURRENCY_SYMBOL; ?><?php echo $record[$model]['budget']; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Spend</div>
                            <div class="col-md-7 value"><?php echo CURRENCY_SYMBOL; ?><?php echo $record[$model]['spend']; ?></div>
                        </div>
                    </div>

                    <div class="col-md-2">
                    </div>
                    <div class="col-md-5">  
                        <div class="row static-info">
                            <div class="col-md-5 name">Campaign No.</div>
                            <div class="col-md-7 value"><?php echo $record['Campaign']['campaign_no']; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Start Date</div>
                            <div class="col-md-7 value"><?php echo $record['Campaign']['start_date']; ?></div>
                        </div>
                        <?php if ($record['CampaignDuration']['id'] == CAMPAIGN_DURATION_FIXED_ID): ?>
                        <div class="row static-info">
                            <div class="col-md-5 name">End Date</div>
                            <div class="col-md-7 value"><?php echo $record['Campaign']['end_date']; ?></div>
                        </div>
                        <?php endif; ?> 
                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Status</div>
                            <div class="col-md-7 value">
                                <?php echo  $this->TSHtml->campaignStatusLabel($record['CampaignStatus']['id'], $record['CampaignStatus']['value']); ?>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Payment Status</div>
                            <div class="col-md-7 value">
                               <?php 
                                    if ($invoice['pending_amount'] > 0)
                                    {
                                        echo $this->TSHtml->transactionStatusLabel(StaticArray::$invoice_status_unpaid); 
                                    }
                                    else if ($invoice['paid_amount'] > 0)
                                    {
                                        echo $this->TSHtml->transactionStatusLabel(StaticArray::$invoice_status_paid); 
                                    }
                                ?>
                            </div>
                        </div>
                        
                         <?php if ($invoice['pending_amount']): ?>
                            <div class="margin-top-20">
                                <div class="note note-info">
                                    <span class="block font-size-14">Payment Due : <?php echo CURRENCY_SYMBOL; ?><b><?php echo $invoice['pending_amount']; ?></b></span>
                                </div>
                            </div>                   
                        <?php endif; ?>
                        
                        <div class="margin-top-20">
                            <div class="note note-info">
                                <span class="block font-size-14">Efficiency : <b><?php echo $efficiency; ?>%</b></span>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>



<div class="row"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">                    
                    <span class="caption-subject font-blue-madison bold">Ad Played Detail on Basis of Automobile</span>
                </div>   
                <div class="tools">
                    <?php 
                        echo $this->Html->link(
                            $this->Html->image("admin/csv2.png"), 
                            array("controller" => "Campaigns", 'action' => 'efficiency_ad_played_automobile_export_csv', "admin" => true, $record["Campaign"]["id"]), 
                            array('escape' => false, "style" => "height : 13px;")
                        );
                     ?>
                    <a href="javascript:;" class="collapse"></a>                            
                </div>
            </div>
            <div class="portlet-body">
               
                <div style="width : 100%; max-height: 300px; overflow-y: auto; ">
                    <table class="table table-striped table-bordered table-hover table-custom" style="width : auto;">
                      <thead>
                          <tr>
                                <?php foreach ($location_automobile["header"] as $name) :?>
                                    <td width = "150"  class="td-center td-border-blue-madison bold"><?php echo $name; ?></td>
                                <?php endforeach; ?>
                            </tr>
                      </thead>
                      <tbody>                          
                            <?php foreach ($location_automobile["data"] as $arr): ?>
                                <tr>
                                    <?php foreach ($arr as $name): ?>
                                        <td class="bold"><?php echo $name; ?></td>
                                    <?php endforeach; ?>
                                </tr>
                            <?php endforeach; ?>
                      </tbody>
                  </table>
                </div>
            </div>
         </div>
    </div>
</div>



<div class="row"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">                    
                    <span class="caption-subject font-blue-madison bold">Ad Played Detail on Basis of Video</span>
                </div>   
                <div class="tools">
                    <?php 
                        echo $this->Html->link(
                            $this->Html->image("admin/csv2.png"), 
                            array("controller" => "Campaigns", 'action' => 'efficiency_ad_played_creative_export_csv', "admin" => true, $record["Campaign"]["id"]), 
                            array('escape' => false, "style" => "height : 13px;")
                        );
                     ?>
                    <a href="javascript:;" class="collapse"></a>                            
                </div>
            </div>
            <div class="portlet-body">
               
                <div style="width : 100%; max-height: 300px; overflow-y: auto; ">
                    <table class="table table-striped table-bordered table-hover table-custom" style="width : auto;">
                      <thead>
                           <tr>
                                <?php foreach ($location_creative["header"] as $name) :?>
                                    <td width = "150"  class="td-center  td-border-blue-madison bold"><?php echo $name; ?></td>
                                <?php endforeach; ?>
                            </tr>
                      </thead>
                      <tbody>                          
                            <?php foreach ($location_creative["data"] as $arr): ?>
                                <tr>
                                    <?php foreach ($arr as $name): ?>
                                        <td class="bold"><?php echo $name; ?></td>
                                    <?php endforeach; ?>
                                </tr>
                            <?php endforeach; ?>
                      </tbody>
                  </table>
                </div>
            </div>
         </div>
    </div>
</div>