<?php
/**
 * Enquiries Controller
 * 
 * 
 * @created    23/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */


class EnquiriesController extends AppController
{
    public $layout = "admin_inner";
    public $components = array('Csv');
    
    
    /*
     * Befor Filter
     */
    public function beforeFilter() 
    {		
        parent::beforeFilter();
        $this->Csv->initializer(array(
                'primaryModel'=>'Enquiry',
                'fields'=>array('Enquiry.id'),
                'order'=>array('Enquiry.id')
            )
        );
    }
    
    /**
     * @Partner Index
     */
    public function admin_partner_index()
    {
        $this->Redirect->urlToNamed();
        $conditions = $this->getSearchConditions(array(
                array('model' => 'Enquiry', 'field' => 'fullname', 'type' => 'string', 'view_field' => 'fullname'),
                array('model' => 'Enquiry', 'field' => 'phone', 'type' => 'string', 'view_field' => 'phone'),
                array('model' => 'Enquiry', 'field' => 'created_on', 'type' => 'from_date', 'view_field' => 'fromdate'),
                array('model' => 'Enquiry', 'field' => 'created_on', 'type' => 'to_date', 'view_field' => 'todate'),
            )
        );
        $conditions['type_id'] = PARTNER_ENQUIRY_TYPE_ID;
        $heading = 'Partner';
        $action = 'partner_index';
        $records = $this->paginate('Enquiry', $conditions);        
        $this->set('title_for_layout', 'Partner Enquiries');
        $this->set(compact('records', 'heading'));
    }
    
    /**
     * @Partner view Data
     */
    public function admin_partner_view($id)
    {
        $this->Redirect->urlToNamed();
        $record = $this->Enquiry->findById($id);        
        $heading = 'Partner View';
        
        $this->set('title_for_layout', 'Partner Enquiry Detail');
        $this->set(compact('record', 'heading'));
    }
    
    /**
     * @Partner Export Data
     */
    public function admin_partner_export() 
    {    
        $this->layout = "";
        $this->Redirect->urlToNamed();
        $conditions = $this->getSearchConditions(array(
                array('model' => 'Enquiry', 'field' => 'name', 'type' => 'string', 'view_field' => 'name'),
                array('model' => 'Enquiry', 'field' => 'phone', 'type' => 'string', 'view_field' => 'phone'),
                array('model' => 'Enquiry', 'field' => 'created_on', 'type' => 'from_date', 'view_field' => 'from_date'),
                array('model' => 'Enquiry', 'field' => 'created_on', 'type' => 'to_date', 'view_field' => 'to_date'),
            )
        );
        $conditions['type_id'] = WEBINAR_ENQUIRY_ID;
        
        $records = $this->Enquiry->find('all', array("conditions" => $conditions));   
        //$data = $this->Csv->getExportData();            
        foreach($records as $rec)
        {
            $data[] = array($rec["Enquiry"]["id"], $rec["Enquiry"]["name"], $rec["Enquiry"]["email"], 
                            $rec["Enquiry"]["mobile"], $rec["Enquiry"]["referance_name"]);
        }
        $headers = array("id", "name", "email", "mobile", "webinar-title");
        array_unshift($data, $headers); 
        $this->set(compact('data'));
        $this->render("admin_export");
    }
    
    /**
     * @Request Demo Index
     */
    public function admin_request_demo_index()
    {
        $this->Redirect->urlToNamed();
        $conditions = $this->getSearchConditions(array(
                array('model' => 'Enquiry', 'field' => 'fullname', 'type' => 'string', 'view_field' => 'fullname'),
                array('model' => 'Enquiry', 'field' => 'phone', 'type' => 'string', 'view_field' => 'phone'),
                array('model' => 'Enquiry', 'field' => 'created_on', 'type' => 'from_date', 'view_field' => 'fromdate'),
                array('model' => 'Enquiry', 'field' => 'created_on', 'type' => 'to_date', 'view_field' => 'todate'),
            )
        );
        $conditions['type_id'] = REQUEST_DEMO_ENQUIRY_TYPE_ID;
        $heading = 'Request Demo';
        
        $this->Enquiry->recurisive = -1;
        $records = $this->paginate('Enquiry', $conditions);       
        
        $this->set('title_for_layout', 'Request Demo Enquiry');
        $this->set(compact('records'));
    }
    
    /**
     * @Request Demo Export
     */
    public function admin_request_demo_export() 
    {    
        $this->layout = "";
        $this->Redirect->urlToNamed();
        $conditions = $this->getSearchConditions(array(
                array('model' => 'Enquiry', 'field' => 'name', 'type' => 'string', 'view_field' => 'name'),
                array('model' => 'Enquiry', 'field' => 'referance_name', 'type' => 'string', 'view_field' => 'referance_name'),
                array('model' => 'Enquiry', 'field' => 'email', 'type' => 'string', 'view_field' => 'email'),
                array('model' => 'Enquiry', 'field' => 'mobile', 'type' => 'string', 'view_field' => 'mobile'),
                array('model' => 'Enquiry', 'field' => 'created_on', 'type' => 'from_date', 'view_field' => 'from_date'),
                array('model' => 'Enquiry', 'field' => 'created_on', 'type' => 'to_date', 'view_field' => 'to_date'),
            )
        );
        $conditions['type_id'] = SEMINAR_ENQUIRY_ID;
        $records = $this->Enquiry->find('all', array("conditions" => $conditions));   
        //$data = $this->Csv->getExportData();    
        
        foreach($records as $rec)
        {
            $data[] = array($rec["Enquiry"]["id"], $rec["Enquiry"]["name"], $rec["Enquiry"]["email"], 
                            $rec["Enquiry"]["mobile"], $rec["Enquiry"]["referance_name"]);
        }
        $headers = array("id", "name", "email", "mobile", "seminar-title");
        array_unshift($data, $headers); 
        $this->set(compact('data'));
        $this->render("admin_export");
    }
    
    /**
     * @Request Demo View
     */
    public function admin_request_demo_view($id)
    {
        $this->Redirect->urlToNamed();
        $record = $this->Enquiry->findById($id);
        $heading = 'Request Demo';
        $this->set('title_for_layout', 'Request Demo Enquiry Detail');
        $this->set(compact('record', 'heading'));
    }
    
}
