<?php
/**
 * Campaigns Controller
 * 
 * 
 * @created    17/01/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

App::import('Vendor', 'getid3/getid3');

class CampaignsController extends AppController 
{
    
    public $tabs = array(
        "details" => "Define",
        "target_location" => "Select Venues",
        "target_duration" => "Set the Days",
        "location" => "Creatives",
        "target_automobile" => "Set the Audience",
        "budget" => "Budget",
        "summary" => "Review & Submit",            
    );
    
    public function beforeFilter() 
    {
        parent::beforeFilter();
        
        $this->Auth->allow(array(
            "ajaxSummary", 
            "ajaxCampaignStatusChartData", 
            "ajaxAdsPlayedChartData",
            "ajaxAdShownChartData",
            "ajaxTopLocationChartData",
            "ajaxTopAutomobileChartData",
            "admin_campaign_version_activate",
            "ajaxCampaignBasicDetails",
            "ajaxCampaignEfficiencyDetails",
            "approve_campaign",
            "admin_ajaxUploadVideo",
            "admin_ajaxDeleteVideo",
            "admin_campaign_submit",
            "ajaxGetCampaignList"
        ));
        
        if ($this->user['group_id'] == ADVERTISER_GROUP_ID)
        {
            $this->{$this->modelClass}->campaignDays = $this->adminSetting['campaign_duration_fixed_min_days'];
            $this->{$this->modelClass}->campaignTrialDays = $this->adminSetting['campaign_trial_max_days'];
        }
        else
        {
            $this->{$this->modelClass}->campaignDays = 1;
            $this->{$this->modelClass}->campaignTrialDays = 30;
        }        
    }
    
    /*
     * @ Summary Screen
     */
    function admin_index()
    {   
        $this->Redirect->urlToNamed();
        
        $conditions = array();
        
        $conditions = $this->getSearchConditions(array(
                array('model' => $this->modelClass, 'field' => 'name', 'type' => 'string', 'view_field' => 'name'),
                array('model' => $this->modelClass, 'field' => 'campaign_no', 'type' => 'string', 'view_field' => 'campaign_no'),
                array('model' => $this->modelClass, 'field' => 'country_id', 'type' => 'integer', 'view_field' => 'country_id'),
                array('model' => $this->modelClass, 'field' => 'status_type_id', 'type' => 'integer', 'view_field' => 'status_type_id'),                
                array('model' => $this->modelClass, 'field' => 'start_date', 'type' => 'from_date', 'view_field' => 'start_date'),
                array('model' => $this->modelClass, 'field' => 'end_date', 'type' => 'to_date', 'view_field' => 'end_date'),
                array('model' => $this->modelClass, 'field' => 'user_id', 'type' => 'integer', 'view_field' => 'user_id'),
            )
        );
        
        if ($this->user['group_id'] == ADVERTISER_GROUP_ID)
        {
            $conditions['Campaign.user_id'] = $this->user['id'];
        }
        
        $this->{$this->modelClass}->contain(array(
            "User" => array(
                "fields" => array("id", "group_id", "full_name")
            ),
            "Country" => array(
                "fields" => array("id", "name")
            ),
            "CampaignDuration" => array(
                "fields" => array("id", "value")
            ),
            "CampaignType" => array(
                "fields" => array("id", "value")
            ),
            "CampaignStatus" => array(
                "fields" => array("id", "value")
            ),  
            "Invoice" => array(
                "fields" => array("id", "status"),                
            )
        ));
        
        
        $records = $this->paginate('Campaign', $conditions);   
        
        foreach ($records as $key => $record)            
        {
            $records[$key]["Campaign"]["total_pending_invoice_count"] = $records[$key]["Campaign"]["total_paid_invoice_count"] = 0;
            
            if ($record['Invoice'])
            {
                foreach ($record['Invoice'] as $arr)
                {   
                    switch($arr['status'])
                    {
                        case StaticArray::$invoice_status_unpaid:
                            $records[$key]["Campaign"]["total_pending_invoice_count"]++;
                        break;
                    
                        case StaticArray::$invoice_status_paid:
                            $records[$key]["Campaign"]["total_paid_invoice_count"]++;
                        break;
                    }
                }
            }
        }
        
        // sets general variables        
        $this->_getAdvertisers();        
        $this->_setCountries();
        $campaign_status_list = $this->_getTypes(CAMPAIGN_STATUS_TYPE);
        
        $this->set(compact('records', 'campaign_status_list'));		
    }
    
    /**
     * Adds New Record 
     */
    public function admin_add() 
    {
        if ($this->request->is('post')) 
        {
            $this->Campaign->create();            
            
            if ($this->request->data['countinue'])
            {
                $this->request->data['Campaign']["tab_name"] = "target_location";
            }
            
            $this->request->data['Campaign']["status_type_id"] = CAMPAIGN_CREATED_ID;
            
            $db = $this->{$this->modelClass}->getdatasource();
        
            $db->begin();
        
            if ($this->Campaign->save($this->request->data["Campaign"])) 
            {
                $db->commit();
                
                if ($this->request->data['Campaign']['name'])
                {
                    $num = "ANPR/" . COUNTRY . "/" . $this->Campaign->id . "/" . get_random_string($this->request->data['Campaign']['name'], 5) ;
                    $this->Campaign->saveField("campaign_no", $num, false);
                }
                
                $this->Session->setFlash('Record has been saved.', 'flash_success');

                if(!empty($this->request->data['countinue']))
                {
                    $this->redirect(array('action'=>'edit', $this->Campaign->id));
                }
                else
                {
                    $this->redirect(array('action' => 'index'));
                }
            } 
            else 
            {
                $db->rollback();
                $this->Session->setFlash('Unable to add Record.', 'flash_failure');
            }
        }		
        
        $last_tab = $current_tab = "details";
        $this->_setGeneralVariables(0, $current_tab, $last_tab);
        $can_save = true;
        
        //providing list for search
        $heading = 'Add '. $this->modelClass;
        $this->set(compact('heading', 'can_save'));
        $this->render('admin_form');        
    }
    
    
    public function admin_edit($id, $tab = null) 
    {  
        $heading = 'Edit '. $this->modelClass;
        
        $this->{$this->modelClass}->contain(array(
            "Country", "CampaignDuration", "CampaignType", "CampaignStatus",
            "User" => array(
                "fields" => array("id", "name", "subname")
            )
        ));
        if (!$this->request->data) 
        {
            $record = $this->{$this->modelClass}->findById($id);
            if (!$record) {
                throw new NotFoundException(__('Invalid Request'));
            }
            $this->request->data = $record;
        }
                
        $last_tab = isset($this->request->data[$this->modelClass]["tab_name"]) ? $this->request->data[$this->modelClass]["tab_name"] : "";
        
        $last_tab = $last_tab == "" ? "details" : $last_tab;                
        $current_tab = $tab == null ? $last_tab : $tab;        
        
        if ($this->request->is('post')) 
        {
            $this->_saveTabFormData($id, $current_tab, $last_tab);
        }
        
        $this->_setGeneralVariables($id, $current_tab, $last_tab);
        
        if (isset($this->request->data['Campaign']['duration_type_id']) && $this->request->data['Campaign']['duration_type_id'] != CAMPAIGN_DURATION_FIXED_ID)
        {
            unset($this->request->data['Campaign']['end_date']);
        }
        
        $can_save = true;
        $can_save_summary = true;
        
        if (isset($this->request->data["CampaignStatus"]['id']))
        {
            switch($this->request->data["CampaignStatus"]['id'])
            {
                case CAMPAIGN_CREATED_ID:
                    $can_save = $can_save_summary = true;
                break;
            
                case CAMPAIGN_SUBMITTED_ID:
                    if ($this->user['group_id'] == ADVERTISER_GROUP_ID)
                    {
                        $can_save = false;
                        $can_save_summary = !$this->request->data["Campaign"]["is_advertiser_approved"];
                    }
                break;
            
                default :
                    $can_save = false;
                    
                    if ($this->user['group_id'] != ADVERTISER_GROUP_ID)
                    {
                        $can_save_summary = true;
                    }
                break;
            
            }
        }
        //debug($this->data); exit;
        
        $this->set(compact('heading', 'can_save', 'can_save_summary'));
        $this->render('admin_form');
    }
    
    public function admin_campaign_detail($id)
    {
        $this->{$this->modelClass}->contain(array(
            "Country" => array(
                "fields" => array("id", "name")
            ), 
            "CampaignDuration" => array(
                "fields" => array("id", "value")
            ), 
            "CampaignType" => array(
                "fields" => array("id", "value")
            ), 
            "CampaignStatus" => array(
                "fields" => array("id", "value")
            ),
            "User" => array(
                "fields" => array("id", "name", "subname")
            ),
            "AdLocation" => array(
               "fields" => array(
                   "id", "target_imperssions", "reached_imperssions",
               ),       
               "conditions" => array("is_deleted" => 0),
               "Location" => array(
                    "fields" => array("id", "name", "price_per_impression", "setup_cost", "is_shared", "partner_id", "partner_revenue_share_percentage"),
                    "conditions" => array("is_deleted" => 0)
               )
           ),
           "Invoice"
        ));
        
        $record = $this->{$this->modelClass}->findById($id);
        
        
        if (!$record) {
            throw new NotFoundException(__('Invalid Request'));
        }
        
        $record = $this->_getCampaignStatusInfo($record);
        
        //debug($record); exit;
                
        // getting automobiles
        $automobile_list = $this->{$this->modelClass}->CampaignAutomobile->find("list", array(
            "fields" => array("automobile_id", "automobile_id"),
            "conditions" => array(
                "campaign_id" => $id
            ),
            "recursive" => -1
        )); 

        $automobiles = array();

        if ($automobile_list)
        {
             $automobiles = $this->{$this->modelClass}->CampaignAutomobile->Automobile->find("threaded", array(
                 "fields" => array("id", "name", "automobile_id"),
                 "order" => "Automobile.name ASC",
                 "recursive" => -1
             ));

             $automobiles = $this->_getFilterAutomobileList($automobiles, $automobile_list);
        }
        
        $invoice = array(
            "paid_amount" => 0,
            "pending_amount" => 0
        );
        
        if ($record["Invoice"])
        {
            foreach ($record["Invoice"] as $arr)
            {
                if ($arr["status"] == StaticArray::$invoice_status_paid)
                {
                    $invoice["paid_amount"] += $arr["amount"];
                }
                else if ($arr["status"] == StaticArray::$invoice_status_unpaid)
                {
                    $invoice["pending_amount"] += $arr["amount"];
                }
            }
        }
        
        $this->set(compact('record', 'automobiles', 'invoice'));
    }
        
    public function admin_efficiency_report($id = null)
    {
        $conditions = array();
        
        if ($this->user["group_id"] == ADVERTISER_GROUP_ID)
        {
            $conditions["Campaign.user_id"] = $this->user["id"];
        }
        
        $campaign_list = $this->{$this->modelClass}->find("list", array(
            "fields" => array("id", "name"),
            "conditions" => $conditions,
            "recursive" => -1
        ));
        
        $advertiser_list = $this->{$this->modelClass}->User->find("list", array(
            "fields" => array("id", "full_name"),
            "conditions" => array(
                "group_id" => ADVERTISER_GROUP_ID
            ),
            "recursive" => -1
        ));
        
        $advertiser_id = 0;
        if ($id)
        {
            $advertiser_id = $this->{$this->modelClass}->find("first", array(
                "fields" => array("user_id"),
                "conditions" => array(
                    "id" => $id
                ),
                "recursive" => -1
            ));
            
            
            
            if (!$advertiser_id)
            {
                throw new NotFoundException(__('Invalid Request'));
            }
            
            $advertiser_id = $advertiser_id['Campaign']["user_id"];
            
        }
        
        //debug($advertiser_list); exit;
        
        $this->set(compact("campaign_list", "advertiser_list", "id", "advertiser_id"));
    }
    
    
    /*--------------------------------- Reports ------------------------------*/
    
    /**
     * csv of ad_played on basis of automobiles
     * @param type $id
     */
    public function admin_efficiency_ad_played_automobile_export_csv($id)
    {
        $record = $this->{$this->modelClass}->find("first", array(
            "fields" => array("id", "name"),
            "condition" => array("id" => $id),
            "recursive" => -1
        ));
        
        $file = $record["Campaign"]["name"] . "_" . date("d-m-Y h:i") . ".csv";
        
        header('Content-Type: text/csv; charset=utf-8');
        header("Content-Disposition: attachment; filename=$file");

        $automobile_records = $this->_getAdPlayedOnBasisOfAutomobiles($id);
        $file = "php://output";
        
        writeCSV("php://output", $automobile_records);
        
        exit;
    }
    
    /**
     * csv of ad_played on basis of creative
     * @param type $id
     */
    public function admin_efficiency_ad_played_creative_export_csv($id)
    {
        $this->{$this->modelClass}->contain(array(            
            "AdLocation" => array(
               "fields" => array(
                   "id", "use_default_creative", "target_imperssions", "reached_imperssions"
               ),       
               "conditions" => array("is_deleted" => 0),
               "Location" => array(
                    "fields" => array("id", "name"),
                    "conditions" => array("is_deleted" => 0)
               )
           ),
        ));
        
        $record = $this->{$this->modelClass}->find("first", array(
            "fields" => array("Campaign.id", "Campaign.name"),
            "conditions" => array("Campaign.id" => $id)
        ));
        
        $file = $record["Campaign"]["name"] . "_" . date("d-m-Y h:i") . ".csv";
        
        header('Content-Type: text/csv; charset=utf-8');
        header("Content-Disposition: attachment; filename=$file");
        
        $automobile_records = $this->_getAdPlayedOnBasisOfCreative($record);
        
        $file = "php://output";
        
        writeCSV("php://output", $automobile_records);
        
        exit;
    }
    
    
    public function admin_export_pdf($id)
    {
        $this->{$this->modelClass}->contain(array(
            "Country" => array(
                "fields" => array("id", "name")
            ), 
            "CampaignDuration" => array(
                "fields" => array("id", "value")
            ), 
            "CampaignType" => array(
                "fields" => array("id", "value")
            ), 
            "CampaignStatus" => array(
                "fields" => array("id", "value")
            ),
            "User" => array(
                "fields" => array("id", "name", "subname")
            ),
            "AdLocation" => array(
               "fields" => array(
                   "id", "target_imperssions", "reached_imperssions",
               ),       
               "conditions" => array("is_deleted" => 0),
               "Location" => array(
                    "fields" => array("id", "name", "price_per_impression", "setup_cost", "is_shared", "partner_id", "partner_revenue_share_percentage"),
                    "conditions" => array("is_deleted" => 0)
               )
           ),
           "Invoice"
        ));
        
        $record = $this->{$this->modelClass}->findById($id);
        
        
        if (!$record) {
            throw new NotFoundException(__('Invalid Request'));
        }
        
        $record = $this->_getCampaignStatusInfo($record);
        
        $location_automobile = $this->_getAdPlayedOnBasisOfAutomobiles($id);
         
        // calculating efficiency  
        if ($record["Campaign"]["campaign_type_id"] == CAMPAIGN_AUTOMOBILE_MASS_ID)
        {
            $automobile_count = $this->{$this->modelClass}->query("CALL sp_get_variant_count");
            $automobile_count = $automobile_count[0][0]["variant_count"];
        }
        else
        {
            $automobile_list = $this->{$this->modelClass}->CampaignAutomobile->find("list", array(
                "fields" => array("automobile_id", "automobile_id"),
                "conditions" => array("campaign_id" => $id),
                "recursive" => -1
            ));
            
            $automobile_count = $this->_getCampaignAutomobileCount($automobile_list);
        }
        
        $efficiency = round( ($location_automobile["variant_count"] * 100 / $automobile_count), 2);
        
        
        $invoice = array(
            "paid_amount" => 0,
            "pending_amount" => 0
        );
        
        if ($record["Invoice"])
        {
            foreach ($record["Invoice"] as $arr)
            {
                if ($arr["status"] == StaticArray::$invoice_status_paid)
                {
                    $invoice["paid_amount"] += $arr["amount"];
                }
                else if ($arr["status"] == StaticArray::$invoice_status_unpaid)
                {
                    $invoice["pending_amount"] += $arr["amount"];
                }
            }
        }
        
        $this->set(compact("record", "efficiency", "invoice"));
        
        $this->createPDF("/Campaigns/campaign_detail_pdf", PDF_SAVE_PATH,  'campaign_' . $id);
        
        //$this->layout = null; $this->render("campaign_detail_pdf");        
    }
    
    public function admin_export_excel($id)
    {
        $this->{$this->modelClass}->contain(array(
            "Country" => array(
                "fields" => array("id", "name")
            ), 
            "CampaignDuration" => array(
                "fields" => array("id", "value")
            ), 
            "CampaignType" => array(
                "fields" => array("id", "value")
            ), 
            "CampaignStatus" => array(
                "fields" => array("id", "value")
            ),
            "User" => array(
                "fields" => array("id", "name", "subname")
            ),
            "AdLocation" => array(
               "fields" => array(
                   "id", "use_default_creative", "target_imperssions", "reached_imperssions",
               ),       
               "conditions" => array("is_deleted" => 0),
               "Location" => array(
                    "fields" => array("id", "name", "price_per_impression", "setup_cost", "is_shared", "partner_id", "partner_revenue_share_percentage"),
                    "conditions" => array("is_deleted" => 0)
               )
           ),
           "Invoice"
        ));
        
        $record = $this->{$this->modelClass}->findById($id);
        
        
        if (!$record) {
            throw new NotFoundException(__('Invalid Request'));
        }
        
        $record = $this->_getCampaignStatusInfo($record);
        
        $location_automobile = $this->_getAdPlayedOnBasisOfAutomobiles($id);
         
        // calculating efficiency  
        if ($record["Campaign"]["campaign_type_id"] == CAMPAIGN_AUTOMOBILE_MASS_ID)
        {
            $automobile_count = $this->{$this->modelClass}->query("CALL sp_get_variant_count");
            $automobile_count = $automobile_count[0][0]["variant_count"];
        }
        else
        {
            $automobile_list = $this->{$this->modelClass}->CampaignAutomobile->find("list", array(
                "fields" => array("automobile_id", "automobile_id"),
                "conditions" => array("campaign_id" => $id),
                "recursive" => -1
            ));
            
            $automobile_count = $this->_getCampaignAutomobileCount($automobile_list);
        }
        
        $efficiency = round( ($location_automobile["variant_count"] * 100 / $automobile_count), 2);
        
        
        $invoice = array(
            "paid_amount" => 0,
            "pending_amount" => 0
        );
        
        if ($record["Invoice"])
        {
            foreach ($record["Invoice"] as $arr)
            {
                if ($arr["status"] == StaticArray::$invoice_status_paid)
                {
                    $invoice["paid_amount"] += $arr["amount"];
                }
                else if ($arr["status"] == StaticArray::$invoice_status_unpaid)
                {
                    $invoice["pending_amount"] += $arr["amount"];
                }
            }
        }
        
        $data["record"] = $record;
        $data["invoice"] = $invoice;
        $data["efficiency"] = $efficiency;
        $data["automoble_ad_played_data"] = $this->_getAdPlayedOnBasisOfAutomobiles($id);
        $data["creative_ad_played_data"] = $this->_getAdPlayedOnBasisOfCreative($record);
        
        //debug($data); exit;
        $this->Excel->options["User"] = $this->user;
        $this->Excel->options["title"] = "Campaign Details";
        $this->Excel->options["subject"] = "Campaign Details";
        $this->Excel->options["desc"] = "Campaign Details";
        $this->Excel->options["filename"] = "campaign_" . $id . ".xlsx";
        
        $this->Excel->exportCampaignDetails($data); 
        exit;
    }
    
    
    
    
    /**
     * create campign version
     * insert record in to history tables from campaign's table
     * @param int $id
     */
    public function admin_add_campaign_version($id, $redirect = true)
    {
        $this->_changeCampaignStatus($id, CAMPAIGN_CREATED_ID, false, false);
        
        // now sending email and notification
        $this->{$this->modelClass}->contain(array(
            "User" => array(
                "fields" => array("id", "name", "subname", "username")
            )
        ));
                
        $record = $this->{$this->modelClass}->find("first", array(
            "fields" => array("id", "name"),
            "conditions" => array(
                "Campaign.id" => $id
            )
        ));
        
        
        $to_email = $record['User']['username'];
        $from_email = FROM_EMAIL;

        $viewData["records"] = $record;
        $viewData["content"]["footer"] = $this->_getEmailFooterContent();

        $subject = "Campaign " . $record['Campaign']['name'] . " Version created";
        
        $this->sendEmail($to_email, $from_email, $subject, "Campaign/version_created", $viewData, $files = array(), $bcc = array());
        $this->sendEmail($from_email, TO_EMAIL, $subject, "Campaign/admin/version_created", $viewData, $files = array(), $bcc = array());
        
        $notification_data = array(
            array
            (
                "group_id" => ADMIN_GROUP_ID,
                "type" => StaticArray::$notification_type_alert_postive,
                "content" => "New version of campaign <b>" . $record['Campaign']['name'] . "</b> created"
            ),
            array
            (
                "user_id" => $record["User"]['id'],
                "type" => StaticArray::$notification_type_alert_postive,
                "content" => "New version of campaign <b>" . $record['Campaign']['name'] . "</b> created"
            ),           
        );
        
        
        
        $this->_saveNotification($notification_data);
        
        if ($redirect)
        {
            $this->Session->setFlash(' Done. Now you able to change campaign', 'flash_success');        
            $this->redirect(array("action" => "edit", "admin" => true, $id));
        }
    }
    
    public function admin_campaign_version_activate($history_campaign_id)
    {
       $this->autoRender = false;
        
       $campaign_id = $this->_activate_history_campaign($history_campaign_id);
       
       // now sending email and notification
        $this->{$this->modelClass}->contain(array(
            "User" => array(
                "fields" => array("id", "name", "subname", "username")
            )
        ));
                
        $record = $this->{$this->modelClass}->find("first", array(
            "fields" => array("id", "name"),
            "conditions" => array(
                "Campaign.id" => $campaign_id
            )
        ));
        
        
        $to_email = $record['User']['username'];
        $from_email = FROM_EMAIL;

        $viewData["records"] = $record;
        $viewData["content"]["footer"] = $this->_getEmailFooterContent();

        $subject = "Campaign " . $record['Campaign']['name'] . " Version #$history_campaign_id Activated";
        
        $this->sendEmail($to_email, $from_email, $subject, "Campaign/version_activated", $viewData, $files = array(), $bcc = array());
        $this->sendEmail($from_email, TO_EMAIL, $subject, "Campaign/admin/version_activated", $viewData, $files = array(), $bcc = array());
        
        $notification_data = array(
            array
            (
                "user_id" => $record["User"]['id'],
                "type" => StaticArray::$notification_type_alert_postive,
                "content" => "version <b> #$history_campaign_id </b> of campaign <b>" . $record['Campaign']['name'] . "</b> is Activated"
            ),
            array
            (
                "group_id" => ADMIN_GROUP_ID,
                "type" => StaticArray::$notification_type_alert_postive,
                "content" => "version <b> #$history_campaign_id </b> of campaign <b>" . $record['Campaign']['name'] . "</b> is Activated"
            )
        );
        
        $this->_saveNotification($notification_data);
        
        $data['CampaignVersionLog'] = array(
            "user_id" => $record["User"]['id'],
            "campaign_id" => $campaign_id,
            "history_campaign_id" => $history_campaign_id
        );
        
        $this->loadModel("CampaignVersionLog");
        
        $this->CampaignVersionLog->save($data);
        
        $this->Session->setFlash(' Done. you have activate the campaign version successfully', 'flash_success');        
        $this->redirect(array("action" => "edit", "admin" => true, $campaign_id));        
    }
    
    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id) 
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid Request'));
        } 
                
        if ($this->{$this->modelClass}->deleteSoft($id)) 
        {
            $this->Session->setFlash('The record deleted Successfully!', "flash_success");
        } else {            
            $this->Session->setFlash('The record cannot be deleted!', "flash_failure");            
        }
        $this->redirect($this->referer());
    }
    
    /**
     * send approval email to advertiser
     * @param int $campaign_id
     */
    public function admin_campaign_advertiser_approval_email($campaign_id)
    {
        $this->_sendCampaignAdvertiserConfirmationEmail($campaign_id);
        
        $this->Session->setFlash('Email has Sent', 'flash_success');
        
        $this->redirect(array("action" => "edit", "admin" => true, $campaign_id));
    }
    
    /**
     * approved from advertiser 
     * @param string $confirmationCode
     */
    public function approve_campaign($confirmationCode)
    {
        $record = $this->{$this->modelClass}->find("first", array(
            "fields" => array("id", "name"),
            "conditions" => array("approval_confirm_code" => $confirmationCode),
            "recursive" => -1
        ));
        
        if ($record)
        {
            $this->{$this->modelClass}->id = $record["Campaign"]["id"];            
            $result = $this->{$this->modelClass}->saveField('is_advertiser_approved', 1, false);
            
            if ($result)
            {
                $notification_data = array(                   
                    array
                    (
                        "group_id" => ADMIN_GROUP_ID,
                        "type" => StaticArray::$notification_type_alert_postive,
                        "content" => "<b>" . $record["Campaign"]["name"]  . "</b> has approved by Advertiser"
                    )
                );

                $this->_saveNotification($notification_data);
                
                echo "<h1>Campaign Approved Successfully</h1>";
            }
            else
            {
                echo "<h1>Failed to save. try after some time</h1>";
            }
        }
        else
        {
            echo "<h1>Invalid Confirm code</h1>";
        }
        
        echo "<br><br><a href='" . Router::url(array('controller' => '/', 'action' => '/'))  . "'> Click Here </a> to goto Site";
        
        exit;
    }
    
    /**************------------- Ajax functions ---------- *****************/
    
    public function ajaxCampaignEfficiencyDetails($id)
    {
       $this->{$this->modelClass}->contain(array(
            "Country" => array(
                "fields" => array("id", "name")
            ), 
            "CampaignDuration" => array(
                "fields" => array("id", "value")
            ), 
            "CampaignType" => array(
                "fields" => array("id", "value")
            ), 
            "CampaignStatus" => array(
                "fields" => array("id", "value")
            ),
            "User" => array(
                "fields" => array("id", "name", "subname")
            ),
            "AdLocation" => array(
               "fields" => array(
                   "id", "use_default_creative", "target_imperssions", "reached_imperssions", "ad_type", "ad_name", "ad_url"
               ),       
               "conditions" => array("is_deleted" => 0),
               "Location" => array(
                    "fields" => array("id", "name", "setup_cost", "price_per_impression", "is_shared", "partner_id", "partner_revenue_share_percentage"),
                    "conditions" => array("is_deleted" => 0)
               )
           ),
        ));
        
        $record = $this->{$this->modelClass}->findById($id);
        
        $record = $this->_getCampaignStatusInfo($record);    
        
        $temp = $this->{$this->modelClass}->query("CALL sp_invoice_get_sum(NULL, " . $id . ", " . StaticArray::$invoice_status_unpaid . ", NULL);");        
        $pending_invoice_amount = $temp[0][0]["sum_amount"] ? $temp[0][0]["sum_amount"] : 0;

        $temp = $this->{$this->modelClass}->query("CALL sp_invoice_get_sum(NULL, " . $id . ", " . StaticArray::$invoice_status_paid . ", NULL);");
        $paid_invoice_amount = $temp[0][0]["sum_amount"] ? $temp[0][0]["sum_amount"] : 0;
        
        $location_automobile = $this->_getAdPlayedOnBasisOfAutomobiles($id);
        
        $location_creative = $this->_getAdPlayedOnBasisOfCreative($record);
         
        // calculating efficiency  
        if ($record["Campaign"]["campaign_type_id"] == CAMPAIGN_AUTOMOBILE_MASS_ID)
        {
            $automobile_count = $this->{$this->modelClass}->query("CALL sp_get_variant_count");
            $automobile_count = $automobile_count[0][0]["variant_count"];
        }
        else
        {
            $automobile_list = $this->{$this->modelClass}->CampaignAutomobile->find("list", array(
                "fields" => array("automobile_id", "automobile_id"),
                "conditions" => array("campaign_id" => $id),
                "recursive" => -1
            ));
            
            $automobile_count = $this->_getCampaignAutomobileCount($automobile_list);
        }
        
        $efficiency = round( ($location_automobile["variant_count"] * 100 / $automobile_count), 2);
        
        $invoice = array(
            "paid_amount" => $paid_invoice_amount,
            "pending_amount" => $pending_invoice_amount
        );
        
        $this->set(compact("record", "invoice", "location_automobile", "location_creative", "efficiency"));
    }
    
    public function ajaxAdShownChartData($campaign_id)
    {
       $this->layout = "ajax";
         
       $this->{$this->modelClass}->AdLocation->contain(array(
            "AdPlayedLog" => array(
                "fields" => array("id", "created_on"), 
                "order" => array(
                    "created_on" => "ASC"
                ),
                "conditions" => array(
                    "confirm_web_service_log_id > " => 0
                )
            ),
            "Location" => array(
                "fields" => array("id", "name"),
                "conditions" => array(
                    "Location.is_deleted" => 0
                )
            )
        ));
        
        $records = $this->{$this->modelClass}->AdLocation->find("all", array(
            "fields" => array("id"),
            "conditions" => array(
                "campaign_id" => $campaign_id
            )
        ));
        
        $campaign_record = $this->{$this->modelClass}->find("first", array(
            "fields" => array("id", "start_date", "end_date", "duration_type_id"),
            "conditions" => array(
                "id" => $campaign_id,                
            ),
            "recursive" => -1
        ));
                
        $start_date = DateUtility::getDateObj($campaign_record['Campaign']['start_date']);
        $end_date = $campaign_record['Campaign']['duration_type_id'] == CAMPAIGN_DURATION_FIXED_ID ? DateUtility::getDateObj($campaign_record['Campaign']['end_date']) : NULL;
        $current_date = DateUtility::getCurrentDateTimeObj();
        
        if (!$end_date || $current_date < $end_date)
        {
            $end_date = $current_date;
        }
        
        $list = DateUtility::getDayListBetweenTwoDates($start_date, $end_date, DEFAULT_SQL_DATE_FORMAT);
                
        $data = array();

        foreach ($records as $record)
        {
            $key = $record['Location']['name'];
            foreach ($list as $date)
            {
                $data[$date][$key] = 0;
                $data[$date]['date'] = $date;
                
                foreach ($record['AdPlayedLog'] as $arr)
                {
                    $str_date = DateUtility::getFormatDateFromString($arr['created_on'], DEFAULT_SQL_DATE_FORMAT);
                    
                    if ($str_date == $date)
                    {
                        $data[$date][$key]++;
                    }
                }
            }
        }
        
        $location_list = Hash::combine($records, "{n}.Location.id", "{n}.Location.name");
        
        echo json_encode(array(
            "data" => $data,
            "group_list" => $location_list
        )); 
        exit;
    }
    
    public function ajaxTopLocationChartData($campaign_id, $last_month = 0, $limit = 3)
    {
       $user_id = $this->user['group_id'] == ADVERTISER_GROUP_ID ? $this->user['id'] : "NULL";
       $date = DateUtility::getCurrentDateTimeString("Y-m-01");       
       if ($last_month != 0)
       {
            $date = DateUtility::getFormatDateFromString(strtotime($date . " " . $last_month. " month"), DEFAULT_SQL_DATE_FORMAT);  
       }
        
       $records = $this->{$this->modelClass}->query("CALL sp_get_top_locations($campaign_id, NULL, $user_id, '" . $date . "', $limit);");
       
       foreach ($records as $k => $record)
       {
           $records[$k]["L"]["name"] =  wordwrap($records[$k]["L"]["name"], 15 ,"\n");
       }
       
       echo json_encode($records); exit;
    }
    
    public function ajaxTopAutomobileChartData($campaign_id, $last_month = 0, $limit = 3)
    {
       $user_id = $this->user['group_id'] == ADVERTISER_GROUP_ID ? $this->user['id'] : "NULL";
       $date = DateUtility::getCurrentDateTimeString("Y-m-01");       
       if ($last_month != 0)
       {
            $date = DateUtility::getFormatDateFromString(strtotime($date . " " . $last_month. " month"), DEFAULT_SQL_DATE_FORMAT);  
       }
       
       $records = $this->{$this->modelClass}->query("CALL sp_get_top_automobile_variant($campaign_id, NULL, NULL, $user_id, '" . $date . "', $limit);");
       
       $data = array();
       foreach ($records as $k => $record)
       {
           $name = trim($record["Manufacture"]["name"]) . "-" . trim($record["Model"]["name"]) . "-" . trim($record["Variant"]["name"]);
           $name = wordwrap($name, 15 ,"\n");
           
           $data[] = array(
               "name" => $name,
               "count" => $record[0]["total_count"]
           );
       }
       
       
        echo json_encode($data); exit;
    }
    
    public function ajaxGetCampaignList($advertiser_id)
    {
        $list = $this->{$this->modelClass}->find("list", array(
            "fields" => array("id", "name"),
            "conditions" => array(
                "user_id" => $advertiser_id
            ),
            "recursive" => -1
        ));
        
        echo json_encode($list); exit;
    }
    
        
    /**------------------------ Private functions -------------------------*/
    
    /**
     * 
     * save the current tab data in diffrents table,
     * save the campaigns table details
     * @param type $id
     * @param type $current_tab
     * @param type $last_tab
     */
    private function _saveTabFormData($id, $current_tab, $last_tab)
    {
        $submitted_by_advertister = false;
        
        $saved = true;
        switch($current_tab)
        {
            case "target_location":
                $saved = $this->_saveTargetLocation($id);
            break;            
            
            case "target_duration": 
                $saved = $this->_saveTargetDuration();                
            break;
            
            case "location":                 
                $saved = $result = $this->{$this->modelClass}->AdLocation->saveMany($this->request->data["AdLocation"]);
            break;
            
            case "target_automobile":
                $saved = $this->_saveTargetAutomobile($id);
            break;
        
            case "budget" : 
                $saved = $this->_saveBudget($id);
            break;
        
            case "summary":                  
                $this->request->data['countinue'] = 0;
                
                $saved = true;
                if (isset($this->request->data["Campaign"]["status_type_id"]) && $this->request->data["Campaign"]["status_type_id"])
                {
                    if ($this->request->data["Campaign"]["status_type_id"] == CAMPAIGN_SUBMITTED_ID)
                    {
                        if ($this->request->data["Campaign"]["status"])
                        {
                            $saved = $this->_changeCampaignStatus($id, $this->request->data["Campaign"]["status_type_id"]);
                         
                            if ($this->request->data["Campaign"]["user_id"] != $this->user["id"])
                            {
                                $this->_sendCampaignAdvertiserConfirmationEmail($id);
                            }
                            else
                            {
                                $submitted_by_advertister = true;
                            }
                        }
                    }
                    else
                    {
                        $saved = $this->_changeCampaignStatus($id, $this->request->data["Campaign"]["status_type_id"]);

                        if ($this->request->data["Campaign"]["status_type_id"] == CAMPAIGN_APRROVED_ID && $saved)
                        {
                            $this->_generateInvoice();
                            
                            $this->{$this->modelClass}->query("CALL sp_campaign_version_create($id, @last_id);");   
                        }
                    }
                    
                    unset($this->request->data["Campaign"]["status_type_id"]);
                }                
                
            break;
        }
        
        if ($saved)
        {
            if ($last_tab == $current_tab)
            {
                $tab_keys = array_keys($this->tabs);
                $tab_flip = array_flip($tab_keys);

                if ($tab_flip[$last_tab] + 1 < count($this->tabs))
                {
                    $tab = $tab_keys[$tab_flip[$last_tab] + 1];                
                    $this->request->data[$this->modelClass]["tab_name"] = $tab;
                }
            }
            
            if (!$this->request->data['countinue'])
            {
                unset($this->request->data[$this->modelClass]["tab_name"]);
            }
            
            
            $this->{$this->modelClass}->id = $id;            
            if (!empty($this->request->data["Campaign"]))
            {
                $saved = $this->Campaign->save($this->request->data["Campaign"]); 
            }
            
            if ($saved) 
            {
                if ($submitted_by_advertister)
                {
                    $this->redirect(array('action'=>'campaign_submit', "admin" => true, $this->Campaign->id));
                }
                $this->Session->setFlash('Record has been saved.', 'flash_success');
                
                // for invoice redirection
                if (isset($this->request->data['Campaign']['paynow']) && $this->request->data['Campaign']['paynow'] && $this->request->data['Campaign']['invoice_id'])
                {
                    $this->redirect(array("controller" => "Invoices", "action" => "edit", $this->request->data['Campaign']['invoice_id'], "admin" => true));
                }
                
                
                if(!empty($this->request->data['countinue']))
                {
                    $this->redirect(array('action'=>'edit', $this->Campaign->id));
                }
                else
                {
                    $this->redirect(array( 'action' => 'index'));
                }
            } 
        }
        
        
        if (!$saved)
        {
            $this->Session->setFlash('Unable to save Record.', 'flash_failure');
        }
        
    }
    
    /**
     * sets variables according to current tab
     * @param type $current_tab
     */
    private function _setGeneralVariables($id, $current_tab, $last_tab)
    {
        $tabs = $this->tabs;      
        
        $this->set(compact('tabs', 'current_tab', 'last_tab', 'id'));
        
        switch($current_tab)
        {    
            case "details":
                $this->_getAdvertisers();        
                $this->_setCountries();
        
                $duration_type_list = $this->_getTypes(CAMPAIGN_DURATION_TYPE);                
                
                $adminSetting = $this->_getAdminSetting(array("campaign_duration_fixed_min_days"));//  $this->AdminSetting->find("first", array("fields" => array("paypal_id")));
                $adminSetting = $adminSetting['AdminSetting'];
                
                $this->set(compact('duration_type_list'));
            break;     
        
            case "target_location":
                $country_id = $this->request->data[$this->modelClass]["country_id"];
                
                $location_points = $this->_setLocationGeoPoints($id, $country_id);
                $this->_setStateCityList($country_id);
                $this->set(compact('location_points'));
           break;
       
           case "target_duration": 
               
               $this->{$this->modelClass}->AdLocation->contain(array(
                   "Location" => array(
                            "fields" => array("id", "name"),
                            "State" => array(
                                "fields" => array("id", "name")
                            )
                       ),
                    "Campaign" => array(
                        "fields" => array("id", "start_date", "end_date")
                    )
                ));
               
               $ad_locations = $this->{$this->modelClass}->AdLocation->find("all", array(                   
                   "conditions" => array("AdLocation.campaign_id" => $id)
               ));
               
               
               $start_date = DateUtility::getFormatDateFromString($ad_locations[0]['Campaign']['start_date'], DEFAULT_DATE_FORMAT);
               $end_date = DateUtility::getFormatDateFromString($ad_locations[0]['Campaign']['end_date'], DEFAULT_DATE_FORMAT);
               
               $start_date1 = DateUtility::addDaysInDateString($start_date, 1, DEFAULT_DATE_FORMAT);
               $end_date1 = DateUtility::addDaysInDateString($end_date, "-1", DEFAULT_DATE_FORMAT);
               
               if (!isset ($this->request->data["AdLocation"]))
               {
                   $data = Hash::combine($ad_locations, "{n}.AdLocation.id", "{n}.AdLocation");
                   $this->request->data["AdLocation"] = $data;
               }
               
               $records = Hash::combine($ad_locations, "{n}.AdLocation.id", "{n}.Location.name", "{n}.Location.State.name");
               
               $this->set(compact("records", "start_date", "end_date", "start_date1", 'end_date1'));
           break;
            
           case "location": 
               $this->{$this->modelClass}->AdLocation->contain(array(
                    "Location" => array(
                        "fields" => array(
                            "name", "price_per_impression", "setup_cost", "is_exclusive"
                        )
                    )
                ));

               $records = $this->{$this->modelClass}->AdLocation->find("all" , array(
                    "fields" => array(
                        "id", "use_default_creative", "ad_url", "ad_name", "ad_upload_path",
                        "ad_size", "ad_type", "ad_mime", "ad_ext", "ad_length"
                     ),
                    "conditions" => array("AdLocation.campaign_id" => $id)
                ));
               
               if (!isset($this->request->data['AdLocation']))
               {                   
                   $data = Hash::combine($records, "{n}.AdLocation.id", "{n}.AdLocation");                   
                   $this->request->data['AdLocation'] = $data;
               }
               
               $this->set(compact('records'));
           break;
           
           case "target_automobile":
               
               $this->{$this->modelClass}->CampaignAutomobile->Automobile->recursive = -1;
               $automobiles = $this->{$this->modelClass}->CampaignAutomobile->Automobile->find("threaded", array(
                   "fields" => array("id", "name", "automobile_id"),
                   "order" => "Automobile.name ASC"
               ));
               
               
               $ad_location_list = $this->{$this->modelClass}->AdLocation->find("list", array(
                   "fields" => array("id", "location_id"),
                   "conditions" => array("campaign_id" => $id),
                   "recursive" => -1
               ));
               
               
               $location_list = $this->{$this->modelClass}->AdLocation->Location->find("list", array(
                   "conditions" => array("id" => $ad_location_list),
                   "recursive" => -1
               ));
               
                              
               $record_list = $this->{$this->modelClass}->CampaignAutomobile->find("list", array(
                   "fields" => array("id", "automobile_id"),
                   "conditions" => array("campaign_id" => $id),                      
               ));   
               
               $location_list = array_flip(sort_array_on_value_string_len($location_list));               
               
               $target_types = $this->_getTypes(CAMPAIGN_TYPE);
               
               $this->set(compact('automobiles', 'record_list', 'target_types', 'ad_locations', 'ad_location_list', 'location_list'));
           break;
       
           case "budget":
               $this->{$this->modelClass}->AdLocation->contain(array(
                    "Location" => array(
                        "fields" => array(
                            "name", "price_per_impression", "setup_cost", "is_exclusive"
                        )
                    ),
                   "Campaign" => array(
                       "fields" => array("budget")
                   )
                ));

               $records = $this->{$this->modelClass}->AdLocation->find("all" , array(
                    "fields" => array(
                        "id", "actual_budget", "target_imperssions"
                     ),
                    "conditions" => array("AdLocation.campaign_id" => $id)
                ));
               
               //debug($records); exit;
               
               if (!isset($this->request->data['AdLocation']))
               {                   
                   $data = Hash::combine($records, "{n}.AdLocation.id", "{n}.AdLocation");                   
                   $this->request->data['AdLocation'] = $data;
               }
               
               $this->set(compact('records'));
           break;
           
           case "summary":
               
               if ($this->request->is(array('post', "put"))) 
               {
                    $records = $this->{$this->modelClass}->findById($id);
               }
               else
               {
                   $records = $this->request->data;
               }
               
                $this->{$this->modelClass}->AdLocation->contain(array(
                    "Location" => array(
                         "fields" => array(
                             "name", "price_per_impression", "setup_cost", "is_exclusive", 
                         )
                     ),
                ));
                
               $ad_location_records = $this->{$this->modelClass}->AdLocation->find("all" , array( 
                    "fields" => array(
                        "id", "use_default_creative", "target_imperssions", "reached_imperssions", "actual_budget",
                        "is_exclusive", "ad_url", "ad_name", "ad_type",
                        "from_date", "to_date"
                    ),
                    "conditions" => array("campaign_id" => $id)
                ));
               
               $automobile_list = $this->{$this->modelClass}->CampaignAutomobile->find("list", array(
                   "fields" => array("automobile_id", "automobile_id"),
                   "conditions" => array(
                       "campaign_id" => $id
                   ),
                   "recursive" => -1
               )); 
               
               $automobiles = array();
               
               
               if ($automobile_list)
               {
                    $automobiles = $this->{$this->modelClass}->CampaignAutomobile->Automobile->find("threaded", array(
                        "fields" => array("id", "name", "automobile_id"),
                        "order" => "Automobile.name ASC",
                        "recursive" => -1
                    ));

                    $automobiles = $this->_getFilterAutomobileList($automobiles, $automobile_list);
               }
               
               $this->loadModel("HistoryCampaign");
               $this->HistoryCampaign->contain(array(
                   "Country" => array(
                        "fields" => array("id", "name")
                    ),
                    "CampaignDuration" => array(
                        "fields" => array("id", "value")
                    ),
                    "CampaignType" => array(
                        "fields" => array("id", "value")
                    ),                    
               ));
               
               $history_campaign_records = $this->HistoryCampaign->find("all", array(
                   "fields" => array("id", "name", "duration_type_id", "start_date", "end_date",  "budget"),
                   "conditions" => array(
                       "HistoryCampaign.parent_id" => $id
                   ),                   
               ));
               
               $temp = $this->{$this->modelClass}->query("CALL sp_invoice_get_sum(NULL, " . $id . ", " . StaticArray::$invoice_status_unpaid . ", NULL);");        
               $pending_invoice_amount = $temp[0][0]["sum_amount"] ? $temp[0][0]["sum_amount"] : 0;
               
               $temp = $this->{$this->modelClass}->query("CALL sp_invoice_get_sum(NULL, " . $id . ", " . StaticArray::$invoice_status_paid . ", NULL);");        
               $paid_invoice_amount = $temp[0][0]["sum_amount"] ? $temp[0][0]["sum_amount"] : 0;
               
               $invoice = array(
                   "paid_amount" => $paid_invoice_amount,
                   "pending_amount" => $pending_invoice_amount
               );
               
               $this->set(compact('records', 'ad_location_records', 'automobiles', 'history_campaign_records', 'invoice'));
           break;
        }
    }
        
    /*
     * @Get Types
     */
    private function _getTypes($type)
    {
        $this->loadModel("Type");
        $types = $this->Type->find("list", array("conditions" => array("Type.type" => $type)));
        return $types;
    }
    
    /*
     * @Get Users
     */
    private function _getAdvertisers()
    {
        $advertiserList = $this->Campaign->User->find("list", array(
            "conditions" => array("User.group_id" => ADVERTISER_GROUP_ID), 
            "order" => "User.full_name ASC",
            "fields" => array("id", "full_name")
        ));
        $this->set(compact("advertiserList"));
    }
    
    /**
     * sets country list
     */
    private function _setCountries()
    {
        $country_list = $this->{$this->modelClass}->Country->find('list', array(
           'conditions' => array('Country.place_id' => NULL),
           'is_active' => 1
        ));
        
       $this->set(compact('country_list'));
    }
    
    /**
     * sets state and city list of country
     * @param type $country_id
     */
    private function _setStateCityList($country_id)
    {
        $place_list = $this->{$this->modelClass}->Country->getTreeList('place_id', array(
                    "or" => array(
                        "Country.id" => $country_id,
                        "Country.place_id" => $country_id
                    )
                ));        
        $this->set(compact('place_list'));
    }
    
    private function _setLocationGeoPoints($id, $place_id)
    {
        $is_exclusive = NULL;
        if ($this->user['group_id'] == ADVERTISER_GROUP_ID)
        {
            $is_exclusive = 0;
        }
        
        $records = $this->_getLocationGeoPoints(null, $place_id, $is_exclusive, 1);
        
        $location_points = array();                
        $temp_campaign = array();
        $i = 0;        
        foreach ($records as $record)
        {
            $temp_campaign = array();
            $location_points[$i] = $record["Location"];
            $location_points[$i]["campaign_count"] = 0;
            $location_points[$i]["selected"] = 0;
            $location_points[$i]["active"] = 0;
            $location_points[$i]["country"] = $record["Country"]["name"];
            $location_points[$i]["state"] = $record["State"]["name"];
            $location_points[$i]["city"] = $record["City"]["name"];
            

            foreach ($record["AdLocation"] as $adLocation)
            {
                if (isset($adLocation['Campaign']['id']) && !isset($temp_campaign[$adLocation['Campaign']['id']]))
                {
                    $location_points[$i]["campaign_count"]++;
                    
                    if (isset($adLocation["Campaign"]['status_type_id']) && $adLocation["Campaign"]['status_type_id'] == CAMPAIGN_ACTIVE_ID)
                    {
                        $location_points[$i]["active"]++;
                    }
                
                    $temp_campaign[$adLocation['Campaign']['id']] = $adLocation['Campaign']['id'];
                }
                
                if ($adLocation["campaign_id"] == $id)
                {
                    $location_points[$i]["selected"] = 1;
                }
            }

            $i++;
        }
        
        
        return json_encode($location_points);
    }
    
    private function _setAutombile($id)
    {
        $automobile_list = $this->{$this->modelClass}->CampaignAutomobile->Automobile->find('list', array(
           'conditions' => array('Automobile.automobile_id' => $parent_id, 'is_active' => 1),           
           'recursive' => -1
        ));
        
        //debug($automobile_list); exit;
        return $automobile_list;
    }
    
    private function _saveTargetLocation($campaign_id)
    {           
        $db = $this->{$this->modelClass}->getdatasource();
        
        $db->begin();
        
        $locations = array_flip(explode(",", $this->request->data[$this->modelClass]["location_ids"]));

        $record_list= $this->{$this->modelClass}->AdLocation->find("list", array(
            "fields" => array("location_id", "id"),
            "conditions" => array(                       
                "AdLocation.campaign_id" => $campaign_id
            ),
            "recursive" => -1
        ));


        /**
         * looping for get records to be add
         */                
        $ad_locations = array();
        foreach ($locations as $location_id => $v)
        {
            if (!isset($record_list[$location_id]))
            {
                // new location
                $ad_locations[] = array(
                    "location_id" => $location_id,
                    "campaign_id" => $campaign_id
                );
            }
        }

        /**
         * Looping to get records which to be delete
         */
        $del_locations = array();
        foreach ($record_list as $location_id => $v)
        {
            if (!isset($locations[$location_id]))
            {
                $del_locations[] = $v;
            }
        }
        //debug($locations);                debug($record_list);                debug($ad_locations);                debug($del_locations); exit;
        
        $saved = true;
        if ($ad_locations)
        {
            $saved = $this->{$this->modelClass}->AdLocation->saveMany($ad_locations);
            $this->request->data[$this->modelClass]["tab_name"] = "target_duration";
            $this->request->data['countinue'] = 1;
        }

        if ($del_locations)
        {
            foreach ($del_locations as $ad_location_id)
            {
                $this->{$this->modelClass}->AdLocation->deleteSoft($ad_location_id);
            }
        }

        unset($this->request->data[$this->modelClass]["location_ids"]); 
        
        if ($saved)
        {
            $db->commit();
        }
        else
        {
            $db->rollback();
        }
        
        return $saved;
    }
    
    private function _saveTargetDuration()
    {
        $db = $this->{$this->modelClass}->getdatasource();
        
        $db->begin();
        
        $result = true;
        
        $ad_location_schedule = array();
        
        foreach ($this->request->data["AdLocation"] as $ad_location_id => $data)
        {
            $this->{$this->modelClass}->AdLocation->id = $ad_location_id;            
            $data["start_time"] = DateUtility::getFormatDateFromString("2015-01-01 " . $data["start_time"], DEFAULT_SQL_TIME_FORMAT);
            $data["end_time"] = DateUtility::getFormatDateFromString("2015-01-01 " . $data["end_time"], DEFAULT_SQL_TIME_FORMAT);
            
            if (!$this->{$this->modelClass}->AdLocation->save($data))
            {
                $this->{$this->modelClass}->AdLocation->validationErrors[$ad_location_id] = $this->{$this->modelClass}->AdLocation->validationErrors;                
                $result = false;
            }
        }
        
        $result = empty($this->{$this->modelClass}->AdLocation->validationErrors);
                
        if ($result)
        {
            $db->commit();
        }
        else
        {
            $db->rollback();
        }
        
        return $result;
    } 
    
    private function _saveTargetAutomobile($campaign_id)
    {
        $q = "DELETE FROM `campaign_automobiles` where campaign_id=". $campaign_id;
        $this->{$this->modelClass}->query($q);
        
        
        if ($this->request->data["Campaign"]["automobile_ids"])
        {
            $automobile_ids = explode(",", $this->request->data["Campaign"]["automobile_ids"]);
        }
        
        $result = true;
        if (isset($automobile_ids))
        {
            $records = array();
            foreach ($automobile_ids as $automobile_id)
            {
                $records[] = array(
                    "campaign_id" => $campaign_id, "automobile_id" => $automobile_id
                );
            }

            $result = $this->{$this->modelClass}->CampaignAutomobile->saveMany($records);
        }
        
        return $result;
    }
    
    private function _saveBudget($campaign_id)
    {
        
        $db = $this->{$this->modelClass}->getdatasource();
        
        $db->begin();
        
        $result = true;
        foreach ($this->request->data["AdLocation"] as $ad_location_id => $data)
        {
            $this->request->data['AdLocation'][$ad_location_id]["target_imperssions"] = floor($data["actual_budget"] / $data["price_per_impression"]);
            $data = $this->request->data['AdLocation'][$ad_location_id];
            
            $this->{$this->modelClass}->AdLocation->id = $ad_location_id;
            if (!$this->{$this->modelClass}->AdLocation->save($data))
            {
                $result = false;
            }
        }
        
        if ($result)
        {
            $db->commit();
        }
        else
        {
            $db->rollback();
        }
        
        return $result;
        
    }
    
    private function _getFilterAutomobileList($data, $list)
    {
        $ret = array();
        
        if (!$list)
        {
            return $ret;
        }
        
        foreach ($data as $key => $inner_data)
        {
            $manufacture_id = $inner_data['Automobile']['id'];
            if (isset($list[$inner_data['Automobile']['id']]))
            {
                $ret[$manufacture_id] = $inner_data['Automobile'];
            }
            
            if ($inner_data['children'])
            {
                foreach ($inner_data['children'] as $k2 => $arr)
                {
                    $model_id = $arr['Automobile']['id'];
                    if (isset($list[$arr['Automobile']['id']]))
                    {
                        if (!isset($ret[$manufacture_id]))
                        {
                            $ret[$manufacture_id] = $inner_data['Automobile'];
                        }
                        
                        $ret[$manufacture_id]['children'][$model_id] = $arr['Automobile'];
                    }
                    
                    if ($arr['children'])
                    {
                        foreach ($arr['children'] as $k3 => $arr2)
                        {
                            $varient_id = $arr['Automobile']['id'];
                            if (isset($list[$arr2['Automobile']['id']]))
                            {
                                if (!isset($ret[$manufacture_id]))
                                {
                                    $ret[$manufacture_id] = $inner_data['Automobile'];
                                }
                                
                                if (!isset($ret[$manufacture_id]['children'][$model_id]))
                                {
                                    $ret[$manufacture_id]['children'][$model_id] = $arr['Automobile'];
                                }
                                
                                $ret[$manufacture_id]['children'][$model_id]['children'][$varient_id] = $arr2['Automobile']['name'];                                
                            }
                        }
                    }
                }
            }
        }
        
        return $ret;
    }
    
    private function _activate_history_campaign($history_campaign_id)
    {
        $this->loadModel("HistoryCampaign");
        
        $this->HistoryCampaign->Behaviors->detach("DateFormat");  
        
        $this->HistoryCampaign->contain(array(
            "HistoryAdLocation"
        ));
        
        $record = $this->HistoryCampaign->find("first", array(            
            "conditions" => array(
                "HistoryCampaign.id" => $history_campaign_id
            )
        ));
       
        if (!$record)
        {
            throw new NotFoundException(__('Invalid Request'));
        }
        
        $campaing_id = (int) $record['HistoryCampaign']['parent_id'];
                
        // updating campaign        
        // deattach all behaviours
        $this->Campaign->Behaviors->detach("DateFormat");  
        $this->Campaign->Behaviors->detach("MeioUpload");
        $this->Campaign->AdLocation->Behaviors->detach("DateFormat");
        $this->Campaign->AdLocation->Behaviors->detach("MeioUpload");
        
        
        $data["Campaign"] = $record["HistoryCampaign"];
        $data["Campaign"]["history_campaign_id"] = $history_campaign_id;        
        $data["Campaign"]["id"] = $campaing_id;
        $data["Campaign"]["is_trial"] = $data["Campaign"]["is_trial"] ? "1" : "0";
        
        unset($data['Campaign']["parent_id"]);
        unset($data['Campaign']["created_on"]);
        unset($data['Campaign']["created_by"]);
        unset($data['Campaign']["modified_on"]);
        unset($data['Campaign']["modified_by"]);
        
        $this->Campaign->id = $campaing_id;
        $result = $this->Campaign->save($data, false);
        
        $ad_location_list = $this->{$this->modelClass}->AdLocation->find("list", array(
            "fields" => array("id", "id"),
            "conditions" => array(
                "campaign_id" => $campaing_id
            ),
            "recursive" => -1
        ));
        
        foreach ($ad_location_list as $id)
        {
            $this->{$this->modelClass}->AdLocation->deleteSoft($id);
        }
        
        //updating adLocations        
        foreach ($record["HistoryAdLocation"] as $arr)
        {
            $data["AdLocation"] = $arr;
            
            unset($data['AdLocation']["parent_id"]);
            unset($data['AdLocation']["created_on"]);
            unset($data['AdLocation']["created_by"]);
            unset($data['AdLocation']["modified_on"]);
            unset($data['AdLocation']["modified_by"]);
            $this->{$this->modelClass}->AdLocation->id = $arr['parent_id'];
            $this->{$this->modelClass}->AdLocation->save($data, false);
        }
        
        //save data in ad_location_schdules, campaign_automobiles
        $this->{$this->modelClass}->query("CALL sp_campaign_version_activate($campaing_id,$history_campaign_id);");
        
        //creating logs
        $data["CampaignVersionLog"] = array(
            "campaign_id" => $campaing_id,
            "history_campaign_id" => $history_campaign_id
        );
        
        $this->{$this->modelClass}->CampaignVersionLog->save($data);
        
        return $campaing_id;
    }
    
    private function _generateInvoice()
    {
        if ($this->request->data["Campaign"]["duration_type_id"] == CAMPAIGN_DURATION_FIXED_ID)
        {
            $start_date = DateUtility::getDateObj($this->request->data["Campaign"]['start_date']);
            $end_date = DateUtility::getDateObj($this->request->data["Campaign"]['end_date']);

            $days = DateUtility::compareDateObjs($start_date, $end_date, DateUtility::$DAYS);

            if ($days <= 31)
            {
                $invoice_count = $this->{$this->modelClass}->Invoice->find("count", array(
                    "conditions" => array("campaign_id" => $this->request->data["Campaign"]["id"]),
                    "recursive" => -1
                ));
                
                if ($invoice_count == 0)
                {
                    $amount = $this->request->data["Campaign"]['budget'] * $this->adminSetting["campaign_initial_payment"] / 100;

                    $invoice_data = array(
                        "user_id" => $this->request->data["Campaign"]['user_id'],
                        "campaign_id" => $this->request->data["Campaign"]['id'],
                        "start_date" => DateUtility::getFormatDateFromObj($start_date, DEFAULT_SQL_DATETIME_FORMAT),
                        "end_date" => DateUtility::getFormatDateFromObj($start_date, DEFAULT_SQL_DATETIME_FORMAT),
                        "amount" => $amount,
                    );

                    $this->loadModel("Invoice");

                    if ($this->Invoice->save($invoice_data))
                    {
                       $this->_invoiceSaveCallBack($this->Invoice->id, StaticArray::$invoice_status_unpaid);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }  
    
    private function _sendCampaignAdvertiserConfirmationEmail($campaign_id)
    {
        $this->{$this->modelClass}->contain(array(
            "User" => array(
                "fields" => array("id", "name", "subname", "username")
            )
        ));
        
        $record = $this->{$this->modelClass}->find("first", array(
            "fields" => array("name"),
            "conditions" => array("Campaign.id" => $campaign_id),
        ));
        
        $confirmationCode = get_random_string("ABCDEFGHIJKLM123456789", 6) . $campaign_id;
         
        $this->{$this->modelClass}->id = $campaign_id;        
        $this->{$this->modelClass}->saveField('approval_confirm_code', $confirmationCode, false);
        
        // send email to user
        $to_email = $record['User']['username'];
        $from_email = FROM_EMAIL;
        $subject = "Campaign Waiting for Approval";  // A string value defining the title of Email .

        $footer_content = $this->_getEmailFooterContent();

        $viewData["record"] = $record;
        $viewData["record"]['confirmationCode'] = $confirmationCode;
        $viewData["content"]['footer'] = $footer_content;
        
        $this->sendEmail($to_email, $from_email, $subject, "Campaign/campaign_advertiser_approval_email", $viewData, $files = array(), $bcc = array());
    }
    
    
    private function _getAdPlayedOnBasisOfAutomobiles($campaign_id)
    {
        $unique_varient_count = 0;
        
        $data = $this->{$this->modelClass}->query("CALL sp_get_campaign_efficiency_details($campaign_id, NULL)");
        
        $records = array();
        $cols = array();
        
        foreach ($data as $arr)
        {
            $location_name = trim($arr["L"]["name"]);
            $cols[$location_name] = $location_name;
            if ($arr['Variant']["name"] && $arr['Model']["name"] && $arr['Manufacture']["name"])
            {
                $unique_varient_count++;
                $key =  trim($arr['Manufacture']["name"]) . "-" . trim($arr['Model']["name"]) . "-" . trim($arr['Variant']["name"]);                
                $records[$key][$location_name] = $arr[0]['variant_count'];
            }            
        }
        
        ksort($cols);
        
        foreach ($records as $k => $arr)
        {
            foreach ($cols as $col)
            {
                if (!isset($arr[$col]))
                {
                    $records[$k][$col] = 0; 
                }
            }
            
           ksort($records[$k]);
           
           array_unshift($records[$k], $k);           
        }
        
        array_unshift($cols, "");
        
        return array(
            "data" => $records,
            "header" => $cols,
            "variant_count" => $unique_varient_count
        );
    }
    
    private function _getAdPlayedOnBasisOfCreative($record)
    {
        $data = $cols = array();
        foreach ($record["AdLocation"] as $arr)
        {
            $location_name = trim($arr["Location"]["name"]);
            $cols[$location_name] = $location_name;
            
            if (!isset($data["Default Video"][$location_name]))
            {
                $data["Default Video"][$location_name] = 0;
            }
            
            if (!isset($data["Location Specfic Video"][$location_name]))
            {
                $data["Location Specfic Video"][$location_name] = 0;
            }
            
            if ($arr["use_default_creative"])
            {
                $data["Default Video"][$location_name] = $arr["reached_imperssions"];
            }
            else
            {
                $data["Location Specfic Video"][$location_name] = $arr["reached_imperssions"];
            }
        }
        
        ksort($cols);
        
        foreach ($data as $k => $arr)
        {
            foreach ($cols as $col)
            {
                if (!isset($arr[$col]))
                {
                    $data[$k][$col] = 0; 
                }
            }
            
            ksort($data[$k]);
           
            array_unshift($data[$k], $k);
        }
        
        array_unshift($cols, "");
        
        return array(
            "data" => $data,
            "header" => $cols
        );

    }
    
    public function admin_ajaxUploadVideo()
    {
        $file = new FileUtility(10, array("mp4"), array(
            "prefix" => $this->request->data["model"] . "_",
            "new_filename" => get_random_string("123456789", 9)
            )
        );
            
        $result = $file->uploadFile($_FILES["ad_name"], AD_CREATIVE, false);
        
        $response = array(
            "status" => 0,
            "msg" => "Failed to save"
        );
        
        if ($result)
        {
             $response = array(
                "status" => 1,
                "msg" => "Successfully Uploaded",
                "file" => $file->full_filename
            );
             
            $getID = new getID3();

            $file_info = $getID->analyze($file->file_path . $file->full_filename);

            if (isset($file_info['playtime_seconds']))
            {
                if ($file_info['playtime_seconds'] > AD_LOCAL_DURATION_LIMIT)
                {
                    $response = array(
                        "status" => 0,
                        "msg" => AD_LOCAL_VIDEO_DURATION_VALIDATION_MSG,
                    );
                    
                    @unlink($filename);
                    $result = false;
                }
            }
            else if (isset($file_info["error"]))
            {
                $response = array(
                    "status" => 0,
                    "msg" => $file_info["error"][0],
                    "seconds" => $file_info['playtime_seconds']
                );
                
                $result = false;
                @unlink($filename);
            }
        }
        
        echo json_encode($response);
        exit;
    }
    
    public function admin_ajaxDeleteVideo()
    {
        $result = true;
        if ($this->request->data["id"])
        {
            $data[$this->modelClass]["ad_name"] = "";
            $data[$this->modelClass]["ad_url"] = "";
            $data[$this->modelClass]["ad_type"] = 0;
            $data[$this->modelClass]["ad_size"] = 0;
            $data[$this->modelClass]["ad_mime"] = "";
            $data[$this->modelClass]["ad_ext"] = "";
            
            
            $this->{$this->modelClass}->id = $this->request->data["id"];
            $result = $this->{$this->modelClass}->save($data , false);
        }
        
        if ($result)
        {
            $result = FileUtility::deleteFile($this->request->data["file"]); 
        }
        
        echo $result; exit;
    }
    
    /**
     * Campaign Submission Message page 
     * @param int $id
     */
    public function admin_campaign_submit($id)
    {
        $this->set(compact("id"));
    }
}
