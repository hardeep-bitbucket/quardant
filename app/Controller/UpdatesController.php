<?php
/**
 * Updates Controller
 * 
 * 
 * @created    17/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */

class UpdatesController extends AppController
{
    public $layout = "admin_inner";
    /*
     * @Before Filter
     */
    public function beforeFilter() 
    {
        parent::beforeFilter();
    }
    
    /*
     * @Admin Summary
     */
    public function admin_index()
    {
        $this->Redirect->urlToNamed();
        $conditions = $this->getSearchConditions(array(
                        array('model' => 'Update', 'field' => 'title', 'type' => 'string', 'view_field' => 'title')                        
                )
        );
        $records = $this->paginate('Update', $conditions);        
        
        $this->set('title_for_layout', 'Update Summary');
        $this->set(compact('records'));
    }
    
    /*
     * @Add Screen
     */
    public function admin_add() 
    {
        parent::add();
        $heading='Add Update';
        $reset_action = 'admin_index';
        $this->set('title_for_layout', 'Add Update');
        
        $this->set(compact('heading','reset_action'));			
        $this->render('admin_form');	
    }
    
    /*
     * @Edit Screen
     */
    public function admin_edit($id = null) 
    {
        parent::edit($id);
        $heading='Edit Update';
        $reset_action = 'admin_index';		
        $this->set('title_for_layout', 'Edit Update');	

        $this->set(compact('heading','reset_action'));
        $this->render('admin_form');
    }
}
