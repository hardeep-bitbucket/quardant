<script type="text/javascript">
    $(document).ready(function() {
        var targetItem = $("ul.main-nav li:nth-child(<?php echo $mainLinkNumber; ?>)");
        $(targetItem).addClass("active");
        $(targetItem).children("ul").removeClass("closed");
        $(targetItem).find("ul li:nth-child(<?php echo $subLinkNumber; ?>) a").addClass("leftbar-active-item");
    });
</script>
<aside class="body_lt">
    <div class="navi">
        <ul class='main-nav'>
            <li> 
                <a href="#" class='light toggle-collapsed'>
                    <div class="ico"><i class="icon-tasks icon-white"></i></div>
                    Page <img src="<?php echo $this->base; ?>/img/toggle-subnav-down.png" alt=""> 
                </a>
                <ul class='collapsed-nav closed'>
                    <li> <?php echo $this->Html->link('Summary', array('controller' => 'pages', 'action' => 'admin_index')) ?> </li>
                    <li> <?php echo $this->Html->link('Add Page', array('controller' => 'pages', 'action' => 'admin_add')) ?> </li>
                </ul>
            </li>
            
            <li> 
                <a href="#" class='light toggle-collapsed'>
                    <div class="ico"><i class="icon-tasks icon-white"></i></div>
                    Module <img src="<?php echo $this->base; ?>/img/toggle-subnav-down.png" alt=""> 
                </a>
                <ul class='collapsed-nav closed'>
                    <li> <?php echo $this->Html->link('Summary', array('controller' => 'modules', 'action' => 'admin_index')) ?> </li>
                    <li> <?php echo $this->Html->link('Add Module', array('controller' => 'modules', 'action' => 'admin_add')) ?> </li>
                </ul>
            </li>
            
             <li> 
                <a href="#" class='light toggle-collapsed'>
                    <div class="ico"><i class="icon-tasks icon-white"></i></div>
                    Graphic <img src="<?php echo $this->base; ?>/img/toggle-subnav-down.png" alt=""> 
                </a>
                <ul class='collapsed-nav closed'>
                    <li> <?php echo $this->Html->link('Summary', array('controller' => 'graphics', 'action' => 'admin_index')) ?> </li>
                    <li> <?php echo $this->Html->link('Add Graphic', array('controller' => 'graphics', 'action' => 'admin_add')) ?> </li>
                </ul>
            </li>
            
            <li> 
                <a href="#" class='light toggle-collapsed'>
                    <div class="ico"><i class="icon-tasks icon-white"></i></div>
                    Testimonial <img src="<?php echo $this->base; ?>/img/toggle-subnav-down.png" alt=""> 
                </a>
                <ul class='collapsed-nav closed'>
                    <li> <?php echo $this->Html->link('Summary', array('controller' => 'testimonials', 'action' => 'admin_index')) ?> </li>
                    <li> <?php echo $this->Html->link('Add Testimonial', array('controller' => 'testimonials', 'action' => 'admin_add')) ?> </li>
                </ul>
            </li>
           
            <li> 
                <a href="#" class='light toggle-collapsed'>
                    <div class="ico"><i class="icon-tasks icon-white"></i></div>
                    Subscriber <img src="<?php echo $this->base; ?>/img/toggle-subnav-down.png" alt=""> 
                </a>
                <ul class='collapsed-nav closed'>
                    <li> <?php echo $this->Html->link('Summary', array('controller' => 'subscribers', 'action' => 'admin_index')) ?> </li>
                    <li> <?php echo $this->Html->link('Export', array('controller' => 'subscribers', 'action' => 'admin_export')) ?> </li>
                </ul>
            </li>
            
            <li> 
                <a href="#" class='light toggle-collapsed'>
                    <div class="ico"><i class="icon-tasks icon-white"></i></div>
                    Enquiry <img src="<?php echo $this->base; ?>/img/toggle-subnav-down.png" alt=""> 
                </a>
                <ul class='collapsed-nav closed'>
                    <li> <?php echo $this->Html->link('Summary', array('controller' => 'enquiries', 'action' => 'admin_index')) ?> </li>
                    <li> <?php echo $this->Html->link('Export', array('controller' => 'enquiries', 'action' => 'admin_export')) ?> </li>
                </ul>
            </li>
        </ul>
    </div>
</aside>