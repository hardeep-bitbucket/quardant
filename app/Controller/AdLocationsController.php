<?php

/**
 * AdLocation Controller
 * 
 * 
 * @created    27/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class AdLocationsController extends AppController 
{
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow("ajaxSaveTempImage", "admin_ajaxDeleteVideo");
    }
    
    public function admin_delete($id) 
    {
        if (!$id) 
        {
            throw new NotFoundException(__('Invalid Request'));
        }
        
        if ($this->{$this->modelClass}->deleteSoft($id)) 
        {
            $this->Session->setFlash('The record deleted Successfully!', "flash_success");                                    
        } 
        else 
        {
            $this->Session->setFlash('The record cannot be deleted!', "flash_failure");            
        }
        $this->redirect($this->referer());
    }
    
    
    /***--------------- ajax functions----------------**********/
    
    
    public function ajaxSaveTempImage()
    {   
        $data = urldecode($this->data);
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        file_put_contents('files/chart-image.png', $data);
        exit;
    }
    
    public function admin_ajaxDeleteVideo()
    {
        $result = true;
        if ($this->request->data["id"])
        {
            $data[$this->modelClass]["ad_name"] = "";
            $data[$this->modelClass]["ad_url"] = "";
            $data[$this->modelClass]["ad_type"] = 0;
            $data[$this->modelClass]["ad_size"] = 0;
            $data[$this->modelClass]["ad_mime"] = "";
            $data[$this->modelClass]["ad_ext"] = "";
            
            
            
            $this->{$this->modelClass}->id = $this->request->data["id"];
            $result = $this->{$this->modelClass}->save($data , false);
        }
        
        if ($result)
        {
            $result = FileUtility::deleteFile($this->request->data["file"]); 
        }
        
        echo $result; exit;
    }
   
}
