<?php
/**
 * Web Service Logs Controller
 * 
 * 
 * @created    06/02/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
class CampaignVersionLogsController extends AppController 
{
    
    public $paginate = array(
        'limit' => 10,
        'order' => array(
            'id' => 'ASC'
        )
    );
    /*
     * @Summary Screen
     */    
    public function admin_index()
    {
        $title_for_layout = 'Campaign Version Logs Summary';        
        
        $this->Redirect->urlToNamed();
        
        $conditions = $this->getSearchConditions(array(
                array('model' => 'User', 'field' => 'name', 'type' => 'string', 'view_field' => 'name'),
                array('model' => 'Campaign', 'field' => 'id', 'type' => 'integer', 'view_field' => 'id'),                
                array('model' => $this->modelClass, 'field' => 'created_on', 'type' => 'from_date', 'view_field' => 'from_date'),
                array('model' => $this->modelClass, 'field' => 'created_on', 'type' => 'to_date', 'view_field' => 'to_date'),
           )
        );
        
        $this->{$this->modelClass}->contain(array(
            "User" => array("fields" => array("id", "name", "subname")),
            "Campaign" => array("fields" => array("id", "name")),
            "HistoryCampaign" => array("fields" => array("id", "name"))
        ));
        
        $country_list = $this->{$this->modelClass}->Campaign->find("list", array(
            "fields" => array("id", "name"),
            "recursive" => -1
        ));
        
        $records = $this->paginate($this->modelClass, $conditions);        
        
        $this->set(compact('records', 'title_for_layout', 'country_list'));
    }
}