<?php
/**
 * home Advertiser
 * 
 * 
 * @created    03/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("admin/form_header");
?>

<div class="portlet light">   
    <div class="portlet-body">   
        <div class="row">
            <div class="col-md-6">                 
            </div>
            <div class="col-md-6" style="text-align: right;">
                <?php
                    echo $this->Html->link("<i class='fa fa-print'></i> Print",                
                        array('action' => 'index', "admin" => true), 
                        array('escape' => false, "class" => "btn btn-circle  btn-primary")
                    );
                ?>
            </div>
        </div>
        <div class="row margin-top-20">           
            <div class="col-md-12">
                <div class="portlet green-meadow box">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-desktop"></i> 
                                <?php 
                                    echo $record[$model]["type"] == StaticArray::$transaction_status_credit_transaction ? "Recharge" : "Refund";                                  
                                ?> 
                            Details
                        </div>    
                        <div class="tools">                           
                            <a href="javascript:;" class="collapse"></a>                             
                        </div>
                    </div>
                    <div class="portlet-body"> 
                        
                         <div class="row static-info">
                             <?php if ($auth_user["group_id"] != ADVERTISER_GROUP_ID): ?>
                                <div class="col-md-6">
                                   <div class="col-md-5 name">Advertiser</div>
                                   <div class="col-md-7 value"><?php echo $record["User"]["name"] . " " . $record["User"]["subname"]; ?></div>
                               </div>  
                             <?php endif; ?>
                            <div class="col-md-6">
                                <div class="col-md-5 name">Payment Mode</div>
                                <div class="col-md-7 value"><?php echo StaticArray::$payment_mode[$record['TransactionDetail']['payment_mode']]; ?></div>
                            </div>                            
                        </div>  
                        
                         <div class="row static-info">                             
                            <div class="col-md-6">
                                <div class="col-md-5 name">Type</div>
                                <div class="col-md-7 value"><?php echo StaticArray::$transaction_status_types[$record[$model]['type']]; ?></div>
                            </div>
                            <?php if ($record['TransactionDetail']['gateway_type']): ?>
                                <div class="col-md-6">
                                    <div class="col-md-5 name">Gateway</div>
                                    <div class="col-md-7 value"><?php echo StaticArray::$gateway[$record['TransactionDetail']['gateway_type']]; ?></div>
                                </div>
                            <?php endif; ?>
                         </div>  
                        
                         <?php if ($record['TransactionDetail']['gateway_type']) : ?>
                            <div class="row static-info">
                               <div class="col-md-6">
                                   <div class="col-md-5 name">                                    
                                       <?php 
                                           echo $record['TransactionDetail']['gateway_type'] == StaticArray::$gateway_papal ? "PayPal ID" : "";
                                       ?>
                                   </div>
                                   <div class="col-md-7 value"><?php echo $record['TransactionDetail']['user_account_id']; ?></div>
                               </div>                           
                           </div>  
                        <?php endif; ?>
                                                
                        <div class="row static-info">
                            <div class="col-md-6">
                                <?php if ($record[$model]['type'] == StaticArray::$transaction_status_credit_transaction): ?>
                                    <div class="col-md-5 name">Recharge Amount</div>
                                    <div class="col-md-7 value"><?php echo CURRENCY_SYMBOL; ?><?php echo $record[$model]['credit']; ?></div>
                                <?php endif; ?>
                                <?php if ($record[$model]['type'] == StaticArray::$transaction_status_debit_transaction): ?>
                                    <div class="col-md-5 name">Refund Amount</div>
                                    <div class="col-md-7 value"><?php echo CURRENCY_SYMBOL; ?><?php echo $record[$model]['debit']; ?></div>
                                <?php endif; ?>                                
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-5 name">Payment Date</div>
                                <div class="col-md-7 value"><?php echo $record[$model]['created_on']; ?></div>
                            </div> 
                        </div>   
                        
                        <div class="row static-info">
                            <div class="col-md-6">
                                <div class="col-md-5 name">Description</div>
                                <div class="col-md-7 value"><?php echo $record["TransactionDetail"]["description"]; ?></div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>
