<?php
/**
 *LocationWebServiceAlerts Controller
 * 
 * 
 * @created    27/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class LocationWebServiceAlertsController extends AppController 
{
    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow("ajaxGetAlerts");
    }
    
    public function admin_index()
    {
        $this->Redirect->urlToNamed();        
        $title_for_layout = 'Location Alert Summary';  
                
        $conditions = $this->getSearchConditions(array(
                array('model' => 'Location', 'field' => 'name', 'type' => 'string', 'view_field' => 'name'),
                array('model' => $this->modelClass, 'field' => 'created_on', 'type' => 'from_date', 'view_field' => 'from_date'),
                array('model' => $this->modelClass, 'field' => 'created_on', 'type' => 'to_date', 'view_field' => 'to_date'),
                array('model' => $this->modelClass, 'field' => 'is_resolved', 'type' => 'integer', 'view_field' => 'is_resolved'),
            )
        );
        
        if (isset($conditions['LocationWebServiceAlert.is_resolved']) && $conditions['LocationWebServiceAlert.is_resolved'] < 0)
        {
            $conditions['LocationWebServiceAlert.is_resolved'] = 0;
        }
        
        $this->{$this->modelClass}->contain(array(
            "Location" => array(
                "fields" => array("id", "name")
            )
        ));
        
        $records = $this->paginate($this->modelClass, $conditions);
        //debug($records); exit;
        
        $this->set(compact('records', 'title_for_layout'));
    }
    
    public function ajaxGetAlerts($id = 0, $partner_id = 0)
    {   
        $this->layout = "ajax";
        $conditions = array(                
            'is_resolved' => 0
        );        
        
        if ($id)
        {
            $conditions['Location.id < '] = $id;
        }       
        
        if ($partner_id)
        {
            $conditions["Location.partner_id"] = $partner_id;
        }
        
        $records = $this->{$this->modelClass}->find("all", array(
            "conditions" => $conditions,
            "limit" => 6,
            "order" => array(
                "LocationWebServiceAlert.id" => "DESC"
            ),
            "recursive" => 0
        ));
        
        $this->set(compact("records"));
    }
    
    /**
     * Toggles the status of is_resolved field
     * 
     * @param Integer $id
     * @param Integer $status
     */
    public function admin_toggleStatus($id, $status) 
    {
        $flag = $this->{$this->modelClass}->preventDeleteAndInactive($id);
        $result = false;
        if (is_numeric($id) && $flag) {
            $this->{$this->modelClass}->id = $id;
            $result = $this->{$this->modelClass}->saveField('is_resolved', !(int) $status, FALSE);
        }
        else {
            $this->Session->setFlash('The record cannot be inactive!');
        }
        
        echo (int) $result; exit;
    }
}