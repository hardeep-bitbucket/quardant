<?php
/**
 * Form  users
 * 
 * 
 * @created    23/02/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h3 class="page-title">
              <?php echo $title_for_layout; ?>
            </h3>
        </div>
    </div>
</div>

<?php
    echo $this->Session->flash();
?>


<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">User</div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
                echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal'));
                echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
                if (isset($this->request->data["User"]['group_id']))
                {
                    $group_id = $this->request->data["User"]['group_id'];                    
                }
                
                echo $this->Form->input('group_id', array('value' => $group_id, 'type' => 'hidden'));                 
            ?>
            <div class="form-body">   
                
                <h3 class="form-section">User Info</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Email<span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                 echo $this->Form->input('username', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control'
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Password <span class="required">*</span></label>
                            <div class="col-md-9">
                                <?php
                                    $attr = $action == "admin_edit" || $action == "admin_edit_profile" ? "disabled" : "";
                                    
                                    echo $this->Form->input('password', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control', $attr
                                    ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <h3 class="form-section">Personal Details</h3>                    
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Name <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                 echo $this->Form->input('name', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control'
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Sub Name <span class="required">*</span></label>
                            <div class="col-md-9">
                                <?php
                                echo $this->Form->input('subname', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Landline No. <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                 echo $this->Form->input('landline_no', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control'
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Mobile <span class="required">*</span></label>
                            <div class="col-md-9">
                                <?php
                                echo $this->Form->input('mobile', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Address <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                 echo $this->Form->input('address', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'rows' => '3', 'cols' => 3,
                                    'class' => 'form-control'
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                </div>
                                
                <h3 class="form-section">Other Details</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Reference Source</label>
                            <div class="col-md-8">
                                <?php
                                    echo $this->Form->input('reference_source', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'cols' => 3, "rows" => 3,
                                        'class' => 'form-control'
                                    ));
                                ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Approve</label>
                            <div class="col-md-1 form-checkbox">
                               <?php 
                                    $attr = "";
                                    if (isset($this->request->data['User']['is_approve']) && $this->request->data['User']['is_approve'])
                                    {
                                        $attr = "disabled";
                                    }                                    
                                    echo $this->Form->input('is_approve', array(
                                    'type' => 'checkbox', 'label' => false, 'div' => false, 'escape' => false,                                                                        
                                    'class' => "", $attr)); 
                                ?>
                            </div>                                        
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Active</label>
                            <div class="col-md-1 form-checkbox">
                               <?php 
                                    echo $this->Form->input('is_active', array(
                                    'type' => 'checkbox', 'label' => false, 'div' => false, 'escape' => false,                                                                        
                                    'class' => "form-control form-checkbox")); 
                                ?>
                            </div>                                        
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>
