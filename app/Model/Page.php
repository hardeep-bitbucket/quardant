<?php

/**
 * @package    Page
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */
class Page extends AppModel 
{
    public $sanitize = false;
    
    /**
     * associations belogs to and has many
     * @var type array
     */
    var $belongsTo = array(
        'ParentPage' => array(
            'className' => 'Page',
            'foreignKey' => 'page_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
        'ChildPage' => array(
            'className' => 'Page',
            'foreignKey' => 'page_id',
            'dependent' => false,
            'conditions' => '',
        )
    );

    /*
     * Behaviours 
     */
    var $actsAs = array(
        'MeioUpload' => array(
            'header_image' => array(
                'dir' => PAGE_IMAGES,
                'create_directory' => false,
                'allowed_mime' => array('image/jpeg', 'image/pjpeg', 'image/png'),
                'allowed_ext' => array('.jpg', '.jpeg', '.png'),
                'thumbnailDir' => false,
                'validations' => false
            )
        ),
        'Sluggable' => array(
            'label' => 'name',
            'slug' => 'slug',
            'separator' => '-',
            'overwrite' => false
        ),
        'Tree' => array(
            'parent' => 'page_id'
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => NAME_REQUIRED_MESSAGE)
        ),
    );

}
