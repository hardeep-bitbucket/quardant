<?php
/**
 * @package    Module
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */

class Module extends AppModel
{
    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
            'title' => array(
                    'notEmptyRule' => array('rule' => 'notEmpty','message' => TITLE_REQUIRED_MESSAGE)
            )
    );
}
