<?php 
    /**
     * Change Password Area
     * 
     * 
     * @created    15/12/2014
     * @package    TFQ
     * @copyright  Copyright 2014
     * @license    Proprietary
     * @author     Sonia
     */

?>
<?php $this->set("title_for_layout","Change password"); ?>
<?php echo $this->Html->script(array('validations'));?>

<aside class="body_rt"> 

  <!--Page head start-->
  <div class="page_head">
	<h1>Change Password</h1>
	<div class="back">
    	<?php echo $this->Html->link($this->Html->image('back.png'),array('controller' => 'dashboards', 'action'=>'admin_index'),array('escape' => false, 'title' => BACK_TITLE)); ?>
    </div>
  </div>
  <!--Page head end--> 
  
   <!--Message head start-->
  <?php if(($this->Session->check('Message.flash'))){ ?> 
  <section class="bolg_warp custom_message">
  	<div class="sub_head">Message</div>
    <div class="flashMessage">
       <?php echo $this->Session->flash(); ?>
    </div>
  </section> 
  <?php } ?> 
  <!--Message head end-->
  
  <!--Blog warp start-->
  <section class="bolg_warp">
	<div class="sub_head"><?php echo $heading;?></div>
	<div class="three_column">
	<?php echo $this->form->create('User', array('action'=>'change_password' ,'onsubmit'=>'return validate();'));?>
	  <h1>Password Details</h1>
	  <ul>
		<li>
		  <h2>Old Password <span>*</span> :</h2>
		  <h3>
          	<?php echo $this->Form->hidden('User.username' , array('div'=>false, 'label'=>false ,'class'=>'notEmpty','value'=>$this->Session->read('Auth.User.username'))); ?>
         	<?php echo $this->Form->input('User.old_password' , array('type'=>'password','div'=>false, 'label'=>false ,'class'=>'notEmpty text_field'))?>
		  </h3>
		</li>
        <li>
		  <h2>New Password <span>*</span>:</h2>
		  <h3>
            <?php echo $this->Form->input('User.new_password' , array('type'=>'password','div'=>false, 'label'=>false ,'class'=>'notEmpty text_field')) ?>
		  </h3>
		</li>
        <li>
		  <h2>Confirm Password <span>*</span> :</h2>
		  <h3>
			<?php echo $this->Form->input('User.confirm_password' , array('type'=>'password','div'=>false, 'label'=>false ,'class'=>'notEmpty text_field')); ?>
		  </h3>
		</li>
	  </ul>
	</div>
	<div class="centerbutton">
	  <ul>
		<li>
          <?php echo $this->Form->submit('save.png'); ?>
		</li>
		<li>
          <?php echo $this->Html->link($this->Html->image('cancle.png'), 
												array('controller'=>'companies','action'=>'admin_index'),array('escape'=>false)); ?>
		</li>
	  </ul>
	</div>
    <?php echo $this->Form->end();?>
  </section>
  <!--Blog warp end--> 
  
</aside>