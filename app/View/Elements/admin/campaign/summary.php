<?php
/**
 * Campaign Controller
 * 
 * summary tab
 * 
 * @created    08/03/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal', 'id' => 'summary-form'));
echo $this->Form->hidden('id', array('label' => false)); 
echo $this->Form->hidden('user_id', array('label' => false)); 
echo $this->Form->hidden('tab_name', array('label' => false)); 
echo $this->Form->hidden('budget', array('label' => false));
echo $this->Form->hidden('start_date', array('label' => false));
echo $this->Form->hidden('end_date', array('label' => false));
echo $this->Form->hidden('budget', array('label' => false));
echo $this->Form->hidden('duration_type_id', array('label' => false));
echo $this->Form->hidden('CampaignStatus.id', array('label' => false));

$campaign_setup_cost = 0;
  foreach ($ad_location_records as $record) 
  {  
        $campaign_setup_cost += $record["Location"]["setup_cost"];
  }
  
?>
<style>
    .amount-box .name { text-align: right;}
    .amount-box .value {text-align: right;}
</style>
<div class="row">
    <div class="col-md-6"> 
        <?php if ($records['CampaignStatus']['id'] == CAMPAIGN_ACTIVE_ID) : ?>
        
        <?php endif;?>
    </div>
    <div class="col-md-6" style="text-align: right;">
        <b class="font-size-14">
            <?php
                echo $this->Html->link("<i class='fa fa-bullseye'></i> Detail View",                
                    array('action' => 'campaign_detail', "admin" => true, $records['Campaign']['id']), 
                    array('escape' => false, "class" => "btn btn-circle  blue")
                );
            ?>
            
            <?php
                if ($records['CampaignStatus']['id'] == CAMPAIGN_ACTIVE_ID) :
            ?>
                <button class="btn btn-circle red" id="btn-stop">
                    <i class='fa fa-times'></i> Stop Campaign
                </button>  
            <?php endif; ?>
            
            <?php 
            if (in_array($records['CampaignStatus']['id'], array(CAMPAIGN_ACTIVE_ID, CAMPAIGN_APRROVED_ID)) && count($history_campaign_records) < CAMPAIGN_VERSION_LIMIT):           
           
                    echo $this->Html->link(
                         "", 
                         array('action' => 'admin_add_campaign_version', $records[$model]['id']), 
                         array('title' => DELETE_TITLE, "class" => "confirm-action-link btn")
                     );
            ?>
                <button href="#" class = "confirm-action btn btn-circle purple" data-confirm_text = "This action will stop your campaign for some time. After approval it will be activate. Do you want to countinue ?"> <i class='fa fa-tasks'></i>  Change Campaign</button>                
                <span>Changes Left : <?php echo CAMPAIGN_VERSION_LIMIT - count($history_campaign_records);  ?> times</span>
            <?php  endif; ?>
            
        </b>    
    </div>
</div>
<div class="row margin-top-10">
    <div class="col-md-12">
        <div class="portlet green-meadow box">
            <div class="portlet-title">
                <div class="caption">Campaign </div>    
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>
            </div>
            <div class="portlet-body">
                <div class="row"> 
                    <div class="col-md-5">                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Name</div>
                            <div class="col-md-7 value"><?php echo $records['Campaign']['name']; ?></div>
                        </div>
                        <?php if ($auth_user['group_id'] != ADVERTISER_GROUP_ID): ?>
                            <div class="row static-info">                        
                                <div class="col-md-5 name">Advertiser</div>
                                <div class="col-md-7 value"><?php echo $records['User']['name'] . " " . $records['User']['subname']; ?></div>
                            </div>                    
                        <?php endif; ?>
                        <div class="row static-info">
                            <div class="col-md-5 name">Type</div>
                            <div class="col-md-7 value"><?php echo $records['CampaignType']['value']; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Duration</div>
                            <div class="col-md-7 value"><?php echo $records['CampaignDuration']['value']; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Status</div>
                            <div class="col-md-7 value">
                                <?php echo  $this->TSHtml->campaignStatusLabel($records['CampaignStatus']['id'], $records['CampaignStatus']['value']); ?>                                
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Payment Status</div>
                            <div class="col-md-7 value">
                                <?php 
                                     if ($invoice['pending_amount'] > 0)
                                     {
                                         echo $this->TSHtml->transactionStatusLabel(StaticArray::$invoice_status_unpaid); 
                                     }
                                     else if ($invoice['paid_amount'] > 0)
                                     {
                                         echo $this->TSHtml->transactionStatusLabel(StaticArray::$invoice_status_paid); 
                                     }
                                 ?>
                            </div>
                        </div>
                      
                        
                        <?php if (!$records[$model]["is_advertiser_approved"] && $records['CampaignStatus']['id'] == CAMPAIGN_SUBMITTED_ID) : ?> 
                            <div class="row static-info">                                    
                                <div class="col-md-12 name"><span class="required"><b>Waiting for Advertiser Approval</b></span></div>                       
                            </div>
                            <?php if ($auth_user["group_id"] != ADVERTISER_GROUP_ID) : ?>                        
                                <div class="row static-info">                                    
                                    <div class="col-md-12 name">
                                        <?php 
                                             echo $this->Html->link("Send Approval Email",                
                                                array('action' => 'admin_campaign_advertiser_approval_email', "admin" => true, $records['Campaign']['id']), 
                                                array('escape' => false, "class" => "btn btn-circle blue")
                                            );
                                        ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php else : ?>
                        
                        <div class="row static-info"> 
                            <?php                         
                                switch($records['CampaignStatus']['id'])
                                {
                                    case CAMPAIGN_SUBMITTED_ID:
                                        if ($auth_user['group_id'] != ADVERTISER_GROUP_ID):
                                    ?>
                                        <div class="col-md-5 name">Approve / Reject / Created</div>
                                        <div class="col-md-7 value my-checkbox">
                                            <?php 

                                                $options = array(                                                
                                                    CAMPAIGN_APRROVED_ID => "Approve",
                                                    CAMPAIGN_REJECTED_ID => "Reject",
                                                    CAMPAIGN_CREATED_ID => "Created"
                                                );

                                                echo $this->Form->input("status_type_id", array(
                                                    "type" => "select", 'label' => false, "div" => false, "escape" => false,
                                                    "options" => $options, "empty" => "Please Select", "required"
                                                    // "class" => "form-control"
                                                ));
                                            ?>        
                                            <?php if ((CAMPAIGN_VERSION_LIMIT - (count($history_campaign_records) + 1)) > 0): ?>
                                                <div class="required">Note: After Approval/Reject you able to change only for <?php echo CAMPAIGN_VERSION_LIMIT - (count($history_campaign_records) + 1); ?> times</div>
                                            <?php else : ?>
                                                <div class="required">Note: After Approval/Reject you are not able to add more versions</div>
                                            <?php endif; ?>
                                        </div>

                                    <?php                                   
                                        endif;
                                    break;


                                    case CAMPAIGN_ACTIVE_ID:                                    
                                    ?>
                                        <div class="col-md-5 name">Stop</div>
                                        <div class="col-md-7 value my-checkbox">
                                            <?php 

                                                $options = array(
                                                    CAMPAIGN_STOPPED_ID => "Stop",                                                  
                                                );

                                                echo $this->Form->input("status_type_id", array(
                                                    "type" => "select", 'label' => false, "div" => false, "escape" => false,
                                                    "options" => $options,  "empty" => "Please Select", "required"
                                                    // "class" => "form-control"
                                                ));
                                            ?>
                                        </div>

                                    <?php 
                                    break;

                                    case CAMPAIGN_STOPPED_ID: case CAMPAIGN_COMPLETED_ID:                                       
                                    ?>
                                        <div class="col-md-5 name">Active</div>
                                        <div class="col-md-7 value my-checkbox">
                                            <?php 

                                                $options = array(
                                                    CAMPAIGN_ACTIVE_ID => "Active",                                                
                                                );

                                                echo $this->Form->input("status_type_id", array(
                                                    "type" => "select", 'label' => false, "div" => false, "escape" => false,
                                                    "options" => $options,  "empty" => "Please Select", "required"
                                                    // "class" => "form-control"
                                                ));
                                            ?>
                                        </div>
                                    <?php                                    
                                    break;

                                    case CAMPAIGN_REJECTED_ID:
                                    ?>
                                        <div class="col-md-5 name">Created</div>
                                        <div class="col-md-7 value my-checkbox">
                                            <?php 

                                                $options = array(
                                                    CAMPAIGN_CREATED_ID => "Created"                                                
                                                );

                                                echo $this->Form->input("status_type_id", array(
                                                    "type" => "select", 'label' => false, "div" => false, "escape" => false,
                                                    "options" => $options,  "empty" => "Please Select", "required"
                                                    // "class" => "form-control"
                                                ));
                                            ?>
                                        </div>

                                    <?php
                                    break;                            
                                }
                            ?>
                        </div>
                        <?php endif; ?>
                    </div>

                    <div class="col-md-2">
                    </div>
                    
                    <div class="col-md-5">       
                        <div class="row static-info">
                            <div class="col-md-5 name">Campaign No.</div>
                            <div class="col-md-7 value"><?php echo $records['Campaign']['campaign_no']; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Start Date</div>
                            <div class="col-md-7 value"><?php echo $records['Campaign']['start_date']; ?></div>
                        </div>
                        <?php if ($records['CampaignDuration']['id'] == CAMPAIGN_DURATION_FIXED_ID): ?>
                        <div class="row static-info">
                            <div class="col-md-5 name">End Date</div>
                            <div class="col-md-7 value"><?php echo $records['Campaign']['end_date']; ?></div>
                        </div>
                        <?php endif; ?>   
                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Default Creative</div>
                            <div class="col-md-7 value">
                                <?php 
                                    if ($this->data['Campaign']["ad_type"] == 2 && isset($this->data['Campaign']['ad_url']) && $this->data['Campaign']['ad_url']) // Web
                                    {
                                        echo $this->TSHtml->videoOptions($this->data['Campaign']['ad_url'], array(
                                            'url_type' => StaticArray::$AdType[$this->data['Campaign']['ad_type']]
                                        ));     
                                    }
                                    else if (isset($this->data['Campaign']['ad_name']) && $this->data['Campaign']['ad_name'])
                                    {
                                        echo $this->TSHtml->videoOptions(SITE_PATH . AD_CREATIVE . $this->data['Campaign']['ad_name'], array(
                                            'url_type' => StaticArray::$AdType[$this->data['Campaign']['ad_type']]
                                        )); 
                                    }
                                ?>
                            </div>
                        </div>
                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Advertiser Approve</div>
                            <div class="col-md-7 value"><?php echo $records['Campaign']['is_advertiser_approved'] ? "Approved" : "Pending"; ?></div>                        
                        </div>
                    </div>
                </div>                 
                
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue-madison">
            <div class="portlet-title">
                <div class="caption">Locations </div>    
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed  table-custom">
                    <thead>
                        <tr class="head  tr-border-blue-madison" >
                            <td width="5%"  class="td-center"><span class="bold">Sr.</span></td>
                            <td><span class="bold">Location</span></td>
                        <?php  if ($auth_user["group_id"] == ADMIN_GROUP_ID) : ?>
                            <td width="10%"  class="td-center"><span class="bold">Setup Cost(<?php echo CURRENCY_SYMBOL; ?>)</span></td>
                            <td width="5%" class="td-center"><span class="bold">Exclusive</span></td>
                        <?php endif; ?>
                            <td width="8%" class="td-center"><span class="bold">Start Date</span></td>
                            <td width="8%" class="td-center"><span class="bold">End Date</span></td>
                            <td width="10%" class="td-center"><span class="bold">Price Per Impression(<?php echo CURRENCY_SYMBOL; ?>)</span></td>
                            <td width="10%" class="td-center"><span class="bold">Budget</span></td>
                            <td width="8%" class="td-center"><span class="bold">Target Imperssions</span></td>
                            <td width="8%" class="td-center"><span class="bold">Reached Imperssions</span></td>
                            <td width="6%" class="td-center"><span class="bold">Creative</span></td>
                            <td width="6%" class="td-center"><span class="bold">Actions</span></td>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $i = 0 ; 
                                foreach ($ad_location_records as $record) 
                                {  
                                    $i++; 
                        ?>
                            <tr>                                
                                <td class="td-center"><?php echo $i; ?></td>  
                                <td><?php echo $record["Location"]["name"];  ?></td>
                            <?php  if ($auth_user["group_id"] == ADMIN_GROUP_ID) : ?>
                                <td  class="td-center"><?php echo $record["Location"]["setup_cost"];  ?></td>
                                <td class="td-center">
                                    <?php
                                        $img = $record['Location']['is_exclusive'] == 1 ? "admin/status_1.png" : "admin/status_0.png";
                                        echo $this->Html->image($img,array(
                                            "alt" => "Active",                                    
                                            "class" => "summary-action-icon",
                                            "escape" => false
                                        ));
                                    ?>
                                </td>  
                            <?php endif; ?>
                                <td class="td-center"><?php echo $record["AdLocation"]["from_date"];  ?></td>
                                <td class="td-center"><?php echo $record["AdLocation"]["to_date"];  ?></td>
                                <td class="td-center"><?php echo $record["Location"]["price_per_impression"];  ?></td>
                                <td class="td-center"><?php echo $record["AdLocation"]["actual_budget"];  ?></td>
                                <td class="td-center"><?php echo $record["AdLocation"]["target_imperssions"];  ?></td>
                                <td class="td-center"><?php echo $record["AdLocation"]["reached_imperssions"];  ?></td>
                                <td class="td-center">
                                    <?php
                                    
                                        if (isset($record["AdLocation"]['ad_url']) && $record["AdLocation"]['ad_url']) // Web
                                        {
                                            echo $this->TSHtml->videoOptions($record["AdLocation"]['ad_url'], array(
                                                'url_type' => StaticArray::$AdType[$record["AdLocation"]['ad_type']]
                                            ));     
                                        }
                                        else if (isset($record["AdLocation"]['ad_name']) && $record["AdLocation"]['ad_name']) // LOCAL
                                        {
                                            echo $this->TSHtml->videoOptions($record["AdLocation"]['ad_name'], array(
                                                'url_type' => StaticArray::$AdType[$record["AdLocation"]['ad_type']]
                                            )); 
                                        }                                        
                                    ?>
                                </td>
                                <td class="td-center">
                                    <?php
                                    if ($i > 1 && $can_save)
                                    {
                                       echo $this->Html->link(
                                                   "link", 
                                                   array("controller" => 'AdLocations', 'action' => 'admin_delete', $record['AdLocation']['id']), 
                                                   array('escape' => false, 'title' => DELETE_TITLE, "class" => "summary-action-delete-link")
                                               );

                                        echo $this->Html->image("admin/summary_delete.png" , array(
                                            "class" => "summary-action-icon summary-action-delete",
                                            "data-confirm_text" => "Are you sure to delete record ?"
                                        ));
                                    }
                                    ?>
                                </td>                                           
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>                
            </div>               

        </div>
    </div>
</div>


<?php if ($automobiles): ?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet purple-soft box">
            <div class="portlet-title">
                <div class="caption">Target Automobiles </div>    
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <ul class="manufacture">
                        <?php  foreach ($automobiles as $manufacture): ?>
                                <li>
                                    <span class="automobile-block-title"><?php echo $manufacture['name']; ?></span>
                                    <?php if (isset($manufacture['children']) && $manufacture['children']) : ?>
                                        <ul  class="model">
                                            <?php foreach ($manufacture['children'] as $model): ?>
                                            <li>
                                                <span><?php echo $model["name"]; ?></span>
                                               <?php if (isset($model['children']) && $model['children']) : ?>
                                                <ul class='varient'>
                                                    <?php foreach ($model['children'] as $variant): ?>
                                                    <li> <span><?php echo $variant; ?><span></li>
                                                    <?php  endforeach; ?>
                                                </ul>
                                                <?php endif; ?>
                                            </li>
                                            <?php endforeach; ?>    
                                        </ul>
                                    <?php endif; ?>
                                </li>

                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
        
    </div>
</div>
<?php endif;?>

<?php if ($history_campaign_records) : ?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue-madison">
            <div class="portlet-title">
                <div class="caption">Campaign Versions </div>    
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>
            </div>
            <div class="portlet-body">                
                <table class="table table-bordered table-striped table-condensed  table-custom">
                    <thead>                        
                        <tr class="head  tr-border-blue-madison" >
                            <td width="5%"  class="td-center"><span class="bold">Sr.</span></td>
                            <td width="15%"><span class="bold">Name</span></td>
                            <td width="15%"><span class="bold">Country</span></td>
                            <td width="15%"><span class="bold">Duration Type</span></td>
                            <td width="15%"><span class="bold">Automobile Type</span></td>
                            <td width="8%" class="td-center"><span class="bold">Start Date</span></td>
                            <td width="8%" class="td-center"><span class="bold">End Date</span></td>                            
                            <td width="10%" class="td-center"><span class="bold">Budget(<?php echo CURRENCY_SYMBOL; ?>)</span></td>                            
                            <td width="8%" class="td-center"><span class="bold">Actions</span></td>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $i = 0 ; 
                                foreach ($history_campaign_records as $record) 
                                {  
                                    $i++; 
                        ?>
                            <tr>                                
                                <td class="td-center"><?php echo $i; ?></td>
                                <td><?php echo $record["HistoryCampaign"]["name"];  ?></td>
                                <td><?php echo $record["Country"]["name"];  ?></td>
                                <td><?php echo $record["CampaignDuration"]["value"];  ?></td>
                                <td><?php echo $record["CampaignType"]["value"];  ?></td>
                                <td class="td-center"><?php echo $record["HistoryCampaign"]["start_date"];  ?></td>
                                <td class="td-center"><?php echo $record["HistoryCampaign"]["duration_type_id"] == CAMPAIGN_DURATION_FIXED_ID ? $record["HistoryCampaign"]["end_date"] : "";  ?></td>
                                <td class="td-center"><?php echo $record["HistoryCampaign"]["budget"];  ?></td>                                
                                <td class="td-center">
                                    <?php
                                        if ($records['Campaign']['history_campaign_id'] != $record["HistoryCampaign"]['id'])
                                        {
                                            if (in_array($records['CampaignStatus']['id'], array(CAMPAIGN_APRROVED_ID, CAMPAIGN_ACTIVE_ID, CAMPAIGN_REJECTED_ID, CAMPAIGN_STOPPED_ID)))
                                            {
                                                echo $this->Html->link(
                                                    "Activate", 
                                                    array('action' => 'admin_campaign_version_activate', $record['HistoryCampaign']['id']), 
                                                    array('escape' => false)
                                                );
                                            }
                                            else
                                            {
                                                echo "<a href='#' class='warning-not-activate-version'>Activate</a>"; 
                                            }
                                        }
                                        else {
                                            echo "Activated";
                                        }
                                    ?>
                                </td>                                           
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>  
                
            </div>               

        </div>
    </div>
</div>
<?php endif; ?>


<div class="row">    
    <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12 pull-right">
        <?php if ($records['CampaignStatus']['id'] == CAMPAIGN_CREATED_ID): ?>  
            <div class="row static-info">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right">
                    <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12 name" style="text-align: right;">
                        <?php if ($auth_user['group_id'] == ADVERTISER_GROUP_ID): ?>
                            I accept the <i><b><a href="#">Terms & Conditions </a></b></i>  <i class="required-lbl">*</i>
                        <?php else : ?>
                            Submit
                        <?php endif; ?>
                            
                        <?php if ($auth_user['group_id'] == ADVERTISER_GROUP_ID): ?>
                            <div class="margin-top-15">
                            <b><span class="required">Note: After Submit you able to change only for <?php echo CAMPAIGN_VERSION_LIMIT - (count($history_campaign_records) + 1); ?> times</span></b>
                            </div>
                        <?php endif;?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-10 col-xs-12  value">
                        <?php 
                            echo $this->Form->hidden("status_type_id", array(
                                'label' => false, "value" => CAMPAIGN_SUBMITTED_ID
                            ));
                            
                            echo $this->Form->input('status', array(
                                'label' => false, 'type' => 'checkbox', 
                                'class' => "form-control form-checkbox"
                            )); 
                            
                            if ($auth_user["group_id"] == ADVERTISER_GROUP_ID)
                            {
                                echo $this->Form->hidden("is_advertiser_approved", array(
                                    'label' => false, "value" => 1
                                ));
                            }
                        ?>                     
                    </div>  
                    
                </div>                
            </div>
        <?php elseif (!$this->request->data["Campaign"]["is_advertiser_approved"] && $auth_user['group_id'] == ADVERTISER_GROUP_ID): ?>
        
            <div class="row static-info">
                    <div class="col-md-6 pull-right">
                        <div class="col-md-8 name">Approve</div>
                        <div class="col-md-4 value">
                            <?php 
                                echo $this->Form->input('is_advertiser_approved', array(
                                    'label' => false, 'type' => 'checkbox', 
                                    'class' => "form-control form-checkbox"
                                )); 
                            ?>                     
                        </div>
                    </div>                
                </div>
        <?php endif; ?>
    </div>
</div>

<div class="row" style="margin-top:20px;">    
    <div style="width:300px; margin: auto; ">         
            <div>                 
                <div class="pull-right">
                    <div class="col-md-6">
                        <?php 
                        if ($can_save_summary)
                        {
                            echo $this->Form->button("<i class='fa fa-check'></i> Submit and Close", array("class" => "btn btn-circle green btn-save", "type" => "submit", 'label' => false, 'div' => false, 'escape' => false));                       
                        }
                        ?>
                    </div>                    
                </div>
                
            </div>
          <?php echo $this->Form->end(); ?>
    </div>                                
</div>

<script>
     
    $(document).ready(function()
    {
        $("#discount-coupon").trigger("blur");
    });
    
    $(".warning-not-activate-version").click(function()
    {
        warn("You can't Activate Version if your current Campaign is not Aprooved or Active", { type : "error" , desc : ""});
        return false;
    });
    
    $("#summary-form").submit(function(e, data)
    {
        if (typeof data == "undefined")
        {
            var v = $("#CampaignStatusTypeId").val();
            if (v && v == '<?php echo CAMPAIGN_STOPPED_ID; ?>')
            {
                confirm_box
                ({
                    title : "Do you want to stop Campaign",
                    onConfirmCallBack : function ()
                    {
                        $("#summary-form").trigger("submit", {countinue : 1});                        
                    }
                });
                
                return false;
            }   
        }
    });
    
    $("#btn-stop").click(function(e)
    {
        $("#CampaignStatusTypeId").val("<?php echo CAMPAIGN_STOPPED_ID ?>");        
    });
</script>

