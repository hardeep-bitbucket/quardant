<?php

/**
 * AdminSetting Model
 * 
 * 
 * @created    12/03/2015
 * @package    anpr
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
class AdminSetting extends AppModel {

    /**
     * Server Valiadtions
     */
    public $validate = array(
        'ws_url' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'ws_ftp_host' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'ws_ftp_username' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'ws_ftp_password' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'paypal_id' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'info_email' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'support_email' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'campaign_duration_fixed_min_days' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),                
        'campaign_budget_medium_from' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'campaign_budget_medium_to' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),        
        'campaign_duration_fixed_intial_payment' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'campaign_duration_ongoing_intial_payment' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'campaign_trial_max_days' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),        
        'campaign_initial_fixed_payment' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'campaign_initial_onging_payment' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
    );

}