<?php
/**
 * Graphics Controller
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */

class GraphicsController extends AppController
{
    public $layout = "admin_inner";
    
    /*
     * @Before Filter
     */
    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->getGraphicTypes();
    }
    
    /*
     * @Admin Summary
     */
    public function admin_index()
    {
        $this->Redirect->urlToNamed();
        $conditions = $this->getSearchConditions(array(
                        array('model' => 'Graphic', 'field' => 'type_id', 'type' => 'integer', 'view_field' => 'type_id' )               
                )
        );

        $this->{$this->modelClass}->contain(array(
            "Type" => array("fields" => array(
                   "id", "value" 
            ))
        ));
        
        $records = $this->paginate('Graphic', $conditions);                
        $this->set('title_for_layout', 'Graphic Summary');
        $this->set(compact('records'));
    }
    
    /*
     * @Add Screen
     */
    public function admin_add() 
    {
        parent::add();
        $heading='Add Graphic';
        $reset_action = 'admin_index';
        $this->set('title_for_layout', 'Add Graphic');
        $this->set(compact('heading','reset_action'));			
        $this->render('admin_form');	
    }
    
    /*
     * @Edit Screen
     */
    public function admin_edit($id = null) 
    {
        parent::edit($id);
        $heading='Edit Graphic';
        $reset_action = 'admin_index';		
        $this->set('title_for_layout', 'Edit Graphic');	
        $this->set(compact('heading','reset_action'));
        $this->render('admin_form');
    }
    
    /*
     * @Get Graphic Types
     */
    function getGraphicTypes() 
    {
        $this->loadModel("Type");
        $graphic_types = $this->Type->find("list", array("conditions" => array("Type.type" => GRAPHIC_TYPES),
                                                            "fields" => array("id", "value")));
        $this->set(compact("graphic_types"));
    }
}
