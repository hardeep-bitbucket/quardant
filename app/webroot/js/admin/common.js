// JavaScript Document
$(function() {
	//alert("runnings");
	
	//Search reset & form cancel button 
	$(".reset-button").click(function(){
		window.location = $(this).attr('url');
	});
	
	//Preview image click for image file field
	$(".preview_image").click(function(){
		
        $.fancybox.open({            
            href : $(this).attr('url'),            
            padding : 10
        });
	});
	
	//Delete image click for image file field
	$(".delete_image").click(function(){
		//alert($(this).attr('url'));
		$(this).next().val(1);
		$(this).prev().hide();
		$(this).hide();
		return false;
	});
	
	//Datepicker
	//$(".datepicker").datepicker( { dateFormat: 'dd-mm-yy' } );
	
	//Restrict view
	$(".restrict_view").click(function(){
		alert("This manager is not viewable");
		return false;
	});
	
	// Checking all checkboxes in summary screen
	$('.checkAll').click(function ()
	{
    	$(this).closest('table').find('input[checkAll=all]').prop('checked', this.checked);
	});
	
		
	// Hiding flash message div after 3 secs
	 setTimeout(function(){ 
	 $(".custom_message").hide(); 
	 }, 10000);
	
	// Employee name in User add & edit
	$( "#UserEmployeeName" ).autocomplete({
		source: "http://localhost/payroll/admin/users/complete",
		change: function( event, ui ) {
            $( "#UserEmployeeId" ).val(ui.item.id);
      }
	});
	
	// Employee name in User search
	$( "#UserSearchEmployeeName" ).autocomplete({
		source: "http://localhost/payroll/admin/users/complete",
		change: function( event, ui ) {
			 $( "#UserSearchEmployeeId" ).val(ui.item.id);
			 
      }
	});
	
	// Making weekly day off 2 visible
	$("#BranchNoOfWeeklyLeaves").change(function(){
		if($("#BranchNoOfWeeklyLeaves option:selected").text()=="2"){
			$("#weekly_off_day2").css("display","block"); 
			
		}
	});
	
	// Making fields visible according to employee visa status 
	$("#EmployeeVisaStatus").change(function(){
		if($("#EmployeeVisaStatus option:selected").text()=="Expat"){
			$("#emp_local_address").css("display","block");
			$("#emp_passport_no").css("display","block"); 
			$("#emp_hongkong_id").css("display","block");
			$("#emp_hongkong_id h2 span").css("display","none");   
			$("#EmployeeHongkongId").removeClass("notEmpty");
			
		}
		if($("#EmployeeVisaStatus option:selected").text()=="Local"){
			$("#EmployeeHongkongId").addClass("notEmpty");
			$("#emp_hongkong_id").css("display","block"); 
			$("#emp_hongkong_id h2 span").css("display","inline"); 
			$("#emp_passport_no").css("display","block"); 
			
		}
		if($("#EmployeeVisaStatus option:selected").text()=="Dependent"){
			$("#EmployeeHongkongId").removeClass("notEmpty");
			$("#emp_hongkong_id h2 span").css("display","none");  
			$("#emp_hongkong_id").css("display","block"); 
			$("#emp_passport_no").css("display","block"); 
			
		}
	});
	
	
	$(".weekly").click(function(){
		alert("ok");
	});
        
    $(".flash-message").delay(4000).fadeOut(2000);
    
    $(".preview_video").click(function()
    {
        console.log($(this).data("url_type"));
        console.log($(this).data("url"));
        
        var html = "";
        if ($(this).data("url_type") == "LOCAL")
        {
            html = '<video width="740" height="500" autoplay>';
            html += '<source src="' + $(this).data("url") + '" type="video/mp4">';            
            html += 'Your browser does not support the video tag.';
            html += '</video>'; 
        }
        else
        {
            html = '<iframe width="740" height="500"';
            html += 'src="' + $(this).data("url") + '"';                           
            html += '  allowfullscreen frameborder="0"></iframe>'; 
        }
            
        $.fancybox.open(html);
	});
    
    
    /**
     * nested table script
     */
    $(document).on("click" , ".nested-table .child-table-toggler", function ()
    {
        var tr_id = $(this).data("tr_id");

        if ($(this).hasClass("child-table-open"))
        {
            $(this).removeClass("child-table-open").addClass("child-table-close");
            $("#" + tr_id).addClass("hidden");
        }
        else
        {
            $(this).removeClass("child-table-close").addClass("child-table-open");
            $("#" + tr_id).removeClass("hidden");
        }
    });
    
    /** for confirm  **/
    $(".confirm-action").click(function(e)
    {
        e.preventDefault();
        var href = $(this).parent().children("a.confirm-action-link").attr("href");

        var data_text = $(this).data("confirm_text");
        var no_btn_text = $(this).data("confirm_no_btn_text");
        var yes_btn_text = $(this).data("confirm_yes_btn_text");
        var yes_btn_color = $(this).data("confirm_yes_btn_color");

        data_text = data_text ? data_text : "Are you sure?";
        no_btn_text = no_btn_text ? no_btn_text : "No";
        yes_btn_text = yes_btn_text ? yes_btn_text : "Yes";
        yes_btn_color = yes_btn_color ? yes_btn_color : "#DD6B55";

        swal({
            title: data_text,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: no_btn_text,
            confirmButtonColor: yes_btn_color,
            confirmButtonText: yes_btn_text,
            closeOnConfirm: true
        },
        function() {
            window.location = href;
        });
        return false;
    });
    
    /** for post back Delete  **/
    $(".summary-action-delete").click(function(e)
    {
        e.preventDefault();
        var href = $(this).parent().children("a.summary-action-delete-link").attr("href");

        var data_text = $(this).data("confirm_text");
        var no_btn_text = $(this).data("confirm_no_btn_text");
        var yes_btn_text = $(this).data("confirm_yes_btn_text");
        var yes_btn_color = $(this).data("confirm_yes_btn_color");

        data_text = data_text ? data_text : "Are you sure?";
        no_btn_text = no_btn_text ? no_btn_text : "No";
        yes_btn_text = yes_btn_text ? yes_btn_text : "Yes";
        yes_btn_color = yes_btn_color ? yes_btn_color : "#DD6B55";

        swal({
            title: data_text,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: no_btn_text,
            confirmButtonColor: yes_btn_color,
            confirmButtonText: yes_btn_text,
            closeOnConfirm: true
        },
        function() {
            window.location = href;
        });
        return false;
    });
});

function warn(text, options)
{
    options = typeof options == "undefined" ? { type : "error" , desc : ""} : options;
    
    sweetAlert(text, options.desc, options.type);
    return false;
}

function confirm_box(opt)
{
    swal({
        title: opt.title ? opt.title : "Are You Sure",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: opt.cancelBtn ? opt.cancelBtn : "No",
        confirmButtonColor: opt.confirmBtnColor ? opt.confirmBtnColor : "#DD6B55",
        confirmButtonText: opt.confirmColor ? opt.confirmColor : "Yes",
        closeOnConfirm: true
    },
    function() {
        opt.onConfirmCallBack();        
    });
}

$(document).ready(function ()
{
    $(".fancybox-popup").fancybox(
    {
        autoScale : true,
        padding : 10,
    });   
});