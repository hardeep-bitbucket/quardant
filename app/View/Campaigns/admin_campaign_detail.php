<?php
/**
 * campaign detail
 * 
 * 
 * @created    11/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

$months_options =  array(
    "0" => "This Month",
    "-1" => "Last 2 Month",
    "-2" => "Last 3 Month",
    "-3" => "Last 4 Month",
    "-5" => "Last 6 Month",
    "-11" => "This Year",
);

$last_options = array(
    "3" => "Last 3",
    "5" => "Last 5",
    "8" => "Last 8",
    "10" => "Last 10",
    "15" => "Last 15"
); 

?>

<style>
    .automobile{
        overflow-x: auto;        
    }
    
    .automobile .automobile-block{
        padding: 10px;
        width : 250px;
        height : 250px;
        overflow-y: auto;
        float: left;
        border-left : 1px solid #000;
    }
</style>

<div class="row margin-bottom-15"> 
    <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12">
        <h3>Campaign Detail</h3>
    </div>
    <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12 margin-top-20" style="text-align: right;">
       
        <?php
            echo $this->Html->link("<i class='fa fa-th'></i> Go to Campaigns",                
                array('action' => 'index', "admin" => true), 
                array('escape' => false, "class" => "btn btn-circle  btn-primary")
            );
        ?>
        <span></span>
        <?php
            echo $this->Html->link("<i class='fa fa-edit'></i> Edit / Stop",                
                array('action' => 'edit', "admin" => true, $record['Campaign']['id']), 
                array('escape' => false, "class" => "btn btn-circle  green")
            );
       ?>
        <span></span>
        <?php
            echo $this->Html->link("<i class='fa fa-cogs'></i> Efficiency",                
                array('action' => 'efficiency_report', "admin" => true, $record['Campaign']['id']), 
                array('escape' => false, "class" => "btn btn-circle  purple")
            );
        ?>
    </div>
</div>
<div class="row"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="fa fa-tasks theme-font"></i>
                    <span class="caption-subject font-blue-madison bold">Campaign Basic Detail</span>
                </div>     
                <div class="tools">                    
                    <?php 
                        echo $this->Html->link(
                            $this->Html->image("admin/pdf.png", array("class" => "tool-link")), 
                            array("controller" => "Campaigns", 'action' => 'admin_export_pdf', "admin" => true, $record["Campaign"]["id"]), 
                            array('escape' => false)
                        );
                     ?>
                    <?php 
                        echo $this->Html->link(
                            $this->Html->image("admin/excel.png", array("class" => "tool-link")), 
                            array("controller" => "Campaigns", 'action' => 'admin_export_excel', "admin" => true, $record["Campaign"]["id"]), 
                            array('escape' => false)
                        );
                     ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row"> 
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Name</div>
                            <div class="col-md-7 value"><?php echo $record['Campaign']['name']; ?></div>
                        </div>
                        <?php if ($auth_user['group_id'] != ADVERTISER_GROUP_ID): ?>
                            <div class="row static-info">                        
                                <div class="col-md-5 name">Advertiser</div>
                                <div class="col-md-7 value"><?php echo $record['User']['name'] . " " . $record['User']['subname']; ?></div>
                            </div>                    
                        <?php endif; ?>
                        <div class="row static-info">
                            <div class="col-md-5 name">Type</div>
                            <div class="col-md-7 value"><?php echo $record['CampaignType']['value']; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Duration</div>
                            <div class="col-md-7 value"><?php echo $record['CampaignDuration']['value']; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Status</div>
                            <div class="col-md-7 value">
                                <?php echo  $this->TSHtml->campaignStatusLabel($record['CampaignStatus']['id'], $record['CampaignStatus']['value']); ?>                                
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Payment Status</div>
                            <div class="col-md-7 value">
                                <?php 
                                     if ($invoice['pending_amount'] > 0)
                                     {
                                         echo $this->TSHtml->transactionStatusLabel(StaticArray::$invoice_status_unpaid); 
                                     }
                                     else if ($invoice['paid_amount'] > 0)
                                     {
                                         echo $this->TSHtml->transactionStatusLabel(StaticArray::$invoice_status_paid); 
                                     }
                                 ?>
                            </div>
                        </div>
                      
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">       
                        <div class="row static-info">
                            <div class="col-md-5 name">Campaign No.</div>
                            <div class="col-md-7 value"><?php echo $record['Campaign']['campaign_no']; ?></div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Start Date</div>
                            <div class="col-md-7 value"><?php echo $record['Campaign']['start_date']; ?></div>
                        </div>
                        <?php if ($record['CampaignDuration']['id'] == CAMPAIGN_DURATION_FIXED_ID): ?>
                        <div class="row static-info">
                            <div class="col-md-5 name">End Date</div>
                            <div class="col-md-7 value"><?php echo $record['Campaign']['end_date']; ?></div>
                        </div>
                        <?php endif; ?>      
                        
                         <?php if ($invoice['pending_amount']): ?>
                            <div class="margin-top-20">
                                <div class="note note-info">
                                    <span class="block font-size-14">Payment Due : <?php echo CURRENCY_SYMBOL; ?><b><?php echo $invoice['pending_amount']; ?></b></span>
                                </div>
                            </div>                   
                        <?php endif; ?>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>


<div class="row margin-top-10"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="fa fa-tasks theme-font"></i>
                    <span class="caption-subject font-blue-madison bold">Campaign Status Detail</span>
                </div>               
            </div>
            <div class="portlet-body">
                
                <div class="row">
                    <div class="progress progress-striped active margin-5">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $record[$model]["complete"] ? $record[$model]["complete"] : 1; ?>%">                            
                        </div>
                    </div>
                </div>
                <div class="row margin-top-20"> 
                    <div class="col-md-12 margin-5">
                        <?php echo $this->element("admin/campaign/campaign_completation_status", array("record" => $record['Campaign'])); ?>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>

<div class="row margin-top-10"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="fa fa-delicious theme-font"></i>
                    <span class="caption-subject font-blue-madison bold">Locations</span>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>
            </div>
            <div class="portlet-body">
                <div style="max-height : 200px; overflow-y: auto;">
                    <table class="table table-bordered table-striped table-condensed  table-custom">
                        <thead>
                           <tr class="head tr-border-blue-madison">
                                <td width="5%" class="td-center">Sr.</td>                            
                                <td width="25%">Location</td>
                                <td width="15%" class="td-center bold">Target Impressions</td>
                                <td width="15%" class="td-center bold">Reached Impressions</td>                            
                                <td width="20%" class="td-center bold">Spend (<?php echo CURRENCY_SYMBOL; ?>)</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; foreach ($record["AdLocation"] as $ad_location) : $i++; ?>
                                <tr>
                                    <td class="td-center"><?php echo $i; ?></td>
                                    <td><?php echo $ad_location['Location']['name']; ?></td>
                                    <td class="td-center"><?php echo $ad_location['target_imperssions']; ?></td>
                                    <td class="td-center"><?php echo $ad_location['reached_imperssions']; ?></td>
                                    <td class="td-center"><?php echo $ad_location['reached_imperssions'] * $ad_location['Location']['price_per_impression']; ?></td>                                
                                </tr>
                            <?php  endforeach; ?>
                        </tbody>                  
                    </table> 
                </div>
            </div>
         </div>
    </div>
</div>


<?php if (isset($automobiles) && $automobiles) : ?>
<div class="row margin-top-10"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="fa fa-taxi theme-font"></i>
                    <span class="caption-subject font-blue-madison bold">Automobiles</span>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12" style="overflow-x: auto;">                        
                        <div class="automobile">
                            <ul class="manufacture">
                                <?php  foreach ($automobiles as $manufacture): ?>
                                        <li>
                                            <span class="automobile-block-title"><?php echo $manufacture['name']; ?></span>
                                            <?php if (isset($manufacture['children']) && $manufacture['children']) : ?>
                                                <ul  class="model">
                                                    <?php foreach ($manufacture['children'] as $model): ?>
                                                    <li>
                                                        <span><?php echo $model["name"]; ?></span>
                                                       <?php if (isset($model['children']) && $model['children']) : ?>
                                                        <ul class='varient'>
                                                            <?php foreach ($model['children'] as $variant): ?>
                                                            <li> <span><?php echo $variant; ?><span></li>
                                                            <?php  endforeach; ?>
                                                        </ul>
                                                        <?php endif; ?>
                                                    </li>
                                                    <?php endforeach; ?>    
                                                </ul>
                                            <?php endif; ?>
                                        </li>

                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>
<?php endif; ?>


<div class="row margin-top-10"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="fa fa-money theme-font"></i>
                    <span class="caption-subject font-blue-madison bold">Invoices</span>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>
            </div>
            <div class="portlet-body"> 
                <table class="table table-bordered table-striped table-condensed table-custom">
                    <thead>
                       <tr class="head tr-border-blue-madison" >                           
                            <td width="5%"  class="td-center bold">ID</td>
                            <td width="10%" class="td-center bold">Start Date</td>
                            <td width="10%" class="td-center bold">End Date</td>                            
                            <td width="10%" class="td-center bold">Amount(<?php echo CURRENCY_SYMBOL; ?>)</td>
                            <td width="12%" class="td-center bold">Invoice Date</td>                            
                            <td width="8%" class="td-center bold">Status</td>
                            <td width="8%" class="td-center bold">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($record["Invoice"]) foreach ($record["Invoice"] as $arr) { ?>
                            <tr>                                
                                <td class="td-center"><?php echo $arr['id']; ?></td>
                                <td class="td-center"><?php echo $arr['start_date']; ?></td>  
                                <td class="td-center"><?php echo $arr['end_date']; ?></td>                                  
                                <td class="td-center"><?php echo $arr['amount']; ?></td>                                  
                                <td class="td-center"><?php echo DateUtility::getFormatDateFromString($arr['created_on'], DEFAULT_DATETIME_FORMAT); ?></td>
                                <td class="td-center td-label"> <?php echo $this->TSHtml->transactionStatusLabel($arr['status']); ?></td> 
                                <td class="td-center">
                                    <?php
                                        echo $this->Html->link(
                                            $this->Html->image("admin/summary_view.png", array("class" => "summary-action-icon")), 
                                            array("controller" => "Invoices", 'action' => 'admin_edit', $arr['id']), 
                                            array('escape' => false, 'title' => EDIT_TITLE, "class" => 'ajax-page-link')
                                        );
                                    ?>
                                    
                                    <?php
                                        echo $this->Html->link(
                                            $this->Html->image("admin/pdf.png", array("class" => "summary-action-icon")),           
                                            array("controller" => "Invoices", 'action' => 'export_pdf', "admin" => true, $arr['id']), 
                                            array('escape' => false, "class" => "")
                                        );
                                    ?>
                                </td>
                            </tr>
                    <?php } ?>
                    </tbody>                  
                </table>                    
             </div>
         </div>
    </div>
</div>


<div class="row margin-top-10"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="fa fa-tasks theme-font"></i>
                    <span class="caption-subject font-blue-madison bold">Campaign Ad Shown</span>
                </div>                
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                                                
                </div>
                <div class="actions">
                    <a href="#" class="btn btn-circle btn-default btn-icon-only fullscreen" data-chart_id="#ad-shown-line-chart" data-original-title="" title=""></a>                    
                </div>
            </div>
             <div class="portlet-body">
                 <div class="row">
                    <div class="col-md-12 am-chart" id="ad-shown-line-chart" style="height : 500px;">
                        
                    </div>
                 </div>
            </div>
         </div>
    </div>
</div>


<div class="row margin-top-10"> 
    <div class="col-lg-6 col-sm-12 col-xs-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="fa fa-delicious theme-font"></i>
                    <span class="caption-subject font-blue-madison bold">Top Locations</span>
                </div>
                <div class="tools">                                       
                    <a href="javascript:;" class="collapse"></a>                            
                </div>
                <div class="actions">
                    <a href="#" class="btn btn-circle btn-default btn-icon-only fullscreen" data-chart_id="#top-location-chart" data-original-title="" title=""></a>                    
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <?php
                            echo $this->Form->input('limit_type', array(
                                'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                'options' => $last_options,                        
                                'class' => 'select2me form-control', "id" => "top-location-limit-type"
                            ));
                        ?>                                            
                    </div>
                    <div class="col-md-5">
                        <?php
                            
                            echo $this->Form->input('period_type', array(
                                'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                'options' => $months_options,                        
                                'class' => 'select2me form-control', "id" => "top-location-period-type"
                            ));
                        ?>                                            
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 am-chart" id="top-location-chart" style="height : 500px;">
                        
                    </div>
                </div>
            </div>
         </div>
    </div>
    
    <div class="col-lg-6 col-sm-12 col-xs-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="fa fa-automobile theme-font"></i>
                    <span class="caption-subject font-blue-madison bold">Top Vehicles</span>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>
                <div class="actions">
                    <a href="#" class="btn btn-circle btn-default btn-icon-only fullscreen" data-chart_id="#top-automobile-chart" data-original-title="" title=""></a>                    
                </div>
            </div>
            <div class="portlet-body">
                    <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <?php
                            echo $this->Form->input('limit_type', array(
                                'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                'options' => $last_options,                        
                                'class' => 'select2me form-control', "id" => "top-automobile-limit-type"
                            ));
                        ?>                                            
                    </div>
                    <div class="col-md-5">
                        <?php
                            echo $this->Form->input('period_type', array(
                                'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                'options' => $months_options,                        
                                'class' => 'select2me form-control', "id" => "top-automobile-period-type"
                            ));
                        ?>                                            
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 am-chart" id="top-automobile-chart" style="height : 500px">
                        
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>

<script>
    var line_chart, top_location_chart, top_automobile_chart;
    
    var campaign_id = '<?php echo $record['Campaign']['id']; ?>';  
    
    function resize_chart(id)
    {
        if ($(id).hasClass("fullscreen-on"))
        {
            $(id).removeClass("fullscreen-on");
            $(id).css("height", 500);
        }
        else
        {
            var h;
            switch(id)
            {
                case "#ad-shown-line-chart":
                   h = window.innerHeight - 100;
                break;

                case "#top-location-chart":
                   h = window.innerHeight - 130;
                break;

                case "#top-automobile-chart":
                    h = window.innerHeight - 130;
                break;
            }
            $(id).addClass("fullscreen-on");
            $(id).css("height", h);
        }
        
        switch(id)
        {
            case "#ad-shown-line-chart":
                line_chart.handleResize();
            break;
            
            case "#top-location-chart":
                top_location_chart.handleResize();
            break;
            
            case "#top-automobile-chart":
                top_automobile_chart.handleResize();
            break;
        }
    }
    
    function load_ad_shown_line_chart(data, groups)
    {
        var count = Object.keys(data).length;
        // SERIAL CHART
        line_chart = new AmCharts.AmSerialChart();
        line_chart.pathToImages = "/css/assets/global/plugins/amcharts/amcharts/images/";
        line_chart.dataProvider = data;            
        line_chart.categoryField = "date";        

        // listen for "dataUpdated" event (fired when chart is inited) and call zoomChart method when it happens
        line_chart.addListener("dataUpdated", zoomChart);

        // AXES
        // category
        var categoryAxis = line_chart.categoryAxis;
        categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
        categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD
        categoryAxis.minorGridEnabled = true;
        categoryAxis.axisColor = "#DADADA";
        categoryAxis.twoLineMode = true;        
        categoryAxis.dateFormats = [{
             period: 'fff',
             format: 'JJ:NN:SS'
         }, {
             period: 'ss',
             format: 'JJ:NN:SS'
         }, {
             period: 'mm',
             format: 'JJ:NN'
         }, {
             period: 'hh',
             format: 'JJ:NN'
         }, {
             period: 'DD',
             format: 'DD'
         }, {
             period: 'WW',
             format: 'DD'
         }, {
             period: 'MM',
             format: 'MMM'
         }, {
             period: 'YYYY',
             format: 'YYYY'
         }];

        // first value axis (on the left)
        var valueAxis1 = new AmCharts.ValueAxis();
        valueAxis1.axisColor = "#FF6600";
        valueAxis1.axisThickness = 2;
        valueAxis1.gridAlpha = 0;
        valueAxis1.integersOnly = true;
        line_chart.addValueAxis(valueAxis1);

        var graphs = {};

        for(var i in groups)
        {
            graphs[i] = new AmCharts.AmGraph();
            graphs[i].valueAxis = valueAxis1; // we have to indicate which value axis should be used
            graphs[i].title = groups[i];
            graphs[i].valueField = groups[i];
            graphs[i].bullet = "round";
            graphs[i].hideBulletsCount = 30;
            graphs[i].bulletBorderThickness = 1;
            graphs[i].balloonText =  groups[i] + " : <b> [[" + groups[i] + "]] </b>";
            line_chart.addGraph(graphs[i]);
        }

        // CURSOR
        var chartCursor = new AmCharts.ChartCursor();
        chartCursor.cursorAlpha = 0.1;
        chartCursor.fullWidth = true;
        line_chart.addChartCursor(chartCursor);

        // SCROLLBAR
        var chartScrollbar = new AmCharts.ChartScrollbar();
        line_chart.addChartScrollbar(chartScrollbar);

        // LEGEND
        var legend = new AmCharts.AmLegend();
        legend.marginLeft = 110;
        legend.useGraphSettings = true;
        line_chart.addLegend(legend);

        // WRITE
        line_chart.write("ad-shown-line-chart");
        
        
        // this method is called when chart is first inited as we listen for "dataUpdated" event
        function zoomChart() 
        {
            var temp = count > 20 ? 20 : count;
            temp = temp - 1;
            // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
            line_chart.zoomToIndexes(0, temp);
        }
    }
    
    
    AmCharts.ready(function()
    {        
        $.get("<?php echo SITE_URL.$controller ?>/ajaxAdShownChartData/" + campaign_id, function(data, status)
        {
            var data = JSON.parse(data);
            
            var chartData = [];
            
            for(var i in data.data)
            {
                var arr = data.data[i];
                arr['date'] = new Date(arr['date']);
                chartData.push(arr);
            }
            load_ad_shown_line_chart(chartData, data.group_list);
        });
        
        $("#top-location-period-type, #top-automobile-period-type").trigger("change");
    });
    
    
    function load_location_chart()
    {
        var period = $("#top-location-period-type").val();
        var limit = $("#top-location-limit-type").val();
        
        $.get("<?php echo SITE_URL.$controller ?>/ajaxTopLocationChartData/" + campaign_id + "/" + period + "/" + limit, function(data, status)
        {
            var data = JSON.parse(data);
            //console.log(data);
            var chartData = [];
            for (var i in data)
            {
                chartData.push(
                {
                    "title" : data[i]['L']['name'],
                    "value" : data[i][0]['total_count'],
                    "color" : chartConfig.getColor(i)
                });
            }
            
            top_location_chart = new AmCharts.AmSerialChart();
            top_location_chart.dataProvider = chartData;
            chartConfig.setSerialChartDefault(top_location_chart , {colorField : "color"});
            chartConfig.setDownloadLink(top_location_chart);
            
            top_location_chart.categoryAxis.labelRotation = 45;
                        
            var valueAxis1 = new AmCharts.ValueAxis();
            valueAxis1.axisColor = "#FF6600";
            valueAxis1.axisThickness = 2;
            valueAxis1.gridAlpha = 0;
            valueAxis1.integersOnly = true;            
            top_location_chart.addValueAxis(valueAxis1);
            
            top_location_chart.write("top-location-chart");
        });
    }
    
    function load_automobile_chart()
    {
        var period = $("#top-automobile-period-type").val();
        var limit = $("#top-automobile-limit-type").val();
        
        $.get("<?php echo SITE_URL.$controller ?>/ajaxTopAutomobileChartData/" + campaign_id + "/" + period + "/" + limit, function(data, status)
        {
            var data = JSON.parse(data);
            
            var chartData = [];
            for (var i in data)
            {
                chartData.push(
                {
                    "title" : data[i]['name'],
                    "value" : data[i]['count'],
                    "color" : chartConfig.getColor(i, 1)
                });
            }

            top_automobile_chart = new AmCharts.AmSerialChart();
            top_automobile_chart.dataProvider = chartData;
            chartConfig.setSerialChartDefault(top_automobile_chart, {colorField : "color"});
            chartConfig.setDownloadLink(top_automobile_chart);
            
            top_automobile_chart.categoryAxis.labelRotation = 45;
            
            var valueAxis1 = new AmCharts.ValueAxis();
            valueAxis1.axisColor = "#FF6600";
            valueAxis1.axisThickness = 2;
            valueAxis1.gridAlpha = 0;
            valueAxis1.integersOnly = true;            
            top_automobile_chart.addValueAxis(valueAxis1);
            
            top_automobile_chart.write("top-automobile-chart");
        });
    }
    
    
    $("#top-location-period-type, #top-location-limit-type").change(function()
    {
        load_location_chart();
    });
    
    $("#top-automobile-period-type, #top-automobile-limit-type").change(function()
    {
        load_automobile_chart();
    });
    
    
    $(".fullscreen").click(function()
    {
        resize_chart($(this).data("chart_id"));
    });
</script>