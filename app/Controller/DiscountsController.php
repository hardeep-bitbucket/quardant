<?php

/**
 * Discounts Controller
 * 
 * 
 * @created    02/01/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Sonia
 */

/**
 * @property Discount $Discount
 */
class DiscountsController extends AppController {

    public $name = 'Discounts';

    /**
     * Sets Graphics types for this controller and all of it's views
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->_setList();
        $this->Auth->allow("ajaxGetDiscount");
    }

    /**
     * show summary data
     */
    public function admin_index() 
    {
        $this->Redirect->urlToNamed();

        // set search perameter
        $conditions = $this->getSearchConditions(array(
            array('model' => 'Discount', 'field' => 'discount_type', 'type' => 'integer', 'view_field' => 'discount_type'),
            array('model' => 'Discount', 'field' => 'discount_code', 'type' => 'integer', 'view_field' => 'discount_code')
        ));

        $records = $this->paginate($this->modelClass, $conditions);        
        //setting variables

        $this->set(compact('records'));
    }

    /**
     * 
     * Add New record
     * 
     */
    public function admin_add() {
        //calling function from app controller
        parent::add();

        $this->set('title_for_layout', 'Add ' . $this->modelClass);

        $reset_action = 'admin_index';
        $heading = 'Add ' . $this->modelClass;
        $this->set(compact('reset_action', 'heading'));

        $this->render('admin_form');
    }

    /**
     * Updates Existing Record
     * @param type $id 
     */
    public function admin_edit($id) {
        //calling function from app controller
        parent::edit($id);

        $this->set('title_for_layout', 'Edit ' . $this->modelClass);
        $reset_action = 'admin_index';
        $heading = 'Edit ' . $this->modelClass;
        $this->set(compact('reset_action', 'heading'));

        $this->render('admin_form');
    }

    /**
     * @Get list
     */
    private function _setList() 
    {
        $this->loadModel("Type");
        
        $discountTypes = $this->Type->getActiveList(
                array('id', 'value'), array('type' => 'Discount Types')
        );
        
        $discountFor = $this->Type->getActiveList(
                array('id', 'value'), array('type' => 'Discount For')
        );
        
        $campaign_duration_type = $this->Type->find("list", array(
            "fields" => array("id", "value"),
            "conditions" => array("type" => CAMPAIGN_DURATION_TYPE)
        ));
        
        $campaign_target_automobile_type = $this->Type->find("list", array(
            "fields" => array("id", "value"),
            "conditions" => array("type" => CAMPAIGN_TYPE)
        ));
        
        $this->set(compact('discountTypes', 'discountFor', 'campaign_target_automobile_type', 'campaign_duration_type'));
    }
    
    
    public function ajaxGetDiscount($user_id, $campaign_id, $code, $amount)
    {
        $this->loadModel("Campaign");
        echo $this->getDiscount($user_id, $campaign_id, $code, $amount);
        exit;
    }
}

?>