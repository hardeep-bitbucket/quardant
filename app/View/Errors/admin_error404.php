<?php
/**
 * 404 View
 * 
 * @created    05/02/2014
 * @package    Suvidha Online 1.02
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Gagandeep Gambhir
 */
?>
 <!--Body Right side start-->
	<section class="error_main">
	    <div style="text-align:center;"><img src="<?php echo $this->base;?>/img/error_icon.png"/></div>
	<h1>It appears the page you were looking for <br>
	doesn't exist. Sorry about that.</h1>
	<div class="back_home"><a href="<?php echo $this->base;?>/admin/users/logout">Click here for Login</a></div>
	</section>
<!--Body Right side end--> 