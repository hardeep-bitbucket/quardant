<?php

/**
 * CampaignAutomobile Model
 * 
 * 
 * @created    22/04/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     HARDEEP
 */
class CampaignVersionLog extends AppModel {
    /*
     * Behaviours 
     */
    public $actsAs = array(
        'DateTimeFormat' => array('created_on' => array('in_format' => ''))
    );

    /**
     * Associations
     */
    var $belongsTo = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'HistoryCampaign' => array(
            'className' => 'HistoryCampaign',
            'foreignKey' => 'history_campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

}