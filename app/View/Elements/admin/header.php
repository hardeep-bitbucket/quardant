<?php
/**
 * Common header
 *
 *
 * @created    17/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>
<div class="page-header navbar navbar-fixed-top">
    
    <div class="page-header-inner container">
        <div class="page-logo">
            <?php
            echo $this->Html->link(
                    $this->Html->image('admin/logo-admin.png'), SITE_URL, array('escape' => false)
            );
            ?>
        </div>
        
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">            
		</a>
        <div class="page-top">
            <div class="top-menu pull-right" >
               
                <ul class="nav navbar-nav">
                    <?php if ($auth_user["group_id"] == ADVERTISER_GROUP_ID) : ?>
                        <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <?php 
                                    echo $this->Html->image("admin/wallet.png", array(
                                        "class" => "wallet-img"
                                    ));
                                ?>
                                <span class="badge badge-default" style="height : 21px; font-size: 14px;">
                                    <b> $ <?php echo $auth_user["wallet_amount"]; ?> </b>
                                </span>
                            </a>                            
                        </li>
                    <?php endif; ?>
                    <li class="dropdown dropdown-user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <?php
                            echo $this->Html->image('admin/avatar3_small.png');
                            ?>
                            <span class="username">
                                <?php echo $auth_user["name"]; ?>
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <?php
                                    $controller = "users";
                                    if ($auth_user['group_id'] == ADVERTISER_GROUP_ID)
                                    {
                                        $controller = "advertisers";
                                    }
                                    else if ($auth_user['group_id'] == PARTNER_GROUP_ID)
                                    {
                                        $controller = "partners";
                                    }
                                    
                                    echo $this->Html->link("<i class='fa fa-user'></i> Profile", array("controller" => $controller, "action" => "admin_edit_profile", $auth_user['id']), array("escape" => false));
                                ?>
                            </li>

                            <li>
                                <?php
                                echo $this->Html->link("<i class='fa fa-briefcase'></i> Change Password", array("controller" => "users", "action" => "admin_change_password"), array("escape" => false));
                                ?>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <?php
                                echo $this->Html->link("<i class='fa fa-power-off'></i> Log Out", array("controller" => "users", "action" => "admin_logout"), array("escape" => false));
                                ?>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->

        </div>

        <div id="ajax-loader">
            <div class="ajax-loader-content">
                <?php
                echo $this->Html->image("admin/ajax-loader-green.gif");
                ?>
                <span style="margin:10px;">Loading.....</span>
            </div>
        </div>

        <div id="ajax-loader-faliure">
            <div class="ajax-loader-content">
                <span style="margin:10px;">Oops! Something went wrong</span>
            </div>
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->