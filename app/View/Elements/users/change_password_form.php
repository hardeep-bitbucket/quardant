<?php echo $this->Form->hidden('User.username' , array('div'=>false, 'label'=>false ,'class'=>'notEmpty','value'=>$this->Session->read('Auth.User.username'))); ?>
<tr>
    <td class='label'>
        <?php echo __("Old Password"); ?><span class='required_fields'>*</span>
    </td>
    <td class='control'>
        <?php
         	echo $this->Form->input('User.old_password' , array('type'=>'password','div'=>false, 'label'=>false ,'class'=>'notEmpty'))
        ?>
    </td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
    <td class='label'>
        <?php echo __("New Password"); ?><span class='required_fields'>*</span>
    </td>
    <td class='control'>
        <?php
         	echo $this->Form->input('User.new_password' , array('type'=>'password','div'=>false, 'label'=>false ,'class'=>'notEmpty'))
        ?>
    </td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
    <td class='label'>
        <?php echo __("Confirm Password"); ?> <span class='required_fields'>*</span>
    </td>
    <td class='control'>
        <?php
         	echo $this->Form->input('User.confirm_password' , array('type'=>'password','div'=>false, 'label'=>false ,'class'=>'notEmpty'))
        ?>
    </td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>