<?php
/**
 * Target Automobile 
 * 
 * 
 * @created    28/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     HARDEEP
 */

echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal', 'id' => 'target-automobile-form', 'enctype' => "multipart/form-data"));
echo $this->Form->hidden('tab_name', array('label' => false)); 
echo $this->Form->hidden('automobile_ids', array('label' => false, "id" => "automobile_ids")); 
?>

<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
    
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label col-md-3">Target Type <span class="required">*</span></label>
            <div class="col-md-9">                                            
                <?php
                    echo $this->Form->input('campaign_type_id', array(
                        'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                        'options' => $target_types,
                        'empty' => 'Please Select',
                        'class' => 'select2me form-control',
                    ));
                ?>                                            
            </div>
        </div>
    </div>
    
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group jstree-div">
            <label class="control-label col-md-3">Automobiles <span class="required">*</span></label>
            <div class="col-md-9"  style=" overflow-y: auto;">                                            
                <div class="jstree" style="max-height : 500px;">
                    <?php
                        echo $this->TSHtml->loadTreeLevelUL($automobiles, $record_list, array("dataField" => "automobile_id")); 
                    ?>
                </div>                                          
            </div>
        </div>
        
    </div>
</div>
    
</div>
<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 bar-chart">
    
    <div class="portlet green-meadow box">
        <div class="portlet-title">
            <div class="caption">
                <div>Automobiles on <b><?php echo implode(", ", array_flip($location_list));  ?></b></div>
            </div>                
        </div>
        <div class="portlet-body"> 
            <div class="row">                
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Period</label>
                        <div class="col-md-8">                                            
                            <?php
                                $options = array(
                                    "0" => "This Month",
                                    "-1" => "Last 2 Month",
                                    "-2" => "Last 3 Month",
                                    "-3" => "Last 4 Month",
                                    "-5" => "Last 6 Month",
                                    "-11" => "This Year",
                                );

                                echo $this->Form->input('period_type', array(
                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                    'options' => $options,                        
                                    'class' => 'select2me form-control', "id" => "period_type"
                                ));
                            ?>                                            
                        </div>
                    </div>
                </div>        
            </div>   
            <div class="row">
                <div class="col-md-12">
                    <div id="am-chart" class="am-chart" style="height : 550px;">
                    </div>
                    
                    <div style="width:100px; margin: auto;" class="radio-btn-list">
                        <input type="radio" checked="true" name="chart-radio" id="rb-2d">
                            <span>2D</span>                            
                        <input type="radio" name="chart-radio" id="rb-3d">
                            <span>3D</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-10">
        <div style="margin-top: 10px;">
            <?php echo $this->element("admin/tab_save_btn"); ?>
        </div>
    </div>
</div>

<script>
    
    var automobiles = JSON.parse('<?php echo json_encode($automobiles); ?>');       
    var record_list = JSON.parse('<?php echo json_encode($record_list); ?>');  
    var location_list = JSON.parse('<?php echo json_encode($location_list); ?>');    
    var chart;
    console.log(location_list);
    function loadTree()
    {
        $(".jstree").jstree({
            "core": {
                 "themes":{
                     "icons":false
                 }
             },
             "plugins" : ["checkbox", "html_data", "ui"]
        }).on("changed.jstree", function ()
        {
            load_chart(false);
        });
         
        for (var i in record_list)
        {
            $(".jstree").jstree("select_node", record_list[i]);
        }
    }
    
    $("#CampaignCampaignTypeId").change(function(e, data)
    {
        if ($(this).val() == '<?php echo CAMPAIGN_AUTOMOBILE_MASS_ID; ?>')
        {
            $(".jstree-div, .bar-chart").addClass("hidden");
        }
        else
        {
            $(".jstree-div, .bar-chart").removeClass("hidden");
        }
    });
    
    //tree.jstree("open_all");
    
    function get_node_ids (data, ids, unset)
    {
        for (var i in data)
        {
            if (data[i]["children"].length > 0)
            {
                var found = $.inArray(data[i]["Automobile"]['id'], ids) >= 0;
                ids = get_node_ids(data[i]["children"], ids, found);
            }
            
            if (unset)
            {
                var k = $.inArray(data[i]['Automobile']['id'], ids);
                if (k >= 0)
                {
                    delete ids[k];
                }
            }
        }
        
        return ids;
    }
    
    function get_manufacture_from_variant(variant_id)
    {
        for (var i in automobiles)
        {
            for(var a in automobiles[i]['children'])
            {
                for(var j in automobiles[i]['children'][a]['children'])
                {
                    var arr = automobiles[i]['children'][a]['children'][j]['Automobile'];                    
                    if (arr.id == variant_id)
                    {
                        return automobiles[i]['Automobile'];
                    }
                }
            }
        }
    }
    
    $(".btn-save").click(function()
    {
        var ids = [];
        $("#automobile_ids").val("");
        if (!$(".jstree-div").hasClass("hidden"))
        {
            ids = $(".jstree").jstree("get_checked", null, true);
            
            if (ids.length == 0)
            {
                warn("Please select at least one Automobile");
                return false;
            }

            ids = get_node_ids(automobiles, ids, false);            
            var ret = [];
            var a = 0;
            for (var i in ids)
            {
                if (typeof ids[i] != "undefined" && ids[i] != "")
                {
                    ret[a] = ids[i];
                    a++;
                }
            }
            $("#automobile_ids").val(ret.join());
        }
        return true;
    });
    
    
    var location_automobile_count_list = {};
    
    function set_chart_depth() 
    {
        if (document.getElementById("rb-2d").checked) {
            chart.depth3D = 0;
            chart.angle = 0;
        } else {
            chart.depth3D = 25;
            chart.angle = 30;
        }        
    }
    
    $("#rb-2d, #rb-3d").change(function()
    {
        set_chart_depth();
        chart.validateNow();
    });
     
    function load_chart(loaded)
    {
        var keys = Object.keys(location_automobile_count_list);
        if (keys.length > 0)
        {
            // getting all checked nodes
            var ids = $(".jstree").jstree("get_checked", null, true);
                      
            var chartData = [];
            
            for (var i in location_list)
            {
                var data = location_automobile_count_list[location_list[i]];                
                var obj = {
                    name : data["Location"]["name"],
                    count : 0
                };
                
                //initilize count for each manufacture
                for (var a in automobiles)
                {
                    obj[automobiles[a]['Automobile']['name']] = 0;
                }
                  
                for (var a in data["AutomobileLocation"])
                {   
                    var arr = data["AutomobileLocation"][a];                    
                    if ($.inArray(arr["variant_id"], ids) >= 0)
                    {
                        // getting manufacture from variant
                        var arr1 = get_manufacture_from_variant(arr["variant_id"]);                        
                        obj[arr1['name']] += parseInt(arr["total_count"]);
                    }
                }                
                chartData.push(obj);
            }
            
            // creating chart.
            chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = "name";
            chart.plotAreaBorderAlpha = 0.2;
            set_chart_depth();

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.gridAlpha = 0.1;
            categoryAxis.axisAlpha = 0;
            categoryAxis.gridPosition = "start";
            categoryAxis.labelRotation = 45;

            // value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.stackType = "regular";
            valueAxis.gridAlpha = 0.1;
            valueAxis.axisAlpha = 0;
            chart.addValueAxis(valueAxis);
            
            // adding graph for each manufacture
            var graph = [];
            for (var a in automobiles)
            {
                graph[a] = new AmCharts.AmGraph();
                graph[a].title = automobiles[a]['Automobile']['name'];
                graph[a].labelText = "[[value]]";
                graph[a].valueField = automobiles[a]['Automobile']['name'];
                graph[a].type = "column";
                graph[a].lineAlpha = 0;
                graph[a].fillAlphas = 1;                
                graph[a].balloonText = "<span>[[category]]</span><br><span style='font-size:14px'>[[title]]:<b>[[value]]</b></span>";
                chart.addGraph(graph[a]);
            }
            
            var legend = new AmCharts.AmLegend();
            legend.borderAlpha = 0.2;
            legend.horizontalGap = 10;
            chart.addLegend(legend);

            // WRITE
            chart.write("am-chart");          
        }
        else
        {
            $("#am-chart").html("<span class='required'>No data found</span>")
        }
        
        if (loaded)
        {
            $("#CampaignCampaignTypeId").trigger("change");
        }
    }
    
    function get_location_automobile_count(loaded)
    {
        var ad_location_list = JSON.parse('<?php echo json_encode($ad_location_list); ?>');
        var period = $("#period_type").val();
        
        $.post('<?php echo SITE_URL ?>Automobiles/ajaxGetAutomobileCountOnLocation', { ad_location_list : ad_location_list, month : period }, function (data, status)
        {
            location_automobile_count_list = JSON.parse(data);
            load_chart(loaded);
        });
    }
        
    $(document).ready(function()
    {
        loadTree(); 
        get_location_automobile_count(true);
    });
    
    $("#period_type").change(function()
    {
        get_location_automobile_count(false);
    });
    
    
</script>