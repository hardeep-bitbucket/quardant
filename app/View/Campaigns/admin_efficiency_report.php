<?php
/**
 * Campaign Efficiency Report
 * 
 * 
 * @created    06/05/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<div class="row margin-bottom-15"> 
    <div class="col-md-6">
        <h3>Detailed Efficiency Report</h3>
    </div>
    <div class="col-md-6 margin-top-20" style="text-align: right;">
        <?php
            echo $this->Html->link("<i class='fa fa-th'></i> Go to Campaigns",                
                array('action' => 'index', "admin" => true), 
                array('escape' => false, "class" => "btn btn-circle  btn-primary")
            );
        ?>        
    </div>
</div>

<div class="portlet light">
    <div class="portlet-body">
        <div class="row">            
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                   <label class="control-label col-md-4">Advertiser <span class="required">*</span></label>
                   <div class="col-md-8">                                            
                       <?php
                      echo $this->Form->input('user_id', array(
                           'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                           'options' => $advertiser_list, "value" => $advertiser_id,
                           'empty' => 'Please Select',
                           'class' => 'select2me form-control',
                       ));
                       ?>                                            
                   </div>
               </div>                       
            </div>  
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                   <label class="control-label col-md-4">Campaign <span class="required">*</span></label>
                   <div class="col-md-8">                                            
                       <?php
                      echo $this->Form->input('campaign_id', array(
                           'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                           'options' => $campaign_list, "value" => $id,
                           'empty' => 'Please Select',
                           'class' => 'select2me form-control',
                       ));
                       ?>                                            
                   </div>
               </div>                       
            </div>                    
        </div>
    </div>
</div>

<div id="campaign-block">
    
</div>

<script>
    $("#campaign_id").change(function ()
    {
        var v = $(this).val();
        if (v)
        {
            
            $("#campaign-block").load("<?php echo SITE_URL . $controller ?>/ajaxCampaignEfficiencyDetails/" + v, function (data, status){
                
            });
        }
        else
        {
            $("#campaign-block").html("");
        }
    });
    
    $("#user_id").change(function()
    {
        var v = $(this).val();
        if (!v)
        {
             v = 0;
        }
        
        $.get("<?php echo SITE_URL.$controller ?>/ajaxGetCampaignList/" + v, function(data, status)
        {
            $("#campaign_id").html("<option value=''>Please Select</option>");
            data = JSON.parse(data);
            
            for (var i in data)
            {
                $("#campaign_id").append("<option value=" + i + ">" + data[i] + "</option>");
            }
            
            $("#campaign_id").trigger("change");
        });
    });
    
    $("#campaign_id").trigger("change");
</script>