<?php
/**
 * locations Controller
 * 
 * 
 * @created    18/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("admin/summary_header");

$action_for_search = str_replace("admin_", '', $action);
?>

<div class="row">
    <div class="col-md-12">   
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">Search</div>  
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>

            </div>
            <div class="portlet-body">    
                
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->                    
                    <?php echo $this->Form->create($model, array('type' => 'GET' , 'class' => 'ajax-form form-horizontal', 'data-action' => $action_for_search)); ?>
                     
                        <div class="form-body">                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Location ID</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('LocationSearch.location_id',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $Locationlocation_id, 
                                                    'class' => 'form-control'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Name</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('LocationSearch.name',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $Locationname, 
                                                    'class' => 'form-control'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Country</label>
                                        <div class="col-md-8">                                            
                                             <?php
                                                echo $this->Form->input('LocationSearch.country_id', array(
                                                    'options' => $countryList,
                                                    'escape' => false,
                                                    'label' => false,
                                                    'div' => false,
                                                    'empty' => 'ROOT',
                                                    'value' => $Locationcountry_id,
                                                    'class' => 'select2me form-control',
                                                ));
                                              ?>                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Partner</label>
                                        <div class="col-md-8">                                            
                                             <?php
                                                echo $this->Form->input('LocationSearch.partner_id', array(
                                                    'options' => $partner_list,
                                                    'escape' => false,
                                                    'label' => false,
                                                    'div' => false,
                                                    'empty' => 'Please Select',
                                                    'value' => $Locationpartner_id,
                                                    'class' => 'select2me form-control',
                                                ));
                                              ?>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                        </div>
                        <?php echo $this->element("admin/form_search_btn"); ?>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
        
        
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Summary</div>                        
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed table-custom">
                    <thead>
                       <tr class="head" >
                            <td width="5%"  class="td-center"><?php echo $this->Paginator->sort('id', 'ID', array('class' => "ajax-page-link")); ?></td>
                            <td width="12%"><?php echo $this->Paginator->sort('location_id', 'Location ID', array('class' => "ajax-page-link")); ?></td>
                            <td width="12%"><?php echo $this->Paginator->sort('name', 'Location',array('class' => "ajax-page-link")); ?></td>
                            <td width="12%"><?php echo $this->Paginator->sort('Country.name', 'Country',array('class' => "ajax-page-link")); ?></td>
                            <td width="12%"><?php echo $this->Paginator->sort('User.name', 'Partner',array('class' => "ajax-page-link")); ?></td>
                            <td width="6%" class="td-center"><?php echo $this->Paginator->sort('partner_revenue_share_percentage', 'Shared Revenue(%)', array('class' => "ajax-page-link")); ?></td>
                            <td width="8%" class="td-center"><?php echo $this->Paginator->sort('price_per_impression', "PPI(". CURRENCY_SYMBOL . ')', array('class' => "ajax-page-link")); ?></td>
                            <td width="10%" class="td-center"><?php echo $this->Paginator->sort('setup_cost', 'Setup Cost(' . CURRENCY_SYMBOL . ')', array('class' => "ajax-page-link")); ?></td>
                            <td width="5%" class="td-center"><?php echo $this->Paginator->sort('is_exclusive', 'Exclusive',array('class' => "ajax-page-link")); ?></td>
                            <td width="5%" class="td-center"><?php echo $this->Paginator->sort('is_activated', 'Activated',array('class' => "ajax-page-link")); ?></td>
                            <td width="5%" class="td-center"><?php echo $this->Paginator->sort('is_exclusive', 'Status',array('class' => "ajax-page-link")); ?></td>                            
                            <td width="20%" class="td-center">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $record) { ?>
                            <tr>                                
                                <td class="td-center"><?php echo $record[$model]['id']; ?></td>
                                <td><?php echo $record[$model]['location_id']; ?></td>
                                <td><?php echo $record[$model]['name']; ?></td>
                                <td><?php echo $record["Country"]['name']; ?></td>
                                <td><?php echo $record["User"]['name'] . " " . $record["User"]['subname']; ?></td>
                                <td class="td-center"><?php echo $record[$model]['partner_revenue_share_percentage'] ? $record[$model]['partner_revenue_share_percentage'] : ""; ?></td>
                                <td class="td-center"><?php echo $record[$model]['price_per_impression']; ?></td>
                                <td class="td-center"><?php echo $record[$model]['setup_cost']; ?></td>
                                <td class="td-center">
                                    <?php                            
                                        $img = $record[$model]['is_exclusive'] == 1 ? "admin/status_1.png" : "admin/status_0.png";
                                        echo $this->Html->image($img,array(                                            
                                            "class" => "summary-action-icon",
                                            "escape" => false
                                        )
                                    );
                                    ?> 
                                </td>  
                                <td class="td-center">
                                    <?php                            
                                        $img = $record[$model]['is_activated'] == 1 ? "admin/status_1.png" : "admin/status_0.png";
                                        echo $this->Html->image($img,array(                                                                                                               
                                            "class" => "summary-action-icon",
                                            "escape" => false
                                        )
                                    );
                                    ?> 
                                </td>  
                        <td class="td-center">
                            <?php                            
                            $class = $record[$model]['is_active'] == 1 ? SUMMARY_ACTIVE_CLASS : SUMMARY_INACTIVE_CLASS;
                            echo $this->Html->link('', array('controller' => $controller, 'action' => 'admin_toggleStatus', $record[$model]['id']), array(
                                    "alt" => "Active",                                    
                                    "title" => $record[$model]['is_active'] == 1 ? "Active" : "InActive",
                                    "class" => "ajax-status summary-action-icon $class",
                                    "escape" => false
                                )
                            );
                            ?> 
                        </td>
                        <td class="td-center">
                            <div class="summary-action">                                
                                <?php 
                                    echo $this->Html->link(
                                            $this->Html->image("admin/summary_edit.png", array("class" => "summary-action-icon")), 
                                            array('action' => 'admin_edit', $record[$model]['id']), 
                                            array('escape' => false, 'title' => EDIT_TITLE, "class" => 'ajax-page-link')
                                        );
                                ?>
                                <span> </span>
                                <?php
                                     if (count ($record['AdLocation']) == 0)
                                     {
                                        echo $this->Html->link(
                                                "link", 
                                                array('action' => 'admin_delete', $record[$model]['id']), 
                                                array('escape' => false, 'title' => DELETE_TITLE, "class" => "summary-action-delete-link")
                                            );

                                        echo $this->Html->image("admin/summary_delete.png" , array(
                                            "class" => "summary-action-icon summary-action-delete",
                                            "data-confirm_text" => "Are you sure to delete record ?"
                                        ));
                                     }
                               ?>
                            </div>
                        </td>
                    </tr>
<?php } ?>
                    </tbody>                  
                </table>                    
                    <div>    
                      <?php echo $this->element("admin/pagination"); ?>
                    </div> 
            </div>
        </div>               

    </div>
</div>
