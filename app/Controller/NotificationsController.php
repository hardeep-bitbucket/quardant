<?php
/**
 *NotificationsController Controller
 * 
 * 
 * @created    05/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class NotificationsController extends AppController 
{
    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow("ajaxGetNotification");
    }
    
    public function ajaxGetNotification($id = 0)
    {   
        $this->layout = "ajax";
        $conditions = array(                
            array(
                "OR" => array(                    
                    array ("group_id" => null),
                    array ("group_id" => $this->user['group_id'])
                ),                             
            ),
            array(
                 "OR" => array(        
                    array ("user_id" => null),
                    array ("user_id" => $this->user['id'])                    
                )  
            )
        );
        
        
        if ($id)
        {
            $conditions['id < '] = $id;
        }
        
        
        $records = $this->{$this->modelClass}->find("all", array(
            "conditions" => $conditions,
            "limit" => 6,
            "order" => array(
                "id" => "DESC"
            ),
            "recursive" => -1
        ));
        
        $this->set(compact("records"));
    }
}