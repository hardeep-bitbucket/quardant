// JavaScript Document

function validate(){
    var canSubmit = true;
    $(".numeric").each(function(){
        var rx = /^([])|([0-9]{1,11})$/;
        if(!rx.test($(this).val()) && (!$(this).val()=="")){
            $(this).parent().find(".error-message").remove();
            $(this).after("<div class='error-message'>Must be numeric</div>")
            canSubmit=false;
        }
    });
    $('.validEmail').each(function(){
        var filter =/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
        if(!filter.test( $(this).val())){
            $(this).parent().find(".error-message").remove();
            $(this).after("<div class='error-message'>Valid email required</div>");
            canSubmit=false;
        }
    })
    $(".notEmpty").each(function(){
        if($(this).val()==""){
            $(this).parent().find(".error-message").remove();
            $(this).after("<div class='error-message'>Value required</div>");
            canSubmit=false;
		}
    });
    return canSubmit;
}
