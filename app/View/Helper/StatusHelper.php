<?php
class StatusHelper extends AppHelper {
    var $helpers = array('Html');
    
    function create($id , $status){
    	$link = null;
    	if($status){
    		$link = $this->Html->link($this->Html->image("admin/active.png") , array('action'=>'admin_toggleStatus' , $id ,0) , array('escape'=>false));
    	}else{
    		$link = $this->Html->link($this->Html->image("admin/inactive.png") , array('action'=>'admin_toggleStatus' , $id ,1) ,array('escape'=>false));
    	}
    	return $this->output($link);
    }
    
    function change($id , $status, $field){
    	$link = null;
    	if($status){
    		$link = $this->Html->link($this->Html->image("admin/active.png") , array('action'=>'changeStatus' , $id ,0, $field) , array('escape'=>false));
    	}else{
    		$link = $this->Html->link($this->Html->image("admin/inactive.png") , array('action'=>'changeStatus' , $id ,1, $field) ,array('escape'=>false));
    	}
    	return $this->output($link);
    }
}
