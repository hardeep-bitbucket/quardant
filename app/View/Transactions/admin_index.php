<?php
/**
 * Transaction Controller
 * 
 * Credit /  debit Transaction
 * 
 * @created    30/04/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

$action_for_search = str_replace("admin_", '', $action);
?>

<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h3 class="page-title">
              <?php echo $title_for_layout; ?>
            </h3>
        </div>
    </div>
</div>
<?php
    echo $this->Session->flash();
?>

<div class="row">
    <div class="col-md-12">   
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">Search</div>  
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>

            </div>
            <div class="portlet-body">    
                
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->                    
                    <?php echo $this->Form->create($model, array('type' => 'GET' , 'class' => 'ajax-form form-horizontal', 'data-action' => $action_for_search)); ?>
                     
                        <div class="form-body">                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Payment From Date</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('TransactionSearch.from_date',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $Transactionfrom_date, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Payment To Date</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('TransactionLogSearch.to_date',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $Transactionto_date, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                            
                            <div class="row">                                
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Transaction Type</label>
                                        <div class="col-md-8">                                            
                                             <?php
                                                $options = array(
                                                       StaticArray::$transaction_status_credit_transaction => "Recharge",
                                                       StaticArray::$transaction_status_debit_transaction => "Refund",
                                                 );
                                            
                                                echo $this->Form->input('TransactionSearch.type', array(
                                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                                    'options' => StaticArray::$transaction_status_types, 'empty' => "Please Select",                                                   
                                                    'value' => $Transactiontype,
                                                    'class' => 'select2me form-control',
                                                ));
                                              ?>                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Transaction No.</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('TransactionDetailSearch.transaction_no',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $TransactionDetailtransaction_no, 
                                                    'class' => 'form-control'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                            </div>     
                            
                            <div class="row">  
                                <?php if ($auth_user['group_id'] != ADVERTISER_GROUP_ID): ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Advertiser</label>
                                            <div class="col-md-8">                                            
                                                 <?php                                               
                                                    echo $this->Form->input('TransactionSearch.user_id', array(
                                                        'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                                        'options' => $advertiser_list, 'empty' => "Please Select",                                                   
                                                        'value' => $Transactionuser_id,
                                                        'class' => 'select2me form-control',
                                                    ));
                                                  ?>                                            
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <!--/span-->
                            </div> 
                        </div>
                        <?php echo $this->element("admin/form_search_btn"); ?>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
        
        
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Summary</div>                        
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed table-custom">
                    <thead>
                       <tr class="head" >                           
                            <td width="5%"  class="td-center"><?php echo $this->Paginator->sort('id', 'ID', array('class' => "ajax-page-link")); ?></td>
                            <td width="12%"><?php echo $this->Paginator->sort('User.name', 'Advertiser', array('class' => "ajax-page-link")); ?></td>
                            <td width="12%">Transaction No</td>
                            <td width="8%">Type</td>
                            <td width="8%">Payment Mode</td>
                            <td width="8%">Gateway</td>
                            <td width="15%"><?php echo $this->Paginator->sort('TransactionDetail.user_account_id', 'User Account Id'); ?></td>
                            <td width="8%"><?php echo $this->Paginator->sort('Transaction.credit', 'Recharge'); ?></td>
                            <td width="8%"><?php echo $this->Paginator->sort('Transaction.debit', 'Cash Back'); ?></td>
                            <td width="12%" class="td-center"><?php echo $this->Paginator->sort('Transaction.created_on', 'Payment DateTime'); ?></td>
                            <td width="6%" class="td-center">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $record) { ?>
                            <tr>                                
                                <td class="td-center"><?php echo $record[$model]['id']; ?></td>
                                <td><?php echo $record['User']['name'] . " " . $record['User']['subname']; ?> </td>
                                <td><?php echo $record['TransactionDetail']['transaction_no']; ?></td>
                                <td><?php echo StaticArray::$transaction_status_types[$record[$model]['type']]; ?></td>
                                <td><?php echo $record['TransactionDetail']['payment_mode'] ? StaticArray::$payment_mode[$record['TransactionDetail']['payment_mode']] : ""; ?></td>
                                <td><?php echo $record['TransactionDetail']['gateway_type'] ? StaticArray::$gateway[$record['TransactionDetail']['gateway_type']] : ""; ?></td>
                                <td><?php echo $record['TransactionDetail']['user_account_id']; ?></td>
                                <td><?php echo $record[$model]['credit']; ?></td>
                                <td><?php echo $record[$model]['debit']; ?></td>
                                <td class="td-center"><?php echo $record[$model]['created_on']; ?></td>
                                <td class="td-center">
                                    <div class="summary-action">  
                                        <?php 
                                            if ($record[$model]['type'] != StaticArray::$transaction_status_discount_transaction && $record[$model]['type'] != StaticArray::$transaction_status_invoice_transaction)
                                            {
                                                echo $this->Html->link(
                                                    $this->Html->image("admin/summary_view.png", array("class" => "summary-action-icon")), 
                                                    array('action' => 'admin_view', $record[$model]['id']), 
                                                    array('escape' => false)
                                                );
                                            }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                    <?php } ?>
                    </tbody>                  
                </table>                    
                    <div>    
                      <?php echo $this->element("admin/pagination"); ?>
                    </div> 
            </div>
        </div>               

    </div>
</div>
