<?php

/**
 * Notification Model
 * 
 * 
 * @created    05/04/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
class Notification extends AppModel 
{
    public $actsAs = array(        
        "DateTimeFormat" => array(
            "created_on" => array()
        )
    );
    
    public $sanitize = false;
}