<?php
/**
 * home campaign details
 * 
 * 
 * @created    03/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<style>
    .my-portfolio-info{
        text-transform: initial !important;
        font-size:18px !important;
    }
</style>

<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <a href="<?php echo Router::url(array('controller'=>'Campaigns', 'action'=>'index', "admin" => true), false); ?>" class="dashboard-stat dashboard-stat-light blue-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-tasks"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $campaigns['total']; ?></div>
                <div class="desc"><b>Total Campaigns</b></div>                
            </div>
        </a>
    </div>
    
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <a href="<?php echo Router::url(array('controller'=>'Campaigns', 'action'=>'index', "admin" => true, "status_type_id" => CAMPAIGN_ACTIVE_ID), false); ?>" class="dashboard-stat dashboard-stat-light green-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-tasks"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $campaigns['active']; ?></div>
                <div class="desc"><b>Active</b></div>                
                <div class="desc">
                    Spend : <?php echo CURRENCY_SYMBOL; ?><?php echo $campaigns['active_spend']; ?>                    
                </div>                
            </div>
        </a>
    </div>
    
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <a href="<?php echo Router::url(array('controller'=>'Campaigns', 'action'=>'index', "admin" => true, "status_type_id" => CAMPAIGN_SUBMITTED_ID), false); ?>" class="dashboard-stat dashboard-stat-light purple-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-tasks"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $campaigns['submitted']; ?></div>
                <div class="desc"><b>Submitted</b></div>                
                <div class="desc">
                    Budget : $<?php echo $campaigns['submitted_budget']; ?>                    
                </div>                
            </div>
        </a>
    </div>
    
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <a href="<?php echo Router::url(array('controller'=>'Campaigns', 'action'=>'index', "admin" => true, "status_type_id" => CAMPAIGN_COMPLETED_ID), false); ?>" class="dashboard-stat dashboard-stat-light red-soft round-10 margin-bottom-10">
            <div class="visual" >
                <i class="fa fa-tasks"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $campaigns['completed']; ?></div>
                <div class="desc"><b>Completed</b></div>                                
            </div>
        </a>
    </div>
</div>


<div class="row top-news margin-top-10">    
   
    <?php if (isset($best_campaign["id"])) : ?>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <a href="<?php echo Router::url(array('controller'=>'campaigns', 'action'=>'campaign_detail', "admin" => true, $best_campaign["id"]), false); ?>" class="btn green round-10  margin-bottom-10" style="height : 100px;">
                <span>Best Performing Campaign </span>            
                <span><b><?php  echo $best_campaign ? $best_campaign['name'] : ""; ?></b> </span>            
                <i class="fa fa-tasks top-news-icon"></i>
            </a>
        </div>
    <?php endif; ?>
    
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <a href="#" class="btn round-10 purple  margin-bottom-10" style="height : 100px;">
            <span>Best Performing Location </span>            
            <span><b><?php  echo $best_location ? $best_location['name'] : ""; ?></b> </span>            
            <i class="fa fa-tasks top-news-icon"></i>
        </a>
    </div>
    
     <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <a href="#" class="btn round-10 red  margin-bottom-10" style="height : 100px;">
            <span> Highest Reach Automobile </span>     
            <?php if ($best_vehicle) : ?>
                <span><b><?php echo $best_vehicle["manufacture"] . "-" . $best_vehicle["model"] . "-" . $best_vehicle["variant"]; ?></b> </span>            
            <?php endif; ?>
            <i class="fa fa-tasks top-news-icon"></i>
        </a>
    </div>
       
</div>