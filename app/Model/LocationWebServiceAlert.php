<?php

/**
 * LocationWebServiceAlert Model
 * 
 * 
 * @created    27/04/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
class LocationWebServiceAlert extends AppModel 
{
    public $actsAs = array(        
        "DateTimeFormat" => array(
            "created_on" => array()
        )
    );
    
    
    var $belongsTo = array(
        'Location' => array(
            'className' => 'Location',
            'foreignKey' => 'location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}