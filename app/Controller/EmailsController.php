<?php
/**
 * Email Controller
 * 
 * 
 * @created    31/03/2015
 * @package    ANPr
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class EmailsController extends AppController
{
    public $layout = "admin_inner";
    /*
     * @Before Filter
     */
    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow('home', 'index', 'contact_form', 'partner_form', 'request_demo_form');
        $this->_setAdminSettings();
        
        $this->getModule();
        $this->getPages();
        $this->getMetaInfo();
        $this->getUpdatesForFooter();
    }
    
    /*
     * @Admin Summary
     */
    public function admin_index()
    {
        $this->Redirect->urlToNamed();
        $conditions = $this->getSearchConditions(array(
                        array('model' => 'Page', 'field' => 'page_id', 'type' => 'integer', 'view_field' => 'page_id' ),
                        array('model' => 'Page', 'field' => 'name', 'type' => 'string', 'view_field' => 'name')                        
                )
        );

        //In case there is no condition then show only Root level records
        if( !isset($this->params['named']) || empty($this->params['named']) ) {
            $conditions['Page.page_id'] = "";
        }
        if(empty($conditions)) {
            $conditions['Page.page_id'] = "";
        }	
        $records = $this->paginate('Page', $conditions);
        
        //Add child count value to data
        foreach ($records as $key => $record) {
            $records[$key]['Page']['child_count'] = $this->Page->childCount($record['Page']['id'], true);
        }
        
        //debug($records); exit;
        $parentPageList = $this->Page->getTreeList('page_id');           
        
        $this->set('title_for_layout', 'Page Summary');
        $this->set(compact('records', 'parentPageList'));
    }
    
    /*
     * @Add Screen
     */
    public function admin_add() 
    {
        parent::add();
        $heading='Add Page';
        $reset_action = 'admin_index';
        $this->set('title_for_layout', 'Add Page');
        
        $parentPageList = $this->Page->getTreeList('page_id');
        
        $this->set(compact('heading','reset_action', 'parentPageList'));			
        $this->render('admin_form');	
    }
    
    /*
     * @Edit Screen
     */
    public function admin_edit($id = null) 
    {
        parent::edit($id);
        $heading='Edit Page';
        $reset_action = 'admin_index';		
        $this->set('title_for_layout', 'Edit Page');	
        
        $parentPageList = $this->Page->getTreeList('page_id');

        $this->set(compact('heading','reset_action', 'parentPageList'));
        $this->render('admin_form');
    }
    
    /**
    /*
     * Frontend Home
     */
    public function home()
    {
        $this->layout = 'home';
        // Get Page Content
        $slug = "home";
        $this->_getContent($slug);
        
        // Get Graphics
        $this->loadModel("Graphic");
        $graphics = $this->Graphic->find("all", array("recursive" => -1, "order" => "Graphic.display_order ASC", 
                                            "conditions" => array("Graphic.type_id" => HOME_HEADER_SLIDER_TYPE_ID, "Graphic.is_active" => 1)));
        
        // Get Page Contents
        $aboutus_content = $this->_getHomePageContent(ABOUT_US_SLUG);
        $autoad_content = $this->_getHomePageContent(AUTO_AD_SLUG);
        $howit_works_content = $this->_getHomePageContent(HOW_IT_WORKS_SLUG);
        $services_content = $this->_getHomePageContent(SERVICES_SLUG);
        $tryitout_content = $this->_getHomePageContent(TRY_IT_OUT_DEMO_SLUG);
        $request_demo_content = $this->_getHomePageContent(REQUEST_DEMO_SLUG);
        
        $this->set(compact('graphics', 'aboutus_content', 'autoad_content', 'howit_works_content', 'services_content', 
                            'request_demo_content', 'tryitout_content'));
    }
    
    /**
    /*
     * Frontend Index
     */
    public function index()
    {
        $this->layout = 'inner';
        
        // Get Page Content
        $slug = "";
        if(isset($this->params["slug"]))
        {
            $slug = $this->params["slug"];
        }
        $this->_getContent($slug);
        $this->_getIndustryTypes();
    }
    
    /*
     * @get Content for Home and Index Actions
     */
    private function _getContent($slug)
    {       
        // get Page content
        $content = $this->Page->find("first", array("recursive" => 2, "conditions" => array("Page.slug" => $slug, "Page.is_active" => 1)));
        $meta_keywords = $content["Page"]["meta_keywords"];
		$meta_description = $content["Page"]["meta_description"];     
        
        $this->set(compact("meta_keywords", "meta_description", "content", "testimonials"));
    }
    
    /*
     * @get Industry types from types table
     */
    private function _getIndustryTypes()
    {       
        $this->loadModel("Type");     
        $industry_types = $this->Type->find("list", array("conditions" => array("Type.type" => INDUSTRY_TYPES)));
        $this->set(compact("industry_types"));
    }
    
     /*
     * @get Content for Home and Index Actions
     */
    private function _getHomePageContent($slug)
    {       
        // get Page content
        $content = $this->Page->find("first", array("recursive" => -1, "conditions" => array("Page.slug" => $slug, "Page.is_active" => 1)));
        return $content;
    }
    
    /*
     * @Contact Form
     */
    public function contact_form()
    {    
        $this->autoRender = false;
        // get Page content
        if(!empty($this->request->data))
        {
            //$this->loadModel("Enquiry");
            //$data["Enquiry"] = $this->request->data["Page"];
            //$this->Enquiry->save($data);
            $from_email = $this->request->data["email"];
            $to_email = TO_EMAIL;
            $subject = "Contact " . $this->request->data["name"];
            $template = 'contact';
            $viewData = $this->request->data;
            
            if($this->sendEmail($to_email, $from_email, $subject, $template, $viewData = array(), $files = array(), $bcc = array()))	
            {
                echo "success";
            }
            else
            {
                echo "<p>There was a problem sending your message. Please try again.</p>";
            }
        }
    }
    
    /*
     * @Partner Form
     */
    public function partner_form()
    {    
         $this->autoRender = false;
        // get Page content
        if(!empty($this->request->data))
        {
            $this->loadModel("Enquiry");
            $data["Enquiry"] = $this->request->data;
            $this->Enquiry->save($data);
            $from_email = FROM_EMAIL;
            $to_email = TO_EMAIL;
            $subject ='Partner';
            $template = 'partner';
            $viewData = $this->request->data;
            if($this->sendEmail($to_email, $from_email, $subject, $template, $viewData = array(), $files = array(), $bcc = array()))	
            {
                echo "success";
            }
            else
            {
                echo "<p>There was a problem sending your message. Please try again.</p>";
            }
        }
    }
    
    /*
     * @Request Demo Form
     */
    public function request_demo_form()
    {    
         $this->autoRender = false;
        // get Page content
         $data = $this->request->data;
         
         if(empty($data["Page"]['advertisement_segments']))
         {
             echo "<p>Please Select at least one advertisement segment.</p>";
             exit;
         }
         
         foreach ($data["Page"]['advertisement_segments'] as $value)
         {
             $data[$value] = 1;
         }
         
         unset($data["Page"]);
         
         
        if(!empty($data))
        {
            $this->loadModel("Enquiry");
            $data["Enquiry"] = $data;
            if ($this->Enquiry->save($data))
            {
                $from_email = FROM_EMAIL;
                $to_email = TO_EMAIL;
                $subject ='Request A Demo';
                $template = 'request_demo';
                $viewData = $this->request->data;
                if($this->sendEmail($to_email, $from_email, $subject, $template, $viewData = array(), $files = array(), $bcc = array()))	
                {
                    echo "success";
                }
                else
                {
                    echo "<p>There was a problem sending your message. Please try again.</p>";
                }
            }
            else
            {
                echo "<p>There is problem in saving data. Please try again.</p>";
            }
        }
        exit;
    }
    
    
    private function _setAdminSettings()
    {
        $this->loadModel("AdminSetting");
        $adminSetting = $this->AdminSetting->find("first");
        $adminSetting = $adminSetting['AdminSetting'];
        
        $this->set(compact('adminSetting'));
    }
}
