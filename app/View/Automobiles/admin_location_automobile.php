<?php
/**
 * Automobile Controller
 * 
 * @created    30/03/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("admin/form_header");
?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">Location's Automobiles</div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>    
    <div class="portlet-body form"> 
        <div class="form-body form-horizontal"> 
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">Month <span class="required">*</span></label>
                        <div class="col-md-8">                                            
                            <?php
                            echo $this->Form->input('month', array(
                                'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                'options' => StaticArray::$months, "value" => $current_month,
                                'class' => 'select2me form-control'
                            ));
                            ?>                                            
                        </div>
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">Year <span class="required">*</span></label>
                        <div class="col-md-8">                                            
                            <?php
                            echo $this->Form->input('year', array(
                                'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,                                    
                                'options' => $year_list,
                                'class' => 'select2me form-control',
                            ));
                            ?>                                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">Manufacture <span class="required">*</span></label>
                        <div class="col-md-8">                                            
                            <?php
                            echo $this->Form->input('manufacture', array(
                                'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                'options' => $manufacture_list,
                                'empty' => 'Please Select',
                                'class' => 'select2me form-control'
                            ));
                            ?>                                            
                        </div>
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">Model <span class="required">*</span></label>
                        <div class="col-md-8">                                            
                            <?php
                            echo $this->Form->input('model', array(
                                'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,                                    
                                'empty' => 'Please Select',
                                'class' => 'select2me form-control',
                            ));
                            ?>                                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                   <div class="form-group">
                       <label class="control-label col-md-4">Variant <span class="required">*</span></label>
                       <div class="col-md-8">                                            
                           <?php
                           echo $this->Form->input('variant', array(
                               'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,                                    
                               'empty' => 'Please Select',
                               'class' => 'select2me form-control',
                           ));
                           ?>                                            
                       </div>
                   </div>
               </div>               
             </div>
            
            <div class="row">
                <div class="col-md-12" id='form-location'>
                    
                </div>
            </div>
        </div>
    </div>
</div>
    
<script>
    $(document).ready(function ()
    {
        function fill_select_options(id, html)
        {
            switch (id)
            {
                case "manufacture":
                    $("#model").html(html);
                break;

                case "model":
                    $("#variant").html(html);
                break;
            }
            
            $("#variant").trigger("change");
        }
        
        function fill_select_input(id, val)
        {       
            var html = "<option value=''>Please Select</option>";
            if (!val)
            {
                 fill_select_options(id, html);
            }
            else
            {
                $.get("<?php echo SITE_URL ?>Automobiles/ajaxGetAutomobiles/" + val, function (data, status)
                {
                    data = JSON.parse(data);                

                    for (var a in data)
                    {
                        html += "<option value="+ a +">" + data[a] + "</option>";
                    }

                    fill_select_options(id, html);
                });
            }
        }
        
        $("#manufacture, #model").change(function()
        {
            fill_select_input($(this).attr("id"), $(this).val());
        });
        
        $("#month, #year").change(function ()
        {
            $("#variant").trigger("change");
        })
        
        $("#variant").change(function()
        {
            var v = $("#variant").val().trim();
            
            if (v == '')
            {
                v = 0;
            }
            
            var m = $("#month").val();
            var y = $("#year").val();
            
            var url = "<?php echo SITE_URL.$controller ?>/ajaxAutmobileLocationForm/" + v + "/" + m + "/" + y
            
            $("#form-location").load(url, function(data, status)
            {
                
            });
        });
        
    });
</script>
