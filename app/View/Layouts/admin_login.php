<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.1
Version: 3.3.0
Author: KeenThemes
Website: http://www.keenthemes.com/
-->

<html lang="en">
<head>
<meta charset="utf-8"/>
<title><?php echo SITE_NAME; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>

<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="favicon.ico"/>

<?php
    echo $this->Html->css(array(
        "assets/global/plugins/font-awesome/css/font-awesome.min",
        "assets/global/plugins/simple-line-icons/simple-line-icons.min",
        "assets/global/plugins/bootstrap/css/bootstrap.min",
        "assets/global/plugins/uniform/css/uniform.default",        
        "assets/admin/pages/css/login",
        "assets/global/css/components",
        "assets/global/css/plugins",
        "assets/admin/layout/css/layout",
        "assets/admin/layout/css/themes/default",        
        "assets/admin/layout/css/custom",        
        "assets/admin_login_override",   
        "admin/admin_custom",  
    ));   
    
     echo $this->Html->script(array(
        "assets/global/plugins/jquery.min",
        "assets/global/plugins/jquery-migrate.min",
        "assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min",
        "assets/global/plugins/bootstrap/js/bootstrap.min",
        "assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min",
        "assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min",
        "assets/global/plugins/jquery.blockui.min",
        "assets/global/plugins/jquery.cokie.min",
        "assets/global/plugins/uniform/jquery.uniform.min",
        "assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min",
        "assets/global/scripts/metronic",
        "assets/admin/layout2/scripts/layout",
        "assets/admin/layout2/scripts/demo",                
       // "assets/admin/pages/scripts/login"
         
        //time zone 
        "jstz.min"
    ));
?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->

<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="outer">
    <div class="middle">
        <div class="login-inner">
            <div class="content">
                    <div class="logo">	
                        <?php             
                            echo $this->Html->link(
                                $this->Html->image('admin/logo-admin.png'), SITE_URL, array('escape' => false)
                            );
                        ?>
                    </div>  

               <?php echo $this->fetch('content'); ?>            
            </div>
            <div class="copyright">
                2015 © <?php echo SITE_NAME ; ?>
           </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function() {     
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout	
        Demo.init();
    });
    
    $(".flash-message").delay(3000).fadeOut(1000);
</script>
</body>
</html>