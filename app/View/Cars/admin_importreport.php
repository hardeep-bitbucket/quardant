<style type="text/css">
    #flashMessage
    {
        width: 778px;
    }
</style>
<aside class="body_rt">
    <?php $this->set("title_for_layout", "Import Car Manager"); ?>
    <!--Page head start-->
    <div class="page_head">
        <h1>Import Car Manager</h1>
        <div class="back">
            <?php echo $this->Html->link($this->Html->image('back.png'), array('controller' => "cars", 'action' => 'admin_index'), array('escape' => false, 'title' => BACK_TITLE)); ?>
        </div>	
    </div>
    <!--Page head end--> 
    <table width="786" border="0" cellspacing="0" cellpadding="0" class="summary">
        <tr>
            <th class="center">Total Record</th>
            <th class="center">Saved Record</th>
            <th class="center">Rejected Record</th>
        </tr>
        <?php
        if (!empty($Logs)) {
            ?>
            <tr>
                <td><?php echo $Logs['Log']['total_records']; ?></td>
                <td><?php echo $Logs['Log']['accepted']; ?></td>
                <td><?php echo $Logs['Log']['rejected']; ?></td>
            </tr>
            <?php
                } else {
                ?>
                    <tr><td colspan="5">Data not saved : Invalid Columns Order,Try again!</td></tr>
                <?php
                }
            ?>
    </table>
    <br/><br/>
    <?php 
        if(isset($Logs['Log']['message']))
        {
            $message = str_replace("\n", "<br/>", $Logs['Log']['message']); 
            echo str_replace(",", "", $message); 
        }
    ?>
    <br/><br/>
    <table width="786" border="0" cellspacing="0" cellpadding="0" class="summary">
        <tr>
            <td class="content_heading">
                <?php echo $this->Html->link('Go back to Import Manager', array('controller' => 'cars', 'action' => 'import', 'admin' => true), array('class' => 'ImportBackLink')); ?>
            </td></tr>
    </table>
</aside