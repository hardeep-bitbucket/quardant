/**
 * common ajax
 * 
 * @created    17/02/2014
 * @package    Quardant
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */

var Ajax = {
    afterLoad: function(data, status)
    {
        if (status == "success")
        {
            eval($("#main-body script").html());
        }
        else
        {
            Ajax.fail();
        }
    },
    fail: function()
    {
        $("#ajax-loader, .ajax-loader").hide();
        $("#ajax-loader-faliure").show();
        $("#ajax-loader-faliure").fadeOut(5000);
    },
    load: function(href)
    {
        if (href == '')
        {
            return;
        }

        $("#main-body").load(href, function(data, status)
        {
            Ajax.afterLoad(data, status);
        });
    },
    validateVideoFile : function (file)
    {
        if (file.type.trim() != "video/mp4")
        {
            return "mp4 video allowed only";
        }
        
        if (file.size > 1024 * 1024 * 10)
        {
            return "video file size should be less than 10Mb"
        }
        return "";
    }
//  setCookie : function (href)
//  {
//      var obj = new Date();
//      obj.setHours(obj.getHours() + 2);
//      document.cookie = "ajax_request_url=" + href + ";expires=" + obj.toUTCString();
//  }
};

$(document).ready(function()
{
    //Ajax start show loading overlay
    $(document).ajaxStart(function()
    {
        $("#ajax-loader-faliure").hide();
        $("#ajax-loader, .ajax-loader").show();
    });

    //Ajax start hide loading overlay
    $(document).ajaxComplete(function()
    {
        $("#ajax-loader, .ajax-loader").fadeOut(700);
    });


/**
    $(document).on("click", "a.ajax-page-link", function(e)
    {
        e.preventDefault();
        var href = $(this).attr("href");

        Ajax.load(href);

        return false;
    });

    $(document).on("click", "a", function(e)
    {
    });

    /**
     * required function in pagination linking
     
    $(document).on("click", "li.ajax-page-link a, span.ajax-page-link a", function(e)
    {
        e.preventDefault();
        var href = $(this).attr("href");

        Ajax.load(href);

        return false;
    });

    $(document).on("submit", ".ajax-form", function(e)
    {
        e.preventDefault();
        var href = $(this).attr("action");
        var method = $(this).attr("method");

        if (method == "post")
        {
            $("#main-body").load(href, $(this).serializeArray(), function(data, status)
            {
                Ajax.afterLoad(data, status);
            });
        }
        else
        {
            /**
             * if form method is get then we have to get all form data and merge into url             
             
            var action = $(this).data("action");
            var form_data = $(this).serialize();
            form_data = form_data.replace(/\&/g, '/');
            form_data = form_data.replace(/\=/g, ':');

            if (href.substr(href.length - 1) != "/")
            {
                href = href + "/";
            }

            var action_pos = href.indexOf(action);
            if (action_pos == -1)
            {
                href = href + action + "/";
            }
            else
            {
                href = href.substr(0, action_pos);
                href = href + action + "/";
            }

            $("#main-body").load(href + form_data, function(data, status)
            {
                Ajax.afterLoad(data, status);
            });
        }

        return false;
    });

    $(document).on('click', ".summary-action-delete", function(e)
    {
        e.preventDefault();
        var href = $(this).parent().children("a.summary-action-delete-link").attr("href");

        var data_text = $(this).data("confirm_text");
        var no_btn_text = $(this).data("confirm_no_btn_text");
        var yes_btn_text = $(this).data("confirm_yes_btn_text");
        var yes_btn_color = $(this).data("confirm_yes_btn_color");

        data_text = data_text ? data_text : "Are you sure?";
        no_btn_text = no_btn_text ? no_btn_text : "No";
        yes_btn_text = yes_btn_text ? yes_btn_text : "Yes";
        yes_btn_color = yes_btn_color ? yes_btn_color : "#DD6B55";

        swal({
            title: data_text,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: no_btn_text,
            confirmButtonColor: yes_btn_color,
            confirmButtonText: yes_btn_text,
            closeOnConfirm: true
        },
        function() {
            $("#main-body").load(href, function(data, status)
            {
                Ajax.afterLoad(data, status);
            });
        });
        return false;
    });

*/
    $(document).on('click', ".ajax-status", function(e)
    {
        e.preventDefault();
        var me = $(this);

        var href = me.attr("href");

        var active_status = me.hasClass(Constants.SUMMARY_ACTIVE_CLASS) ? 1 : 0;

        $.get(href + "/" + active_status, function(data, status)
        {
            if (data == "1" || data == 1)
            {
                if (active_status == "1")
                {
                    me.removeClass(Constants.SUMMARY_ACTIVE_CLASS);
                    me.addClass(Constants.SUMMARY_INACTIVE_CLASS);
                }
                else
                {
                    me.addClass(Constants.SUMMARY_ACTIVE_CLASS);
                    me.removeClass(Constants.SUMMARY_INACTIVE_CLASS);
                }
            }
            else
            {
                Ajax.fail();
            }
        });

        return false;
    });
});
