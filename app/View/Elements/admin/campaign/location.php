<?php
/**
 * Campaign Controller
 * 
 * Target duration tab
 * 
 * @created    26/02/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

//debug($this->validationErrors); exit;
echo $this->Form->create($model, array('type' => 'POST', 'class' => 'form-horizontal', 'id' => 'location-form', 'enctype' => "multipart/form-data"));
echo $this->Form->hidden('id', array('label' => false)); 
echo $this->Form->hidden('tab_name', array('label' => false)); 
?>

<div class="margin-5 margin-bottom-20">
    <div class="row">        
       <div class="col-md-12">
           <fieldset class="creative-div">
               <legend>Instructions For video Upload</legend>

               <div class="margin-5">
                   <ul style="list-style: upper-roman;"  class="note-color">
                       <li>Video should be mp4 type</li>
                       <li>Size of file should be less than 10MB</li>
                       <li>Duration of Video should be less than <?php echo AD_LOCAL_DURATION_LIMIT; ?> seconds</li>
                   </ul>
               </div>
           </fieldset>

           <fieldset class="url-div">
               <legend>Instructions For Youtube URL</legend>

               <div class="margin-5">
                   <ul style="list-style: upper-roman;"  class="note-color">                        
                       <li>Duration of Video should be less than <?php echo AD_URL_DURATION_LIMIT; ?> seconds</li>
                   </ul>
               </div>
           </fieldset>            
       </div>
    </div>
</div>
<div class="portlet-body margin-top-20">
    <table class="table table-bordered table-striped table-condensed table-custom">
        <thead>
            <tr class="head" >
                <td width="4%" class="td-center">Sr.</td>
                <td>name</td>
                <td width="12%" class="td-center">Price Per Impression (<?php echo CURRENCY_SYMBOL; ?>)</td>
            <?php  if ($auth_user["group_id"] == ADMIN_GROUP_ID) : ?>
                <td width="12%" class="td-center">Setup Cost</td>
                <td width="5%" class="td-center">Exclusive</td>
            <?php endif; ?>
                <td width="9%" class="td-center">Use Default Creative</td>
                <td width="10%">Creative Type</td>
                <td>Creative</td>
                <td width="5%" class="td-center">Action</td>
            </tr>        
        </thead>
        <tbody>
            <?php $i = 0;  foreach ($records as $record) {  $i++; ?>
                <tr>
                    <td class="td-center"><?php echo $i; ?></td>
                    <td><?php echo $record['Location']['name']; ?></td>
                    <td class="td-center"><?php echo $record['Location']['price_per_impression']; ?></td>
                <?php  if ($auth_user["group_id"] == ADMIN_GROUP_ID) : ?>
                    <td class="td-center"><?php echo $record['Location']['setup_cost']; ?></td> 
                    <td class="td-center">
                        <?php
                            $img = $record['Location']['is_exclusive'] == 1 ? "admin/status_1.png" : "admin/status_0.png";
                            echo $this->Html->image($img,array(
                                "alt" => "Active",                                    
                                "class" => "summary-action-icon",
                                "escape" => false
                            ));
                        ?>
                    </td>  
                <?php endif; ?>
                    <td class="td-center td-checkbox use-default-div">
                        <?php
                            echo $this->Form->hidden('AdLocation.' . $record["AdLocation"]['id'] . ".id", array('label' => false)); 

                            echo $this->Form->input('AdLocation.' . $record["AdLocation"]['id'] . '.use_default_creative', array(
                                'label' => false, 'type' => 'checkbox', 'div' => false, 'escape' => false,
                                'class' => "form-control use-default-chk"
                            ));
                        ?>
                    </td>
                    <td>
                        <?php
                            echo $this->Form->input('AdLocation.' . $record["AdLocation"]['id'] . '.ad_type', array(
                                 'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                 'options' => StaticArray::$AdType_details,
                                 'class' => 'my-select ad-type-select'                           
                            ));      

                        ?>
                    </td>
                    <td>
                        <div class='ads'>                         
                            <div class="upload-section">
                                <a href="#upload-box<?php echo $record["AdLocation"]['id'] ?>" class="btn blue btn-circle btn-upload">Upload</a>
                                <span id="<?php echo 'AdLocation_' . $record["AdLocation"]['id'] . "_upload-status"; ?>"></span>
                            </div>
                            <div class="play-section">
                                <?php
                                    echo $this->Form->hidden('AdLocation.' . $record["AdLocation"]['id'] . '.ad_id', array('escape' => false, "class" => "ad_id", "value" => $record["AdLocation"]['id']));
                                    echo $this->Form->hidden('AdLocation.' . $record["AdLocation"]['id'] . '.ad_name', array('escape' => false, "class" => "ad_name", "id" => 'AdLocation_' . $record["AdLocation"]['id'] . '_ad_name'));
                                    echo $this->Form->hidden('AdLocation.' . $record["AdLocation"]['id'] . '.ad_upload_path', array('escape' => false, "value" => AD_CREATIVE));
                                    echo $this->Form->hidden('AdLocation.' . $record["AdLocation"]['id'] . '.ad_size', array('escape' => false, "class" => "ad_size"));
                                    echo $this->Form->hidden('AdLocation.' . $record["AdLocation"]['id'] . '.ad_mime', array('escape' => false, "class" => "ad_mime"));
                                    echo $this->Form->hidden('AdLocation.' . $record["AdLocation"]['id'] . '.ad_ext', array('escape' => false, "class" => "ad_ext"));
                                    echo $this->Form->hidden('AdLocation.' . $record["AdLocation"]['id'] . '.ad_length', array('escape' => false, "class" => "ad_length"));
                                ?>
                                <a href="#" class ="ad-view view_link preview_video btn btn-circle blue"  data-url = "" data-url_type = "LOCAL">View</a>
                                <a href="<?php echo Router::url(array("controller" => "ad_locations",  "action" => "ajaxDeleteVideo"));  ?>" class ="ad-delete btn btn-circle  red" data-url="">Delete</a>
                            </div>
                        </div>
                        
                        <div class='url'>
                            <?php                       
                               echo $this->Form->input('AdLocation.' . $record["AdLocation"]['id'] . '.ad_url', array(
                                   'type' => "text", 'div' => false, 'label' => false, 'escape' => false,
                                   'class' => "my-text"
                               )); 

                               if (isset($this->data['AdLocation'][$record["AdLocation"]['id']]['ad_url']) && !empty($this->data['AdLocation'][$record["AdLocation"]['id']]['ad_url'])) 
                               {
                                   echo $this->TSHtml->videoOptions($this->data['AdLocation'][$record["AdLocation"]['id']]['ad_url'], array(
                                       'url_type' => "WEB" 
                                   ));                            
                               }
                           ?>
                        </div>
                    </td>
                    <td class="td-center">
                        <?php
                        if ($i > 1 && $can_save)
                        {
                           echo $this->Html->link(
                                       "link", 
                                       array("controller" => 'AdLocations', 'action' => 'admin_delete', $record['AdLocation']['id']), 
                                       array('escape' => false, 'title' => DELETE_TITLE, "class" => "summary-action-delete-link")
                                   );

                            echo $this->Html->image("admin/summary_delete.png" , array(
                                "class" => "summary-action-icon summary-action-delete",
                                "data-confirm_text" => "Are you sure to delete record ?"
                            ));
                        }
                        ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php echo $this->element("admin/tab_save_btn"); ?>


<?php foreach ($records as $record) : ?>
<div id="upload-box<?php echo $record["AdLocation"]['id'] ?>" style="display :none;">
    <?php 
        echo $this->element("admin/campaign/video_upload_form", array(
            "id" => 'AdLocation_' . $record["AdLocation"]['id'] . '_ad_name', 
            "type" => "location",
            "field_name" => "ad_name",
            "updateID" => 'AdLocation_' . $record["AdLocation"]['id'] . "_upload-status"   
        )); 
    ?>
</div>
<?php endforeach; ?>

<script>

$(document).ready(function()
{
    function toggle_disable_inputs(tr)    
    {
        if (tr.find(".use-default-chk").prop("checked"))
        {
            tr.find(".ad-type-select").attr("disabled", true);
        }
        else
        {
            tr.find(".ad-type-select").removeAttr("disabled");
        }
        
        if (tr.find(".ad-type-select").prop("disabled"))
        {
            tr.find(".url input").attr("disabled", true).removeAttr("required");
            tr.find(".ads input").attr("disabled", true).removeAttr("required");  
            tr.find(".ads").hide();
            tr.find(".url").hide();
        }
        else if (tr.find(".ad-type-select").val() == "1")
        {
             tr.find(".url input").attr("disabled", true).removeAttr("required");            
             tr.find(".ads input").removeAttr("disabled")
             tr.find(".ads").show();
             tr.find(".url").hide();
        }
        else if (tr.find(".ad-type-select").val() == "2")
        {
            tr.find(".ads input").attr("disabled", true).removeAttr("required");         
            tr.find(".url input").removeAttr("disabled").attr("required", true);
            tr.find(".ads").hide();
            tr.find(".url").show();
        }        
    }
    
    $(".use-default-chk").change(function ()
    {
        var tr = $(this).parents("tr");
        toggle_disable_inputs(tr);
    });

    $(".ad-type-select").change(function()
    {
        var tr = $(this).parent().parent();
        toggle_disable_inputs(tr);
    })

    $(".ad-type-select").trigger("change");
    
    /////////////////////////
    $(".btn-upload").fancybox({
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'speedIn': 600,
        'speedOut': 200,
        'overlayShow': false
    });

    $(".ad-delete").click(function(e)
    {
        e.preventDefault();

        var me = $(this);
        confirm_box
        ({
            title : "Do you want to delete creative permanently",
            onConfirmCallBack : function ()
            {
                var data = {
                    file : me.attr("data-url"),
                    id : me.parents(".ads").find(".ad_id").val()
                };
                
                $.post(me.attr("href"), data, function(data, status)
                {
                    if (data == "1")
                    {
                        warn("Creative deleted successfully", { type : "success", desc : ""});
                        var id = "#" + me.parents(".ads").find(".ad_name").attr("id");
                        set_ad_video_inputs(id,  {}, "");                    
                    }
                    else
                    {
                        warn("Failed to delete the creative, Please try again");
                    }
                });                     
            }
        });


        return false;
    });
    
    // submit form
    $("#location-form").submit(function(e, data)
    {
        var result = true;
        var len = $(".ad_name:enabled").length;
        
        for ( var i = 0; i < len; i++)
        {
            if (!$(".ad_name").val())
            {
                result = false;
            }
        }
        
        if (!result)
        {
            warn("Please upload the creative");
            return false;
        }
    })

    //loading default value and trigger events
    $(".ad_name").each(function()
    {
        set_ad_video_inputs("#" + $(this).attr("id"), {}, $(this).val());
    });
});


function toggle_upload(field_id)
{
    if ($(field_id).val())
    {
        $.fancybox.close();
        $(field_id).parents(".ads").find(".play-section").show();
        $(field_id).parents(".ads").find(".upload-section").hide();
    }
    else
    {
        $(field_id).parents(".ads").find(".play-section").hide();
        $(field_id).parents(".ads").find(".upload-section").show();
    }
}

function set_ad_video_inputs(field_id, data, file)
{
    $(field_id).val(file);
    $(field_id).parents(".ads").find(".ad-view").attr("data-url", '/<?php echo AD_CREATIVE ?>' + file);
    $(field_id).parents(".ads").find(".ad-delete").attr("data-url", '<?php echo AD_CREATIVE ?>' + file);
        
    if (data && Object.keys(data).length > 1)
    {
        $(field_id).parents(".ads").find(".ad_size").val(data.size);
        $(field_id).parents(".ads").find(".ad_mime").val(data.type);
        $(field_id).parents(".ads").find(".ad_ext").val(data.ext);
        $(field_id).parents(".ads").find(".ad_length").val(data.length);
    }

    toggle_upload(field_id);
}

function upload_update_callback(field_id, update_id, status, text, data)
{
    field_id = "#" + field_id;
    $("#" + update_id).html(text);

    if (status == 1)
    {
        set_ad_video_inputs(field_id, data, data.file);
    }
}


</script>