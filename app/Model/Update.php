<?php

/**
 * @package    Update
 * 
 * 
 * @created    17/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */
class Update extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'title' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => TITLE_REQUIRED_MESSAGE)
        ),
        'date' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => DATE_REQUIRED_MESSAGE)
        )
    );

}
