<?php
/**
 * Module Controller
 * 
 * 
 * @created    24/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("admin/summary_header");
$action_for_search = str_replace("admin_", '', $action);
?>

<div class="row">
    <div class="col-md-12">   
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">Search</div>  
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>

            </div>
            <div class="portlet-body">    
                
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->                    
                    <?php echo $this->Form->create($model, array( 'type' => 'GET' , 'class' => 'ajax-form form-horizontal', 'data-action' => $action_for_search)); ?>
                     
                        <div class="form-body">                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Title</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('ModuleSearch.title', array(
                                                'type' => "text", 'label' => false, 'div' => false, "escape" => false,
                                                'value' => $Moduletitle, 
                                                'class' => 'form-control')); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <?php echo $this->element("admin/form_search_btn"); ?>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Summary</div>                        
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed table-custom">
                    <thead>
                       <tr class="head" >
                            <td width="5%" class="td-center"><?php echo $this->Paginator->sort('id', 'ID', array('class' => "ajax-page-link")); ?></td>
                            <td><?php echo $this->Paginator->sort('title', 'Title', array('class' => "ajax-page-link")); ?></td>                            
                            <td width="5%"  class="td-center">Status</td>
                            <td width="10%" class="td-center">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $record) { ?>
                            <tr>
                                <td  class="td-center"><?php echo $record['Module']['id']; ?></td>  
                                <td><?php echo $record['Module']['title']; ?></td>  
                                <td class="td-center">
                                    <?php                            
                                    $class = $record[$model]['is_active'] == 1 ? SUMMARY_ACTIVE_CLASS : SUMMARY_INACTIVE_CLASS;
                                    echo $this->Html->link('', array('controller' => $controller, 'action' => 'admin_toggleStatus', $record[$model]['id']), array(
                                            "alt" => "Active",                                    
                                            "title" => $record[$model]['is_active'] == 1 ? "Active" : "InActive",
                                            "class" => "ajax-status summary-action-icon $class",
                                            "escape" => false
                                        )
                                    );
                                    ?> 
                                </td>
                                <td class="td-center">
                                    <div class="summary-action">                                
                                        <?php 
                                            echo $this->Html->link(
                                                    $this->Html->image("admin/summary_edit.png", array("class" => "summary-action-icon")), 
                                                    array('action' => 'admin_edit', $record[$model]['id']), 
                                                    array('escape' => false, 'title' => EDIT_TITLE, "class" => 'ajax-page-link')
                                                );
                                        ?>
                                        <span> </span>
                                        <?php
                                            /*
                                            echo $this->Html->link(
                                                    "link", 
                                                    array('action' => 'admin_delete', $record[$model]['id']), 
                                                    array('escape' => false, 'title' => DELETE_TITLE, "class" => "summary-action-delete-link")
                                                );

                                            echo $this->Html->image("admin/summary_delete.png" , array(
                                                "class" => "summary-action-icon summary-action-delete",
                                                "data-confirm_text" => "Are you sure to delete record ?"
                                            ));*/
                                       ?>
                                    </div>
                                </td>
                        </tr>
                    <?php } ?>      
                    </tbody>                  
                </table>                    
                    <div>    
                      <?php echo $this->element("admin/pagination"); ?>
                    </div> 
            </div>
        </div>               

    </div>
</div>