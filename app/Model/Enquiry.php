<?php

/**
 * Enquiry Model
 * 
 * 
 * @created    23/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class Enquiry extends AppModel {

    /**
     * Behaviours
     */
    public $actsAs = array('DateFormat' => array('start_date', 'end_date'));

    /**
     * Server Validations
     */
    public $validate = array();
    
    public $belongsTo = array(
        'Type' => array(
            'className' => 'Type',
            'foreignKey' => 'type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}

?>