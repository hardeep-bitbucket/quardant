<?php
/**
 * ajax location summary
 * 
 * 
 * @created    30/03/2015
 * @package    ANPR
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal', "action" => "ajaxAutmobileLocationFormSave"));
echo $this->Form->hidden('variant_id', array('label' => false, 'div' => false, 'escape' => false, "value" => $variant_id));
?>

<div class="row">
    <div class="col-md-12" id="msg">
        
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Summary</div>                        
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed table-custom">
                    <thead>
                       <tr class="head" >
                            <td width="5%"  class="td-center">Sr.</td>
                            <td width="15%">Location</td>
                            <td width="30%">Address</td>
                            <td width="5%" class="td-center">Car Count</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 0; foreach ($records as $record) {  $i++;?>
                            <tr>
                                <td  class="td-center"><?php echo $i; ?></td>                            
                                <td><?php echo $record["Location"]['name']; ?></td>
                                <td><?php echo $record["Location"]['address']; ?></td>
                                <td>
                                <?php                                    
                                    echo $this->Form->input($record['Location']['id'] , array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control numeric location-count', "data-location_id" => $record['Location']['id'],
                                        "value" => isset($record['0']['total_count']) && $record['0']['total_count'] ?  $record['0']['total_count'] : 0
                                    ));
                                ?> 
                                </td>                       
                        </tr>
                    <?php } ?>
                    </tbody>                  
                </table>                    
                <div>    
                  <?php echo $this->element("admin/form_save_btn"); ?> 
                </div> 
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#AutomobileAjaxAutmobileLocationFormSaveForm").submit(function()
        {
            var action = $(this).attr("action");            
            var data = {};
            data.total_count = {};
            data.variant_id = '<?php echo $variant_id ?>';
            data.month = '<?php echo $month; ?>';
            data.year = '<?php echo $year; ?>';
            
            $(".location-count").each(function()
            {
                var id = $(this).data("location_id");
                data.total_count[id] = $(this).val();
            });
            
            
            $.post(action, {data : data}, function (result, status)
            {
                if (result == 1 || result == "1")
                {
                    $("#msg").load("<?php echo SITE_URL.$controller; ?>/ajaxSuccessMessage");
                }
                else
                {
                    $("#msg").load("<?php echo SITE_URL.$controller; ?>/ajaxFailureMessage");
                }
            });
            
            return false;
        });
    })
</script>