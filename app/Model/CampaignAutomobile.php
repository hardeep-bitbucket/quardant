<?php
/**
 * CampaignAutomobile Model
 * 
 * 
 * @created    27/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     HARDEEP
 */

class CampaignAutomobile extends AppModel 
{
  
    /**
    *Associations
    */
    var $belongsTo = array(       
        'Campaign' => array(
                'className' => 'Campaign',
                'foreignKey' => 'campaign_id',
                'conditions' => '',
                'fields' => '',
                'order' => ''
            ),
        'Automobile' => array(
                'className' => 'Automobile',
                'foreignKey' => 'automobile_id',
                'conditions' => '',
                'fields' => '',
                'order' => ''
            )
        );   
    
   
}

?>