<?php
/**
 * Campaign Email template - submitted
 * 
 * @created    01/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>
<pre>
Hello <b> <?php echo $records['User']['name'] . " " . $records['User']['subname']; ?> </b>,<br/><br/>
    

Congratulations! Your campaign <b> <?php echo $records['Campaign']['name'] ?> </b> has now finished. You have leveraged ANPRMotion’s platform to deliver XX targeted images in YY different locations. <br/><br/>

For more in-depth analytics on your campaign, please visit your campaign dashboard at the following link: <br/><br/>

<a href="<?php echo DOMAIN; ?>/admin"><?php echo DOMAIN; ?>/admin</a>

<br/><br/>
<?php echo $content['footer']; ?>