Hello <?php echo $record['User']['name'] ?>,
<br/><br/>
Thank you for filling out your registration details with ANPRMotion. One of our associates will be in touch with you within 36 hours with more information about your application.

<br/><br/>
<?php
    echo $this->Html->link('click here', SITE_URL . 'users/activation/' . $record['confirmationCode']);
    echo " to activate your account. ";
?>

<br/><br/>
<?php echo $content['footer']; ?>
