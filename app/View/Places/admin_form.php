<?php 
/**
 * Form Places
 * 
 * 
 * @created    19/02/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("admin/form_header");

?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal'));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            ?>
            <div class="form-body">                            
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Parent Category <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php                                
                                    echo $this->Form->input('place_id', array(
                                        'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                        'options' => $categoriesList,                                         
                                        'empty' => 'ROOT',                                        
                                        'class'=>'form-control',
                                    ));
                                
                                ?>                                            
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Name <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('name', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Active</label>
                            <div class="col-md-1 form-checkbox">
                                <?php echo $this->Form->input('is_active', array('label' => false, 'type' => 'checkbox', 'class' => "form-control form-checkbox")); ?>
                            </div>                                        
                        </div>
                    </div>
                </div>
                <!--/row-->
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>