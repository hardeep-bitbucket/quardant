<?php
/**
 * Add & edit Graphics
 * 
 * 
 * @created    24/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep 
 */
echo $this->element("admin/form_header");
?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal', "enctype" => "multipart/form-data"));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            ?>
            <div class="form-body">    
                
                <h3 class="form-section">General Details</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Type <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('type_id', array(
                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                    'options' => $graphic_types,
                                    'empty' => 'Please Select',
                                    'class' => 'select2me form-control',
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Title</label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('title', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Logo</label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('logo', array(
                                    'type' => 'file', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => '',
                                )); 
                                
                                if (isset($this->data['Graphic']['logo']) && !empty($this->data['Graphic']['logo']) && is_string($this->data['Graphic']['logo'])) 
                                {
                                    echo $this->TSHtml->imageOptions(SITE_PATH . GRAPHIC_LOGO_IMAGES . "/" . $this->data['Graphic']['logo'], array('required' => false));
                                    echo $this->Form->input('Graphic.image.remove', array('type' => 'hidden', 'value' => ''));
                                }
                                ?>
                                &nbsp;&nbsp;&nbsp;(Type: jpeg/png, Dimensions: 200X200)                                                                     
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Image</label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('image', array(
                                    'type' => 'file', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => '',
                                )); 
                                if (isset($this->data['Graphic']['image']) && !empty($this->data['Graphic']['image']) && is_string($this->data['Graphic']['image'])) 
                                {
                                    echo $this->TSHtml->imageOptions(SITE_PATH . GRAPHIC_IMAGES . "/" . $this->data['Graphic']['image'], array('required' => false));
                                    echo $this->Form->input('Graphic.image.remove', array('type' => 'hidden', 'value' => ''));
                                }
                                ?> 
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Link</label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('link', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => 'form-control',
                                ));
                                ?>    
                                <span>&nbsp;&nbsp;&nbsp;&nbsp;(eg: http://example.com)</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Link Text</label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('link_text', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <h3 class="form-section">Other Details</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Display Order</label>
                            <div class="col-md-8">
                                <?php 
                                echo $this->Form->input('display_order', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>                                        
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Description</label>
                            <div class="col-md-8">
                                <?php echo $this->Form->input('description', array('label' => false, 'rows' => '3', 'cols' => 3, 'class' => 'form-control')); ?>                                             
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Active</label>
                            <div class="col-md-1 form-checkbox">
                                <?php echo $this->Form->input('is_active', array('label' => false, 'type' => 'checkbox', 'class' => "form-control form-checkbox")); ?>
                            </div>                                        
                        </div>
                    </div>
                </div>
                <!--/row-->
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>