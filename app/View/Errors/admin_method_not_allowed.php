<?php
/**
 * Errors Controller
 * 
 * 
 * @created    17/03/2015
 * @package    ANPR
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

?>
<div class="row">
    <div style="width:500px; margin: auto;">
        <div class="pull-left" style="font-size: 62px; color: #ec8c8c; width: 25%;">
             <?php echo ERROR_NON_AUTHORISED; ?>
        </div>
        <div class="pull-left details" style="width: 70%;">
            <h3>You are not authorized to access this page</h3>
            <p>
                <a href="<?php echo SITE_URL ?>admin">click here to login</a>
            </p>
        </div>
    </div>
</div>