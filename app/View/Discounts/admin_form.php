<?php
/**
 * Add & edit Discounts
 * 
 * 
 * @created    23/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep 
 */
echo $this->element("admin/form_header");
?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal'));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            ?>
            <div class="form-body">
                
                <h3 class="form-section">General Details</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Discount Type <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('discount_type', array(
                                        'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                        'options' => $discountTypes,
                                        'empty' => 'Please Select',
                                        'class' => 'select2me form-control',
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Discount For <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                    echo $this->Form->input('discount_for', array(
                                        'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                        'options' => $discountFor,
                                        'empty' => 'Please Select',
                                        'class' => 'select2me form-control',
                                    ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Discount Code <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('discount_code', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                        
                                        'class' => 'form-control',
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                     
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Discount Percentage</label>
                            <div class="col-md-8">
                               <?php
                                    echo $this->Form->input('discount_percentage', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                        
                                        'class' => 'form-control',
                                    ));
                                ?>    
                            </div>
                        </div>
                    </div>
                </div>
                
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Discount amount</label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('discount_amount', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                        
                                        'class' => 'form-control',
                                    ));
                                ?>                                             
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Start Date <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                    echo $this->Form->input('start_date', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                        
                                        'class' => 'form-control datepicker',
                                    ));
                                ?>    
                            </div>
                        </div>
                    </div>
                </div>
                
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">End Date<span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('end_date', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                        
                                        'class' => 'form-control datepicker',
                                    ));
                                ?>                                             
                            </div>
                        </div>
                    </div>
                </div>
                
                <h3 class="form-section">Campaign Type Details</h3>
                
                <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Campaign Trial Type <span class="required">*</span></label>
                            <div class="col-md-8">
                               <?php
                                    echo $this->Form->input('campaign_trial_type', array(
                                        'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,                                        
                                         'options' => StaticArray::$campaign_trial_types,
                                        'empty' => 'Please Select',
                                        'class' => 'select2me form-control',
                                    ));
                                ?>    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Campaign Duration Type <span class="required">*</span></label>
                            <div class="col-md-8">
                               <?php
                                    echo $this->Form->input('campaign_duration_type', array(
                                        'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,                                        
                                         'options' => $campaign_duration_type,
                                        'empty' => 'Please Select',
                                        'class' => 'select2me form-control',
                                    ));
                                ?>    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Campaign Target Automobile Type <span class="required">*</span></label>
                            <div class="col-md-8">
                               <?php
                                    echo $this->Form->input('campaign_target_automobile_type', array(
                                        'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,                                        
                                         'options' => $campaign_target_automobile_type,
                                        'empty' => 'Please Select',
                                        'class' => 'select2me form-control',
                                    ));
                                ?>    
                            </div>
                        </div>
                    </div>
                </div>
                
                <h3 class="form-section">Other Details</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">No. of Uses</label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('no_of_usage', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                        
                                        'class' => 'form-control',
                                    ));
                                ?>                                             
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Min. Order Amt.</label>
                            <div class="col-md-8">
                                <?php
                                    echo $this->Form->input('min_order_amt', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                        
                                        'class' => 'form-control',
                                    ));
                                ?>    
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Max. Order Amt.</label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('max_order_amt', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                        
                                        'class' => 'form-control',
                                    ));
                                ?>                                             
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Active</label>
                            <div class="col-md-1 form-checkbox">
                                <?php echo $this->Form->input('is_active', array('label' => false, 'type' => 'checkbox', 'class' => "form-control form-checkbox")); ?>
                            </div>                                        
                        </div>
                    </div>

                </div>
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>
<script>
    $("#DiscountDiscountType").change(function()
    {
        $("#DiscountDiscountPercentage").attr("disabled", true);
        $("#DiscountDiscountAmount").attr("disabled", true);
        if ($(this).val() == '<?php echo DISCOUNT_TYPE_AMOUNT_ID; ?>')
        {
            $("#DiscountDiscountAmount").removeAttr("disabled");                
        }
        else if ($(this).val() == '<?php echo DISCOUNT_TYPE_PERCENTAGE_ID; ?>')
        {
            $("#DiscountDiscountPercentage").removeAttr("disabled");
        }   
    });
    
    $(document).ready(function()
    {
        $("#DiscountDiscountType").trigger("change");
    });
</script>