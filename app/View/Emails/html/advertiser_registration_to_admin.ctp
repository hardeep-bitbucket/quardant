Hi Admin,<br/><br/>

New Advertiser has been registered with following details:<br/><br/>

<table border="0" cellpadding="4">
    <tr>
        <td>Name</td>
        <td><?php echo $records["User"]['name'] . " " . $records["User"]['subname']; ?></td>
    </tr>
    <tr>
        <td>Username</td>
        <td><?php echo $records["User"]['username']; ?></td>
    </tr>
    <tr>
        <td>Business Name</td>
        <td><?php echo $records["User"]['business_name']; ?></td>
    </tr>
    <tr>
        <td>Address</td>
        <td><?php echo $records["User"]['address']; ?></td>
    </tr>
    <tr>
        <td>Telephone No. / Mobile</td>
        <td><?php echo $records["User"]['landline_no']; ?></td>
    </tr>         
</table>

<br/>
Kind Regards,<br/>
<?php echo SITE_NAME; ?><br/>