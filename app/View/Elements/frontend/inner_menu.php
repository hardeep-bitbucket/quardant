<button type="button" class="navbar-toggle"><i class="fa fa-close fa-lg"></i></button>
<ul class="nav yamm navbar-nav navbar-left main-nav">
    <!-- istanzio un id univoco-->
    <?php foreach($pages as $page) { ?>
    <?php 
        if($page["Page"]["slug"] == LOCATION_SLUG)
        {
            $link = "/" . $page["Page"]["slug"] . ".html";
            if($this->params["controller"] == "locations")
            {
                $class = "active";
            }
        } 
        else
        {
            $link = SITE_URL . "#" . $page["Page"]["slug"];
            $class = "";
        }
        
        if($page["Page"]["name"] == "ANPRMotion")
        { ?>
            <style>
                .navbar-nav > li > a{
                    text-transform: none;
                }
            </style>
        <?php }
    ?>
    <li class = "<?php echo $class; ?>">
        
        <a href="<?php echo $link; ?>" id="menu_item_<?php echo $page["Page"]["name"]; ?>" title="<?php echo $page["Page"]["name"]; ?>" data-ref = "<?php echo $page["Page"]["slug"]; ?>">
            <?php echo $page["Page"]["name"]; ?>
        </a>
    </li>
    <?php } ?>   
</ul>