<?php
/**
 * Transactions Controller
 * 
 * 
 * @created    28/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
class TransactionsController extends AppController 
{
    var $layout = 'admin_inner';
    
    public function beforeFilter() 
    {
        parent::beforeFilter();        
        
        $this->Auth->allow("payPalSuccess", "payPalFailure", "ajaxGetWalletAmount");
        
        if ($this->user['group_id'] != ADVERTISER_GROUP_ID)
        {
            $this->_setAdvertiserList();
        }
    }
   
    /**
     * summary of Credit / Debit Transaction
     */
    public function admin_index()
    {
        $title_for_layout = "Transaction Manager";
        
        $this->Redirect->urlToNamed();
        
        $conditions = $this->getSearchConditions(array(
            array('model' => $this->modelClass, 'field' => 'created_on', 'type' => 'from_date', 'view_field' => 'from_date'),
            array('model' => $this->modelClass, 'field' => 'created_on', 'type' => 'to_date', 'view_field' => 'to_date'),
            array('model' => $this->modelClass, 'field' => 'type', 'type' => 'integer', 'view_field' => 'type'),
            array('model' => $this->modelClass, 'field' => 'user_id', 'type' => 'integer', 'view_field' => 'user_id'),
            array('model' => 'TransactionDetail', 'field' => 'transaction_no', 'type' => 'string', 'view_field' => 'transaction_no'),
        ));
        
        
        if ($this->user['group_id'] == ADVERTISER_GROUP_ID)
        {
            $conditions["Transaction.user_id"] = $this->user['id'];            
        }
        
        $this->{$this->modelClass}->contain(array(               
            "User" => array(
                "fields" => array(
                    "id", "name", "subname"
                )
            ),
            "TransactionDetail" => array(
                "fields" => array(
                    "transaction_no", "payment_mode", "gateway_type", "user_account_id"
                )
            )
        ));
        
        $records = $this->paginate($this->modelClass, $conditions);
        
        $this->set(compact('records', 'title_for_layout'));
    }
    
    
    public function admin_view($id)
    {
        if ($id)
        {
            $this->{$this->modelClass}->contain(array(
                "Campaign" => array(
                    "fields" => array(
                        "id", "name"
                    )
                ),
                "User" => array(
                    "fields" => array(
                        "id", "name", "subname"
                    )
                ),
                "Invoice",
                "TransactionDetail" => array(
                    "fields" => array(
                        "transaction_no", "payment_mode", "gateway_type", "user_account_id", "description"
                    )
                )
            ));
            
            $record = $this->{$this->modelClass}->find("first", array(
                "conditions" => array("Transaction.id" => $id)
            ));
            
            $this->set(compact('record'));
            
            if ($record["Transaction"]['type'] == StaticArray::$transaction_status_invoice_transaction)
            {
                $this->render("admin_Invoice_view");
            }            
        }        
    }
    
    
    /**
     * action hit by paypal
     */
    public function payPalSuccess()
    {
        if (isset($_POST['item_number']) && isset($_POST['payment_status']))
        {
            if (!in_array(strtolower($_POST['payment_status']), array("completed", "processed", "created")))
            {
                $this->redirect(array("action" => "payPalFailure" , "admin" => true));
            }
            
            $data[$this->modelClass] = array(  
               "user_id" => $_POST['item_number'],                              
               "type" => StaticArray::$transaction_status_credit_transaction,
               "credit" => $_POST['payment_gross'],
            ); 
            
            $db = $this->{$this->modelClass}->getdatasource();        
            $db->begin();
            
            $result = false;
            
            if ($this->{$this->modelClass}->save($data))
            {
                $data["TransactionDetail"] = array(                     
                    "transaction_id" => $this->{$this->modelClass}->id,
                    "transaction_no" => $_POST['txn_id'],
                    "payment_mode" => StaticArray::$payment_mode_online, //online
                    "gateway_type" => StaticArray::$gateway_papal, //paypal
                    "user_account_id" => $_POST['payer_email'],                    
                    "total_amount" => $_POST['payment_gross'],
                    "gateway_response" => urldecode(serialize($_POST))
                 ); 
                    
                 $result = $this->{$this->modelClass}->TransactionDetail->save($data);
                    
                 if ($result)
                 {
                    // increase user's amount 
                    $result = $this->_updateWalletAmount($_POST['item_number'], $_POST['payment_gross']);
                 }
            }
            
            if ($result)
            {
                $db->commit();
                $this->Session->setFlash('Transaction Successfully done', 'flash_success');
                
                $this->redirect(array("action" => "view", $this->{$this->modelClass}->id, "admin" => true));
            }
            else
            {
                $db->rollback();
            }
        }
        
        $this->redirect(array("action" => "payPalFailure"));
    }
    
    /**
     * action hit by paypal
     */
    public function payPalFailure()
    {
        $this->Session->setFlash('Transaction failed', 'flash_failure');
        
        $this->redirect(array("action" => "index", $this->{$this->modelClass}->id , "admin" => true));
    }
    
    /**
     * refund the payment
     */
    public function admin_advertiser_refund()
    {
        $heading = "Wallet Cash Back";
        
        if ($this->request->is(array("post", "put")))
        {
            $result =  true;
            
            if (!$this->request->data[$this->modelClass]['paypal_id'])
            {
                $result = false;
                $this->{$this->modelClass}->validationErrors['paypal_id'] = "Please Enter PayPal ID";
            }
            
            if (!$this->request->data[$this->modelClass]['refund_amount'])
            {
                $result = false;
                $this->{$this->modelClass}->validationErrors['refund_amount'] = "Please Enter Refund Amount";
            }
            
            if ($result && !is_numeric($this->request->data[$this->modelClass]['refund_amount']))
            {
                $result = false;
                $this->{$this->modelClass}->validationErrors['refund_amount'] = "Please Enter Numeric Only";
            }
            
            
            if ($result && $this->request->data[$this->modelClass]['refund_amount'] <= 0)
            {
                $result = false;
                $this->{$this->modelClass}->validationErrors['refund_amount'] = "Please Enter greater than 0";
            }
            
            if ($result)
            {   
                if ($this->request->data[$this->modelClass]['refund_amount'] > $this->user['wallet_amount'])
                {
                    $result = false;
                    
                    $this->{$this->modelClass}->validationErrors['refund_amount'] = "you Can't refund amount more than Wallet amount";
                }
            }
            
            if ($result)
            {
                $paypal = new PayPal(PAYPAL_ENVIORMENT);

                $response = $paypal->Refund(array(             
                    "RECEIVERTYPE" => "EmailAddress",
                    "L_EMAIL0" => $this->request->data[$this->modelClass]['paypal_id'],
                    "L_AMT0" => $this->request->data[$this->modelClass]['refund_amount'],            
                ));
                
                if (strtolower($response["ACK"]) == "success")
                {
                    $data[$this->modelClass] = array(
                        "type" => StaticArray::$transaction_status_debit_transaction,
                        "user_id" => $this->user['id'],
                        "debit" => $this->request->data[$this->modelClass]['refund_amount']
                    );
                    
                    
                    $db = $this->{$this->modelClass}->getdatasource();        
                    $db->begin();
            
                    if ($this->{$this->modelClass}->save($data))
                    {
                        $data["TransactionDetail"] = array(
                            "transaction_id" => $this->{$this->modelClass}->id,
                            "transaction_no" => $response["CORRELATIONID"],
                            "payment_mode" => StaticArray::$payment_mode_online,
                            "gateway_type" => 1,
                            "user_account_id" => $this->request->data[$this->modelClass]['paypal_id'],
                            "total_amount" => $this->request->data[$this->modelClass]['refund_amount'],
                            "gateway_response" => urlencode(serialize($response))
                        );
                            
                        $result = $this->{$this->modelClass}->TransactionDetail->save($data);
                        
                        if ($result)
                        {
                            $result = $this->_updateWalletAmount($this->user['id'], "-" . $this->request->data[$this->modelClass]['refund_amount']);
                        }
                    }
                    else
                    {
                        $result = false;
                    }
                    
                    if ($result)
                    {
                        $db->commit();
                        
                        $this->Session->setFlash('Transaction Successfully done', 'flash_success');
                
                        $this->redirect(array("action" => "view", $this->{$this->modelClass}->id, "admin" => true));
                    }
                    else
                    {
                        $db->rollback();
                        
                        $this->Session->setFlash('Transaction Success ! But Failed to save data', 'flash_failure');
                
                        $this->redirect(array("action" => "index", "admin" => true));
                    }
                }
                else
                {
                    debug($response); exit;
                    $this->Session->setFlash('Transaction Successfully done', 'flash_failure');
                }
            }
        }
        
        $this->set(compact("heading"));
    }    
    
    /**
     * recharge by admin
     */
    public function admin_recharge_wallet()
    {
        $heading = "Wallet Recharge";
        
        $this->request->data[$this->modelClass]["type"] = StaticArray::$transaction_status_credit_transaction;
        
        $this->_recharge_refund();
        
        $this->set(compact("heading"));
        $this->render("admin_recharge_refund");
    }
    
    /**
     * refund by admin
     */
    public function admin_refund_wallet()
    {
        $heading = "Wallet Refund";
        
        $this->request->data[$this->modelClass]["type"] = StaticArray::$transaction_status_debit_transaction;
        
        $this->_recharge_refund();
        
        $this->set(compact("heading"));
        $this->render("admin_recharge_refund");
    }
    
    /**
     * rechrage by advertiser
     */
    public function admin_advertiser_recharge()
    {
        $heading = "Wallet Recharge";
        $this->set(compact("heading"));
    }
        
    private function _setAdvertiserList()
    {
        $advertiser_list = $this->{$this->modelClass}->User->getUserList(array(
            "group_id" => ADVERTISER_GROUP_ID
        ));
        
        $this->set(compact("advertiser_list"));
    }
    
    private function _recharge_refund()
    {
        if ($this->request->is(array("post", "put")))
        {
            $db = $this->{$this->modelClass}->getdatasource();        
            $db->begin();
            
            $result = false;
            
            $amount = 0;
            if ($this->{$this->modelClass}->save($this->request->data))
            {
                if (isset($this->request->data["Transaction"]["credit"]))
                {
                    $amount = $this->request->data["TransactionDetail"]["total_amount"] = $this->request->data["Transaction"]["credit"];
                }
                else
                {
                    $amount = $this->request->data["TransactionDetail"]["total_amount"] = $this->request->data["Transaction"]["debit"];
                    
                    $amount  = -1 * abs($amount);
                }
                
                $this->request->data["TransactionDetail"]["transaction_id"] = $this->{$this->modelClass}->id;
                
                $result = $this->{$this->modelClass}->TransactionDetail->save($this->request->data);
            }
            
            if ($result)
            {
                $result = $this->_updateWalletAmount($this->request->data["Transaction"]["user_id"], $amount);
            }
            
            if ($result)
            {
                // updateing wallet
                $db->commit();                        
                $this->Session->setFlash('Transaction Successfully done', 'flash_success');
                $this->redirect(array("action" => "view", $this->{$this->modelClass}->id, "admin" => true));
            }
            else
            {
                $db->rollback();                        
                $this->Session->setFlash('Failed to save data', 'flash_failure');
            }
        }
    }
    
       
    public function ajaxGetWalletAmount($user_id)
    {
        $data = $this->{$this->modelClass}->User->find("first", array(
            "fields" => array("id", "wallet_amount"),
            "conditions" => array("id" => $user_id),
            "recursive" => -1
        ));
        
        if ($data)
        {
            echo $data["User"]['wallet_amount'];
        }
        else
        {
            echo -1;
        }
        
        exit;
    }
}