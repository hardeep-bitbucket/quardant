<?php
/**
 * Exception Handler
 * 
 * 
 * @created    17/03/2015
 * @package    ANPR
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

App::uses('ExceptionRenderer', 'Error');

class AppExceptionRenderer extends ExceptionRenderer 
{
	public function notFound($error)
	{
	    //SessionComponent::write('error', $error->getMessage());
	    $this->controller->redirect(array('controller' => 'errors', 'action' => 'error404'));
	}
	
	public function badRequest($error) 
	{
	    $this->notFound($error);
	}

	public function forbidden($error) 
	{
	    $this->notFound($error);
	}

	public function methodNotAllowed($error) 
	{
	    $this->notFound($error);
	}

	public function internalError($error) 
	{
	   $this->notFound($error);
	}

	public function fatalError($error) 
	{     
	   $this->notFound($error);
	}

	public function databaseError($error) 
	{
	    $this->notFound($error);
	}
	public function notImplemented($error) 
	{
	    $this->notFound($error);
	}

	public function missingController($error) 
	{
	    $this->notFound($error);
	}

	public function missingAction($error) 
	{
	    $this->notFound($error);
	}

	public function missingView($error) 
	{
	    $this->notFound($error);
	}

	public function missingLayout($error) 
	{
	    $this->notFound($error);
	}

	public function missingHelper($error) 
	{
	    $this->notFound($error);
	}
	public function missingBehavior($error) 
	{
	   $this->notFound($error);
	}

	public function missingComponent($error) 
	{
	    $this->notFound($error);
	}

	public function missingTask($error) 
	{
	    $this->notFound($error);
	}

	public function missingShell($error) 
	{
	    $this->notFound($error);
	}

	public function missingShellMethod($error) 
	{
	    $this->notFound($error);
	}

	public function missingDatabase($error) 
	{
	    $this->notFound($error);
	}

	public function missingConnection($error) 
	{
	    $this->notFound($error);
	}

	public function missingTable($error) 
	{
	    $this->notFound($error);
	}

	public function missingColumn($error) 
	{
	    $this->notFound($error);
	}

	public function privateAction($error) 
	{
	    $this->notFound($error);
	}
}
?>
