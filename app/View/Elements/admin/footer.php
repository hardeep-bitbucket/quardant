<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner col-md-6">        
        Copyright © <?php echo date("Y"); echo " " . SITE_NAME; ?>. All rights reserved                
    </div>
    <div class="col-md-6" style="text-align: right;">
        <a href="http://www.inspheresolutions.com/" target="_blank">
            Powered By :
            <img src="<?php echo SITE_URL ?>img/insphere_logo.jpg">
        </a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<div class="clearfix"></div>
<div>
    <?php echo $this->element('sql_dump'); ?>
</div>

</body>
</html>
