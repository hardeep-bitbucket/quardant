<?php
/**
 * Add & edit Page
 * 
 * 
 * @created    19/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("admin/form_header");

?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&sensor=false&libraries=geometry"></script>

<script type="text/javascript">
$(function(){  
    $("select.county_list").on('change',function(){
       var v = $(this).val();
       if (v != '')
       {
           $.post('<?php echo $this->base; ?>/places/ajaxPlaceList/' + v,  function(data){
               // $("#CarStateId").html(data);
                var obj=$.parseJSON(data);
                $("#LocationStateId option").remove();
                $("#LocationStateId").append("<option value=''>Please Select</option>")
                $.each(obj,function(i,value){$("#LocationStateId").append("<option value='" + i + "'>" + value + "</option>")});
            });
       }
   });
   $("select.state_list").on('change',function(){
       var v = $(this).val();
       if (v != '')
       {
           $.post('<?php echo $this->base; ?>/places/ajaxPlaceList/' + v, function(data){
               // $("#CarStateId").html(data);
                var obj=$.parseJSON(data);
                $("#LocationCityId option").remove();
                $("#LocationCityId").append("<option value=''>Please Select</option>")
                $.each(obj,function(i,value){$("#LocationCityId").append("<option value='" + i + "'>" + value + "</option>")});
            });
       }
   });
});    
</script>  


<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->   
            <?php
                echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal location'));
                echo $this->Form->hidden('id', array('label' => false));                 
                echo $this->Form->hidden('is_client_config_change', array('label' => false, "value" => "1"));
            ?>
            
            <div class="form-body">  
                
                <h3 class="form-section">Location Details</h3>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Name <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('name', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,
                                        'class'=>'form-control '
                                        ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Location ID <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('location_id', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,
                                        'class'=>'form-control', 'readOnly'
                                        ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                </div>

                    
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Activation Key <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    $attr = "";
                                    if($action == 'admin_edit')
                                    {
                                        $attr = "readOnly";
                                    }
                                    echo $this->Form->input('activation_key', array(
                                            'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,
                                            'class'=>'form-control ', $attr
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Security Code <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php                                    
                                    echo $this->Form->input('security_code', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false, 
                                        'class'=>'form-control ', $attr
                                        ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Country <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('country_id', array(
                                        'type' => 'select' ,'label' => false,'div' => false, 'escape' => false, 
                                        'options' => $countryList,                                        
                                        'empty' => 'Please Select',                                                    
                                        'class' => 'form-control  county_list',
                                    ));
                                  ?>                                              
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">State <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                   echo $this->Form->input('state_id', array(
                                        'type' => 'select' ,'label' => false,'div' => false, 'escape' => false,
                                        'options' => $stateList,                                        
                                        'empty' => 'Please Select',                                                    
                                        'class' => 'form-control  state_list',
                                        'id' => 'LocationStateId'
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">City <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('city_id', array(
                                        'type' => 'select' ,'label' => false,'div' => false, 'escape' => false,
                                        'options' => $cityList,                                        
                                        'empty' => 'Please Select',                                                    
                                        'class' => 'form-control ',
                                        'id' => 'LocationCityId'
                                    ));
                                  ?>                                              
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Description </label>
                            <div class="col-md-8">                                            
                               <?php 
                                    echo $this->Form->input('description', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,
                                        'rows' => 3 ,
                                        'label' => false,'class'=>'form-control '
                                        )); 
                               ?>                                       
                            </div>
                        </div>
                    </div>                    
                </div>
                
                <h3 class="form-section">Partner Details</h3>
                
                <div class="row">
                    <div class="col-md-6">
                         <div class="form-group radio-btn-list">
                            <span class="col-md-4 lbl" style="text-align: right;">Shared  <span class="required">*</span></span>
                            <div class="col-md-8">
                                <?php
                                  echo $this->Form->input('is_shared', array(
                                        'type' => "radio",
                                        'options' => StaticArray::$YesNo,
                                        'div' => false,
                                        'legend' => false,                            
                                        'before' => '',
                                        'after' => '',
                                        'between' => '',
                                        'separator' => '', 
                                        'class' => "shared-radio",
                                    ));
                                ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Partner <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('partner_id', array(                                        
                                        'type' => 'select' ,'label' => false,'div' => false, 'escape' => false,                                        
                                        'label' => false,'class'=>'form-control partner-controls',
                                        "options" => $partner_list, "empty" => "Please Select"
                                    )); 
                                  ?>                                              
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Shared Revenue (%) <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('partner_revenue_share_percentage', array(                                        
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,                                        
                                        'label' => false,'class'=>'form-control partner-controls per',                                         
                                    )); 
                                  ?>                                              
                            </div>
                        </div>
                    </div>
                </div>
                
                <h3 class="form-section">Price Details</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Price Per Impression (<?php echo CURRENCY_SYMBOL; ?>) <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                               <?php 
                                    echo $this->Form->input('price_per_impression', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,                                        
                                        'label' => false,'class'=>'form-control '
                                        )); 
                               ?>                                       
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Setup Cost (<?php echo CURRENCY_SYMBOL; ?>) <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('setup_cost', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,
                                        'label' => false,'class'=>'form-control '
                                        )); 
                                  ?>                                              
                            </div>
                        </div>
                    </div>
                </div>
                
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Monthly Expenses (<?php echo CURRENCY_SYMBOL; ?>)</label>
                            <div class="col-md-8">                                            
                               <?php 
                                    echo $this->Form->input('monthly_expenses', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,                                        
                                        'label' => false,'class'=>'form-control '
                                    )); 
                               ?>                                       
                            </div>
                        </div>
                    </div>
                 </div>
                                
                
                <h3 class="form-section">Auth Details</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Username <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('username', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,
                                        'class'=>'form-control', $attr
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Password <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('password', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,
                                        'class'=>'form-control ', $attr
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                </div>
                
                <h3 class="form-section">Web Service Related Details</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Ad Download Path </label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('client_ad_download_path', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,
                                        'class'=>'form-control'
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Welcome Message</label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('client_welcome_msg', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,
                                        'class'=>'form-control '
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                </div>
                
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">No. of Ads to be send to Location <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('ws_no_of_ads', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,
                                        'class'=>'form-control numeric postive'
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                 </div>
                
                
                <h3 class="form-section">Google Map</h3>
                
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Latitude <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('latitude', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false, 
                                        'class'=>'form-control ', "readonly"
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                     
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Longitude <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('longitude', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false, 
                                        'class'=>'form-control ', "readonly"
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">Address <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('address', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false, 
                                        'class'=>'form-control'
                                    ));
                                ?>                                            
                            </div>
                            <div class="col-md-2">
                                <button id="btn-search-address" class="btn blue-soft">Search</button>
                            </div>
                        </div>
                    </div>    
                    
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div id="map-canvas" class="map-canvas"></div>                        
                    </div>
                </div>
                
                <div class="row margin-5">
                    <span class="margin-5">
                        <img src="" id="map-marker-current" />
                    </span>                    
                    <span class="margin-5">
                        Current Position
                    </span>
                    
                    <span class="margin-5">
                        <img src="" id="map-marker-old" />
                    </span>
                    
                    <span class="margin-5">
                        Old Position
                    </span>
                </div>
                
                
                <h3 class="form-section">Other Details</h3>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-4">Exclusive </label>
                            <div class="col-md-1 form-checkbox">                                            
                                <?php
                                   echo $this->Form->input('is_exclusive', array(
                                            'type' => 'checkbox' ,'label' => false,'div' => false, 'escape' => false,
                                            'class'=>'form-control', 
                                        )); 
                                  ?>                                              
                            </div>
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-4">Activated</label>
                            <div class="col-md-1 form-checkbox">                                            
                                <?php
                                    echo $this->Form->input('is_activated', array(
                                        'type' => 'checkbox' ,'label' => false,'div' => false, 'escape' => false,
                                        'class'=>'', "disabled"
                                    )); 
                                  ?>                                              
                            </div>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-4">Active </label>
                            <div class="col-md-1 form-checkbox">                                            
                                <?php
                                    if ($attr)
                                    {
                                       echo $this->Form->input('is_active', array(
                                            'type' => 'checkbox' ,'label' => false,'div' => false, 'escape' => false,
                                            'class'=>'', $attr
                                        ));  
                                    }
                                    else
                                    {
                                        echo $this->Form->input('is_active', array(
                                            'type' => 'checkbox' ,'label' => false,'div' => false, 'escape' => false,
                                            'class'=>'form-control', 
                                        )); 
                                    }
                                  ?>                                              
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php echo $this->element("admin/form_save_btn"); ?>
            </div>
        </div>
    </div>
</div>

<script>
    
    
       function setInfo()
       {
           $("#map-marker-current").attr("src", mapConfig.marker.icon.default);
           $("#map-marker-old").attr("src", mapConfig.marker.icon.old);
       }
       
       var map, geocoder, marker, infoWindow;
       
       function get_lat_lng_from_string(address)
       {
           geocoder.geocode( {address:address}, function(results, status) 
           {
                if (status == google.maps.GeocoderStatus.OK) 
                {
                    map.setCenter(results[0].geometry.location);//center the map over the result  
                    
                    map_click(results[0].geometry.location);
                } 
                else 
                {
                  warn('Address was not found');
                }
            });
       }
       
       function get_address_from_lat_lng(latlng)
        {
            geocoder.geocode({'latLng': latlng}, function(results, status) 
            {
                if (status == google.maps.GeocoderStatus.OK) 
                {
                    if (results[1]) 
                    {
                        $("#LocationAddress").val(results[1].formatted_address);
                    }
                } 
                else 
                {
                    $("#LocationAddress").val("");
                    warn("Address can't find on this position");
                }
         });
       }
       
        /**
        * Listeners for click on map any where
        */
       function map_click(latlng, e, opt)
       {
           var lat = latlng.lat(), lng = latlng.lng();

           if (typeof opt == "undefined")
           {
               opt = {};
           }

           if (typeof opt.content == "undefined")
           {
               opt.content = "Lattitude : " + lat + "<br/> Longitude : " + lng;
           }

           if(typeof marker == "undefined" || marker == null)
           {
               placeMarker(opt, latlng);
           }
           else
           {
               marker.setPosition(latlng);                     
           }
           
           setInputField(lat, lng);
       }
       
       function infoWindowOpen()
       {
            return function() {
                infoWindow.open(map, marker);
            };
       }
       
        function infoWindowClose()
        {
            return function() {
                infoWindow.close(map, marker);
            };
        }

        function placeMarker(point, latlngset)
        {   
            marker = new google.maps.Marker({
                map: map,
                position: latlngset,
                icon: typeof point.icon != "undefined" ? point.icon : mapConfig.marker.icon.default,                    
            });

            if ( typeof point['content'] != "undefined" && mapConfig.marker.info_window)
            {                                
                var content = "<div class='map-marker-window'>" + point['content'] + "</div>" ;

                infoWindow = new google.maps.InfoWindow();
                infoWindow.setContent(content);   
                
                /**
                 * adding Listeners for inflate window
                 */                    
                google.maps.event.addListener(marker, 'mouseover', infoWindowOpen);
                google.maps.event.addListener(marker, 'mouseout', infoWindowClose);                    
            }
        }

        function setInputField(lat, lng)
        {
            $(mapConfig.lat_input_id).val(lat);
            $(mapConfig.lng_input_id).val(lng);
        }
    
       function initialize()
       {
            mapConfig.lat_input_id = "#LocationLatitude";
            mapConfig.lng_input_id = "#LocationLongitude";
            mapConfig.marker.info_window = true;
            
            // call after setting all values
            setInfo();
            
            var home = new google.maps.LatLng('<?php echo INDIA_HOME_LAT ?>', '<?php echo INDIA_HOME_LNG ?>');

            infoWindow = new google.maps.InfoWindow();
            
            var mapOptions = {
                zoom: 4,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: home
            };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            geocoder = new google.maps.Geocoder();
            
            google.maps.event.addListener(map, 'click', function(e)
            {
                map_click(e.latLng, e);
                get_address_from_lat_lng(e.latLng);
            });

            // edit mode            
            var lat = $(mapConfig.lat_input_id).val().trim();
            var lng = $(mapConfig.lng_input_id).val().trim();
            
            if (lat != "" && lng != "")
            {
                console.log("lat : " +  lat + " lng : " + lng);
                map_click(new google.maps.LatLng(lat, lng), null, { icon : mapConfig.marker.icon.old});                
                map.panTo(new google.maps.LatLng(lat, lng));
                marker = null;
            }
        }
        
       $(document).ready(function()
       {
            google.maps.event.addDomListener(window, 'load', initialize);
            
            $("form.location").submit(function()
            {
                var lat = $("#LocationLatitude").val().trim();
                var lng = $("#LocationLongitude").val().trim();
                
                if (lat == "" || lng == "")
                {
                    warn("Please select a position on map");
                    return false;
                }
                
                return true;
            });
            
            $("#btn-search-address").click(function()
            {
                var v = $("#LocationAddress").val();
                if (v)
                {
                    get_lat_lng_from_string(v);
                }
                return false;
            });
            
            $(".shared-radio").change(function()
            {
                if ($(this).val() == "1")
                {
                    $(".partner-controls").removeAttr("disabled");
                }
                else
                {
                    $(".partner-controls").attr("disabled", true);
                }
            });
            
            $(".per").blur(function ()
            {
                var v = parseFloat($(this).val());
                if (v > 100)
                {
                    $(this).val(100);
                }
            });
            
            $(".shared-radio:checked").trigger("change");
       });
</script>