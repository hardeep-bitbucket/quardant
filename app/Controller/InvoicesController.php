<?php
/**
 * Invoices Controller
 * 
 * 
 * @created    21/03/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
class InvoicesController extends AppController 
{
    /*
     * @Summary Screen
     */    
    public function admin_index()
    {
        $title_for_layout = $this->modelClass. ' Summary';        
        
        $this->Redirect->urlToNamed();
        
        $conditions = array(            
            array('model' => $this->modelClass, 'field' => 'transaction_id', 'type' => 'string', 'view_field' => 'transaction_id'),    
            array('model' => $this->modelClass, 'field' => 'status', 'type' => 'integer', 'view_field' => 'status'),
            array('model' => $this->modelClass, 'field' => 'start_date', 'type' => 'from_date', 'view_field' => 'from_date'),
            array('model' => $this->modelClass, 'field' => 'end_date', 'type' => 'to_date', 'view_field' => 'to_date'),
            array('model' => $this->modelClass, 'field' => 'payment_datetime', 'type' => 'from_date', 'view_field' => 'payment_datetime_from'),
            array('model' => $this->modelClass, 'field' => 'payment_datetime', 'type' => 'to_date', 'view_field' => 'payment_datetime_to'),
            array('model' => "Campaign", 'field' => 'name', 'type' => 'integer', 'view_field' => 'campaign_name'),
            array('model' => $this->modelClass, 'field' => 'payment_mode', 'type' => 'integer', 'view_field' => 'payment_mode'),
        );
        
        if ($this->user['group_id'] != ADVERTISER_GROUP_ID)
        {
           $conditions[] = array('model' => $this->modelClass, 'field' => 'user_id', 'type' => 'integer', 'view_field' => 'user_id');
        }
        
        $conditions = $this->getSearchConditions($conditions);        
        
        if (isset($conditions['Invoice.status']) && $conditions['Invoice.status'] < 0)
        {
            $conditions['Invoice.status'] = 0;
        }
        
        if ($this->user['group_id'] == ADVERTISER_GROUP_ID)
        {
            $conditions['Invoice.user_id'] = $this->user['id'];
        }
        
        $this->{$this->modelClass}->contain(array(
            "Campaign" => array(
                "fields" => array("id", "name"),
           ),
           "User" => array(
                "fields" => array(
                    "id", "full_name"
                )
            ),
            "Transaction" => array(
                "fields" => array("id"),
                "TransactionDetail" => array(
                    "fields" => array("transaction_no", "payment_mode", "created_on")
                        
                )
            )
        ));
        
        $records = $this->paginate($this->modelClass, $conditions);
        //debug($records); exit;
        
        $this->_setAdvertiserList();
        
        $this->set(compact('records', 'title_for_layout'));
    }
    
    public function admin_edit($id)
    {
        $this->_filterData();
        
        $this->{$this->modelClass}->contain(array(
            "Campaign" => array(
                "fields" => array("id", "name"),
           ),
           "User" => array(
                "fields" => array(
                    "id", "full_name", "wallet_amount"
                )
            ),
            "Transaction" => array(
                "fields" => array("id", "debit", "created_on"),
            ),
            "DiscountTransaction" => array(
                "fields" => array("id", "credit", "created_on"),
            )
        ));
       
        $db = $this->{$this->modelClass}->getdatasource();
        
        $db->begin();
        
        $saved = parent::edit($id, null);
        

        if ($saved)
        {
            $saved = $this->{$this->modelClass}->Transaction->save($this->request->data);
            
            $deduct_amount = $this->request->data["Transaction"]['debit'];
            if ($saved && isset($this->request->data['DiscountTransaction']))
            {
                $deduct_amount -= $this->request->data['DiscountTransaction']['credit'];
                $saved = $this->{$this->modelClass}->DiscountTransaction->save($this->request->data);                
            }
            
            
            if ($saved)
            {
                $deduct_amount  = $deduct_amount > 0 ?  (-1 * abs($deduct_amount)) : abs($deduct_amount);
                
                $saved = $this->_updateWalletAmount($this->request->data["Transaction"]['user_id'], $deduct_amount);                
            }            
            
            if ($saved)
            {
                $db->commit();
                
                $this->Session->setFlash('Updated successfully.', 'flash_success');
            
                if (isset($this->request->data["Invoice"]["status"]) && $this->request->data["Invoice"]["status"])
                {
                    $this->_invoiceSaveCallBack($this->{$this->modelClass}->id, $this->request->data["Invoice"]["status"]);
                }
            }
            else
            {
                $db->rollback();
                $this->Session->setFlash('Failed to Pay Invoice, Please try after some time', 'flash_failure');                                    
            }
            
            $this->redirect(array("action" => "edit", $id, "admin" => true));
        }
        
        $heading = 'Edit ' . $this->modelClass;        
        $this->set(compact('heading'));
        $this->render('admin_form');
    }
    
        
    private function _setAdvertiserList()
    {
        $advertiser_list = $this->{$this->modelClass}->User->getUserList(array(
            "group_id" => ADVERTISER_GROUP_ID
        ));
        
        $this->set(compact("advertiser_list"));
    }
    
    /**
     * filter data to prevent enter wrong data
     */
    private function _filterData()
    {
        if ($this->request->is(array("post", "put")))
        {
            //debug($this->data); exit;
            if (isset($this->data["Invoice"]['discount_coupon']) && $this->data["Invoice"]['discount_coupon'])
            {
                $this->{$this->modelClass}->recursive = -1;
                $record = $this->{$this->modelClass}->find("first", array(
                    "fields" => array("id", "user_id", "campaign_id", "amount"),
                    "conditions" => array(
                        "id" => $this->data['Invoice']['id']
                    ),
                ));
                
                $record = $record['Invoice'];
                
                // getting discount again
                $this->loadModel("Campaign");
                $this->loadModel("Discount");
                $discount_amount = $this->getDiscount($record['user_id'], $record['campaign_id'], $this->data["Invoice"]['discount_coupon'], $record['amount']);
            }  
            else 
            {
                $discount_amount = $this->data['Invoice']['net_amount'] * $this->data['Invoice']['discount'] / 100;
                
                $record = $this->data['Invoice'];
            }
           
            if ($discount_amount > 0)
            {
                $this->request->data['DiscountTransaction'] = array(
                    "type" => StaticArray::$transaction_status_discount_transaction,
                    "user_id" => $record['user_id'],
                    "campaign_id" => $record['campaign_id'],
                    "invoice_id" => $record['id'],
                    "credit" => $discount_amount
                );
            }

            $this->request->data['Transaction'] = array(
                "type" => StaticArray::$transaction_status_invoice_transaction,
                "user_id" => $record['user_id'],
                "campaign_id" => $record['campaign_id'],
                "invoice_id" => $record['id'],
                "debit" => $record['amount']
            );
            
            if ($this->user["group_id"] == ADVERTISER_GROUP_ID)
            {
                $this->request->data['TransactionDetail'] = array(                
                    "payment_mode" => StaticArray::$payment_mode_online,
                    "total_amount" => $record['amount'],
                );
            }
            else
            {
                $this->request->data['TransactionDetail']["total_amount"] = $record['amount'];
            }
            
            //debug($this->request->data); exit;
        }
    }
    
    public function admin_export_pdf($id)
    {        
        $this->{$this->modelClass}->contain(array(
            "Campaign" => array(
                "fields" => array("id", "name"),
           ),
           "User" => array(
                "fields" => array(
                    "id", "full_name"
                )
            ),            
            "DiscountTransaction" => array(
                "fields" => array("id", "credit", "created_on"),
            )
        ));
        
        $report_data = $this->{$this->modelClass}->findById($id);
        
        $this->set(compact("report_data"));
        
        $this->createPDF("/Invoices/invoice_pdf", PDF_SAVE_PATH,  'Invoice_' . $id);
        //$this->layout = null;         $this->render("/Invoices/invoice_pdf");
    }
}