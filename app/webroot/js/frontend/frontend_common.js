// JavaScript Document
$(function() {
	//alert("runnings");
	
	//Search reset & form cancel button 
	$(".reset-button").click(function(){
		window.location = $(this).attr('url');
	});
	
	//Preview image click for image file field
	$(".preview_image").click(function(){
		
        $.fancybox.open({            
            href : $(this).attr('url'),            
            padding : 10
        });
	});
	
	//Delete image click for image file field
	$(".delete_image").click(function(){
		//alert($(this).attr('url'));
		$(this).next().val(1);
		$(this).prev().hide();
		$(this).hide();
		return false;
	});
	
	//Datepicker
	//$(".datepicker").datepicker( { dateFormat: 'dd-mm-yy' } );
	
	//Restrict view
	$(".restrict_view").click(function(){
		alert("This manager is not viewable");
		return false;
	});
	
	// Checking all checkboxes in summary screen
	$('.checkAll').click(function ()
	{
    	$(this).closest('table').find('input[checkAll=all]').prop('checked', this.checked);
	});
	
		
	// Hiding flash message div after 3 secs
	 setTimeout(function(){ 
	 $(".custom_message").hide(); 
	 }, 10000);
	
	// Employee name in User add & edit
//	$( "#UserEmployeeName" ).autocomplete({
//		source: "http://localhost/payroll/admin/users/complete",
//		change: function( event, ui ) {
//            $( "#UserEmployeeId" ).val(ui.item.id);
//      }
//	});
//	
//	// Employee name in User search
//	$( "#UserSearchEmployeeName" ).autocomplete({
//		source: "http://localhost/payroll/admin/users/complete",
//		change: function( event, ui ) {
//			 $( "#UserSearchEmployeeId" ).val(ui.item.id);
//			 
//      }
//	});
	
	// Making weekly day off 2 visible
	$("#BranchNoOfWeeklyLeaves").change(function(){
		if($("#BranchNoOfWeeklyLeaves option:selected").text()=="2"){
			$("#weekly_off_day2").css("display","block"); 
			
		}
	});
	
	// Making fields visible according to employee visa status 
	$("#EmployeeVisaStatus").change(function(){
		if($("#EmployeeVisaStatus option:selected").text()=="Expat"){
			$("#emp_local_address").css("display","block");
			$("#emp_passport_no").css("display","block"); 
			$("#emp_hongkong_id").css("display","block");
			$("#emp_hongkong_id h2 span").css("display","none");   
			$("#EmployeeHongkongId").removeClass("notEmpty");
			
		}
		if($("#EmployeeVisaStatus option:selected").text()=="Local"){
			$("#EmployeeHongkongId").addClass("notEmpty");
			$("#emp_hongkong_id").css("display","block"); 
			$("#emp_hongkong_id h2 span").css("display","inline"); 
			$("#emp_passport_no").css("display","block"); 
			
		}
		if($("#EmployeeVisaStatus option:selected").text()=="Dependent"){
			$("#EmployeeHongkongId").removeClass("notEmpty");
			$("#emp_hongkong_id h2 span").css("display","none");  
			$("#emp_hongkong_id").css("display","block"); 
			$("#emp_passport_no").css("display","block"); 
			
		}
	});
	
	
	$(".weekly").click(function(){
		alert("ok");
	});
        
    $(".flash-message").delay(4000).fadeOut(2000);
    
    $(".preview_video").click(function()
    {
        console.log($(this).data("url_type"));
        console.log($(this).data("url"));
        
        var html = "";
        if ($(this).data("url_type") == "LOCAL")
        {
            html = '<video width="740" height="500" autoplay>';
            html += '<source src="' + $(this).data("url") + '" type="video/mp4">';            
            html += 'Your browser does not support the video tag.';
            html += '</video>'; 
        }
        else
        {
            html = '<iframe width="740" height="500"';
            html += 'src="' + $(this).data("url") + '"';                           
            html += '  allowfullscreen frameborder="0"></iframe>'; 
        }
            
        $.fancybox.open(html);
	});

});

function warn(text, options)
{
    options = typeof options == "undefined" ? { type : "error" , desc : ""} : options;
    
    sweetAlert(text, options.desc, options.type);
    return false;
}
	
function refreshCaptcha() 
{
	$("#captcha_code").attr('src', '/Campaigns/ajaxCaptcha?' + (new Date()).getTime());
}

	
