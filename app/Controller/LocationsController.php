<?php

/**
 * Locations Controller
 * 
 * 
 * @created    18/12/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */

/**
 * @property Location $Location
 */
class LocationsController extends AppController {
    
    /**
     * beforeFilter
     */
    public function beforeFilter() 
    {
        parent::beforeFilter();        
        $this->Auth->allow(array(
            'index', 
            'ajaxAdsPlayedChartData', 
            'ajaxSpotFinder', 
            'ajaxGetAutomobileReach',
            "ajaxLocationLastAd", 
            "ajaxGetLocationOnLastAdPlayed"
        ));       
        
        $this->_setPartnerList();
        $countryList = $this->Location->Country->getChildList();
        
        $this->set(compact("countryList"));
    }

    /**
     * show summary data
     */
    public function admin_index() {
        $this->Redirect->urlToNamed();

        // set search perameter
        $conditions = $this->getSearchConditions(array(
            array('model' => 'Location', 'field' => 'name', 'type' => 'string', 'view_field' => 'name'),
            array('model' => 'Location', 'field' => 'country_id', 'type' => 'integer', 'view_field' => 'country_id'),
            array('model' => 'Location', 'field' => 'location_id', 'type' => 'integer', 'view_field' => 'location_id'),
            array('model' => 'Location', 'field' => 'partner_id', 'type' => 'integer', 'view_field' => 'partner_id'),
        ));

        $this->{$this->modelClass}->contain(array(
            'AdLocation' => array("fields" => array("id")),
            "State",
            "Country"
        ));
        
        $this->{$this->modelClass}->contain(array(
           "Country" => array("fields" => array("id", "name")),
           "City" => array("fields" => array("id", "name")),
           "AdLocation" => array("fields" => array("id")),
           "User" => array("fields" => array("id", "name", "subname"))
        ));

        $records = $this->paginate($this->modelClass, $conditions);

        //debug($records); exit;
        $this->set('title_for_layout', $this->modelClass . ' Summary');
        $this->set(compact('records'));
    }

    /**
     * 
     * Add New record
     * 
     */
    public function admin_add() {
        //calling function from app controller        
        parent::add();

        $this->set('title_for_layout', 'Add ' . $this->modelClass);

        $reset_action = 'admin_index';
        $heading = 'Add ' . $this->modelClass;
        $this->set(compact('reset_action', 'heading'));

        $this->_setPlaceList();                
        $this->render('admin_form');
    }

    /**
     * Updates Existing Record
     * @param type $id 
     */
    public function admin_edit($id) {
        $this->{$this->modelClass}->recursive = -1;

        //calling function from app controller
        parent::edit($id);

        $this->set('title_for_layout', 'Edit ' . $this->modelClass);
        $reset_action = 'admin_index';
        $heading = 'Edit ' . $this->modelClass;
        $this->set(compact('reset_action', 'heading'));


        $this->_setPlaceList();                
        $this->render('admin_form');
    }

    private function _setPlaceList() 
    {
        $stateList = array();
        if (isset($this->request->data[$this->modelClass]["country_id"])) {
            $stateList = $this->Location->Country->getChildList($this->request->data[$this->modelClass]["country_id"]);
        }
        
        $cityList = array();
        if (isset($this->request->data[$this->modelClass]["state_id"])) {
            $cityList = $this->Location->Country->getChildList($this->request->data[$this->modelClass]["state_id"]);
        }
        
        $this->set(compact('stateList', 'cityList'));
    }
    
    private function _setPartnerList()
    {
        $this->Location->User->recursive = -1;
        $partner_list = $this->Location->User->getUserList(array("group_id" => PARTNER_GROUP_ID));
        
        $this->set(compact("partner_list"));
    }

    /*
     * spot finder page
     * @Frontend Action Index
     */

    public function index() 
    {
        $this->layout = "inner";
        
        $this->loadModel("Page");
        $this->getModule();
        $this->getPages();
        $this->getMetaInfo();
        $this->getUpdatesForFooter();

        // Get Page Content
        $page = $this->Page->findByslug(LOCATION_SLUG);

        $points = $this->_getLocationPoints();

        $this->set(compact("page", "locations", "points"));
    }

    public function ajaxAdsPlayedChartData($location_id) 
    {
        $date = DateUtility::getFormatDateFromObj(DateUtility::addDaysInDateObj(DateUtility::getCurrentDateTimeObj(), "-20"), "Y-m-d");

        $records = $this->{$this->modelClass}->AdLocation->AdPlayedLog->find("all", array(
            "fields" => array("AdPlayedLog.*"),
            "conditions" => array(
                "AdPlayedLog.created_on >= " => $date,
                "AdPlayedLog.location_id" => $location_id
            ),
            "recursive" => -1
        ));

        $data = array();

        if ($records) {
            foreach ($records as $record) {
                $record = $record['AdPlayedLog'];
                $key = date_format(date_create($record['created_on']), "d-M");

                if (!isset($data[$key])) {
                    $data[$key] = array(
                        "title" => $key,
                        "value" => 1,
                    );
                } else {
                    $data[$key]['value'] += 1;
                }
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * get campaign location points for map
     */
    private function _getLocationPoints() 
    {
        $records = $this->_getLocationGeoPoints(NULL, NULL, NULL, 1);

        //debug($records); exit;

        $points = array();
        $i = 0;
        foreach ($records as $record) {
            $points[$i] = $record["Location"];
            $points[$i]["ad_location_count"] = count($record["AdLocation"]);
            $points[$i]["active"] = 0;
            $points[$i]["country"] = $record["Country"]["name"];
            $points[$i]["state"] = $record["State"]["name"];
            $points[$i]["city"] = $record["City"]["name"];


            if ($record["AdLocation"]) {
                foreach ($record["AdLocation"] as $adLocation) {
                    if (isset($adLocation["Campaign"]['status_type_id']) && $adLocation["Campaign"]['status_type_id'] == CAMPAIGN_ACTIVE_ID) {
                        $points[$i]["active"] ++;
                    }
                }
            }

            $i++;
        }

        return json_encode($points);
    }

    public function ajaxGetAutomobileReach($id) 
    {
        $records = $this->{$this->modelClass}->query("CALL sp_get_automobile_locations_count($id, NULL);");
       
        $records = Hash::combine($records, "{n}.AL.date", "{n}.{n}.total_count");
       
        if ($records) {
            $total = 0;
            foreach ($records as $num_count) {
                $total += (int) $num_count;
            }

            $avg = round($total / count($records), 2);

            $arr = array(
                "avg_car_reach" => $avg
            );
        } else {
            $arr = array(
                "avg_car_reach" => 0
            );
        }

        echo json_encode($arr);
        exit;
    }

    /**
     * Action used in front end on see it action' click
     */
    public function ajaxLocationLastAd() 
    {
        $this->layout = 'frontend_ajax';
    }

    /**
     * function return last ad played with all data of location based on id
     * @param json_object
     */
    public function ajaxGetLocationOnLastAdPlayed($country_id = null) 
    {
        $conditions = array();
        
        if ($country_id)
        {
            $conditions["Location.country_id"] = $country_id;
        }
        
        $id_list = $this->{$this->modelClass}->AdPlayedLog->find("list", array(
            "fields" => array("id", "id"),
            "conditions" => array(
                "AdPlayedLog.confirm_web_service_log_id > " => 0
            ),
            "recursive" => -1
        ));
        
        $id = array_rand($id_list, 1);
        ///debug($id);
          
        $this->{$this->modelClass}->AdPlayedLog->contain(array(
            "Location" => array(
                "fields" => array(
                    "name", "address", "price_per_impression", "latitude", "longitude"
                ),
                "conditions" => $conditions
            ),
            "Variant" => array(
                "fields" => "id"
            )
        ));
        
        $record = $this->{$this->modelClass}->AdPlayedLog->find("first", array(
            "fields" => array("AdPlayedLog.id", "ad_location_id", "created_on"),
            "conditions" => array(
                //"AdPlayedLog.confirm_web_service_log_id > " => 0
                "AdPlayedLog.id" => $id
            ),
            "order" => array("AdPlayedLog.id" => "DESC")
        ));

        if ($record["Location"]) 
        {
            $temp = $this->{$this->modelClass}->AdPlayedLog->Variant->getParentChildHierarchy($record["Variant"]["id"], array("name"));
                   
            $record["Automobile"]["name"] = trim($temp[0]["Variant"]["name"]) . "-" .  trim($temp[1]["Variant"]["name"]) . "-" .  trim($temp[2]["Variant"]["name"]);
                    
            $record["AdPlayedLog"]['created_on'] = DateUtility::getFormatDateFromString($record["AdPlayedLog"]['created_on'], DEFAULT_DATETIME_FORMAT);
            $record["next_id"] = $record["AdPlayedLog"]["id"] + 1;
        }
        echo json_encode($record);
        exit;
    }
}
