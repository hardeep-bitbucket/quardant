<?php

/**
 * @created    17/01/2014
 * @package    anpr
 * @copyright  Copyright (anpr) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class Campaign extends AppModel 
{
   public $campaignDays;
   public $campaignTrialDays;
     /*
     * Behaviours 
     */
    public $actsAs = array(                
//        'MeioUpload' => array(
//            'ad_name' => array(
//                'dir' => AD_CREATIVE,
//                'createDirectory' => false,
//                'allowedMime' => array('video/mp4'),
//                'allowedExt' => array('.mp4'),
//                'maxSize' => 10000000, //10 MB
//                'thumbnails' => false,                
//                'thumbnailDir' => false,   
//                "video" => array(
//                    "duration" => array(
//                        "max" => array(
//                            "length" => AD_LOCAL_DURATION_LIMIT, //seconds
//                            "error_msg" => AD_LOCAL_VIDEO_DURATION_VALIDATION_MSG
//                        )
//                    )
//                ),
//                "validations" => array(
//                    'MaxSize' => array(
//                        "message" => "Maximum Uploading size is 10 MB"
//                    ),
//                    "InvalidMime" => array(
//                        "message" => "Only mp4 files are allowed"
//                    ),
//                    "InvalidExt" => array(
//                        "message" => "Only mp4 files are allowed"
//                    ),
//                )
//            ),
//        ),
        'DateTimeFormat' => array(
            'submit_datetime' => array('out_format' => DEFAULT_DATE_FORMAT),
            'approve_datetime' => array('out_format' => DEFAULT_DATE_FORMAT),
            'active_datetime' => array('out_format' => DEFAULT_DATE_FORMAT),
            'created_on' => array('in_format' => ''),
            "start_date" => array('in_format' => DEFAULT_SQL_DATETIME_FORMAT, 'out_format' => DEFAULT_DATE_FORMAT),
            "end_date" => array('in_format' => DEFAULT_SQL_DATETIME_FORMAT, 'out_format' => DEFAULT_DATE_FORMAT)
        ),
    );

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Country' => array(
            'className' => 'Place',
            'foreignKey' => 'country_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CampaignDuration' => array(
            'className' => 'Type',
            'foreignKey' => 'duration_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CampaignType' => array(
            'className' => 'Type',
            'foreignKey' => 'campaign_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CampaignStatus' => array(
            'className' => 'Type',
            'foreignKey' => 'status_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )        
    );
    public $hasMany = array(       
        'AdLocation' => array(
            'className' => 'AdLocation',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        "CampaignAutomobile" => array(
            'className' => 'CampaignAutomobile',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        "Invoice" => array(
            'className' => 'Invoice',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        "CampaignVersionLog" => array(
            'className' => 'CampaignVersionLog',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

   
    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'user_id' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty', 'message' => REQUIRED_FIELD
            )
        ),
        'is_trial' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty', 'message' => REQUIRED_FIELD
            )
        ),
        'ad_url' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty', 'message' => REQUIRED_FIELD
            )
        ),
        'name' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty', 'message' => REQUIRED_FIELD
            ),           
        ),
        'industry_id' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty', 'message' => REQUIRED_FIELD
            )
        ),
        'country_id' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty', 'message' => REQUIRED_FIELD
            )
        ),
        'duration_type_id' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty', 'message' => REQUIRED_FIELD
            )
        ),
        'start_date' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty', 'message' => REQUIRED_FIELD
            ),            
        ),
        'campaign_type_id' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty', 'message' => REQUIRED_FIELD
            )
        ),
    );
                
                
    public function afterValidate() 
    {
        parent::afterValidate();
        
        if (!isset($this->validationErrors['start_date']))
        {
           $msg = $this->checkRange();
           if ($msg)
           {
               $this->validationErrors['start_date'][] = $msg;
               return false;
           }
        }   
		
        if (empty($this->validationErrors) && isset($this->data['Campaign']["ad_url"]))
        {
            if ($this->data['Campaign']["ad_url"])
            {
                if (is_youtube_url($this->data['Campaign']["ad_url"]))
                {
                    $this->data['Campaign']["ad_url"] = get_youtube_embed_url($this->data['Campaign']["ad_url"]);
                    if (!$this->data['Campaign']["ad_url"])
                    {
                        $this->validationErrors['ad_url'][] = "Please enter valid youtube URL";
                    }
                }
                
                $msg = $this->ValidateYouTubeURL($this->data['Campaign']["ad_url"]);
                
                if ($msg)
                {
                    $this->validationErrors['ad_url'][] = $msg;
                }
            }
        }
		
        return true;
    }
            
    public function beforeSave($options = array()) 
    { 
        if (isset($this->data["Campaign"]["ad_name"]) && !empty($this->data["Campaign"]["ad_name"]) && isset($this->data["Campaign"]["filesize"]))
        {
            $this->data["Campaign"]["ad_upload_path"] = AD_CREATIVE;
            $this->data["Campaign"]["ad_size"] = $this->data["Campaign"]["filesize"];
            $this->data["Campaign"]["ad_mime"] = $this->data["Campaign"]["mimetype"];
            
            $info =  pathinfo($this->data["Campaign"]["ad_name"]);                        
            $this->data["Campaign"]["ad_ext"] = $info["extension"];
            
            if (isset($this->data["Campaign"]['id']) && $this->data["Campaign"]['id'])
            {
                $record = $this->find("first", array(
                    "fields" => array("ad_name"),
                    "conditions" => array("Campaign.id" => $this->data["Campaign"]['id']), 
                    "recursive" => -1
                ));

                if ($record["Campaign"]["ad_name"])
                {
                    $this->meio_old_file = AD_CREATIVE . $record["Campaign"]["ad_name"];
                }
            }
        }
        
        parent::beforeSave($options);        
        
        return $options;
    }

    
    public function checkRange()
    {
        if (isset($this->data['Campaign']["duration_type_id"]))
        {
            if ($this->data['Campaign']["duration_type_id"] == StaticArray::$Campaign_duration_types["Fixed"] && isset($this->data['Campaign']["start_date"]) && isset($this->data['Campaign']["end_date"]))
            {   
                $start_date = DateUtility::getDateObj($this->data['Campaign']["start_date"]);
                $end_date = DateUtility::getDateObj($this->data['Campaign']["end_date"]);
                    
                if ($start_date > $end_date)
                {
                    return "Start date should be less than End date";
                }
                
                $days = DateUtility::compareDateObjs($start_date, $end_date, DateUtility::$DAYS);
                
                if (isset($this->data['Campaign']['is_trial']))
                {
                    if ($this->data['Campaign']['is_trial'])
                    {
                        if ($days > $this->campaignTrialDays)
                        {
                            return "In case of trial, Campaign can be create for $this->campaignTrialDays days only";
                        }
                    }
                    else if ($days < $this->campaignDays)
                    {
                        return "Start date must be $this->campaignDays  days before End date";
                    }
                }
            }
        }
        return false;
    }
    
     /**
     * just update fields, not hard delete
     * @param int $id
     * @return bool
     */
    
    public function deleteSoft($id)
    {
        $db = $this->getdatasource();
        
        $db->begin();
        
        $this->id = $id;
        
        $ad_list = $this->AdLocation->find("list", array(
            "fields" => array("id", "id"),
            "conditions" => array(
                "campaign_id" => $id
            ),            
        ));
        
        $result = true;
        if ($ad_list)
        {
            foreach($ad_list as $ad_id)
            {
                if (!$this->AdLocation->deleteSoft($ad_id))
                {
                    $result = false;
                }
            }
        }
        
        if ($result)
        {
            $result = $this->saveField("is_deleted", 1, false);
        }
        
        if ($result) 
        {
            $db->commit();            
        } else {            
            $db->rollback();            
        }
        
        return $result;
    }
    
}
