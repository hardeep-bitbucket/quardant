<?php
/**
 * ajax Spot finder location controller
 * 
 * 
 * @created    26/03/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("frontend/Locations/spot_finder");
?>