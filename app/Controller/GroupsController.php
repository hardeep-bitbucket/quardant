<?php
/**
 * Groups Controller
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */

class GroupsController extends AppController
{
    
    /*
     * @Before Filter
     */
    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow();
    }
/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Group->recursive = 0;
		$groups = $this->paginate($this->modelClass);
		$this->set(compact('groups'));
	}	

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) 
	{
		if (!$this->Group->exists($id)) {
			throw new NotFoundException(__('Invalid group'));
		}
		$options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
		$this->set('group', $this->Group->find('first', $options));
	}

/**
 * add method
 * @return void
 */
	public function add() 
	{
		parent::add();
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		parent::edit($id);
	}
}
