<?php
/**
 * Modules Controller
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */

class ModulesController extends AppController
{
    public $layout = "admin_inner";
    /*
     * @Before Filter
     */
    public function beforeFilter() 
    {
        parent::beforeFilter();
    }
    
    /*
     * @Admin Summary
     */
    public function admin_index()
    {
        $this->Redirect->urlToNamed();
        $conditions = $this->getSearchConditions(array(
                        array('model' => 'Module', 'field' => 'title', 'type' => 'string', 'view_field' => 'title')                        
                )
        );
        $records = $this->paginate('Module', $conditions);        
        
        $this->set('title_for_layout', 'Module Summary');
        $this->set(compact('records'));
    }
    
    /*
     * @Add Screen
     */
    public function admin_add() 
    {
        parent::add();
        $heading='Add Module';
        $reset_action = 'admin_index';
        $this->set('title_for_layout', 'Add Module');
        
        $this->set(compact('heading','reset_action'));			
        $this->render('admin_form');	
    }
    
    /*
     * @Edit Screen
     */
    public function admin_edit($id = null) 
    {
        parent::edit($id);
        $heading='Edit Module';
        $reset_action = 'admin_index';		
        $this->set('title_for_layout', 'Edit Module');	

        $this->set(compact('heading','reset_action'));
        $this->render('admin_form');
    }
}
