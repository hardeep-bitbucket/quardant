<?php
/**
 * flash for failure
 * 
 * 
 * @created    19/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */
?>
<div class="Metronic-alerts alert alert-danger fade in flash-message">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
    <i class="fa-lg fa fa-warning"></i>  
    <?php echo h($message); ?>
</div>

