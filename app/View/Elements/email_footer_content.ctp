<?php
/**
 * common content for all emails
 * 
 * 
 * @created    11/04/2015
 * @package    ANPT
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

Kind Regards,
<br/>
ANPRMotion Client Onboarding
<br/>
<img style="height : 40px; width : auto;" src="<?php echo SITE_URL; ?>img/frontend/email-logo-dark.png" >

