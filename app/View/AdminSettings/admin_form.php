<?php
/**
 * Add & edit Automobile
 * 
 * 
 * @created    19/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep 
 */
?>

<div class="row">   
    <div class="col-md-12">
        <div class="pull-left">
            <h3 class="page-title">
              <?php echo ucwords($model); ?> <small>Manager</small>
            </h3>
        </div>
        <div class="pull-right">
            
        </div>
    </div>
</div>

<?php
    //echo $this->element("admin/breadcrumb"); 
    echo $this->Session->flash();
?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">Web Service Settings</div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'form-horizontal'));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            echo $this->Form->hidden('is_client_config_change', array('label' => false, 'div' => false, 'escape' => false, "value" => 1));
            ?>
            <div class="form-body">                 
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Web service URL <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('ws_url', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control'
                                    ));
                                ?>                                         
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Ftp Host <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                    echo $this->Form->input('ws_ftp_host', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control'
                                    ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Ftp Username <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('ws_ftp_username', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control'
                                    ));
                                ?>                                         
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Ftp Password <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                    echo $this->Form->input('ws_ftp_password', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control'
                                    ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>


<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">Settings</div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'form-horizontal'));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            ?>
            <div class="form-body"> 
                <h3 class="form-section">Account Settings</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Paypal ID <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('paypal_id', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control'
                                    ));
                                ?>                                         
                            </div>
                        </div>
                    </div>
                </div>
                
                <h3 class="form-section">Contact</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Info Email <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('info_email', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control'
                                    ));
                                ?>                                         
                            </div>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Support Email <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('support_email', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control'
                                    ));
                                ?>                                         
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>


<div class="portlet box purple-soft">
    <div class="portlet-title">
        <div class="caption">Campaign Settings</div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'form-horizontal'));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            ?>
            <div class="form-body"> 
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5">Fixed Duration Min Days <span class="required">*</span></label>
                            <div class="col-md-7">                                            
                                <?php
                                    echo $this->Form->input('campaign_duration_fixed_min_days', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control numeric postive'
                                    ));
                                ?>                                         
                            </div>
                        </div>
                    </div>  
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5">Trial Campaign Max Days <span class="required">*</span></label>
                            <div class="col-md-7">                                            
                                <?php
                                    echo $this->Form->input('campaign_trial_max_days', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control numeric postive'
                                    ));
                                ?>                                         
                            </div>
                        </div>
                    </div>  
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5">Initial Payment (%) <span class="required">*</span></label>
                            <div class="col-md-7">
                                <?php
                                    echo $this->Form->input('campaign_initial_payment', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control percentage postive'
                                    ));
                                ?>                                         
                            </div>
                        </div>
                    </div>
                </div>
                
                <h3 class="form-section">Medium Budget Range ($)</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5">From <span class="required">*</span></label>
                            <div class="col-md-7">                                            
                                <?php
                                    echo $this->Form->input('campaign_budget_medium_from', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control'
                                    ));
                                ?>                                         
                            </div>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5">To <span class="required">*</span></label>
                            <div class="col-md-7">                                            
                                <?php
                                    echo $this->Form->input('campaign_budget_medium_to', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control'
                                    ));
                                ?>                                         
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>


<div class="portlet box red-soft">
    <div class="portlet-title">
        <div class="caption">Wallet</div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'form-horizontal'));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            ?>
            <div class="form-body"> 
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5">Minimum Wallet Amount($) <span class="required">*</span></label>
                            <div class="col-md-7">                                            
                                <?php
                                    echo $this->Form->input('advertiser_threshold_wallet_amount', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control numeric postive'
                                    ));
                                ?>                                         
                            </div>
                        </div>
                    </div> 
                    
                </div>                
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>
