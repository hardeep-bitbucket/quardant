<?php
/**
 * @package    Date Format
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Gagandeep Gambhir
 */

class DateFormatBehavior extends ModelBehavior 
{
	
	/**
	 * Initiate setup
	 * 
	 * @param Model $model
	 * @param Array $config
	 */
    public function setup(Model $model, $config = array()) 
	{
        $this->settings[$model->alias] = $config;
    }

	/**
	 * Sets date in dd-mmm-yyyy format
	 * 
	 * @param \Model $model
	 * @param Array $results
	 * @param boolean $primary
	 */
	public function afterFind(Model $model, $results, $primary = false)
	{
		parent::afterFind($model, $results, $primary);
		
		foreach($results as $key => $record)
		{
			foreach($this->settings[$model->alias] as $field)
			{
				if (isset($record[$model->alias]) && isset($results[$key][$model->alias][$field]) && $record[$model->alias][$field])
				{
					//$results[$key][$model->alias][$field] = DateUtility::getClientDateString($record[$model->alias][$field], DEFAULT_DATE_FORMAT);
                    $results[$key][$model->alias][$field] = DateUtility::getFormatDateFromString($record[$model->alias][$field], DEFAULT_DATE_FORMAT);
				}
			}
		}
		
		return $results;
	}
	
	/**
	 * Sets date in yyyy-mm-dd format
	 * 
	 * @param \Model $model
	 * @param Array $options
	 */
	public function beforeSave(Model $model, $options = array())
	{
		foreach($this->settings[$model->alias] as $field)
		{
			if (isset($model->data[$model->alias][$field]))
			{
                $model->data[$model->alias][$field] = DateUtility::getFormatDateFromString($model->data[$model->alias][$field], DEFAULT_SQL_DATE_FORMAT);
			}
		}
		parent::beforeSave($model, $options);
	}

}