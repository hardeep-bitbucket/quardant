<?php
/**
 * import Logs Controller
 * 
 * 
 * @created    22/04/2015
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Hardeep
 */
class ImportLogsController extends AppController 
{
    var $layout='admin_inner';

    /*
     * @ Summary Screen
     */
    function admin_index()
    {
        $title_for_layout = 'Import Logs Summary';
        
        $this->Redirect->urlToNamed();
        
        $conditions = $this->getSearchConditions(array(
                array('model' => $this->modelClass, 'field' => 'filename', 'type' => 'string', 'view_field' => 'filename'),
                array('model' => 'Type', 'field' => 'id', 'type' => 'integer', 'view_field' => 'id'),                
                array('model' => $this->modelClass, 'field' => 'created_on', 'type' => 'from_date', 'view_field' => 'from_date'),
                array('model' => $this->modelClass, 'field' => 'created_on', 'type' => 'to_date', 'view_field' => 'to_date'),
           )
        );
        
        $type_list = $this->{$this->modelClass}->Type->getList(array("type" => IMPORT_TYPE));
        
        $this->{$this->modelClass}->contain(array("Type"));
        
        $records = $this->paginate($this->modelClass, $conditions);        
        
        $this->set(compact('records', 'title_for_layout', 'type_list'));
    }   
}
