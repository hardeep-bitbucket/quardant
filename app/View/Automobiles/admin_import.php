<?php
/**
 * import Automobile
 * 
 * 
 * @created    25/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep 
 */
echo $this->element("admin/form_header");
?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal', "enctype" => "multipart/form-data"));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            ?>
            <div class="form-body">                            
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">CSV file <span class="required">*</span></label>
                            <div class="col-md-8" style="margin-top:7px;">                                            
                                <?php
                                    echo $this->Form->input('csv_file', array(
                                    'type' => 'file', 'label' => false, 'div' => false, 'escape' => false,                                   
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                             <div class="form-group">
                                <label class="control-label col-md-4"></label>
                                <div class="col-md-8">                                            
                                    <?php
                                    echo $this->Html->link("Click here for sample file format  ".$this->Html->image('xls.gif',
                                            array('style'=>'padding-left:10px;padding-top: 2px;position: absolute;')), 
                                            AUTOMOBILE_IMPORT_SAMPLE_FILE, array('class' => 'button','escape'=>false ,
                                            'target' => '_blank','style'=>'color:grey;text-decoration: none;'));  
                                    ?>                                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row margin-5">   
                    <div class="margin-5 alert-error">
                        <?php echo $msg; ?>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">Total Records : <?php echo isset($total_records) ? $total_records : "" ;  ?></div>                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">Accepted : <?php echo isset($total_records) ? ($total_records - $rejected_records) : "" ;  ?></div>
                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">Rejected : <?php echo isset($rejected_records) ? $rejected_records : "" ; ?></div>
                        </div>
                    </div>                    
                </div>
                
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>