<?php
/**
 * MastTransaction model
 * 
 * 
 * @created    29/04/2015
 * @package    ANPR
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
class Transaction extends AppModel 
{
    
    public $actsAs = array(
        "DateTimeFormat" => array(
            "created_on" => array(
                "out_format" => DEFAULT_DATETIME_FORMAT,
                "in_format" => ""
            )
        )
    );
    
    var $hasOne = array(
        'TransactionDetail' => array(
            'className' => 'TransactionDetail',
            'foreignKey' => 'transaction_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    ); 
    
    public $belongsTo = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Invoice' => array(
            'className' => 'Invoice',
            'foreignKey' => 'invoice_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
    
    var $validate = array(
        'type' => array(
            'notEmptyRule' => array( 'rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'user_id' => array(
            'notEmptyRule' => array( 'rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
    );
}
