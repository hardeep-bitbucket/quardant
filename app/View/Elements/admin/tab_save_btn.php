<?php
/**
 * common tab save btns
 * 
 * 
 * @created    26/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<input class="countinue" type="hidden" value="0" name="countinue"/>
<div class="row">
    <div class="col-md-11">
        <?php if ($can_save): ?>
            <div class="pull-right">
                <?php 
                    echo $this->Form->button("<i class='fa fa-check'></i> Save and Close", array("class" => "btn btn-circle green btn-save", "type" => "submit", 'label' => false, 'div' => false, 'escape' => false)); 
                ?>
                <span class="space-10"></span>
                <?php 
                    echo $this->Form->button("Save and Continue <i class='m-icon-swapright m-icon-white'></i>", array("class" => "btn btn-circle green btn-countinue btn-save", "type" => "submit", 'label' => false, 'div' => false, 'escape' => false));
                ?>
                <span class="space-10"></span>
                <?php
                    //echo $this->Form->button("Cancel", array("class" => "btn default", "type" => "reset", 'label' => false, 'div' => false,)); 
                    echo $this->Html->link("Cancel", array("action" => "admin_edit", $id, "details"), array("class" => "btn btn-circle default"));
                    echo $this->Form->end();
                ?>
            </div>
        <?php endif; ?>
    </div>                                
</div>  

<script>
    $(".btn-countinue").click(function()
    {
        $(".countinue").val("1");
    });
    
    $(".countinue").val("0");
</script>
