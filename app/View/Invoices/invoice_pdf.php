<?php
/**
 * Invoice PDF 
 * 
 * 
 * @created    09/05/2015
 * @package    ANPRMotion
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
echo $this->element('/report/pdf/header');
?>

<div id="content">
    <div class="header-title">
        Invoice
    </div>
    <table cellpadding="5" cellspacing = "5" border="0">
        <tr>
            <td><span class="label blue">Invoice No. : </span></td>
            <td><span class=""><?php echo $report_data["Invoice"]["id"]; ?></span></td>
        </tr>
        <tr>
            <td><span class="label blue">Invoice Date : </span></td>
            <td><span class=""><?php echo $report_data["Invoice"]["created_on"]; ?></span></td>
        </tr>
        <tr>
            <td><span class="label blue">Customer : </span></td>
            <td><span class=""><?php echo $report_data["User"]["full_name"]; ?></span></td>
        </tr>        
    </table>
    
    <div class="margin-5">
        <table class="summary-table" style="width : 100%;" border="0" cellpadding="0">
            <tr>
                <th>Campaign</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Amount</th>
            </tr>
            <tr>
                <td><?php echo $report_data["Campaign"]["name"]; ?></td>
                <td><?php echo $report_data["Invoice"]["start_date"]; ?></td>
                <td><?php echo $report_data["Invoice"]["end_date"]; ?></td>
                <td><?php echo $report_data["Invoice"]["amount"]; ?></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align:right;">Subtotal</td>
                <td><?php echo $report_data["Invoice"]["amount"]; ?></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align:right;">Tax</td>
                <td>0</td>
            </tr>
            <tr>
                <td colspan="3" style="text-align:right;">Grand Total</td>
                <td><?php echo $report_data["Invoice"]["amount"]; ?></td>
            </tr>
        </table>
    </div>
    
    <div class="margin-5font-size-12">
        <div style="float: left; width : 50%;">
            <b>Declaration:</b>
            <p>
                We declare that this invoice shows the actual price of
                the goods/services described and that all the
                particulars are true and correct
            </p>
        </div>
        <div style="float: right; width : 50%; text-align: right;">
            <p>Signature</p>
            <p>ANPRMotion</p>
        </div>
    </div>
</div>

<?php 
echo $this->element('/report/pdf/footer');