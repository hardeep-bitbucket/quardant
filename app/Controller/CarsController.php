<?php

/**
 * Cars Controller
 * 
 * 
 * @created    19/12/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Sonia
 */

/**
 * @property Page $Page 
 */
class CarsController extends AppController 
{
    /**
     * set $countryList, $categories, $placesList
     */
    public function beforeFilter() 
    {
        parent::beforeFilter();               
        $this->Auth->allow(array('getStates'));
    }

    /**
     * Shows Summary Data 
     */
    public function admin_index() {
        //Converts querystring to named parameter
        $this->Redirect->urlToNamed();

        // Sets Search Parameters
        $conditions = $this->getSearchConditions(array(
            array('model' => 'Car', 'field' => 'owner_name', 'type' => 'string', 'view_field' => 'owner_name'),
            array('model' => 'Car', 'field' => 'reg_no', 'type' => 'string', 'view_field' => 'reg_no'),
            array('model' => 'Car', 'field' => 'automobile_manufacture_id', 'type' => 'integer', 'view_field' => 'automobile_manufacture_id'),
            array('model' => 'Car', 'field' => 'country_id', 'type' => 'integer', 'view_field' => 'country_id'),
        ));
        
        $this->{$this->modelClass}->contain(array(
            "Manufacture" => array(
                "fields" => array(
                   "id", "name" 
                )
            ),
            "Model" => array(
                "fields" => array(
                   "id", "name" 
                )
            ),
            "Variant" => array(
                "fields" => array(
                   "id", "name" 
                )
            ),
            "Country" => array(
                "fields" => array(
                   "id", "name" 
                )
            ),
            "State" => array(
                "fields" => array(
                   "id", "name" 
                )
            )
        ));

        
        $records = $this->paginate($this->modelClass, $conditions);
        
        $this->_setPlaceList();
        $this->_setAutomobileList();        
         
        $this->set(compact('records', 'pagesList'));
    }

    /**
     * Adds New Record 
     */
    public function admin_add() {
        //calling function from app controller
        parent::add();
        
        $this->_setPlaceList();
        $this->_setAutomobileList();

        //providing list for search
        $heading = 'Add '. $this->modelClass;
        
        $this->set(compact('heading'));
        $this->render('admin_form');
    }

    /**
     * Updates Existing Record
     * @param type $id 
     */
    public function admin_edit($id) 
    {
        //calling function from app controller        
        parent::edit($id);
        
        $this->_setPlaceList();
        $this->_setAutomobileList();
        
        $heading = 'Edit '. $this->modelClass;
        
        $this->set(compact('heading'));
        $this->render('admin_form');
    }
    
    /**
     * Get places list
     */
    private function _setPlaceList()
    {
        $countryList = $this->Car->Country->getChildList(0);
        
        $stateList = array();
        
        if (isset($this->request->data['Car']['country_id']) && $this->request->data['Car']['country_id'])
        {
            $stateList = $this->Car->Country->getChildList($this->request->data['Car']['country_id']);
        }
        
        $this->set(compact('countryList', 'stateList'));
    }
    
    /**
     * sets autmobile related list
     */
    private function _setAutomobileList()
    {
        $manufacture_list = $this->Car->Manufacture->getChildList(0);
        
        $model_list = $variant_list = array();
        
        if (isset($this->request->data['Car']['automobile_manufacture_id']) && $this->request->data['Car']['automobile_manufacture_id'])
        {
            $model_list = $this->Car->Manufacture->getChildList($this->request->data['Car']['automobile_manufacture_id']);
        }
        
        if (isset($this->request->data['Car']['automobile_model_id']) && $this->request->data['Car']['automobile_model_id'])
        {
            $variant_list = $this->Car->Manufacture->getChildList($this->request->data['Car']['automobile_model_id']);
        }
        
        $this->set(compact('manufacture_list', 'model_list', 'variant_list'));
    }
    
    /*
     * @Import Function
     */
    function admin_import()
    {            
        set_time_limit(5000);
        // Get a site, company and id for save userid in customer target achieve table
        
        //$customers = $this->Customer->find("all", array("recursive" => -1, "fields" => array("user_id", "company", "site")));
        
        if(!empty($this->request->data))
        {
            $places = $this->Car->Country->getChildList(0);        
            $places = array_map('strtolower', $places);
            $places = array_flip($places);
        
            $categories = $this->Car->Category->find("list");
            $categories = array_map('strtolower', $categories);
            $categories = array_flip($categories);
        
            $attachment = $this->request->data["Car"]["csv"];
            $file = $this->request->data["Car"]["csv"]["tmp_name"];
            $i = 1;
            $error_message = array();
            $rejected_records = 0;
            $total_records = 0;
            $date = "";
            
            if (($handle = fopen($file, "r")) !== FALSE) 
            {
                $import_data = array();
                $achieved = array();
                $cumilative = array();
                while (($values = fgetcsv($handle, 1000, ",")) !== FALSE) 
                {
                    /*
                     * Record Started from 2nd row
                     */
                    if($i > 1)
                    {
                        if(isset($places[trim(strtolower($values[3]))]))
                        {
                            $country_id = $places[$values[3]];
                        }
                        if(isset($places[trim(strtolower($values[4]))]))
                        {
                            $state_id = $places[$values[4]];
                        }
                        if(isset($categories[trim(strtolower($values[13]))]))
                        {
                            $category_id = $categories[$values[13]];
                        }
                        $total_records++;
                        $import_data["Car"] = array("active" => 1, "reg_no" => $values[0], "make" => $values[1],
                                                    "chase_no" => $values[2], "country_id" => $country_id, "state_id" => $state_id, 
                                                    "color" => $values[5], "owner_name" => $values[6], "ower_address" => $values[7],
                                                    "owner_city" => $values[8], "owner_zip" => $values[9], "ower_email" => $values[10], 
                                                    "owner_phone" => $values[11], "reg_date" => $values[12], "category_id" => $category_id);
                        
                        if($this->Car->save($import_data["Car"]))
                        {
                            //$this->redirect(array('action' =>'importreport', $log_id));
                        }
                        else 
                        {
                            $errors = $this->Car->validationErrors;
                            if(isset($errors["color"]))
                            {
                                $error_message["color"][] = "<b>Row " . $i  . "</b> ". $errors["color"][0];
                            }
                            if(isset($errors["reg_no"]))
                            {
                                $error_message["reg_no"][] = "<b>Row " . $i  . "</b> ". $errors["reg_no"][0];
                            }
                            if(isset($errors["reg_date"]))
                            {
                                $error_message["reg_date"][] = "<b>Row " . $i  . "</b> ". $errors["reg_date"][0];
                            }
                            if(isset($errors["chase_no"]))
                            {
                                $error_message["chase_no"][] = "<b>Row " . $i  . "</b> ". $errors["chase_no"][0];
                            }
                            if(isset($errors["owner_name"]))
                            {
                                $error_message["owner_name"][] = "<b>Row " . $i  . "</b> ". $errors["owner_name"][0];
                            }
                            if(isset($errors["owner_city"]))
                            {
                                $error_message["owner_city"][] = "<b>Row " . $i  . "</b> ". $errors["owner_city"][0];
                            }
                            if(isset($errors["owner_zip"]))
                            {
                                $error_message["owner_zip"][] = "<b>Row " . $i  . "</b> ". $errors["owner_zip"][0];
                            }
                            $rejected_records++;
                        }
                    }   
                    $i++;
                } 
                $error_message["saved_records"] = $total_records - $rejected_records;
                $error_message["rejected_records"] = $rejected_records;
                $message = "";
                
                $message .= "<br/><b>Saved Records:</b> " . $error_message["saved_records"]; 
                $message .= "<br/><b>Rejected Records:</b> " . $rejected_records;
                if(isset($error_message["color"]))
                {
                    $message .= "<br/><b>Validation Errors Color:</b> " . implode(", ", $error_message["color"]);
                }
                if(isset($error_message["reg_no"]))
                {
                    $message .= "<br/><b>Validation Errors Registration No.:</b> " . implode(", ", $error_message["reg_no"]);
                }
                if(isset($error_message["reg_date"]))
                {
                    $message .= "<br/><b>Validation Errors Registration Date:</b> " . implode(", ", $error_message["reg_date"]);
                }
                if(isset($error_message["chase_no"]))
                {
                    $message .= "<br/><b>Validation Errors Chase No.:</b> " . implode(", ", $error_message["chase_no"]);
                }
                if(isset($error_message["owner_name"]))
                {
                    $message .= "<br/><b>Validation Errors Owner Name:</b> " . implode(", ", $error_message["owner_name"]);
                }
                if(isset($error_message["owner_city"]))
                {
                    $message .= "<br/><b>Validation Errors Owner City:</b> " . implode(", ", $error_message["owner_city"]);
                }
                if(isset($error_message["owner_zip"]))
                {
                    $message .= "<br/><b>Validation Errors Owner Zip:</b> " . implode(", ", $error_message["owner_zip"]);
                }
                $log_id = $this->saveLogs($total_records, $rejected_records, $attachment, $message, $date, CAR_IMPORT_PATH);
                if(!empty($log_id))
                {
                    $this->redirect(array('action' =>'importreport', $log_id));
                }
             }
        }
    }
    
    /*
     * Save a Logs
     */
    function saveLogs($total_records, $rejected_records, $attachment, $message, $date, $CsvFilePath)
    {
        $this->request->data['Log']['type_id']= CAR_IMPORT_LOG_TYPE_ID;
        $this->request->data['Log']['date']= date("Y-m-d");
        $this->request->data['Log']['message']= $message;
        $this->request->data['Log']['total_records']= $total_records;
        $this->request->data['Log']['accepted']= $total_records - $rejected_records;
        $this->request->data['Log']['rejected']= $rejected_records;
        $this->request->data['Log']['filename']= $attachment["name"];
        $this->loadModel("Log");
        $this->Log->create();
        if($this->Log->save($this->request->data))
        {
            $this->UploadCsvFile($attachment, $this->Log->id, $CsvFilePath);
        }
        $log_id = $this->Log->id;
        return $log_id; 
    }
    
    /*
     * Upload CSV File
     */
                
    function UploadCsvFile($attachment, $id, $CsvFilePath)
    {
        //$filename =strtolower(basename($attachment['name']));
        //$ext = substr($filename, strrpos($filename, '.') + 1);
        if($attachment['error'] ==0)
        {
            $FileName = $attachment['name'];
            if (file_exists($CsvFilePath.$FileName))
            {
                //random number between one and one million
                $int = rand(1,1000000);
                $FileName = $int."_".$FileName;
                //-----------------------
                $int = rand(0,51);
                $a_z = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $rand_letter = $a_z[$int];
                //-------------------------
            }
            $target = $CsvFilePath.$FileName;
            $this->loadModel("Log");
            if(move_uploaded_file($attachment['tmp_name'],$target)) 
            { 
                $this->Log->id = $id;
                $this->Log->saveField('filename',$FileName);
            }
        }
    }
                
    /*
     * @Import Report Function
     */
    function admin_importreport($log_id = NULL)
    {  
        $this->loadModel("Log");
        $Logs = $this->Log->find("first", array("conditions" => array("Log.id" => $log_id)));
        $this->set(compact('Logs'));
    }
}
