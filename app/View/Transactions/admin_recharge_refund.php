<?php 
/**
 * Form Recharge controller transaction
 * 
 * 
 * @created    05/05/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("admin/form_header");

?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal',));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            echo $this->Form->hidden('type', array('label' => false, 'div' => false, 'escape' => false));
            ?>
            <div class="form-body">  
                <div class="row margin-top-15">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Advertisers <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php                                
                                    echo $this->Form->input('user_id', array(
                                        'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,                                        
                                        "options" => $advertiser_list, "empty" => "Please Select",
                                        'class'=>'form-control',  'required'
                                    ));

                                ?>                                            
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-3">
                        <span>Wallet Amount : <?php echo CURRENCY_SYMBOL; ?> </span>                            
                        <b><span id="wallet-amt"></span></b>
                    </div>
                </div>
                <div class="row">                         
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Payment Mode <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                    $options = StaticArray::$payment_mode;
                                    unset($options[StaticArray::$payment_mode_online]);

                                    echo $this->Form->input('TransactionDetail.payment_mode', array(
                                        'type' => 'select' ,'label' => false,'div' => false, 'escape' => false, 
                                        'options' => $options,
                                        'empty' => 'Please Select',
                                        'class' => 'form-control',
                                        'required'
                                    ));                                   
                                ?>                                              
                            </div>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Amount (<?php echo CURRENCY_SYMBOL; ?>) <span class="required">*</span></label>
                            <div class="col-md-5">        
                                <?php 
                                    if ($this->request->data["Transaction"]["type"] == StaticArray::$transaction_status_credit_transaction) 
                                    {
                                        echo $this->Form->input('credit', array(
                                            'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,                                         
                                            'class' => 'form-control numeric postive', 'required'
                                        ));                                      
                                    }
                                    else
                                    {
                                        echo $this->Form->input('debit', array(
                                            'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,                                         
                                            'class' => 'form-control numeric postive', 'required'
                                        ));  
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">                       
                        <div class="form-group">
                            <label class="control-label col-md-4">Description <span class="required">*</span></label>
                            <div class="col-md-8">        
                                <?php 
                                    echo $this->Form->input('TransactionDetail.description', array(
                                        'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,                                         
                                        'class' => 'form-control',
                                        "rows" => 2,  "col" =>10, 'required'

                                    ));   
                                ?>
                            </div>
                        </div>
                    </div>  

            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>
        </div>
    </div>
</div>
    
<script>
    $("#TransactionUserId").change(function()
    {
       var v = $(this) .val();
       
       if (v)
       {
           $.get("<?php echo SITE_URL.$controller; ?>/ajaxGetWalletAmount/" + v , function (data, status)
           {
               $("#wallet-amt").html(data);
           });
       }
       else
       {
            $("#wallet-amt").html("");
       }
    });
    
    $("#TransactionAdminRefundWalletForm").submit(function()
    {
        var amount = parseFloat($("#wallet-amt").html());
       
        if ($("#TransactionDebit").length > 0)
        {
            var debit = parseFloat($("#TransactionDebit").val());
            
            if (debit > amount)
            {
                warn("You can't refund more than wallet amount");
                return false;
            }
        }
    });
    
    $("#TransactionUserId").trigger("change");
</script>