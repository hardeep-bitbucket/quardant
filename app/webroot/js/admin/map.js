var mapConfig = {
    marker : {
        icon : {
            default : "http://maps.google.com/mapfiles/ms/icons/red-dot.png",
            old : "http://maps.google.com/mapfiles/ms/icons/green-dot.png",
            new : "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
            nonSelected : "http://maps.google.com/mapfiles/ms/icons/red-dot.png",
            selected : "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
            nonActive : "http://maps.google.com/mapfiles/ms/icons/red-dot.png",
            active : "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
        },     
    },
    map : {
        map_options : {
            zoom: 8 ,            
        },        
    }
    
};