<?php
/**
 * Spot finder element
 * 
 * 
 * @created    28/03/2015
 * @package    ANPR
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */ 
?>


<style>    
    .no-padding *{
        padding : 0; margin : 0;
    }
    
    .info-box{
        background: #fff;
        border : 5px solid #3FABA4;
        color : #000;
    }
    
    .info-box-title{
        background-color: #3FABA4;
        padding: 0 0 5px 10px;
        color : #fff;
    }
    
    .info-box-body{
        padding : 5px;
    }
    
    .info-box-body label {
        font-weight: bold;
    }
</style>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&sensor=false&libraries=geometry"></script>

<div class="page-title-line sep-bottom-md">
    <div class="row">
            <div class="col-md-3" style="display:none;">
                Country
                <div class="form-group">
                    <?php
                    echo $this->Form->input('country_id', array(
                        'label' => false,
                        'options' => $countryList,
                        'escape' => false,
                        'class' => 'county_list form-control input-lg rounded primary',
                        'empty' => 'Please Select',
                        "id" => "LocationCountryId",
                    ));
                    ?>	
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    State
                    <?php
                    echo $this->Form->input('state_id', array(
                        'label' => false,
                        'options' => "",
                        'escape' => false,
                        'class' => 'form-control input-lg rounded primary',
                        'empty' => 'All',
                        'id' => 'LocationStateId'
                            )
                    );
                    ?>	
                </div>
            </div>
            <div class="col-md-3">
                City
                <div class="form-group">
                    <?php
                    echo $this->Form->input('city_id', array(
                        'label' => false,
                        'options' => "",
                        'escape' => false,
                        'class' => 'form-control input-lg rounded primary',
                        'empty' => 'All',
                        'id' => 'LocationCityId'
                            )
                    );
                    ?>	
                </div>
            </div>   
            
            <div class="col-md-3">
                <div class="info-box no-padding" id="location-car-reach-info" style="display:none;">
                    <div class="info-box-title">
                        <span id="location-name"></span>
                    </div>
                    <div class="info-box-body">
                        <div class='row'>    
                            <div class="col-md-10">
                                Avg. Car Reach on Location : 
                            </div>
                            <div class="col-md-2">
                                <label><span id="avg-car-reach"></span></label>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </div>                
                <div style="clear:both;"></div>
            </div>
        <div style="clear:both;"></div>
    </div>
</div>

<div class="row">
    <div class="col-md-9">
        <div id="map-canvas" style="height: 450px;"></div>
    </div>
    <div class="col-md-3">
        <div class="info-box">
            <div class="info-box-title">
                <span> Locations</span>
            </div>
            <div class="info-box-body"  id='location-list' style="overflow-y: auto; height: 400px;">  
            </div>        
        </div>
    </div>
</div>
<div class="row" style="margin-top:5px;">
    <div class="col-md-6">
        <span class="margin-5">
            <img src="" id="map-marker-non-active" />
        </span>                    
        <span class="margin-5">
            Non-Active Locations
        </span>

        <span class="margin-5">
            <img src="" id="map-marker-active" />
        </span>
        <span class="margin-5">
            Active Locations
        </span>
    </div>
</div>

<script type="text/javascript">
    $(function()
    {
        function load_select_list(data, id)
        {
            var obj = $.parseJSON(data);
            
            var arr = sortJSON(obj);
            
            $(id + " option").remove();
            $(id).append("<option value=''>All</option>")
            
            for (var i in arr)
            {
                $(id).append("<option value='" + arr[i].key + "'>" + arr[i].value + "</option>");
            }
        }

        $("#LocationCountryId").on('change', function()
        {
            var id = $(this).val();
            set_point_visible(id);
            if (id != '')
            {
                $.get('<?php echo SITE_URL; ?>/Places/ajaxPlaceList/' + id, function(data)
                {
                    load_select_list(data, "#LocationStateId");
                });
            }
        });

        $("#LocationStateId").on('change', function()
        {
            var id = $(this).val();

            if (id != '')
            {
                $.get('<?php echo SITE_URL; ?>/Places/ajaxPlaceList/' + id, function(data)
                {
                    set_point_visible(id);
                    load_select_list(data, "#LocationCityId");
                });
            }
            else
            {
                $("#LocationCountryId").trigger("change");
            }
        });

        $("#LocationCityId").on('change', function()
        {
            var id = $(this).val();

            if (id != '')
            {
                set_point_visible(id);
            }
            else
            {
                $("#LocationStateId").trigger("change");
            }

        });
        
        
        function get_current_car_reach(id, name)
        {
            $("#location-car-reach-info").show();
            $("#location-name").html(name);
            $.get('<?php echo SITE_URL; ?>Locations/ajaxGetAutomobileReach/' + id, function (data, status)
            {
                data = JSON.parse(data);
                //$("#total-car-reach-per").html(data.total_car_reach_per_day);
                $("#avg-car-reach").html(data.avg_car_reach);
            });
        }

        var points = JSON.parse('<?php echo $points; ?>');
        var map, latlngbounds, markers = [];

        $("#map-marker-non-active").attr("src", mapConfig.marker.icon.nonActive);
        $("#map-marker-active").attr("src", mapConfig.marker.icon.active);

        function set_point_visible(place_id)
        {
            var html = "";
            var visible_count = 0;
            latlngbounds = new google.maps.LatLngBounds();
            var a = 0;
            for (var i in markers)
            {
                a++;
                if (place_id == markers[i].data.country_id || place_id == markers[i].data.state_id || place_id == markers[i].data.city_id || place_id == "")
                {
                    markers[i].setVisible(true);
                    var latlngset = markers[i].getPosition();
                    latlngbounds.extend(latlngset);
                    visible_count++;

                    html += "<div class='row' style='margin-bottom:10px;'>";
                    html += "<div class='col-md-12'>";
                    html += "<div style='border-bottom:1px solid; color: #486196;'>" + a +".<b> " + markers[i].data.name + "</b></div>";
                    html += "<div>" + markers[i].data.address + "</div>";
                    html += "</div>";
                    html += "</div><div style='clear:both;'></div>";
                }
                else
                {
                    markers[i].setVisible(false);
                }
            }
            
            if (!html)
            {
                html = "No Data Found";
            }

            $("#location-list").html(html);

            if (visible_count > 1)
            {
                map.fitBounds(latlngbounds);
            }
            else
            {
                map.setZoom(4);
            }

        }

        function placeMarker(point, latlngset)
        {
            var icon = typeof point.active != "undefined" && point.active == 1 ? mapConfig.marker.icon.active : mapConfig.marker.icon.nonActive;

            var data = typeof point.data != "undefined" ? point.data : {};

            var marker = new google.maps.Marker({
                map: map,
                position: latlngset,
                icon: icon,
                data: data
            });
            
            google.maps.event.addListener(marker, 'click', function(e) 
            {   
                get_current_car_reach(this.data.id, this.data.name);
            });

            markers.push(marker);

            if (typeof point['content'] != "undefined")
            {
                var content = "<div class='map-marker-window'>" + point['content'] + "</div>";

                var infoWindow = new google.maps.InfoWindow();
                infoWindow.setContent(content);

                /**
                 * adding Listeners for inflate window
                 */
                google.maps.event.addListener(marker, 'mouseover', infoWindowOpen(infoWindow, marker));
                google.maps.event.addListener(marker, 'mouseout', infoWindowClose(infoWindow, marker));
            }
        }

        function loadData()
        {
            latlngbounds = new google.maps.LatLngBounds();
            if (markers.length > 0)
            {
                for (var i in  markers)
                {
                    markers[i].setMap(null);
                }
            }

            if (points.length > 0)
            {
                for (var i in points)
                {
                    points[i]['active'] = parseInt(points[i]['active']);
                    var content = "<div style='width:250px;'> <b>Location</b> : " + points[i]["name"] + "<br/>";
                    content += "<b>Price Per Impression</b> : $" + points[i]["price_per_impression"] + "<br/>";
                    content += "<b>Total Campaigns</b> : " + points[i]["ad_location_count"] + "<br/>";
                    content += "<b>Active Campaigns</b> : " + points[i]["active"] + "<br/>";
                    content += "<b>Add. : </b> : " + points[i]["address"] + "<br/>";
                    content += "</div>";

                    var latlngset = new google.maps.LatLng(points[i]["latitude"], points[i]["longitude"]);

                    placeMarker({
                        content: content,
                        id: points[i]['id'],
                        active: points[i]['active'] > 0 ? true : false,
                        data: {
                            id : points[i]['id'],
                            country_id: points[i]['country_id'],
                            state_id: points[i]['state_id'],
                            city_id: points[i]['city_id'],
                            name: points[i]["name"],
                            country: points[i]['country'],
                            state: points[i]['state'],
                            address: points[i]['address'],
                            city: points[i]['city'] ? points[i]['city'] : ''
                        }
                    },
                    latlngset);

                    latlngbounds.extend(latlngset);
                }

                if (points.length > 1)
                {
                    map.fitBounds(latlngbounds);
                }
                else
                {
                    map.panTo(latlngset);
                }
                $("#LocationCountryId").change();
            }
            else
            {
                alert("No Location found");
            }
        }

        function infoWindowOpen(infoWindow, marker)
        {
            return function() {
                infoWindow.open(map, marker);
            };
        }
        function infoWindowClose(infoWindow, marker)
        {
            return function() {
                infoWindow.close(map, marker);
            };
        }

        function initialize()
        {
            var home = new google.maps.LatLng('<?php echo MAP_HOME_LAT ?>', '<?php echo MAP_HOME_LNG ?>');

            var mapOptions = {
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: home
            };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


            $("#LocationCountryId").val(1);
            loadData();
        }


        $(document).ready(function()
        {
            google.maps.event.addDomListener(window, 'load', initialize);
        });

    });
</script> 