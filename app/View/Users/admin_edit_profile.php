<?php
/**
 * Add & edit Users
 * 
 * 
 * @created    20/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep 
 */
echo $this->element("admin/user/form");