<?php
/**
 * Location Email template - Activation
 * 
 * @created    01/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

?>
Hello Admin, <br/><br/>
    
New Location Activated<br/><br/>

Details <br/><br/>

Location name : <b><?php echo $record['Location']['name']; ?></b><br/>
Location address : <b><?php echo $record['Location']['address']; ?></b>

<br/><br/>
<?php echo $content['footer']; ?>