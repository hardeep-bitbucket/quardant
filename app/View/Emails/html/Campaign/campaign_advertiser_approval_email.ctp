<?php
/**
 * Campaign Approcal Email template - 
 *  
 * @created    12/05/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

Hello <b> <?php echo $record['User']['name'] . " " . $record['User']['subname']; ?> </b>, <br/><br/>
    
Please Approve your campaign <b> <?php echo $record["Campaign"]["name"] ?> </b> by click on below link  <br/><br/>

<a href="<?php echo DOMAIN; ?>/Campaigns/approve_campaign/<?php echo $record["confirmationCode"]; ?>">Click Here</a>  <br/><br/>

<?php echo $content['footer']; ?>