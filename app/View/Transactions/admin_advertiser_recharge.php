<?php
/**
 * advertiser recharge
 * 
 * 
 * @created    23/05/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("admin/form_header");

?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">    
        <form action='<?php echo PAYPAL_URL; ?>' method='post' name='frmPayPal1' id='paypal-recharge-form' class="form-horizontal">
                   <input type='hidden' name='business' value='<?php echo $adminSetting['paypal_id'];?>'>
                   <input type='hidden' name='cmd' value='_xclick'>

                   <input type='hidden' name='item_name' value='ANPRMotion Wallet Recharge'>
                   <input type='hidden' name='item_number' value='<?php echo $auth_user['id'];?>'>
                   <input type='hidden' name='no_shipping' value='1'>
                   <input type='hidden' name='currency_code' value='<?php echo PAYPAL_CURRENCY; ?>'>
                   <input type='hidden' name='handling' value='0'>
                   <input type='hidden' name='cancel_return' value='<?php echo SITE_URL . "transactions/payPalFailure" ?>'>
                   <input type='hidden' name='return' value='<?php echo SITE_URL. "transactions/payPalSuccess" ?>'> 
                   <input type="hidden" value="2" name="rm">

                   <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Amount(<?php echo CURRENCY_SYMBOL; ?>)</label>
                                <div class="col-md-8">  
                                    <input name='amount' value='0' class="amount form-control  numeric postive" required="required"> 
                                    <span id="amount-validation-msg" class="required"></span>
                                </div>
                            </div>
                        </div>

                       <div class="col-md-6">
                            <?php
                                echo $this->Form->button("Recharge Now", array("class" => "btn green btn-save", 'id' => "pay-now", "type" => "submit", 'label' => false, 'div' => false,)); 
                            ?>
                       </div>
                   </div>
               </form>
    </div>
</div>