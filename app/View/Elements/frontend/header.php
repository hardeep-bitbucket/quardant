<nav id="main-navigation" role="navigation" class="navbar navbar-fixed-top navbar-standard" style=" background-color: #fff;">
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="shift-left-block">
                        <div class="shift-left-block">
                            <div class="img-circle social-icons">
                                <i class="fa fa-envelope-o"></i>
                            </div>                            
                        </div>
                        <div class="shift-left-block" style="margin : 2px 0 0 7px;">
                            <a href="mailto:<?php echo $adminSetting["info_email"]; ?>" class="margin-left-2-per"><?php echo $adminSetting["info_email"]; ?></a>
                        </div>
                    </div>  
                    <div class="shift-left-block  margin-left-8-per">
                        <div class="shift-left-block">
                            <div class="img-circle social-icons">
                                <i class="fa fa-phone"></i>
                            </div>                            
                        </div>
                        <div class="shift-left-block" style="margin : 2px 0 0 7px;">
                            (+39) 123-456-789
                        </div>
                    </div>  
                </div>
                <div class="col-md-6">
                    <div class="shift-right-block margin-left-2-per">
                        <a href="#">
                            <div class="img-circle social-icons" style="background-color: #0177B5;">
                                <i class="fa fa-linkedin"></i>
                            </div>
                        </a>
                    </div>   
                    <div class="shift-right-block margin-left-2-per">
                        <a href="#">
                            <div class="img-circle social-icons" style="background-color: #21B0F2;">
                                <i class="fa fa-google-plus"></i>
                            </div>
                        </a>
                    </div>                    
                    <div class="shift-right-block margin-left-2-per">
                        <a href="#">
                            <div class="img-circle social-icons" style="background-color: #F75B4F;">
                                <i class="fa fa-youtube-play"></i>
                            </div>
                        </a>
                    </div>                    
                    <div class="shift-right-block margin-left-2-per">
                        <a href="#">
                            <div class="img-circle social-icons" style="background-color: #FF3D3E;">
                                <i class="fa fa-twitter"></i>
                            </div>
                        </a>
                    </div>
                    <div class="shift-right-block margin-left-2-per">
                        <a href="#">
                            <div class="img-circle social-icons" style="background-color: #3B5999;">
                                <i class="fa fa-facebook"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle">
                <i class="fa fa-align-justify fa-lg"></i>
            </button>
            <a href="/" class="navbar-brand">
                <img src="/img/frontend/logo-white.png" alt="" class="logo-white">
                <img src="/img/frontend/logo-dark.png" alt="" class="logo-dark">
            </a>
        </div>
        <div class="navbar-collapse collapse">
             <?php 
                if(!isset($this->params["slug"]) && $this->params["controller"] == "pages")
                {
                    echo $this->element("frontend/menu"); 
                }
                else
                {
                    echo $this->element("frontend/inner_menu"); 
                }
            ?>
             <ul class="nav navbar-nav navbar-right service-nav">                
                <li>
                    <a id="dropdownMenuLogin1" class="upper dropdown-toggle" data-toggle="dropdown" href="#">GET STARTED</a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>                          
                            <a href="/register-advertiser.html" style="border: none;">Advertisers</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="/register-partner.html" style="border: none;">Partners</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <?php if(empty($auth_user)) { ?>
                    <a id="dropdownMenuLogin" class="upper dropdown-toggle" data-toggle="dropdown" href="#">SIGN IN</a>
                    <div class="dropdown-menu widget-box" aria-labelledby="dropdownMenuLogin">
                        <form method="post" action="/users/login" id="login-form">
                            <div class="form-group">
                                <label class="sr-only">Username or Email</label>
                                <input type="email" value="" required placeholder="Username or Email" name="username" id = "username" class="form-control input-lg">
                            </div>
                            <div class="form-group">
                                <label class="sr-only">Password</label>
                                <input type="password" value="" required placeholder="Password" name="password" id = "password" class="form-control input-lg">
                            </div>
                            <div class="form-inline form-group">                                
                                <div class="checkbox" style="margin-top : 5px;">
                                    <label>
                                        <input type="checkbox" name="remember_me"/>     
                                        <small> Remember me</small>
                                    </label>
                                </div>
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary btn-xs" id = "login">Login</button>                                
                                </div>
                            </div><a href="/forgot-password.html"><small>Forgot Your Password</small></a>
                        </form>
                    </div>
                    <?php } else { ?>
                        <a id="dropdownMenuUser" class="upper dropdown-toggle" data-toggle="dropdown"  href="<?php echo SITE_URL; ?>admin/advertisers/home" class="dropdown-toggle">
                            MY ACCOUNT
                        </a>
                        
                    <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <?php
                                    $controller = "users";
                                    if ($auth_user['group_id'] == ADVERTISER_GROUP_ID)
                                    {
                                        $controller = "advertisers";
                                    }
                                    else if ($auth_user['group_id'] == PARTNER_GROUP_ID)
                                    {
                                        $controller = "partners";
                                    }
                                    
                                    echo $this->Html->link("<i class='fa fa-home'></i> Account Home", array("controller" => $controller, "action" => "home", "admin" => true), array("escape" => false, "style" => "border : none;"));
                                ?>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <?php
                                echo $this->Html->link("<i class='fa fa-power-off'></i> Log Out", array("controller" => "users", "action" => "logout"), array("escape" => false));
                                ?>
                            </li>
                        </ul>
                    <?php } ?>
                </li>
            </ul>           
        </div>
    </div>
</nav>
<script type = "text/javascript">
    $(document).ready(function(){
        $("#login").click(function()
        {
            var username = $("#username").val();
            var password = $("#password").val();
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if(username == "")
            {
                alert("Please enter your username.");
                return false;
            }
            //else if(!emailReg.test(username))
            //{
              //  alert("Please enter valid email address.");
                //return false;
            //}
            else if(password == "")
            {
                alert("Please enter your password.");
            }
            else
            {
                var form_data = $("#login-form").serializeArray();
                
                var tz = jstz.determine();
                
                form_data.push({
                    name : "timezone",
                    value : tz.name()
                });
                
                console.log(form_data);
                
                $.post('<?php echo $this->base; ?>/users/login', form_data, function(data)
                {
                    if(data == 1)
                    {
                        window.location.href = "<?php echo SITE_URL; ?>admin/advertisers/home";
                        return true;
                    }
                    else
                    {
                        alert("Your username or password was incorrect.");
                        return false;
                    }
                });
                return false;
            }
        });         
    })
</script>