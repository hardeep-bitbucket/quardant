<?php

/**
 * Users Controller
 * 
 * acount activatation  advertiser template
 * 
 * 
 * @created    21/04/2015
 * @package    ANPR
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     hardeep
 */
?>

<pre>
Hello <?php echo $User['name'];?>,

We have now accepted your application and would like to welcome you as a client!

You can now leverage ANPRMotion’s platform to reach your target audience and manage your advertisement campaigns.

Please read the following information on how to activate your account and get started.

To login to ANPRMotion please visit the following link and click Sign In on the top right:

<a href='<?php echo DOMAIN; ?>'><?php echo DOMAIN; ?></a>

User Name: <?php echo $username;?>

For information on how to use the campaign creation tool please review the following user guide/video.


<?php echo $content['footer']; ?>