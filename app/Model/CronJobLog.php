<?php

/**
 * CronJobLog Model
 * 
 * 
 * @created    06/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
class CronJobLog extends AppModel 
{
    public $sanitize = false;
}