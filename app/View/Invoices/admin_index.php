<?php
/**
 * Invoices Controller
 * 
 * 
 * @created    21/03/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */

$action_for_search = str_replace("admin_", '', $action);

?>

<div class="pull-left">
    <h3 class="page-title">
      <?php echo ucwords($model); ?> <small>Manager</small>
    </h3>
</div>
<div class="pull-right">
    <div style="margin-top:10px;">    
    </div>
</div>

<div class="clearfix"></div>

<?php
    //echo $this->element("admin/breadcrumb"); 
    echo $this->Session->flash();
?>

<div class="row">
    <div class="col-md-12">   
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">Search</div>  
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>

            </div>
            <div class="portlet-body">    
                
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->                    
                    <?php echo $this->Form->create($model, array('type' => 'GET' , 'class' => 'ajax-form form-horizontal', 'data-action' => $action_for_search)); ?>
                     
                        <div class="form-body">  
                            <div class="row">
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Campaign</label>
                                        <div class="col-md-8">                                            
                                             <?php
                                                echo $this->Form->input('InvoiceSearch.campaign_name', array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                                    
                                                    'value' => $Campaigncampaign_name,
                                                    'class' => 'form-control',
                                                ));
                                              ?>                                            
                                        </div>
                                    </div>
                                </div>
                                <?php if ($auth_user['group_id'] != ADVERTISER_GROUP_ID): ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Advertiser</label>
                                        <div class="col-md-8">
                                             <?php
                                                echo $this->Form->input('InvoiceSearch.user_id', array(
                                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                                    'options' => $advertiser_list, 'empty' => "Please Select",                                                   
                                                    'value' => $Invoiceuser_id,
                                                    'class' => 'select2me form-control',
                                                ));
                                              ?>  
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Invoice Date From</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('InvoiceSearch.from_date',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $Invoicefrom_date, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Invoice Date To</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('InvoiceSearch.to_date',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $Invoiceto_date, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Payment Date From</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('InvoiceSearch.payment_datetime_from',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $Invoicepayment_datetime_from, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Payment Date To</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('InvoiceSearch.payment_datetime_to',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $Invoicepayment_datetime_to, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                            
                            <div class="row">                                  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Status</label>
                                        <div class="col-md-8">                                            
                                             <?php
                                               $options = StaticArray::$invoice_status_type;
                                               unset($options[0]);
                                               $options["-1"] = "Unpaid";
                                               
                                                echo $this->Form->input('InvoiceSearch.status', array(
                                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                                    'options' => $options, 'empty' => "Please Select",                                                   
                                                    'value' => $Invoicestatus,
                                                    'class' => 'select2me form-control',
                                                ));
                                              ?>                                            
                                        </div>
                                    </div>
                                </div>
                                 
                            </div>
                                                       
                        </div>
                        <?php echo $this->element("admin/form_search_btn"); ?>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
        
        
        
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Summary</div>                        
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed table-custom">
                    <thead>
                       <tr class="head" >                           
                            <td width="5%"  class="td-center"><?php echo $this->Paginator->sort('id', 'ID', array('class' => "ajax-page-link")); ?></td>
                            <td width="15%"><?php echo $this->Paginator->sort('User.name', 'Advertiser', array('class' => "ajax-page-link")); ?></td>
                            <td width="15%"><?php echo $this->Paginator->sort('Campaign.name', 'Campaign', array('class' => "ajax-page-link")); ?></td>                                                 
                            <td width="10%" class="td-center"><?php echo $this->Paginator->sort('start_date', 'Start Date', array('class' => "ajax-page-link")); ?></td>
                            <td width="10%" class="td-center"><?php echo $this->Paginator->sort('end_date', 'End Date', array('class' => "ajax-page-link")); ?></td>                            
                            <td width="10%" class="td-center"><?php echo $this->Paginator->sort('amount', 'Amount (' . CURRENCY_SYMBOL . ')', array('class' => "ajax-page-link")); ?></td>                                                    
                            <td width="12%" class="td-center"><?php echo $this->Paginator->sort('Invoice.created_on', 'Created Date', array('class' => "ajax-page-link")); ?></td>                            
                            <td width="8%" class="td-center">Status</td>
                            <td width="5%"  class="td-center">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $record) { ?>
                            <tr>                                
                                <td class="td-center"><?php echo $record[$model]['id']; ?></td>
                                <td><?php echo $record['User']['full_name']; ?> </td>
                                <td><?php echo $record['Campaign']['name']; ?> </td>                                
                                <td class="td-center"><?php echo $record[$model]['start_date']; ?></td>  
                                <td class="td-center"><?php echo $record[$model]['end_date']; ?></td>                                  
                                <td class="td-center"><?php echo $record[$model]['amount']; ?></td>                                  
                                <td class="td-center"><?php echo $record[$model]['created_on']; ?></td>
                                <td class="td-center td-label"> <?php echo $this->TSHtml->transactionStatusLabel($record[$model]['status']); ?></td>
                                <td class="td-center">
                                    <?php
                                        echo $this->Html->link(
                                                $this->Html->image("admin/summary_view.png", array("class" => "summary-action-icon")), 
                                                array('action' => 'admin_edit', $record[$model]['id']), 
                                                array('escape' => false, 'title' => EDIT_TITLE, "class" => 'ajax-page-link')
                                            );
                                    ?>
                                    <?php
                                        echo $this->Html->link(
                                            $this->Html->image("admin/pdf.png", array("class" => "summary-action-icon")),           
                                            array("controller" => "Invoices", 'action' => 'export_pdf', "admin" => true, $record[$model]['id']), 
                                            array('escape' => false, "class" => "")
                                        );
                                    ?>
                                </td>
                            </tr>
                    <?php } ?>
                    </tbody>                  
                </table>                    
                    <div>    
                      <?php echo $this->element("admin/pagination"); ?>
                    </div> 
            </div>
        </div>               

    </div>
</div>
