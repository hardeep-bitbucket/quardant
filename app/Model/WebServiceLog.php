<?php

/**
 * WebServiceLog Model
 * 
 * 
 * @created    06/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
class WebServiceLog extends AppModel 
{
    public $sanitize = false;
    /**
    *Associations
    */
    var $belongsTo = array(
        'Location' => array(
                'className' => 'Location',
                'foreignKey' => 'location_id',
                'conditions' => '',
                'fields' => '',
                'order' => ''
            )
         );
}