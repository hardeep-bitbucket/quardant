<?php

/**
 * summary header
 * 
 * 
 * @created    17/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */
?>
<div class="row">   
    <div class="col-md-12">
        <div class="pull-left">
            <h3 class="page-title">
              <?php echo ucwords($model); ?> <small>Manager</small>
            </h3>
        </div>
        <div class="pull-right">
            <div style="margin-top:10px;">
            <?php
                echo $this->Html->link('<i class="fa fa-th"></i>', array("action" => "admin_index"), array("escape" => false));
            ?>
            </div>
        </div>
    </div>
</div>

<?php
    //echo $this->element("admin/breadcrumb"); 
    echo $this->Session->flash();
?>