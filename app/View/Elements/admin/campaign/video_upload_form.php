<?php
/**
 * Campaign Controller
 * 
 * Detail tab
 * 
 * @created    05/06/2015
 * @package    ANPR
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

?>
<form action = "<?php echo Router::url(array("controller" => "campaigns",  "action" => "ajaxUploadVideo"));  ?>" method="post" enctype="multipart/form-data" id="<?php echo $field_name.$id; ?>">
    <input type="hidden" name="model" value="<?php echo $type ?>">
    <input type="hidden" name="id" value="<?php echo $id; ?>">

    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">Upload the file</div>  
        </div>

        <div class="portlet-body form">
            <div style="padding : 10px;">
                <div>
                    <div class="col-md-8" style="padding-top : 5px;">
                        <input type="file" name="<?php echo $field_name; ?>" id="file"/>
                    </div>                    
                    <div class="clearfix"></div>
                </div>

                <div>
                    <div style="padding-top : 25px; margin : auto; width : 100px;">
                        <button type="submit" class="btn green btn-submit">Upload</button>
                    </div>
                </div>

                <div class="ajax-status">
                    <div style="margin-top:15px;" class="margin-5">
                        Status : <b> <span class="ajax-status-txt"></span> </b>
                    </div>

                    <div class="progress progress-striped active" style="display:none;">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width : 0%">                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script>
$('#<?php echo $field_name.$id; ?>').submit(function(e) 
{   
    e.preventDefault();
    var updated_id = "<?php echo $updateID; ?>";
    
    var me = $(this);
    
    var options = { 
        target : me.find(".ajax-status-txt"), 
        data : {},
        beforeSubmit:  function()
        {
             var input = me.find("#file");
             console.log(input);
              // pre-submit callback 
              if (window.File && window.FileReader && window.FileList && window.Blob)
              {
                  if(!input.val()) //check empty input filed
                  {
                        $(options.target).html("<span class='required'>Please Select the file</span>");
                        return false;
                  }

                  var file = {};
                  file.size = input[0].files[0].size; //get file size
                  file.type = input[0].files[0].type; // get file type
                  
                  var temp = input.val().split(".");
                  file.ext = temp[temp.length - 1];
                  
                  var msg = Ajax.validateVideoFile(file);
                  
                  options.data = file;
                  if (msg)
                  {
                      $(options.target).html("<span class='required'>" + msg + "</span>");
                      return false;
                  }
              }
              else
              {
                 $(options.target).html("<span class='required'>Please upgrade your browser, because your current browser lacks some new features we need!</span>");
                 return false;
              }
              
              return true;
        },
        success: function (responseText, statusText, xhr)
        {
            responseText = JSON.parse(responseText);
            //console.log(responseText); console.log(statusText); console.log(xhr);
            
            var text = "";
            options.data.file = "";
            
            me.find(".progress").hide();
            me.find(".progress-bar").width('0%');
                
            if (responseText.status == 0)
            {
                text = "<span class='required'>" + responseText.msg + "</span>";
            }
            else
            {
                options.data.file = responseText.file;
                options.data.length = responseText.seconds;
            }
            
            $(options.target).html(text);
            upload_update_callback('<?php echo $id; ?>', updated_id, responseText.status, text, options.data);
        },  
        uploadProgress: function (event, position, total, percentComplete)
        {
           //upload progress callback            
           me.find(".progress").show();           
           me.find(".progress-bar").width(percentComplete + '%') 
           $(options.target).html(percentComplete + '%');
           upload_update_callback('<?php echo $id; ?>', updated_id, "running", "Uploading...(" + percentComplete + '%)');
        }, 
        resetForm : true        // reset the form after successful submit 
    }; 

    // submit the form
    me.ajaxSubmit(options);  			

    return false; 
});

</script>