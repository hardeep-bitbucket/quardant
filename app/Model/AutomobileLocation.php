<?php

/**
 * AutomobileLocation Model
 * 
 * 
 * @created    30/03/2015
 * @package    ANPR
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class AutomobileLocation extends AppModel {

    /**
     * Associations
     */
    var $belongsTo = array(
        'Location' => array(
            'className' => 'Location',
            'foreignKey' => 'location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        
    );
    
    var $hasMany = array(
        'Variant' => array(
            'className' => 'Automobile',
            'foreignKey' => 'vaiant_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        
    );
    
    public function saveRecord($data)
    {
        $record = $this->find("first", array(
            "fields" => array("id", "total_count"),
            "conditions" => array(
                "location_id" => $data['location_id'],
                "variant_id" => $data['variant_id'],
                "month_year =" => $data["month_year"],                
            ),
            "recursive" => -1
        ));
        
        $this->id = null;
                    
        if ($record)
        {            
            $this->id = $record["AutomobileLocation"]['id'];
            
            if (!isset($data["total_count"]))
            {
               $data["total_count"] =  $record["AutomobileLocation"]["total_count"] + 1;
            }
        }
        else
        {
            $data["total_count"] = 1;
        }
        
        
                
        return $this->save($data);
    }
}