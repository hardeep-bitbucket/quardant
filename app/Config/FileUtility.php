<?php

/**
 * file Uplaod Utility
 * 
 * 
 * @created    22/04/2015
 * @package    ANPR
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class FileUtility
{
   public $max_size, $ext, $options, $file_path, $errors;
   
   public $filename, $extension, $full_filename;
   
   // static variables
   public static $FIRST = 1, $LAST = 2;   
   
   
   /**
    * Constructor
    * 
    * @param int $max_size
    * @param array $extensions
    * @param array $options
    */
   public function __construct($max_size, $extensions, $options = array()) 
   {
       $this->max_size = $max_size;
       $this->ext = $extensions;
       $this->options = $options;
       
       foreach ($this->ext as $k => $ext)
       {
           $this->ext[$k] = strtolower($ext);
       }
       
       $this->options['override'] = isset($this->options['override']) ? $this->options['override'] : false;              
       $this->options['generate_filename'] = isset($this->options['generate_filename']) ? $this->options['generate_filename'] : true;              
       $this->options['prefix'] = isset($this->options['prefix']) ? $this->options['prefix'] : "";
       $this->options['postfix'] = isset($this->options['postfix']) ? $this->options['postfix'] : "";
       $this->options['new_filename'] = isset($this->options['new_filename']) ? $this->options['new_filename'] : "";
   }
   
   /**
    * 
    * upload a file to destination
    * @param array $file
    * @param string $dest_path
    * @return boolean
    */
   public function uploadFile($file, $dest_path, $validate = false)
   {
       //validating file
       if ($validate)
       {
            if (!$this->validateFile($file))
            {
                return false;
            }
       }
       
       //creating folder
       $dest_path = self::removePathSlashs($dest_path);
       self::createFolder($dest_path);
       $dest_path = self::addPathSlashs($dest_path, self::$LAST);
       
       // set file name       
       $this->filename = $this->getNextUploadFileName($file, $dest_path);
       
       $this->full_filename = $this->filename . "." . $this->extension;
       
       $this->file_path = $dest_path;
       
       if (!$this->options['override'])
       {
           if (file_exists($dest_path . $this->full_filename))
           {
               $this->errors[] = $this->full_filename . " File already exists";
               return false;
           }
       }
       
       
       return move_uploaded_file($file['tmp_name'], $dest_path . $this->full_filename);
   }   
   
   /**
    * validate the file
    * @param string $file
    * @return boolean
    */
   public function validateFile($file) 
   {
       $result = true;
       
       if ($file['size'] > $this->max_size)
       {
           $this->errors[] = "File size must not exceeds " . round($this->max_size / 1024) . " kb";
           $result = false;
       }
       
       $temp = pathinfo($file["name"]);
       
       $this->filename = $temp['filename'];
       $this->extension = strtolower($temp['extension']);
       
       if (!in_array($this->extension, $this->ext))
       {
           $this->errors[] = "Invalid file Type : " . $this->extension;
           $result = false;
       }    
       
       return $result;
   }
   
   /**
    * get next upload file name
    * @param array $file
    * @param string $dest_path
    * @return string
    */
   public function getNextUploadFileName($file, $dest_path)
   {
       if (!$this->filename)
       {
            $temp = pathinfo($file["name"]);

            $this->filename = $temp['filename'];
            $this->extension = $temp['extension'];
       }
       
       $filename = $this->options['prefix'] ? $this->options['prefix'] : "";
       $filename .= $this->options['new_filename'] ? $this->options['new_filename'] : $this->filename;
       $filename .= $this->options['postfix'] ? $this->options['postfix'] : "";
       
       if ($this->options['generate_filename'])
       {
           $filename = self::getAutoincreamentFileName($filename, $this->extension, $dest_path);
       }
       
       return $filename;
   }
   
   /**
    * return filename which which will be save 
    * @param string $filename
    * @param string $ext
    * @param string $dest_path
    * @return string
    */
   public static function getAutoincreamentFileName($filename, $ext, $dest_path, $sep = "_", $i = 0)
   {
       $temp_name =  $i > 0 ? $filename . $sep . $i : $filename;
       
       if (file_exists($dest_path . $temp_name . "." . $ext))
       {
           return self::getAutoincreamentFileName($filename, $ext, $dest_path, $sep, $i + 1);
       }
       else
       {
           return $temp_name;
       }
   }
   
   public static function createFolder($path)
   {
       if (!file_exists($path) && is_dir($path))
       {
            if (!mkdir($pathname, 0777, TRUE))
            {
                return false;
            }
       }
       return true;
   }
   
   /**
    * remove the slashs in path
    * @param string $path
    * @param const $side  // self::$FIRST, self::$LAST
    * @return string
    */
   public static function removePathSlashs($path, $side = '')
    {            
        $path = trim(str_replace('\\', '/', $path));
       
        if($side == self::$FIRST || !$side)
        {
            if(substr($path, 0, 1) == "/")
            {
                $path = substr($path, 1, strlen($path));
            }
        }               
        
        if($side == self::$LAST || !$side)
        {
            if(substr($path,-1) == "/")
            {
               $path = substr($path, 0, strrpos($path, "/"));
            }
        }        
        return $path;
    }

    /**
     * Add slashs in path
     * @param string $path
     * @param const $side // self::$FIRST, self::$LAST
     * @return string
     */
    public static function addPathSlashs($path, $side = '')
    {
        $path = trim(str_replace('\\', '/', $path));
        
        if(!$side || $side == self::$FIRST)
        {
            if(substr($path, 0, 1) != "/")            
            {
                 $path= "/" . $path;               
            }
        }               
        
        if(!$side || $side == self::$LAST)
        {
            if(substr($path, -1) != "/")
            {
               $path .= "/";
            }
        }
        
        return $path;
    }
   
   
    public static function deleteFile($filename)
    {
        if (file_exists($filename))
        {
            return unlink($filename);
        }
        
        return true;
    }
}