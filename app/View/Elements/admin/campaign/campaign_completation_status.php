<?php
/**
 * element for campaign detail and advertiser dashbaord
 * 
 * 
 * @created    11/04/2015
 * @package    ANPR
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

?>
<style>
    .protfolio-block .portfolio-stat{
        text-align: center;
        border: 1px solid #000;
        margin : 4%;
        padding : 4%;
        border-radius: 20px;
        width : 25%;
    }
    .protfolio-block  .portfolio-info{
        width : 100%;
    }
</style>

<div class="row top-news">
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <a class="btn purple round-10 margin-bottom-10" href="#" style="height : 140px;">
            <span>Completion  </span>                                           
            <span><b> <?php echo $record["complete"]; ?>% </b></span>
            <em>reached so far </em>  
            <i class="fa fa-tasks top-news-icon"></i>
        </a>
    </div>  
    
    <div class="col-lg-4 col-sm-6 col-xs-12">
        <a class="btn blue-soft round-10 margin-bottom-10" href="#">
            <span>Target Reach  </span>                                           
            <span><b> <?php echo $record["target_imperssions"]; ?> </b></span>
            <span>Current Reach  </span>
            <i class="fa fa-tasks top-news-icon"></i>
            <span><b> <?php echo $record["reached_imperssions"]; ?> </b></span>
        </a>
    </div>  
    
     <div class="col-lg-4 col-sm-6 col-xs-12">
        <a class="btn green-soft round-10 margin-bottom-10" href="#">
            <span>Target Budget  </span>                                           
            <span><b> <?php echo CURRENCY_SYMBOL; ?><?php echo $record["budget"]; ?> </b></span>
            <span>Current Budget  </span>
            <i class="fa fa-tasks top-news-icon"></i>
            <span><b> <?php echo CURRENCY_SYMBOL; ?><?php echo $record["spend"]; ?> </b></span>
        </a>
    </div>  
</div>