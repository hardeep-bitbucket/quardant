<footer id="footer">
    <div class="inner sep-bottom-md">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="widget sep-top-lg"><img src="/img/frontend/logo-white.png" alt="" class="logo">
                        <small class="sep-top-xs sep-bottom-md"></small>
                        <!-- <h6 class="upper widget-title">Stay connected</h6>
                        <ul class="social-icon sep-top-xs">
                            <li><a href="#" class="fa fa-github fa-lg"></a></li>
                            <li><a href="#" class="fa fa-twitter fa-lg"></a></li>
                            <li><a href="#" class="fa fa-facebook fa-lg"></a></li>
                            <li><a href="#" class="fa fa-google-plus fa-lg"></a></li>
                        </ul>-->
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="widget sep-top-lg">
                        <h6 class="upper widget-title">Visit Us</h6>
                        <ul class="widget-address sep-top-xs">
                            <li><i class="fa fa-map-marker fa-lg"></i><small>4-5 Bonhill St, Shoreditch, London EC2A 4BX</small></li>
                            <li><i class="fa fa-phone fa-lg"></i><small>(+39) 123-456-789 /<br>(+39) 123-456-789</small></li>
                            <li><i class="fa fa-envelope fa-lg"></i><small><a href="mailto:<?php echo $adminSetting["info_email"]; ?>"><?php echo $adminSetting["info_email"]; ?> /</a><br><a href="mailto:<?php echo $adminSetting["support_email"]; ?>"><?php echo $adminSetting["support_email"]; ?></a></small></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="widget sep-top-lg">
                        <h6 class="upper widget-title">Quick Links</h6>
                        <div class="flickr_container sep-top-xs">
                            <div id="flickr_badge_image1" class="flickr_badge_image">
                                <a href="/faq.html">FAQ</a>
                            </div>                             
                             <div id="flickr_badge_image1" class="flickr_badge_image">
                                <a href="#">Blog</a>
                            </div>
                            <div id="flickr_badge_image1" class="flickr_badge_image">
                                <a href="#">Press</a>
                            </div>
                            <div id="flickr_badge_image1" class="flickr_badge_image">
                                <a href="/careers.html">Careers</a>
                            </div>
                            <div id="flickr_badge_image1" class="flickr_badge_image">
                                <a href="/privacy-policy.html">Privacy Policy</a>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright sep-top-xs sep-bottom-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><small>Copyright © <?php echo date("Y"); echo " " . SITE_NAME; ?>. All rights reserved</small></div>
            </div>
        </div>
    </div>
</footer>
<style>
    .flickr_badge_image{
        float: none !important;
    }
</style>