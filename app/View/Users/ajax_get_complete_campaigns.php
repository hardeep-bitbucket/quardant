<?php

/**
 * dashboard User 
 * 
 * 
 * @created    17/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

?>

<div class="margin-5">
    <?php if ($list):  foreach ($list as $id => $name): ?>            
        <div style="background-color: #FAFAFA; font-size: 15px; padding: 5px" class="margin-5">
                <?php 
                    echo $this->Html->link($name, array(
                        "controller" => "campaigns", "action" => "campaign_detail", $id, "admin" => true,                       
                    ));
                ?>
        </div>        
    <?php endforeach; else: ?>
        <span class="required">No Data</span>
    <?php endif; ?>
</div>
