<?php
/**
 * change password User 
 * 
 * 
 * @created    23/02/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

?>


<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h3 class="page-title">
              <?php echo $title_for_layout; ?>
            </h3>
        </div>
    </div>
</div>

<?php
    echo $this->Session->flash();
?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
                echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal'));
                echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false, 'value' => $auth_user["id"]));                
            ?>
            <div class="form-body">   
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Current Password <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                 echo $this->Form->input('User.old_password', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control', 'required'
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">New Password <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                 echo $this->Form->input('User.new_password', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control', 'required'
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Confirm Password <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                 echo $this->Form->input('User.confirm_password', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control', 'required'
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>