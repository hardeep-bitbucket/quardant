<?php 
    /**
     * Import Area
     * 
     * 
     * @created    21/04/2015
     * @package    TFQ
     * @copyright  Copyright 2015
     * @license    Proprietary
     * @author     Hardeep
     */

echo $this->Session->flash();
?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">Import Car Manager</div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal', 'enctype' => "multipart/form-data"));
            ?>
            <div class="form-body">                 
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">CSV File <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('csv', array(
                                    'type' => 'file', 'label' => false, 'div' => false, 'escape' => false,
                                    "required" => "required",
                                    'class' => '',
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                             <div class="form-group">
                                <label class="control-label col-md-4"></label>
                                <div class="col-md-8">                                            
                                    <?php
                                    echo $this->Html->link("Click here for sample file format  ".$this->Html->image('xls.gif',
                                            array('style'=>'padding-left:10px;padding-top: 2px;position: absolute;')), 
                                            CAR_IMPORT_SAMPLE_FILE, array('class' => 'button','escape'=>false ,
                                            'target' => '_blank','style'=>'color:grey;text-decoration: none;'));  
                                    ?>                                            
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
        </div>
    </div>
</div>

          