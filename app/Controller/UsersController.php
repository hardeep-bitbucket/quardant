<?php

/**
 * Users Controller
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */
class UsersController extends AppController {
    /*
     * @Before Filter
     */

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array(
            'admin_login', "admin_add",
            'admin_logout', "logout" ,
            'initDB', 
            'add', 
            'login', 
            'logout', 
            'forgot_password', 
            'activation',                        
            "ajaxHomeDetails",
            "ajaxGetCompleteCampaigns"
        ));
        $this->_getIndustryType();
        
        $group_id = SUB_ADMIN_GROUP_ID;                
        $this->set(compact("group_id"));
    }

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() 
    {
        $this->Redirect->urlToNamed();
        $conditions = $this->getSearchConditions(array(
            array('model' => 'User', 'field' => 'username', 'type' => 'string', 'view_field' => 'username'),
            array('model' => 'User', 'field' => 'name', 'type' => 'string', 'view_field' => 'name'),            
        ));
        $conditions["User.group_id"] = SUB_ADMIN_GROUP_ID;
        $records = $this->paginate('User', $conditions);

        $this->set('title_for_layout', 'User Summary');
        $this->set(compact('records'));
    }

    /**
     * add method
     * @return void
     */
    public function admin_add() {
        parent::add();
        $heading = 'Add User';
        $reset_action = 'admin_index';
        $this->set('title_for_layout', 'Add User');

        //$parentPageList = $this->Page->getTreeList('page_id');

        $this->set(compact('heading', 'reset_action'));
        $this->render('admin_form');
    }

    /*
     * @Edit Screen
     */
    public function admin_edit($id = null, $redirect = array("action" => "index")) 
    {
        $this->{$this->modelClass}->recursive = -1;
        if (parent::edit($id, false))
        {
            if ($redirect)
            {
                $this->_sendEmailOnAccountApproval($this->request->data);
                
                $notification_data = array(
                    array(
                        "type" => StaticArray::$notification_type_alert,
                        "content" => "Your account has been approoved",
                        "user_id" => $this->request->data['User']["id"]
                    ),
                    array(
                        "type" => StaticArray::$notification_type_alert,
                        "content" => $this->request->data['User']['name'] . "'s account has been approoved",
                        "group_id" => ADMIN_GROUP_ID
                    )
                );
                
                $this->_saveNotification($notification_data);
            }
        }
        
        $heading = 'Edit User';
        $reset_action = 'admin_index';
        $this->set('title_for_layout', 'Edit User');

        $this->set(compact('heading', 'reset_action'));
        $this->render('admin_form');
    }    
    
    /**
     * 
     * @param int $id
     */
    public function admin_edit_profile($id = null)
    {
        $this->admin_edit($id, false);
    }

    /**
     * 
     * @Admin Login
     */
    public function admin_login() 
    {
        $this->layout = "admin_login";
        $this->set('title_for_layout', 'Login');
        
        if ($this->user)
        {
            if (isset($this->user["remember_me"]) && $this->user["remember_me"])
            {
                $this->redirect($this->Auth->loginRedirect);
            }
        }
        
        
        if ($this->request->is('post')) 
        {
            //debug($this->request->data); exit;
            if ($this->Auth->login()) 
            {
                if ($this->_validate_after_login())
                {
                    $this->redirect($this->Auth->loginRedirect);                    
                }                
                else
                {
                    $this->Session->setFlash('Your Account is not activated or approved.', "flash_failure");
                    $this->Auth->logout();
                }
            } 
            else
            {
                $this->Session->setFlash('Your username or password was incorrect.', "flash_failure");
            }
        }
    }
    
    /**
     * 
     * @Login
     */
    public function login() 
    {
        $this->autoRender = false;
        if ($this->request->is('post')) 
        {
            $this->request->data['User'] = $this->request->data;            
            
            if (isset($this->request->data["User"]["remember_me"]) && $this->request->data["User"]["remember_me"])
            {
                $this->request->data["User"]["remember_me"] = 1;
            }
            else
            {
                $this->request->data["User"]["remember_me"] = 0;
            }
            
            if ($this->Auth->login()) 
            {
                if ($this->_validate_after_login())
                {
                    echo 1;
                }
            } 
            else 
            {
                echo 0;
                //$this->Session->setFlash('Your username or password was incorrect.');
            }
        }
    }
    
    /**
     * function validate user alfter login successful
     */
    private function _validate_after_login()
    {
        
        $is_active = $this->Session->read('Auth.User.is_active');
        $is_approve = $this->Session->read('Auth.User.is_approve');

        if ($is_active && $is_approve)
        {
            $this->Session->write('Auth.User.login_time', DateUtility::getCurrentDateTimeString(DEFAULT_SQL_DATETIME_FORMAT));
            $this->Session->write('Auth.User.timezone', $this->request->data["User"]["timezone"]);
            $this->Session->write('Auth.User.remember_me', $this->request->data["User"]["remember_me"]);

            $this->{$this->modelClass}->id = $this->Session->read('Auth.User.id');
            $this->{$this->modelClass}->saveField('last_login', DateUtility::getCurrentDateTimeString(DEFAULT_SQL_DATETIME_FORMAT), FALSE);  

            return true;
        }
        else
        {
            return false;
        }
    }

    
    /**
     * Toggles the status of is_active field
     * 
     * @param Integer $id
     * @param Integer $status
     */
    public function admin_toggleStatus($id, $status) 
    {
        $this->{$this->modelClass}->id = $id;
        $result = $this->{$this->modelClass}->saveField('is_active', !(int) $status, FALSE);        
        echo (int) $result; exit;
    }
    
    /**
     * 
     * @Admin Logout
     */
    public function admin_logout() 
    {
        $this->Session->setFlash('Good-Bye', "flash_success");
        $this->redirect($this->Auth->logout());
    }
    
     /**
     * 
     * @Logout
     */
    public function logout() {
        $this->Auth->logout();
        $this->redirect(array('controller' => '/', 'action' => '/'), null, false);
    }

    
    /**
     * admin home page
     */
    public function admin_home()
    {   
        $country_list = $this->{$this->modelClass}->Campaign->Country->getChildList();        
        $this->set(compact('country_list'));
        $this->set('title_for_layout', 'Home');
    }
    
    /**
     * admin home page detail according to country
     * @param int $country_id
     */
    public function ajaxHomeDetails($country_id = 0)
    {
        $country_id = $country_id ? $country_id : "NULL";
        
        $records = $this->{$this->modelClass}->query("CALL sp_get_top_locations(NULL, $country_id, NULL, NULL, 1);");
        
        $best_location = array();
        if (isset($records[0][0]['total_count']) && $records[0][0]['total_count'])
        {
            $best_location = $records[0]["L"];  
            
            $this->{$this->modelClass}->Campaign->AdLocation->Location->contain(array(
                "State" => array("fields" => array("id", "name")), 
                "Country" => array("fields" => array("id", "name")),
                "City" => array("fields" => array("id", "name"))
            ));
            
            $best_location = $this->{$this->modelClass}->Campaign->AdLocation->Location->find("first", array(                
                "fields" => array("id", "name"),
                "conditions" => array(
                    "Location.id" => $best_location['id']
                )
            ));
        }
        
         // getting best campaign                
        $records = $this->{$this->modelClass}->query("CALL sp_get_top_campaigns($country_id, NULL, NULL, 1);");        
        $best_campaign = array();
        if (isset($records[0][0]['total_count']) && $records[0][0]['total_count'])
        {
            $best_campaign = $records[0]["C"];  
        }
        
        // getting best Advertiser
        $records = $this->{$this->modelClass}->query("CALL sp_get_top_users_on_campaign_count($country_id, 1);");        
        $best_advertiser = array();
        if (isset($records[0][0]['total_count']) && $records[0][0]['total_count'])
        {
            $best_advertiser['campaign_count'] = $records[0]["U"];  
            
            $records = $this->{$this->modelClass}->query("CALL sp_get_top_users_on_campaign_spend($country_id, NULL, 1);");        
            if (isset($records[0][0]['total_count']) && $records[0][0]['total_count'])
            {
                $best_advertiser['campaign_spend'] = $records[0]["U"];  
            }
        }
        
        $conditions = array();
        if ($country_id != "NULL")
        {
            $conditions['country_id'] = $country_id;
        }
        
        $this->{$this->modelClass}->Campaign->contain(array(
            "AdLocation" => array(
                "fields" => array("id", "location_id", "target_imperssions", "reached_imperssions"),
                "Location" => array(
                    "fields" => array("price_per_impression", "setup_cost", "is_shared", "partner_id", "partner_revenue_share_percentage")
                )
            )
        ));
        
        $records = $this->{$this->modelClass}->Campaign->find("all", array(
            "fields" => array("id", "budget", "status_type_id"),
            "conditions" => $conditions,
        ));
        
        $campaigns["total"] = $campaigns["active"] = $campaigns["submitted"] = $campaigns["completed"] = 0;
        $campaigns["active_spend"] = $campaigns["submitted_budget"] = $campaigns['total_spend'] = $campaigns['total_budget'] = 0;        
        $campaigns["total_created"] =  $campaigns["admin_revenue"] = $campaigns["partner_revenue"]  = 0;
        foreach ($records as $key => $record)
        {
            $campaigns["total"]++;
            $records[$key] = $this->_getCampaignStatusInfo($record);     
            
            $campaigns["total_spend"] += $records[$key]['Campaign']['spend'];
            $campaigns["total_budget"] += $records[$key]['Campaign']['budget'];
            $campaigns["admin_revenue"] += $records[$key]['Campaign']['admin_total'];
            $campaigns["partner_revenue"] += $records[$key]['Campaign']['partner_total'];
            
            switch($record["Campaign"]['status_type_id'])
            {
                case CAMPAIGN_ACTIVE_ID:
                    $campaigns["active"]++;                    
                    $campaigns["active_spend"] += $records[$key]['Campaign']['spend'];
                break;
            
                case CAMPAIGN_SUBMITTED_ID:
                    $campaigns["submitted"]++;
                    $campaigns["submitted_budget"] += $record['Campaign']['budget'];
                break;
            
                case CAMPAIGN_COMPLETED_ID:
                    $campaigns["completed"]++;
                break;            
            }            
        }
                
        $others["new_advertisers"] = $this->{$this->modelClass}->find("count", array(
            "conditions" => array(
                "is_approve" => false,
            )
        ));
        
        $this->loadModel("Car");  
        $this->Car->recursive = -1;
        $others['total_audiance'] = $this->Car->find("first", array(
            "fields" => array("count(id) as total_count"),
            "conditions" => array_merge($conditions, array(
                "total_count >" => 0
            ))
        ));
        
        $others['total_audiance'] = $others['total_audiance'][0]['total_count']; 
        
        
        $this->set(compact('best_location', 'best_advertiser', 'best_campaign',  'campaigns', 'others'));
    }
    
    public function admin_dashboard()
    {
        $country_list = $this->{$this->modelClass}->Campaign->Country->getChildList();        
        $this->set(compact('country_list'));
        $this->set('title_for_layout', 'Dashbaord');
    }
    
    public function ajaxGetCompleteCampaigns($country_id, $days)
    {
        $current_date = DateUtility::getCurrentDateTimeObj();
        $date = DateUtility::addDaysInDateObj($current_date, $days);
        
        $list = array();
        
        $conditions = array();
        
        if ($country_id)
        {
            $conditions['country_id'] = $country_id;
        }
        
        if ($date < $current_date)
        {
            $list = $this->{$this->modelClass}->Campaign->find("list", array(
                "conditions" => array_merge($conditions, array(
                    "end_date >=" => DateUtility::getFormatDateFromObj($date, DEFAULT_SQL_DATE_FORMAT),
                    "end_date <=" => DateUtility::getFormatDateFromObj($current_date, DEFAULT_SQL_DATE_FORMAT),
                    "status_type_id" => array(CAMPAIGN_COMPLETED_ID, CAMPAIGN_TERMINATE_ID),                    
                )),
                "recursive" => -1
            ));
        }
        else
        {
            $list = $this->{$this->modelClass}->Campaign->find("list", array(
                "conditions" => array_merge($conditions, array(
                    "end_date >=" => DateUtility::getFormatDateFromObj($current_date, DEFAULT_SQL_DATE_FORMAT),
                    "end_date <=" => DateUtility::getFormatDateFromObj($date, DEFAULT_SQL_DATE_FORMAT),
                )),
                "recursive" => -1
            ));
        }
        
        $this->set(compact('list'));
    }
    
    /**
     * 
     * @Admin initDB
     */
    public function initDB() {
        $group = $this->User->Group;
        
        // Allow admins to everything
        $group->id = ADMIN_GROUP_ID;
        $this->Acl->allow($group, 'controllers');
        $this->Acl->deny($group, 'controllers/Transactions/admin_advertiser_recharge');
        $this->Acl->deny($group, 'controllers/Transactions/admin_advertiser_refund');
        
        $group->id = SUB_ADMIN_GROUP_ID;        
        $this->Acl->allow($group, 'controllers');
        $this->Acl->deny($group, 'controllers/AdminSettings');        
        $this->Acl->deny($group, 'controllers/Users/admin_add');
        $this->Acl->deny($group, 'controllers/Users/admin_index');
        $this->Acl->deny($group, 'controllers/Campaigns');
        
        $group->id = ADVERTISER_GROUP_ID;
        $this->Acl->deny($group, 'controllers');
        $this->Acl->allow($group, 'controllers/Campaigns');                     
        $this->Acl->allow($group, 'controllers/Advertisers/admin_dashboard');
        $this->Acl->allow($group, 'controllers/Advertisers/admin_home');
        $this->Acl->allow($group, 'controllers/Advertisers/admin_edit_profile');
        $this->Acl->allow($group, 'controllers/Users/admin_change_password');                
        $this->Acl->allow($group, 'controllers/AdLocations');
        $this->Acl->allow($group, 'controllers/Invoices');
        $this->Acl->allow($group, 'controllers/Transactions');
        
        $this->Acl->deny($group, 'controllers/Transactions/admin_recharge_wallet');
        $this->Acl->deny($group, 'controllers/Transactions/admin_refund_wallet');
        
        
        
        $group->id = PARTNER_GROUP_ID;
        $this->Acl->deny($group, 'controllers');                        
        $this->Acl->allow($group, 'controllers/Advertisers/admin_home');
        $this->Acl->allow($group, 'controllers/Partners/admin_home');
        $this->Acl->allow($group, 'controllers/Partners/admin_edit_profile'); 
        $this->Acl->allow($group, 'controllers/Users/admin_change_password');
        
        // we add an exit to avoid an ugly "missing views" error message
        echo "all done";
        exit;
    }

    /*
     * @Change Password Screen
     */

    function admin_change_password() 
    {
        if (!empty($this->request->data)) 
        {
            $cansave = true;            
            $this->User->recursive = -1;
            $user = $this->User->findById($this->request->data['User']['id'], array("fields" => "password"));
            
            if (!$user) {
                $this->User->validationErrors['username'] = 'Username not found';
                $cansave = false;
            }
            else if ($user['User']['password'] != $this->Auth->password($this->request->data['User']['old_password'])) 
            {
                $this->User->validationErrors['old_password'] = 'Password is incorrect';
                $cansave = false;
            }
            
            if ($this->request->data['User']['new_password'] != $this->request->data['User']['confirm_password']) {
                $this->User->validationErrors['confirm_password'] = "Password didn't match";
                $cansave = false;
            }
            
            if ($cansave) {                
                $this->User->id = $this->request->data['User']['id'];
                if ($this->User->saveField('password', $this->request->data['User']['new_password'])) {
                    $this->Session->setFlash('Password changed successfully', "flash_success");
                    $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash("password could not be changed", "flash_failure");
                }
            }
        }
        
        $title_for_layout = "User Manager";
        if ($this->user['group_id'] == ADVERTISER_GROUP_ID)
        {
            $title_for_layout = "Advertiser Manager";
        }
        else if ($this->user['group_id'] == PARTNER_GROUP_ID)
        {
            $title_for_layout = "Partner Manager";
        }
        
        $heading = 'Change Password';
        
        $this->set(compact('heading', 'title_for_layout'));
    }

    /*
     * 
     * @Get Industry Types
     */

    private function _getIndustryType() {
        $industry_types = $this->User->Type->find("list", array("conditions" => array("Type.type" => INDUSTRY_TYPES)));
        $this->set(compact("industry_types"));
    }

    /*
     * @Frontend Actions
     * @Add
     */

    /**
     * add method
     * @return void
     */
    public function add() 
    {
        $this->autoRender = false;
        if (!empty($this->request->data)) 
        {
            if ($this->Session->read("captcha_code") != $this->request->data["captcha"])
            {
                echo "<p>Unable to Save.<br> Invalid Confirmation Code</p>";
                return;
            }
            
            if ($this->request->data["User"]["password"] != $this->request->data["User"]["confirm_password"])
            {
                echo "<p>Unable to Save.<br> Passwords didn't match</p>";
                return;
            }
           
            if ($this->User->save($this->request->data)) 
            {
                $confirmationCode = get_random_string($this->request->data['User']['name'], 6) . $this->User->id;
                // send email to user
                $this->User->saveField('confirmation_code', $confirmationCode);
                $to_email = $this->request->data['User']['username'];
                $from_email = FROM_EMAIL;
                $subject = "Registration";  // A string value defining the title of Email .
                
                $footer_content = $this->_getEmailFooterContent();
                
                $viewData["record"] = $this->request->data;
                $viewData["record"]['confirmationCode'] = $confirmationCode;
                $viewData["content"]['footer'] = $footer_content;
                
                $template = 'advertiser_registration'; // for advertisers
                if ($this->request->data[$this->modelClass]['group_id'] == PARTNER_GROUP_ID)
                {
                    $template = 'partner_registration';                
                }                
                
                $this->sendEmail($to_email, $from_email, $subject, $template, $viewData, $files = array(), $bcc = array());

                // Send email to admin
                $from_email = FROM_EMAIL;
                $to_email = TO_EMAIL;
                $subject = "Registration";
                $template = 'advertiser_registration_to_admin';
                $viewData = array("records" =>  $this->request->data);
                $viewData["content"]['footer'] = $footer_content;
                
                if ($this->request->data[$this->modelClass]['group_id'] == PARTNER_GROUP_ID)
                {
                    $template = 'partner_registration_to_admin';                
                }   
                
                // notifications
                if ($this->request->data[$this->modelClass]['group_id'] == PARTNER_GROUP_ID)
                {
                    $data = array(
                        array(
                            "type" => StaticArray::$notification_type_alert,
                            "content" => "Partner <b>" . $this->request->data['User']['name'] . "</b> has Registered an account",
                            "group_id" => ADMIN_GROUP_ID
                        )
                    );
                }
                else
                {
                    $data = array(
                        array(
                            "type" => StaticArray::$notification_type_alert,
                            "content" => "Advertiser <b>" . $this->request->data['User']['name'] . "</b> has Registered an account",
                            "group_id" => ADMIN_GROUP_ID
                        )
                    );
                }
                        
                $this->_saveNotification($data); 
                
                $this->sendEmail($to_email, $from_email, $subject, $template, $viewData, $files = array(), $bcc = array());

                echo "success";
            } 
            else 
            {
                $str = "";
                
                foreach ($this->User->validationErrors as $value)
                {
                    $str .= implode(" , ", $value);
                }
                    
                echo "<p>Unable to Save.<br> $str</p>";
            }
        }
    }

    /*
     * @Activation of Advertiser type user
     */

    function activation($code = null) {
        $this->autoRender = false;
        # id is available in the url.
        
        if (!empty($this->params['pass'][0])) {
            # check activation expiry.
            $user = $this->User->find('first', array('recursive' => -1, 'conditions' => array('User.confirmation_code' => $this->params['pass'][0])));
            if (!empty($user)) {
                # active registered user and set activation_expiry to 1.
                $this->User->id = $user['User']['id'];
                $this->User->saveField('is_active', 1);
                $this->User->saveField('is_confirmed', 1);
                
                // send email to user
                $to_email = $user['User']['username'];
                $from_email = FROM_EMAIL;
                $subject = "Account Activation.";  // A string value defining the title of Email .
                $userData = array();
                $template = 'account_activation';
                
                $viewData = array(
                    "content" => array(
                        'footer' =>  $this->_getEmailFooterContent()
                    ),
                    'username' => $user['User']['name']
                );
                    
                //$this->sendEmail($to_email, $from_email, $subject, $template, $viewData, $files = array(), $bcc = array());

                // Send email to admin
                $from_email = FROM_EMAIL;
                $to_email = TO_EMAIL;
                $subject = "Account Activation.";
                $template = 'account_activation_to_admin';
               
                $this->sendEmail($to_email, $from_email, $subject, $template, $viewData, $files = array(), $bcc = array());
                
                if ($user["User"]["group_id"] == ADVERTISER_GROUP_ID)
                {
                    $link = Router::url(array("controller" => "advertisers", "action"=> "admin_edit", $user['User']['id']));
                    $link = "<a href='" . $link . "'>" . $user['User']['name'] . "</a>";
                    
                    // notifications
                    $data = array(
                        array(
                            "type" => StaticArray::$notification_type_alert,
                            "content" => "Advertiser <b>$link</b> has Activated",
                            "group_id" => ADMIN_GROUP_ID
                        )
                    );
                }
                else
                {
                    $link = Router::url(array("controller" => "partners", "action"=> "admin_edit", $user['User']['id']));
                    $link = "<a href='" . $link . "'>" . $user['User']['name'] . "</a>";
                    
                    $data = array(
                        array(
                            "type" => StaticArray::$notification_type_alert,
                            "content" => "Partner <b>$link</b> has Activated",
                            "group_id" => ADMIN_GROUP_ID
                        )
                    );
                }
                        
                $this->_saveNotification($data); 
                
                echo "<h1>Account Confirmation Successfully</h1>";
            }
            else
            {
                echo "<h1>Failed to save. try after some time</h1>";
            }
            
            echo "<br><br><a href='" . Router::url(array('controller' => '/', 'action' => '/'))  . "'> Click Here </a> to goto Site";
            
            exit;
            
        }
    }

    
   
    /*
     * @Forget Password Screen
     */

    public function forgot_password() 
    {
        $this->loadModel("Page");
        
        $this->getModule();
        $this->getPages();
        $this->getMetaInfo();
        $this->getUpdatesForFooter();
		
        $this->layout = "inner";

        $this->User->recursive = 0;
        if ($this->request->is('post')) 
        {
            $user = $this->User->find('first', array('recursive' => -1, 'conditions' => array('User.username' => $this->request->data['username'])));
            if (empty($user)) 
            {
                $this->Session->setFlash("Email does not exist.");
                $this->redirect($this->referer());
            } 
            else 
            {
                $new_password = get_random_string('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 10);
                
                $this->User->id = $user['User']['id'];                
                $this->User->savefield('password', $new_password);	                
                
                $to_email = $user['User']['username'];
                $from_email = FROM_EMAIL;
                $subject = "Forget Password";                
                $template = "forget_password";
                
                $viewData = array(
                    "User" => array(
                        "name" => $user['User']['name'],
                        "password" => $new_password
                    ),
                    "content" => array(
                        'footer' =>  $this->_getEmailFooterContent()
                    ),
                );
                
                $this->sendEmail($to_email, $from_email, $subject, $template, $viewData, $files = array(), $bcc = array());
                
                $this->Session->setFlash('Email has been sent successfully.');
                $this->redirect($this->referer());
            }
        }
        $meta_title = "Forgot Password";
        $this->set(compact("meta_title"));
    }
    
    private function _sendEmailOnAccountApproval($data)
    {
        $to_email = $data['User']['username'];
        $from_email = FROM_EMAIL;
        $subject = "Forget Password";                
        $template = "account_approval";

        $viewData = array(
            "User" => array(
                "name" => $data['User']['name'],
            ),
            "content" => array(
                'footer' =>  $this->_getEmailFooterContent()
            ),
        );

        $this->sendEmail($to_email, $from_email, $subject, $template, $viewData, $files = array(), $bcc = array());
    }
   
}
