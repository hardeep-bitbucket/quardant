<?php

/**
 * Discount Model
 * 
 * 
 * @created    05/05/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */

/**
 * 
 */
class Discount extends AppModel {

    /**
     * Behaviours
     */
    public $actsAs = array('DateFormat' => array('start_date', 'end_date'));

    /**
     * Server Validations
     */
    public $validate = array(
        'discount_code' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD),
            'isUnique' => array(
                'rule' => array('isUnique', 'discount_code'), 'message' => 'Discount code already exist.'
            )
        ),
        'discount_type' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => DISCOUNT_TYPE_REQUIRED_MESSAGE)
        ),
        'discount_for' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => DISCOUNT_FOR_REQUIRED_MESSAGE)
        ),
        'start_date' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD),
             'checkRange' => array(
                'rule' => 'checkRange', "message" => "Start date must be smaller than or equal to End date"
            )
        ),
        'end_date' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'campaign_trial_type' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'campaign_duration_type' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'campaign_target_automobile_type' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
    );
    
    public function checkRange()
    {
        if (isset($this->data['Discount']["start_date"]) && isset($this->data['Discount']["end_date"]))
        {   
            $start_date = DateUtility::getDateObj($this->data['Discount']["start_date"]);                  
            $end_date = DateUtility::getDateObj($this->data['Discount']["end_date"]);
            
            if ($start_date > $end_date)
            {
                return false;
            }            
        }
        return true;
    }
    
    public function increaseUsage($code)
    {
        if ($code)
        {
            $record = $this->find("first", array(
                "fields" => array(
                    "actual_usage", "id"
                ),
                "conditions" => array(
                    "discount_code" => $code
                )
            ));
            
            if (!$record["Discount"]['actual_usage'])
            {
                $record["Discount"]['actual_usage'] = 0;
            }

            $actual_usage = (int) $record["Discount"]['actual_usage'] + 1;

            $this->id = $record["Discount"]['id'];
            return $this->saveField("actual_usage", $actual_usage, false);
        }
        
        return true;
    }

}