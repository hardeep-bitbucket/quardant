<?php

/**
 * AdLocationLog Model
 * 
 * 
 * @created    05/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     HARDEEP
 */

class AdLocation extends AppModel 
{
    /*
     * Behaviours 
     */
    var $actsAs = array(   
//         'MeioUpload' => array(
//            'ad_name' => array(
//                'dir' => AD_CREATIVE,
//                'createDirectory' => false,
//                'allowed_mime' => array('video/mp4'),
//                'allowed_ext' => array('.mp4'),
//                'maxSize' => 20000000, //20 MB
//                'thumbnails' => false,                
//                'thumbnailDir' => false, 
//                "video" => array(
//                    "duration" => array(
//                        "max" => array(
//                            "length" => AD_LOCAL_DURATION_LIMIT, //seconds
//                            "error_msg" => AD_LOCAL_VIDEO_DURATION_VALIDATION_MSG
//                        )
//                    )
//                )
//            ),
//        ),
        'DateFormat' => array('from_date', 'to_date')
    );
    
    public $validate = array(
        'ad_url' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty', 'message' => REQUIRED_FIELD
            )
        ),
        'end_time' => array(
            'checkTime' => array(
                'rule' => "checkTime", "message" => "End time should be large than Start Time"
            )
        )
    );
    
    /**
    *Associations
    */
    var $belongsTo = array(       
         'Location' => array(
                    'className' => 'Location',
                    'foreignKey' => 'location_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => ''
                ),
         'Campaign' => array(
                    'className' => 'Campaign',
                    'foreignKey' => 'campaign_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => ''
                )
    );
    
    public $hasMany = array(        
        'AdPlayedLog' => array(
            'className' => 'AdPlayedLog',
            'foreignKey' => 'ad_location_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    public function afterFind($results, $primary = false) 
    {
        parent::afterFind($results, $primary);
        
        foreach ($results as $key => $data)
        {
            if (isset($data["AdLocation"]["start_time"]) && $data["AdLocation"]["start_time"])
            {
                $results[$key]["AdLocation"]["start_time"] = DateUtility::getFormatDateFromString("2015-01-01 " . $results[$key]["AdLocation"]["start_time"], DEFAULT_TIME_FORMAT);
            }
            
            if (isset($data["AdLocation"]["end_time"]) && $data["AdLocation"]["end_time"])
            {
                $results[$key]["AdLocation"]["end_time"] = DateUtility::getFormatDateFromString("2015-01-01 " . $results[$key]["AdLocation"]["end_time"], DEFAULT_TIME_FORMAT);
            }            
        }
        
        return $results;
    }
    
    public function afterValidate() 
    {
        parent::afterValidate();
        
        if (empty($this->validationErrors) && isset($this->data['AdLocation']["ad_url"]))
        {
            if ($this->data['AdLocation']["ad_url"])
            {
                $msg = $this->ValidateYouTubeURL($this->data['AdLocation']["ad_url"]);
                
                if ($msg)
                {
                    $this->validationErrors['ad_url'][] = $msg;
                }
            }
        }
        return true;
    }
    
    public function beforeSave($options = array()) 
    {
        $result = true;
        
        if (isset($this->data["AdLocation"]["use_default_creative"]))
        {
            if ($this->data["AdLocation"]["use_default_creative"])
            {
                $this->data["AdLocation"]["ad_name"] = "";
                $this->data["AdLocation"]["ad_type"] = "0";
                $this->data["AdLocation"]["ad_url"] = "";
            }
        }
        
        if ($result)
        {
            parent::beforeSave($options);
        }
        
        return $result;
    }
        
    public function checkTime()
    {
        if (isset($this->data['AdLocation']['start_time']) && $this->data['AdLocation']['start_time'] && isset($this->data['AdLocation']['end_time']) && $this->data['AdLocation']['end_time'])
        {
            $this->data['AdLocation']["start_time"] = $this->data['AdLocation']["start_time"] == "24:00:00" ? "23:59:00" : $this->data['AdLocation']["start_time"];
            $this->data['AdLocation']["end_time"] = $this->data['AdLocation']["end_time"] == "24:00:00" ? "23:59:00" : $this->data['AdLocation']["end_time"];
            
            $start_date = DateUtility::getDateObj("2015-01-01 " . $this->data['AdLocation']["start_time"]);
            $end_date = DateUtility::getDateObj("2015-01-01 " . $this->data['AdLocation']["end_time"]);
            
            if ($start_date >= $end_date)
            {
                return false;
            }
        }
        return true;
    }
    
    /**
     * just update fields, not hard delete
     * @param int $id
     * @return bool
     */
    public function deleteSoft($id) 
    {
        $data = $this->findById($id, array(
            "fields" => "is_download"
        ));
        
        $data["AdLocation"] = array(
            "is_download" => 0,
            "is_delete" => $data['AdLocation']['is_download'] == 2 ? 1 : 0,
            "is_deleted" => 1
        );
        
        $this->id = $id;
        
        return $this->save($data);
    }
    
    
}

?>