<?php
/**
 * Campaign Controller
 * 
 * Target duration tab
 * 
 * @created    26/02/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
//debug($this->validationErrors); exit;
echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal', 'enctype' => "multipart/form-data"));
echo $this->Form->hidden('tab_name', array('label' => false)); 
?>

<div class="row">    
    <div class="col-md-12">
        <h3 class="form-section">Global Date & Time Range</h3>                                    
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">From Date <span class="required">*</span></label>
                    <div class="col-md-8">                                            
                        <?php
                            echo $this->Form->input("global_from_date", array(
                                'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                            
                                'class' => 'form-control global-from-date'
                            ));
                        ?>                                            
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">To Date <span class="required">*</span></label>
                    <div class="col-md-8">
                        <?php
                            echo $this->Form->input("global_to_date", array(
                                'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                'class' => 'form-control global-to-date'
                            ));
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">Start Time <span class="required">*</span></label>
                    <div class="col-md-8">                                            
                        <?php
                            echo $this->Form->input("global_start_time", array(
                                'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                            
                                'class' => 'form-control global-from-time'
                            ));
                        ?>                                            
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">End Time <span class="required">*</span></label>
                    <div class="col-md-8">
                        <?php
                            echo $this->Form->input("global_to_time", array(
                                'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                'class' => 'form-control global-to-time'
                            ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="col-md-12">
   <div class="panel-group accordion" id="accordion1">
        
       <?php $i = 0;  foreach ($records as $state_name => $record): $i++; ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion3" href="#<?php echo $i; ?>">
                            <?php echo $state_name; ?>
                        </a>
                    </h4>
                </div>
                <div id="<?php echo $i; ?>" class="panel-collapse in">
                                        
                    <div class="portlet-body form-body">
                        <ul class="nav nav-tabs">
                            <?php $a = 0 ; foreach ($record as $ad_location_id => $location_name): $a++; ?>
                                <li class="<?php echo $a == 1 ? 'active' : '' ?>">
                                    <a href="#<?php echo $i . "-" . $a; ?>" data-toggle="tab">
                                        <?php echo $location_name; ?> 
                                    </a>
                                </li>                                
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content">
                            <?php $a = 0 ; foreach ($record as $ad_location_id => $location_name): $a++; ?>
                                <div class="tab-pane fade <?php echo $a == 1 ? 'active' : '' ?> in" id="<?php echo $i . "-" . $a; ?>">
                                    
                                    <h3 class="form-section">Days</h3>
                                    
                                    <span class="required">
                                        <?php                                        
                                            if (isset($this->validationErrors["AdLocation"][$ad_location_id]["days"]))
                                            {
                                                echo $this->validationErrors["AdLocation"][$ad_location_id]["days"];
                                            }
                                        ?>
                                    </span>
                                    <div class="row">
                                        <div class="col-md-12 days-checkbox-div">
                                            <?php
                                                foreach (StaticArray::$campaign_days as $key => $label):
                                            ?>        
                                                <div style="width : 14%; float: left;">
                                                    
                                                    <div class="col-md-3">
                                                        <?php
                                                             echo $this->Form->input("AdLocation.$ad_location_id.$key", array(
                                                                 'type' => "checkbox", 'div' => false, 'label' => false, "escape" => false,
                                                                 'class' => "form-control form-checkbox days"
                                                             ));    

                                                         ?>
                                                    </div>
                                                    <label class="control-label col-md-9" style="text-align : left;"><?php echo $label;  ?></label>
                                                    
                                                </div>
                                            
                                            <?php endforeach; ?>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    
                                    <h3 class="form-section">Date & Time Range</h3>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">From Date <span class="required">*</span></label>
                                                <div class="col-md-8">                                            
                                                    <?php
                                                        echo $this->Form->input("AdLocation.$ad_location_id.from_date", array(
                                                            'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                            
                                                            'class' => 'form-control datepicker-custom  from-date', "required"
                                                        ));
                                                    ?>                                            
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">To Date <span class="required">*</span></label>
                                                <div class="col-md-8">
                                                    <?php
                                                        echo $this->Form->input("AdLocation.$ad_location_id.to_date", array(
                                                            'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                            'class' => 'form-control datepicker-custom to-date', "required"
                                                        ));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Start Time <span class="required">*</span></label>
                                                <div class="col-md-8">                                            
                                                    <?php
                                                        echo $this->Form->input("AdLocation.$ad_location_id.start_time", array(
                                                            'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                            
                                                            'class' => 'form-control  timepicker timepicker-no-seconds from-time', "required"
                                                        ));
                                                    ?>                                            
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-4">End Time <span class="required">*</span></label>
                                                <div class="col-md-8">
                                                    <?php
                                                        echo $this->Form->input("AdLocation.$ad_location_id.end_time", array(
                                                            'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                            'class' => 'form-control timepicker timepicker-no-seconds to-time', "required"
                                                        ));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                </div>                                
                            <?php endforeach; ?>
                        </div>
                    </div>

                </div>
            </div>
     <?php endforeach; ?>
    </div>
</div>
</div>

<div class="row">
    <div class="col-md-12">
        <div style="margin-top: 10px;">
            <?php echo $this->element("admin/tab_save_btn"); ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function ()
    {
       $(".from-time").timepicker({
           defaultTime : "12:00 AM"
       });       
       
       $(".to-time").timepicker({
           defaultTime : "11:59 PM"
       });
       
        $(".global-from-time").timepicker({
           defaultTime : "12:00 AM"
        }).
        on("changeTime.timepicker", function(e)
        {
            $(".from-time").timepicker('setTime', e.time.value);
        });
        
        $(".global-to-time").timepicker({
           defaultTime : "11:59 PM"
        }).
        on("changeTime.timepicker", function(e)
        {
            $(".to-time").timepicker('setTime', e.time.value);
        });
        
       
       $(".from-date").datepicker(
        {            
            format: '<?php echo DEFAULT_JS_DATE_FORMAT  ?>', 
            startDate : '<?php echo $start_date; ?>',
            endDate : '<?php echo $end_date1; ?>',
            autoclose: true,
        }).on("changeDate", function (e)
        {            
            var d = $(this).datepicker("getDate");                        
            d.setDate(d.getDate() + 1);
            var row = $(this).parent().parent().parent().parent();
            
            row.find(".to-date").datepicker("setStartDate", d);
        });
        
        $(".to-date").datepicker(
        { 
            format: '<?php echo DEFAULT_JS_DATE_FORMAT  ?>',   
            startDate : '<?php echo $start_date1; ?>',
            endDate : '<?php echo $end_date; ?>',
            autoclose: true,
        }).on("changeDate", function (e)
        {
            var d = $(this).datepicker("getDate");
            d.setDate(d.getDate() - 1);      
            var row = $(this).parent().parent().parent().parent();
            
            row.find(".from-date").datepicker("setEndDate", d);
        });  
        
        ///////////////// global ////////////////////////
        $(".global-from-date").datepicker(
        {            
            format: '<?php echo DEFAULT_JS_DATE_FORMAT  ?>', 
            startDate : '<?php echo $start_date; ?>',
            endDate : '<?php echo $end_date1; ?>',
            autoclose: true,
        }).on("changeDate", function (e)
        {   
            var d = $(this).datepicker("getDate");                   
            $(".from-date").datepicker("update", d);            
        });
        
        $(".global-to-date").datepicker(
        { 
            format: '<?php echo DEFAULT_JS_DATE_FORMAT  ?>',   
            startDate : '<?php echo $start_date1; ?>',
            endDate : '<?php echo $end_date; ?>',
            autoclose: true,
        }).on("changeDate", function (e)
        {
            var d = $(this).datepicker("getDate");             
            $(".to-date").datepicker("update", d);
        });  
        
        
        $(".days-checkbox-div").each(function()
        {
            var row = $(this);
            var check_count = 0;
            $(this).find("input.days").each (function()
            {
                if (this.checked)
                {
                    check_count++;
                }
            });
            
            if (check_count == 0)
            {
                $(this).find("input.days").prop("checked", true);
            }
        });
    });
</script>