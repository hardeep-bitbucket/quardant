Dear Admin,<br/><br/>

You have received a enquiry from website. The Details are as follows:<br/><br/>

<table border=0>
    <tr>
        <th>Full Name</th>
        <th>Company Name</th>
        <th>Company URL</th>
        <th>Post Code</th>
        <th>Email Address</th>
        <th>Phone Number</th>
        <th>Address</th>
        <th>Your Spot</th>
    </tr>
    <tr>
        <td><?php echo $this->request->data['fullname']; ?></td>
        <td><?php echo $this->request->data['company_name']; ?></td>
        <td><?php echo $this->request->data['company_url']; ?></td>
        <td><?php echo $this->request->data['postcode']; ?></td>
        <td><?php echo $this->request->data['email']; ?></td>
        <td><?php echo $this->request->data['phone']; ?></td>
        <td><?php echo $this->request->data['address']; ?></td>
        <td><?php echo $this->request->data['spot']; ?></td>
    </tr>
</table>

<br/>
Kind Regards,<br/>
<?php echo SITE_NAME; ?><br/>