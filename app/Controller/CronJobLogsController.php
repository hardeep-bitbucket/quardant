<?php
/**
 *CronJob Logs Controller
 * 
 * 
 * @created    16/03/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class CronJobLogsController extends AppController 
{
    var $layout = 'admin_inner';
    
    /*
     * @Summary Screen
     */    
    public function admin_index()
    {
        $title_for_layout = $this->modelClass. ' Summary';        
        
        $this->Redirect->urlToNamed();
        
        if (isset($this->params['']['status']) && $this->params[$this->modelClass]['status'] < 0)
        {
            $this->params[$this->modelClass]['status'] = 0;
        }
        
        $conditions = $this->getSearchConditions(array(                
                array('model' => $this->modelClass, 'field' => 'status', 'type' => 'integer', 'view_field' => 'status'),                
                array('model' => $this->modelClass, 'field' => 'created_on', 'type' => 'from_date', 'view_field' => 'from_date'),
                array('model' => $this->modelClass, 'field' => 'created_on', 'type' => 'to_date', 'view_field' => 'to_date'),
            )
        );
        
        if (isset($conditions['CronJobLog.status']) && $conditions['CronJobLog.status'] < 0)
        {
            $conditions['CronJobLog.status'] = 0;
        }
        
        $records = $this->paginate($this->modelClass, $conditions);
        //debug($records); exit;
        
        $this->set(compact('records', 'title_for_layout'));
    }
}