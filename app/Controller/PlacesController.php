<?php
/**
 * Place Controller
 * 
 * 
 * @created    19/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */
class PlacesController extends AppController 
{
    public $name = 'Places';
    public $layout='admin_inner';

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow("ajaxPlaceList");
    }
     
    /*
     * @Summary Screen
     */
    public function admin_index() 
    {
        $this->Redirect->urlToNamed();
        $conditions = $this->getSearchConditions(array(
                        array('model' => 'Place', 'field' => 'name', 'type' => 'string', 'view_field' => 'name'), 
                        array('model' => 'Place', 'field' => 'place_id', 'type' => 'integer', 'view_field' => 'place_id' )
                )
        );

        //In case there is no condition then show only Root level records
        if( !isset($this->params['named']) || empty($this->params['named']) ) {
            $conditions['Place.place_id'] = "";
        }
        if(empty($conditions)) {
            $conditions['Place.place_id'] = "";
        }	
        
        $this->{$this->modelClass}->contain(array(
            "LocationCity" => array("fields" => "id"),
            "LocationState" => array("fields" => "id"),
            "LocationCountry" => array("fields" => "id"),
            "CarState" => array("fields" => "id"),
            "CarCountry" => array("fields" => "id"),
        ));
        
        $records = $this->paginate('Place', $conditions);
        
        //Add child count value to data
        foreach($records as $key => $place) 
        {
            $records[$key]['Place']['child_count'] = $this->Place->childCount($place['Place']['id'], true);
            $records[$key]['Place']['have_relation_count'] = $records[$key]['Place']['child_count'];
            $records[$key]['Place']['have_relation_count'] += empty($place["LocationCity"]) ? 0 : 1;
            $records[$key]['Place']['have_relation_count'] += empty($place["LocationState"]) ? 0 : 1;
            $records[$key]['Place']['have_relation_count'] += empty($place["LocationCountry"]) ? 0 : 1;
            $records[$key]['Place']['have_relation_count'] += empty($place["CarState"]) ? 0 : 1;
            $records[$key]['Place']['have_relation_count'] += empty($place["CarCountry"]) ? 0 : 1;
        }
        $categoriesList = $this->Place->getTreeList('place_id');

        $this->set('title_for_layout', 'Place Summary');
        $this->set(compact('records', 'categoriesList'));
    }

    /*
     * @Add Screen
     */
    public function admin_add()
    {
        parent::add();
        $categoriesList = $this->Place->getTreeList('place_id');

        $heading='Add Place';
        $reset_action = 'admin_index';

        $this->set(compact('categoriesList', 'heading', 'reset_action'));
        $this->set('title_for_layout', 'Add Place');		
        $this->render('admin_form');
    }

    /*
     * @Edit Screen
     */
    public function admin_edit($id = null) 
    {
        parent::edit($id);
        $categoriesList = $this->Place->getTreeList('place_id');

        $heading='Add Place';
        $reset_action = 'admin_index';
        $this->set(compact('categoriesList', 'heading', 'reset_action'));
        $this->set('title_for_layout', 'Edit Place');	
        $this->render('admin_form');	

    }
    
    public function ajaxPlaceList($parent_id = 0)
    {
        $list = $this->{$this->modelClass}->getChildList($parent_id);
        
        asort($list);
                
        //debug($list); exit;
        
        echo json_encode($list); exit;
    }
   
}