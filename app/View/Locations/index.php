<?php
/**
 * Locations Controller
 * 
 * front end spot finder
 * 
 * @created    20/03/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>
<section style="margin-top : 104px;" class="other-page-banner-background">
    <img src="/img/frontend/pages_header/spot-finder.jpg" style="width:100%; height: auto;">
</section>

<!-- End header Section-->
<section class="sep-top-2x sep-bottom-2x">
    <div class="container">
       <!-- <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="text-center sep-bottom-md">
                     <div class="icon-box icon-horizontal icon-md">
                         <div class="icon-content fa fa-delicious" style="border-radius: 50px;">                             
                         </div>
                    </div>
                    <h2 class="upper wow bounceInLeft animated" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: bounceInLeft;">
                        <?php //echo $page["Page"]["name"]; ?>
                    </h2>
                    <?php //echo $page["Page"]["contents"]; ?>
                </div>
            </div>
        </div>
       -->
        <?php echo $this->element("frontend/Locations/spot_finder"); ?>
    </div>
</section>
        