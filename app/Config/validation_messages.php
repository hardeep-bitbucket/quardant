<?php
//Common
define('NAME_REQUIRED_MESSAGE', 'Name is required.');
define('LAST_NAME_REQUIRED_MESSAGE', 'Last name is required.');
define('TITLE_REQUIRED_MESSAGE', 'Title is required.');
define('TYPE_REQUIRED_MESSAGE', 'Type is required.');
define('CATEGORY_REQUIRED_MESSAGE', 'Category is required.');
define('IMAGE_REQUIRED_MESSAGE', 'Image is required.');
define('MOBILE_REQUIRED_MESSAGE', 'Mobile is required.');
define('EMAIL_REQUIRED_MESSAGE', 'Email is required.');
define('DATE_REQUIRED_MESSAGE', 'Date is required.');
define('CONTENTS_REQUIRED_MESSAGE', 'Contents is required.');

// Cars
define('COLOR_REQUIRED_MESSAGE', 'Color is required.');
define('REG_NO_REQUIRED_MESSAGE', 'Registration no. is required.');
define('REG_DATE_REQUIRED_MESSAGE', 'Registration date is required.');
define('CHASE_NO_REQUIRED_MESSAGE', 'Chase no. is required.');
define('OWNER_NAME_REQUIRED_MESSAGE', 'Owner name is required.');
define('OWNER_CITY_REQUIRED_MESSAGE', 'Owner city is required.');
define('OWNER_ADDRESS_REQUIRED_MESSAGE', 'Owner address is required.');
define('OWNER_ZIP_REQUIRED_MESSAGE', 'Owner zip is required.');

// Users
define('USERNAME_REQUIRED_MESSAGE', 'Username is required');
define('PASSWORD_REQUIRED_MESSAGE', 'Password is required');
define('USERNAME_UNIQUE_MESSAGE', 'This username has already been taken.');
define('INDUSTRY_REQUIRED_MESSAGE', 'Industry is required');
define('BUSINESS_NAME_REQUIRED_MESSAGE', 'Business name is required');

//Discounts
define('DISCOUNT_TYPE_REQUIRED_MESSAGE', 'Discount type is required');
define('DISCOUNT_FOR_REQUIRED_MESSAGE', 'Discount for is required');

//Campaigns
define('ADVERTISER_REQUIRED_MESSAGE', 'Advertiser is required');
define('DEFAULT_CREATIVE_REQUIRED_MESSAGE', 'Default creative is required');
define('START_DATE_REQUIRED_MESSAGE', 'Start date is required');
define('DURATION_TYPE_REQUIRED_MESSAGE', 'Duration type is required');



/***---- form constants ------------- ****/
define("REQUIRED_FIELD", "This field is required");
define("UNIQUE_FIELD", "Already Exist");
define("NUMERIC_FIELD", "Enter numeric only");


define("AD_LOCAL_VIDEO_DURATION_VALIDATION_MSG", "Video duration should be less than " . AD_LOCAL_DURATION_LIMIT . " seconds");
define("AD_WEB_VIDEO_DURATION_VALIDATION_MSG", "Video duration should be less than " . AD_URL_DURATION_LIMIT . " seconds");