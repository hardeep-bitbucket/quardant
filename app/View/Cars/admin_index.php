<?php 
/**
 * cars Controller
 * 
 * 
 * @created    20/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("admin/summary_header");
$action_for_search = str_replace("admin_", '', $action);
?>


<div class="row">
    <div class="col-md-12">   
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">Search</div>  
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>

            </div>
            <div class="portlet-body">    
                
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->                    
                    <?php echo $this->Form->create($model, array('controller' => $controller, 'type' => 'GET' , 'class' => 'ajax-form form-horizontal', 'data-action' => $action_for_search)); ?>
                     
                        <div class="form-body">                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Owner Name</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('CarSearch.owner_name', array(
                                                    'type' => "text", 'label' => false, 'div' => false, "escape" => false,
                                                    'value' => $Carowner_name, 
                                                    'class' => 'form-control'
                                                ));
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Reg. No.</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('CarSearch.reg_no', array(
                                                    'type' => "text", 'label' => false, 'div' => false, "escape" => false,
                                                    'value' => $Carreg_no, 
                                                    'class' => 'form-control'
                                                ));
                                            ?> 
                                        </div>
                                    </div>
                                </div>                         
                            </div>
                            
                            <div class="row">  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Country</label>
                                        <div class="col-md-8">                                            
                                             <?php
                                                echo $this->Form->input('CarSearch.country_id', array(
                                                    'type' => "select", 'label' => false, 'div' => false, "escape" => false,
                                                    'options' => $countryList, 'empty' => 'Please Select',
                                                    'value' => $Carcountry_id,
                                                    'class' => 'select2me form-control',
                                                ));
                                              ?>                                            
                                        </div>
                                    </div>
                                </div>                                
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Manufacture</label>
                                        <div class="col-md-8">                                            
                                             <?php
                                                echo $this->Form->input('CarSearch.automobile_manufacture_id', array(
                                                    'type' => "select", 'label' => false, 'div' => false, "escape" => false,
                                                    'options' => $manufacture_list, 'empty' => 'Please Select',
                                                    'value' => $Carautomobile_manufacture_id,
                                                    'class' => 'select2me form-control',
                                                ));
                                              ?>                                            
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            
                        </div>
                         <?php echo $this->element("admin/form_search_btn"); ?>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Summary</div>                        
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed table-custom">
                    <thead>
                       <tr class="head" >
                            <td width="5%" class="td-center"><?php echo $this->Paginator->sort('id', 'ID', array('class' => "ajax-page-link")); ?></td>
                            <td><?php echo $this->Paginator->sort('Manufacture.name', 'Manufacture', array('class' => "ajax-page-link")); ?></td>
                            <td><?php echo $this->Paginator->sort('Model.name', 'Model', array('class' => "ajax-page-link")); ?></td>
                            <td><?php echo $this->Paginator->sort('Variant.name', 'Variant', array('class' => "ajax-page-link")); ?></td>
                            <td><?php echo $this->Paginator->sort('reg_no', 'Reg No', array('class' => "ajax-page-link")); ?></td>                            
                            <td><?php echo $this->Paginator->sort('owner_name', 'Owner Name', array('class' => "ajax-page-link")); ?></td>
                            <td><?php echo $this->Paginator->sort('Country.name', 'Country', array('class' => "ajax-page-link")); ?></td>
                            <td><?php echo $this->Paginator->sort('State.name', 'State', array('class' => "ajax-page-link")); ?></td>
                            <td class="td-center"><?php echo $this->Paginator->sort('total_count', 'Ad View count', array('class' => "ajax-page-link")); ?></td>
                            <td width="8%"  class="td-center"><?php echo $this->Paginator->sort('Car.is_active', 'Status', array('class' => "ajax-page-link")); ?></td>                            
                            <td width="10%" class="td-center">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $record) { ?>
                            <tr>
                                <td class="td-center"><?php echo $record[$model]['id']; ?></td>
                                <td><?php echo $record['Manufacture']['name']; ?></td>
                                <td><?php echo $record['Model']['name']; ?></td>
                                <td><?php echo $record['Variant']['name']; ?></td>
                                <td><?php echo $record[$model]['reg_no']; ?></td>
                                <td><?php echo $record[$model]['owner_name']; ?></td>
                                <td><?php echo $record['Country']['name']; ?></td>
                                <td><?php echo $record['State']['name']; ?></td>
                                <td class="td-center"><?php echo $record[$model]['total_count']; ?></td>
                                <td class="td-center">
                                    <?php                            
                                    $class = $record[$model]['is_active'] == 1 ? SUMMARY_ACTIVE_CLASS : SUMMARY_INACTIVE_CLASS;
                                    echo $this->Html->link('', array('controller' => $controller, 'action' => 'admin_toggleStatus', $record[$model]['id']), array(
                                            "alt" => "Active",                                    
                                            "title" => $record[$model]['is_active'] == 1 ? "Active" : "InActive",
                                            "class" => "ajax-status summary-action-icon $class",
                                            "escape" => false
                                        )
                                    );
                                    ?> 
                                </td>
                                <td class="td-center">
                                    <div class="summary-action">                                
                                        <?php 
                                            echo $this->Html->link(
                                                    $this->Html->image("admin/summary_edit.png", array("class" => "summary-action-icon")), 
                                                    array('action' => 'admin_edit', $record[$model]['id']), 
                                                    array('escape' => false, 'title' => EDIT_TITLE, "class" => 'ajax-page-link')
                                                );
                                        ?>
                                        <span> </span>
                                        <?php
                                            echo $this->Html->link(
                                                        "link", 
                                                        array('action' => 'admin_delete', $record[$model]['id']), 
                                                        array('escape' => false, 'title' => DELETE_TITLE, "class" => "summary-action-delete-link")
                                                    );

                                            echo $this->Html->image("admin/summary_delete.png" , array(
                                                "class" => "summary-action-icon summary-action-delete",
                                                "data-confirm_text" => "Are you sure to delete record ?"
                                            ));
                                       ?>
                                    </div>
                                </td>
                    </tr>
                <?php } ?>
                    </tbody>                  
                </table>  
                
                  <div>    
                      <?php echo $this->element("admin/pagination"); ?>
                  </div> 
            </div>
        </div>               

    </div>
</div>