<?php
/**
 * Ad Model
 * 
 * 
 * @created    27/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     HARDEEP
 */

class Ad extends AppModel 
{
    
    var $actsAs = array(
        'MeioUpload' => array(
            'ad_name' => array(
                'dir' => AD_CREATIVE,
                'createDirectory' => false,
                'allowed_mime' => array('video/mp4'),
                'allowed_ext' => array('.mp4'),
                'maxSize' => 20000000, //20 MB
                'thumbnails' => false,                
                'thumbnailDir' => false,
                //'validations' => false
            ),
        )
    );
    
    /**
    *Associations
    */
    var $belongsTo = array(       
         'Campaign' => array(
                    'className' => 'Campaign',
                    'foreignKey' => 'campaign_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => ''
                )
         );   
    
    public $hasMany = array(
        'AdLocation' => array(
            'className' => 'AdLocation',
            'foreignKey' => 'ad_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    public function SaveData($data, $default_ad, $campaign_id)
    {        
        $ad_location_ad_list = array();
        //debug($data);            debug($default_ad); exit;
        if ($default_ad)
        {
            $do_save = true;
            $default_ad = $this->_setData($default_ad, $campaign_id);
            $default_ad["is_default"] = 1;  
            
            if ($default_ad["ad_id"])
            {
                $this->id = $default_ad["ad_id"];
                if (!$default_ad["ad_name"]["name"])
                {
                    $do_save = false;
                }
            }
            else
            {
                $this->create();
            }
            
            if ($do_save)
            {       
                $default_ad = $this->_save($default_ad);
                
                if (!$default_ad)
                {
                    $error['default'] = $this->validationErrors;
                    unset($this->validationErrors);
                    $this->validationErrors = $error;
                }
            }
        }
        
        foreach ($data as  $key => $inner_data)
        {
            if( $this->validationErrors)
            {
                return false;
            }
            
            
            if (!$inner_data['ad_name']['name'] && !$inner_data['ad_id'] && $default_ad)
            {
                $inner_data['ad_id'] = $default_ad['ad_id'];
            }
            else
            {
                if ($inner_data)
                { 
                    $do_save = true;
                    $inner_data = $this->_setData($inner_data, $campaign_id);                    
                    if ($inner_data["ad_id"])
                    {
                        $this->id = $inner_data["ad_id"];
                        if (!$inner_data["ad_name"]["name"])
                        {
                            $do_save = false;
                        }
                    }
                    else
                    {
                        $this->create();
                    }
                
                    if ($do_save)
                    {
                        $inner_data = $this->_save($inner_data);
                        
                        if (!$inner_data)
                        { 
                            $error[$key] = $this->validationErrors;
                            unset($this->validationErrors);
                            $this->validationErrors = $error;
                        }
                    }
                }
            }
            $ad_location_ad_list[$inner_data["ad_location_id"]] = $inner_data["ad_id"];
        }
        
        return $ad_location_ad_list;
    }
    
    private function _setData($data, $campaign_id)
    {
        if (isset($data["ad_name"]["name"]) && $data["ad_name"]["name"])
        {
            $data["ad_name"] = $data["ad_name"];
            $info = pathinfo($data["ad_name"]['name']);            
            $data["ad_size"] = $data["ad_name"]['size'];
            $data["ad_mime"] = $info["extension"];
            $data["ad_type"] = 1;
        }
        
        if (isset($data["ad_upload_path"]) && $data["ad_upload_path"])
        {
            
        }
        $data["campaign_id"] = $campaign_id;
        $data["is_active"] = 1;
        
        return $data;
    }
    
    private function _getLastFileName($id)
    {
        $this->recursive = -1;
        $data = $this->findById($id);        
        return $data["Ad"]["ad_name"];
    }
    
    private function _save($data)
    {
        $file_name = "";
        if ($this->id)
        {
            $file_name = $this->_getLastFileName($this->id);  
        }
        
        if ($this->save($data))
        {
            if ($file_name)
            {
                unlink(AD_CREATIVE . "/" . $file_name);
            }
            
            $data["ad_id"] = $this->id;  
            return $data;
        }        
        else
        {
            return false;
        }
    }
    
    
    public function beforeSave($options = array()) 
    {
        parent::beforeSave($options);
        if (isset($this->data["Ad"]["ad_name"]["name"]) && !empty($this->data["Ad"]["ad_name"]["name"]))
        {
            $this->data["Ad"]["ad_upload_path"] = AD_CREATIVE;
            $this->data["Ad"]["ad_size"] = $this->data["Ad"]["ad_name"]["size"];
            $this->data["Ad"]["ad_mime"] = $this->data["Ad"]["ad_name"]["type"];
            
            $info =  pathinfo($this->data["Ad"]["ad_name"]["name"]);            
            $this->data["Ad"]["ad_ext"] = $info["extension"];
        }
        
        if (isset($this->data["Ad"]['id']) && $this->data["Ad"]['id'])
        {
            $record = $this->find("first", array(
                "fields" => array("ad_name"),
                "conditions" => array("id" => $this->data["Ad"]['id']),
                "recursive" => -1
            ));
            
            $this->old_file = AD_CREATIVE . "/" . $record["Ad"]["ad_name"];
        }
        
        return $options;
    }
    
    public function afterSave($created, $options = array()) 
    {
        if (isset($this->old_file) && !empty($this->old_file))
        {
            if (file_exists($this->old_file))
            {
                unlink($this->old_file);
            }
        }
    }
    
    public function beforeDelete($cascade = true) 
    {
        parent::beforeDelete($cascade);
        
        $this->recursive = -1;
        $record = $this->findById($this->id);
            
        $file = AD_CREATIVE . "/" . $record["Ad"]["ad_name"];
        
        if (file_exists($file))
        {
            unlink($file);
        }
        
        return true;
    }
    
}

?>