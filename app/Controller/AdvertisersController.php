<?php

/**
 * Advertisers Controller
 * 
 * 
 * @created    10/01/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Sonia
 */
class AdvertisersController extends AppController {

    var $uses = "User";

    /*
     * @Before Filter
     */

    public function beforeFilter() 
    {
        parent::beforeFilter();
        
        $this->Auth->allow(array(
            "ajaxDashBoardActiveCampaigns", 
            "ajaxDashBoardCompletedCampaigns",
            "ajaxDashBoardTrialCampaigns",
            "ajaxHomeCampaigns"
        ));        
        $this->_getIndustryType();

        $group_id = ADVERTISER_GROUP_ID;
        $this->set(compact("group_id"));        
    }

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() 
    {
        $this->layout = "admin_inner";
        $this->Redirect->urlToNamed();
        $conditions = $this->getSearchConditions(array(
            array('model' => 'User', 'field' => 'username', 'type' => 'string', 'view_field' => 'username'),
            array('model' => 'User', 'field' => 'name', 'type' => 'string', 'view_field' => 'name'),
            array('model' => 'User', 'field' => 'type_id', 'type' => 'integer', 'view_field' => 'type_id')
        ));
        $conditions["User.group_id"] = ADVERTISER_GROUP_ID;

        $this->{$this->modelClass}->contain(array(
            "Campaign" => array(
                "fields" => array("id")
            ),
            "Type"
        ));
        
        $records = $this->paginate('User', $conditions);        
        $this->set(compact('records'));
    }

    /**
     * add method
     * @return void
     */
    public function admin_add() {
        parent::add();

         $heading = 'Add Advertiser';

        $this->set(compact('heading'));
        $this->set('title_for_layout', $heading);
        $this->render('admin_form');
    }

    /*
     * @Edit Screen
     */
    public function admin_edit($id = null, $redirect = array("action" => "index")) 
    {
        $this->{$this->modelClass}->recursive = -1;
        parent::edit($id, $redirect);
        
        $heading = 'Edit Advertiser';
        $this->set('title_for_layout', $heading);

        $this->set(compact('heading'));
        $this->render('admin_form');
    }    
    
    /**
     * 
     * @param int $id
     */
    public function admin_edit_profile($id = null)
    {
        $this->admin_edit($id, false);
    }
    
     /**
     * Toggles the status of is_active field
     * 
     * @param Integer $id
     * @param Integer $status
     */
    public function admin_toggleStatus($id, $status) 
    {
        $this->{$this->modelClass}->id = $id;
        $result = $this->{$this->modelClass}->saveField('is_active', !(int) $status, FALSE);        
        echo (int) $result; exit;
    }
    
    public function admin_dashboard()
    {   
        if ($this->user['group_id'] != ADVERTISER_GROUP_ID)
        {
            $this->redirect(array("controller" => "users", "action" => "admin_dashboard", "admin" => true));
        }      
        $this->set('title_for_layout', "Dashboard");
    }
    
    public function admin_home()
    {   
        if ($this->user['group_id'] == ADMIN_GROUP_ID || $this->user['group_id'] == SUB_ADMIN_GROUP_ID)
        {
            $this->redirect(array("controller" => "users", "action" => "home", "admin" => true));
        } 
        else if ($this->user['group_id'] == PARTNER_GROUP_ID)
        {
            $this->redirect(array("controller" => "partners", "action" => "home", "admin" => true));
        }
        
        $user_id = $this->user['id'];        
        $pending_invoice_amount = $this->{$this->modelClass}->query("CALL sp_invoice_get_sum($user_id, NULL, " . StaticArray::$invoice_status_unpaid . ", NULL);");
 
        $pending_invoice_amount = $pending_invoice_amount[0][0]["sum_amount"] ? $pending_invoice_amount[0][0]["sum_amount"] : 0;
        
        $country_list = $this->{$this->modelClass}->Campaign->Country->getChildList();        
        $this->set(compact('country_list', "pending_invoice_amount"));
        $this->set('title_for_layout', "Home");
    }
    
    public function ajaxHomeCampaigns($country_id = 0)
    {
        $user_id = $this->user['id'];
        $country_id = $country_id ? $country_id : "NULL";
        
        // getting best Automobile
        $records = $this->{$this->modelClass}->query("CALL sp_get_top_automobile_variant(NULL, NULL, $country_id, $user_id, NULL, 1);");        
        $best_vehicle = array();
        if (isset($records[0][0]['total_count']) && $records[0][0]['total_count'])
        {
            $best_vehicle['manufacture'] = $records[0]['Manufacture']['name'];
            $best_vehicle['model'] = $records[0]['Model']['name'];
            $best_vehicle['variant'] = $records[0]['Variant']['name'];
        }
        
        // getting best Location
        $records = $this->{$this->modelClass}->query("CALL sp_get_top_locations(NULL, $country_id, $user_id, NULL, 1);");        
        $best_location = array();
        if (isset($records[0][0]['total_count']) && $records[0][0]['total_count'])
        {
            $best_location = $records[0]["L"];              
        }
        
        // getting best campaign                
        $records = $this->{$this->modelClass}->query("CALL sp_get_top_campaigns($country_id, $user_id, NULL, 1);");
        $best_campaign = array();
        if (isset($records[0][0]['total_count']) && $records[0][0]['total_count'])
        {
            $best_campaign = $records[0]["C"];  
        }
        
        $conditions = array("Campaign.user_id" => $user_id);
        if ($country_id != "NULL")
        {
            $conditions['country_id'] = $country_id;
        }
        
        $this->{$this->modelClass}->Campaign->contain(array(
            "AdLocation" => array(
                "fields" => array("id", "location_id", "target_imperssions", "reached_imperssions"),
                "Location" => array(
                    "fields" => array("price_per_impression", "setup_cost",  "is_shared", "partner_id", "partner_revenue_share_percentage")
                )
            )
        ));
        
        $records = $this->{$this->modelClass}->Campaign->find("all", array(
            "fields" => array("id", "budget", "status_type_id"),
            "conditions" => $conditions,
        ));
        
        $campaigns["total"] = $campaigns["active"] = $campaigns["submitted"] = $campaigns["completed"] = 0;
        $campaigns["active_spend"] = $campaigns["submitted_budget"] = $campaigns['total_spend'] = $campaigns['total_budget'] = 0;                
        foreach ($records as $key => $record)
        {
            $campaigns["total"]++;
            $records[$key] = $this->_getCampaignStatusInfo($record);     
            
            $campaigns["total_spend"] += $records[$key]['Campaign']['spend'];
            $campaigns["total_budget"] += $records[$key]['Campaign']['budget'];            
            
            switch($record["Campaign"]['status_type_id'])
            {
                case CAMPAIGN_ACTIVE_ID:
                    $campaigns["active"]++;                    
                    $campaigns["active_spend"] += $records[$key]['Campaign']['spend'];
                break;
            
                case CAMPAIGN_SUBMITTED_ID:
                    $campaigns["submitted"]++;
                    $campaigns["submitted_budget"] += $record['Campaign']['budget'];
                break;
            
                case CAMPAIGN_COMPLETED_ID:
                    $campaigns["completed"]++;
                break;            
            }            
        }        
        
        $this->set(compact('best_vehicle', 'best_location', 'best_campaign', 'campaigns'));
    }
    
    public function ajaxDashBoardActiveCampaigns($limit = 0)
    {   
        $conditions = array(
             "status_type_id" => CAMPAIGN_ACTIVE_ID,
             "is_trial" => 0
        );
        
        if ($this->user["group_id"] == ADVERTISER_GROUP_ID)
        {
            $conditions["Campaign.user_id"] = $this->user["id"];
        }
        
        $order = array(
             "Campaign.id" => "DESC"
        );
        
        $arr = array(
            "fields" => array("id", "name", "budget", "start_date", "end_date", "duration_type_id", "campaign_type_id"),
            "conditions" => $conditions
        );
        
        if ($order)
        {
            $arr['order'] = $order;
        }
        
        if ($limit)
        {
            $arr['limit'] = $limit;
        }        
        
        $records = $this->_getDashbaordCampaigns($arr);
        
        $this->set(compact('records'));
    }
    
    public function ajaxDashBoardCompletedCampaigns($limit = 0)
    {   
        $conditions = array(
             "status_type_id" => CAMPAIGN_COMPLETED_ID,
             "is_trial" => 0
        );
        
        if ($this->user["group_id"] == ADVERTISER_GROUP_ID)
        {
            $conditions["Campaign.user_id"] = $this->user["id"];
        }
        
        $order = array(
             "Campaign.id" => "DESC"
        );
        
        $arr = array(
            "fields" => array("id", "name", "budget", "start_date", "end_date", "duration_type_id", "campaign_type_id"),
            "conditions" => $conditions
        );
        
        if ($order)
        {
            $arr['order'] = $order;
        }
        
        if ($limit)
        {
            $arr['limit'] = $limit;
        }        
        
        $records = $this->_getDashbaordCampaigns($arr);
        
        $this->set(compact('records'));
        
        $this->render("ajax_dash_board_active_campaigns");
    }
    
     public function ajaxDashBoardTrialCampaigns($limit = 0)
    {   
        $conditions = array(
             "is_trial" => 1
        );
        
        if ($this->user["group_id"] == ADVERTISER_GROUP_ID)
        {
            $conditions["Campaign.user_id"] = $this->user["id"];
        }
        
        $order = array(
             "Campaign.id" => "DESC"
        );
        
        $arr = array(
            "fields" => array("id", "name", "budget", "start_date", "end_date", "duration_type_id", "campaign_type_id"),
            "conditions" => $conditions
        );
        
        if ($order)
        {
            $arr['order'] = $order;
        }
        
        if ($limit)
        {
            $arr['limit'] = $limit;
        }        
        
        $records = $this->_getDashbaordCampaigns($arr);
        
        $this->set(compact('records'));
        
        $this->render("ajax_dash_board_active_campaigns");
    }
    
    private function _getDashbaordCampaigns($arr)
    {
        $this->{$this->modelClass}->Campaign->contain(array(
           "AdLocation" => array(
               "fields" => array(
                   "id", "target_imperssions", "reached_imperssions",
               ),              
               "Location" => array(
                    "fields" => array("id", "price_per_impression")
               )
           ),
           "CampaignType" => array(
               "fields" => array(
                   "id", "value"
               )
           ),
           "CampaignDuration" => array(
               "fields" => array(
                   "id", "value"
               )
           )
        ));
        
        $records = $this->{$this->modelClass}->Campaign->find("all", $arr);
        
        $data = array();
        
        if (!$records)
        {
            return $data;
        }
        
        foreach ($records as $record)
        {
            $key = $record["Campaign"]["id"];
            $data[$key] = $record["Campaign"];
            $data[$key]["CampaignType"] = $record["CampaignType"]["value"];
            $data[$key]["CampaignDuration"] = $record["CampaignDuration"]["value"];
            
            $data[$key]["spend"] = 0;
            $data[$key]["target_imperssions"] = $data[$key]["reached_imperssions"] = $data[$key]["complete"] = 0;
            foreach ($record["AdLocation"] as $inner_data)
            {
                $data[$key]["target_imperssions"] += $inner_data["target_imperssions"];
                $data[$key]["reached_imperssions"] += $inner_data["reached_imperssions"];
                
                $data[$key]["spend"] += ($inner_data["reached_imperssions"] * $inner_data["Location"]["price_per_impression"]);
            }
            
            if ($data[$key]["target_imperssions"])
            {
                $data[$key]["complete"] = round(($data[$key]["reached_imperssions"] * 100 / $data[$key]["target_imperssions"]), 2);
            }
        }
        
        return $data;
    }

    /*
     * @Get Industry Types
     */

    private function _getIndustryType() {
        $industry_types = $this->User->Type->find("list", array("conditions" => array("Type.type" => INDUSTRY_TYPES)));
        $this->set(compact("industry_types"));
    }
    
}
