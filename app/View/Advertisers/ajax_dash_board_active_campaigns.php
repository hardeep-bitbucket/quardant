<?php
/**
 * ajax file used in Advertiser dashboard
 * 
 * 
 * @created    02/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<?php if ($records) : ?>
<div class="row">    
<div class="col-md-12">
   <div class="panel-group accordion" id="accordion<?php echo $action; ?>">
        
       <?php $i = 0;  foreach ($records as $record): $i++; ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <b>
                            <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion<?php echo $action; ?>" href="#<?php echo $action.$i; ?>">
                                <?php echo $record["name"]; ?>
                            </a>
                        </b>
                        
                        <div class="progress progress-striped active margin-5">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $record["complete"] ? $record["complete"] : 1; ?>%">                            
                            </div>
                        </div>
                    </h4>
                </div>
                <div id="<?php echo $action.$i; ?>" class="panel-collapse in">
                    <div class="row">
                        <div class="col-md-12 margin-5">
                            
                            <?php echo $this->element("admin/campaign/campaign_completation_status", array('record' => $record)); ?>
                            
                            <div class="col-md-4 portfolio-stat">
                                <div class="font-size-14">
                                    <hr>
                                    <div class="margin-5 highlighted-2">
                                        Type : <b style="color : #793A93;"><?php echo $record["CampaignType"]; ?></b>
                                    </div>
                                    <hr>
                                    <div class="margin-5 highlighted-2">
                                        Duration : <b style="color : #793A93;"><?php echo $record["CampaignDuration"]; ?></b>
                                    </div>  
                                    <hr>
                                </div>                                
                            </div>  
                            
                            <div class="col-md-4 portfolio-stat">
                                <div class="font-size-14">
                                    <hr>
                                    <div class="margin-5 highlighted-2">
                                        Start Date : <b style="color : #3F75A2;"><?php echo $record["start_date"]; ?></b>
                                    </div>
                                    <hr>
                                    <?php if ($record['duration_type_id'] == CAMPAIGN_DURATION_FIXED_ID) : ?>
                                        <div class="margin-5 highlighted-2">
                                            End date : <b style="color : #3F75A2;"><?php echo $record["end_date"]; ?></b>
                                        </div>                                    
                                        <hr>
                                    <?php endif; ?>
                                    
                                </div>                                
                            </div>  
                            
                            <div class="col-md-4 portfolio-stat" style="margin-top:10px;">
                                <div class="pull-right">                                    
                                    <div class="font-size-16">
                                        <?php
                                            echo $this->Html->link("<b>View full details</b>", 
                                                array("controller" => "campaigns", "action" => "campaign_detail", $record['id'], "admin" => true), 
                                                array("escape" => false, "class" => "btn btn-circle green-soft btn-sm")
                                            );
                                        ?>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
       <?php  endforeach; ?>
   </div>
</div>
</div>

<?php endif; ?>