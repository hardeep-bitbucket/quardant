<?php

/**
 * @created    07/04/2015
 * @package    anpr
 * @copyright  Copyright (anpr) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class HistoryCampaign extends AppModel 
{
     /*
     * Behaviours 
     */
    public $actsAs = array( 
        'DateFormat' => array('start_date', 'end_date')
    );
    
    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Country' => array(
            'className' => 'Place',
            'foreignKey' => 'country_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CampaignDuration' => array(
            'className' => 'Type',
            'foreignKey' => 'duration_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'CampaignType' => array(
            'className' => 'Type',
            'foreignKey' => 'campaign_type_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
    
    public $hasMany = array(
        'HistoryAdLocation' => array(
            'className' => 'HistoryAdLocation',
            'foreignKey' => 'history_campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
