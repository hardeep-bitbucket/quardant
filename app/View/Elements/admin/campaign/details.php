<?php
/**
 * Campaign Controller
 * 
 * Detail tab
 * 
 * @created    23/02/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal', 'enctype' => "multipart/form-data", "id" => "detail-form"));
echo $this->Form->hidden('id', array('label' => false));
echo $this->Form->hidden('tab_name', array('label' => false));
?>
<div class="row">
    <?php if ($auth_user['group_id'] != ADVERTISER_GROUP_ID): ?>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-md-4">Advertiser <span class="required">*</span></label>
                <div class="col-md-8">                                            
                    <?php
                    echo $this->Form->input('user_id', array(
                        'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                        'options' => $advertiserList,
                        'empty' => 'Please Select',
                        'class' => 'select2me form-control',
                    ));
                    ?>                                            
                </div>
            </div>
        </div>
        <?php
    else :
        echo $this->Form->hidden('user_id', array('label' => false, 'value' => $auth_user['id']));
    endif;
    ?>

    <div class="col-md-6"> 
        <div class="form-group radio-btn-list">
            <span class="col-md-4 lbl" style="text-align: right;">Trial  <span class="required">*</span></span>
            <div class="col-md-8">
                <?php
                echo $this->Form->input('is_trial', array(
                    'type' => "radio",
                    'options' => StaticArray::$YesNo,
                    'div' => false,
                    'legend' => false,
                    'before' => '',
                    'after' => '',
                    'between' => '',
                    'separator' => '',
                    'class' => "trial-radio",
                ));
                ?>
            </div>
        </div>
    </div>   

    <?php if ($auth_user['group_id'] != ADVERTISER_GROUP_ID): ?>
    </div>
    <div class="row">
    <?php endif; ?>

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-4">Country <span class="required">*</span></label>
            <div class="col-md-8">
                <?php
                echo $this->Form->input('country_id', array(
                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                    'options' => $country_list,
                    'empty' => "Please Select",
                    'label' => false, 'class' => 'form-control'
                ));
                ?>
            </div>
        </div>
    </div>
</div>

<h3 class="form-section">Campaign Details</h3>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-4">Campaign Name <span class="required">*</span></label>
            <div class="col-md-8">
                <?php
                echo $this->Form->input('name', array(
                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                    'class' => 'form-control'
                ));
                ?>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-4">Purpose of Campaign <span class="required">*</span></label>
            <div class="col-md-8">
                <?php
                echo $this->Form->input('purpose', array(
                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                    'label' => false, 'rows' => '3', 'cols' => 3, 'class' => 'form-control'
                ));
                ?>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-4">Duration Type <span class="required">*</span></label>
            <div class="col-md-8">  
                <div id="duration-select-block">
                    <?php
                    echo $this->Form->input('duration_type_id', array(
                        'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                        'options' => $duration_type_list,
                        'empty' => 'Please Select',
                        'class' => 'select2me form-control',
                    ));
                    ?>      
                </div>

                <div id="duration-text-block" style="display:none;">                        
                    <?php
                    echo $this->Form->input('temp', array(
                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                        'value' => $duration_type_list[CAMPAIGN_DURATION_FIXED_ID],
                        'class' => 'form-control', 'readonly',
                    ));

                    echo $this->Form->input('duration_type_id', array(
                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                        'class' => 'hidden',
                        'readonly', 'value' => CAMPAIGN_DURATION_FIXED_ID
                    ));
                    ?>      
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">                    

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label col-md-4">Start Date <span class="required">*</span></label>
            <div class="col-md-8">
                <?php
                echo $this->Form->input('start_date', array(
                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                    'class' => 'form-control'
                ));
                ?>
            </div>
        </div>
    </div>

    <div class="col-md-6 end-date-block">
        <div class="form-group">
            <label class="control-label col-md-4">End Date <span class="required">*</span></label>
            <div class="col-md-8">
                <?php
                echo $this->Form->input('end_date', array(
                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                    'class' => 'form-control', "required"
                ));
                ?>
            </div>
        </div>
    </div>
</div>

<h3 class="form-section">Other Details</h3>

<div class="row">        
    <div class="col-md-6">
        <fieldset class="creative-div">
            <legend>Instructions For video Upload</legend>

            <div class="margin-5">
                <ul style="list-style: upper-roman;" class="note-color">
                    <li>Video should be mp4 type</li>
                    <li>Size of file should be less than 10MB</li>
                    <li>Duration of Video should be less than <?php echo AD_LOCAL_DURATION_LIMIT; ?> seconds</li>
                </ul>
            </div>
        </fieldset>

        <fieldset class="url-div">
            <legend>Instructions For Youtube URL</legend>

            <div class="margin-5">
                <ul style="list-style: upper-roman;"  class="note-color">                        
                    <li>Duration of Video should be less than <?php echo AD_URL_DURATION_LIMIT; ?> seconds</li>
                </ul>
            </div>
        </fieldset>            
    </div>

    <div class="col-md-6">

        <div class="form-group">
            <label class="control-label col-md-4">Creative Type <span class="required">*</span></label>
            <div class="col-md-8">
                <?php
                echo $this->Form->input('ad_type', array(
                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                    'options' => StaticArray::$AdType_details,
                    'class' => 'form-control'
                ));
                ?>
            </div>
        </div>
        
        <div class="form-group url-div">
            <label class="control-label col-md-4">Web Url <span class="required">*</span></label>
            <div class="col-md-8">
                <?php
                echo $this->Form->input('ad_url', array(
                    'type' => "text", 'div' => false, 'label' => false, 'escape' => false,
                    'class' => "form-control"
                ));

                if (isset($this->data['Campaign']['ad_url']) && !empty($this->data['Campaign']['ad_url']))
                {
                    echo $this->TSHtml->videoOptions($this->data['Campaign']['ad_url'], array(
                        'url_type' => "WEB"
                    ));
                }
                ?>
            </div>           
        </div>

        <div class="form-group creative-div">
            <label class="control-label col-md-4">Creative <span class="required">*</span></label>
            <div class="col-md-8" style="padding-top : 5px;">
                <div id="upload-section">
                    <a id="btn-upload" href="#upload-box" class="btn btn-circle blue">Upload</a>
                    <span id="upload-status"></span>
                </div>
                <div id="play-section">
                    <?php
                        echo $this->Form->hidden('ad_name', array('escape' => false));
                        echo $this->Form->hidden('ad_upload_path', array('escape' => false, "value" => AD_CREATIVE));
                        echo $this->Form->hidden('ad_mime', array('escape' => false));
                        echo $this->Form->hidden('ad_size', array('escape' => false));
                        echo $this->Form->hidden('ad_ext', array('escape' => false));
                        echo $this->Form->hidden('ad_length', array('escape' => false));
                    ?>
                    <a href="#" class ="ad-view view_link preview_video btn btn-circle blue"  data-url = "" data-url_type = "LOCAL">View</a>
                    <a href="<?php echo Router::url(array("controller" => "campaigns",  "action" => "ajaxDeleteVideo"));  ?>" class ="ad-delete btn btn-circle  red" data-url="">Delete</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->element("admin/tab_save_btn"); ?> 

<div id="upload-box" style="display :none;">
    <?php echo $this->element("admin/campaign/video_upload_form", array("id" => $id, "type" => "campaign", "field_name" => "ad_name",  "updateID" => "upload-status")); ?>
</div>

<script type ="text/javascript">
    
    $(document).ready(function()
    {
        function set_date_limits(id, d)
        {
            return;
            var v = $(".trial-radio:checked").val();

            var isTrial = v == "0" ? false : true;

            var min_days_for_campaign = parseInt('<?php echo $auth_user["group_id"] == ADVERTISER_GROUP_ID ? $adminSetting['campaign_duration_fixed_min_days'] : 1 ?>');
            var max_days_for_trial = parseInt('<?php echo $auth_user["group_id"] == ADVERTISER_GROUP_ID ? $adminSetting['campaign_trial_max_days'] : 30 ?>');

            var tempd = d;
            if (id == "#CampaignStartDate")
            {
                if (isTrial)
                {
                    tempd.setDate(tempd.getDate() + max_days_for_trial);
                    $("#CampaignEndDate").datepicker("setEndDate", tempd);
                }
                else
                {
                    tempd.setDate(tempd.getDate() + min_days_for_campaign);
                    $("#CampaignEndDate").datepicker("setStartDate", tempd);
                }
            }
            else
            {
                if (isTrial)
                {
                    tempd.setDate(tempd.getDate() - max_days_for_trial);
                    $("#CampaignStartDate").datepicker("setStartDate", tempd);
                }
                else
                {
                    tempd.setDate(tempd.getDate() - min_days_for_campaign);
                    $("#CampaignStartDate").datepicker("setEndDate", tempd);
                }
            }
        }

        $("#CampaignDurationTypeId").change(function()
        {
            var type = $(this).val();
            if (type != "")
            {
                if (type == '<?php echo CAMPAIGN_DURATION_FIXED_ID; ?>')
                {
                    $(".end-date-block").show();
                    $("#CampaignEndDate").attr("required", "true").removeAttr("disabled");
                }
                else
                {
                    $(".end-date-block").hide();
                    $("#CampaignEndDate").removeAttr("required").attr("disabled", true);
                }
            }
        });


        $("#CampaignAdType").change(function()
        {
            if ($(this).val() == "")
            {
                return false;
            }

            if ($(this).val() == "1")
            {
                $(".creative-div").show();
                $(".url-div").hide();
                $("#CampaignAdUrl").attr("disabled", true);
            }
            else
            {
                $(".creative-div").hide();
                $(".url-div").show();
                $("#CampaignAdUrl").removeAttr("disabled");
            }
        });

        $("#CampaignStartDate").datepicker(
        {
            format: '<?php echo DEFAULT_JS_DATE_FORMAT ?>',
            autoclose: true,
        }).on("changeDate", function(e)
        {
            var d = $(this).datepicker("getDate");
            set_date_limits("#CampaignStartDate", d);
        });

        $("#CampaignEndDate").datepicker(
        {
            format: '<?php echo DEFAULT_JS_DATE_FORMAT ?>',
            autoclose: true,
        }).on("changeDate", function(e)
        {
            var d = $(this).datepicker("getDate");
            set_date_limits("#CampaignEndDate", d);
        });


        $(".trial-radio").change(function(e, data)
        {
            var v = $(this).val();

            if (v == '0')
            {
                $("#CampaignDurationTypeId").attr("required", "true").trigger("change");
                $("#duration-text-block").hide();
                $("#duration-select-block").show();
            }
            else if (v == '1')
            {
                $("#CampaignDurationTypeId").val('<?php echo CAMPAIGN_DURATION_FIXED_ID; ?>').removeAttr("required").trigger("change");
                $("#duration-text-block").show();
                $("#duration-select-block").hide();
            }

            if (typeof data == "undefined")
            {
                $('#CampaignStartDate, #CampaignEndDate').datepicker('update', '');
                $("#CampaignStartDate, #CampaignEndDate").datepicker("setEndDate", '');
                $("#CampaignDurationTypeId").val("").trigger("change");
            }

            if ($("#CampaignStartDate").val() == "")
            {
                $("#CampaignStartDate").datepicker("setStartDate", new Date());
            }

            if ($("#CampaignEndDate").val() == "")
            {
                $("#CampaignEndDate").datepicker("setStartDate", new Date());
            }
        });

        $("#CampaignAdType").trigger("change");

        $(".trial-radio:checked").trigger("change", {trigger: 1});

        $("#btn-upload").fancybox({
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'speedIn': 600,
            'speedOut': 200,
            'overlayShow': false
        });
        
        $(".ad-delete").click(function(e)
        {
            e.preventDefault();
            
            var me = $(this);
            confirm_box
            ({
                title : "Do you want to delete creative permanently",
                onConfirmCallBack : function ()
                {
                    var data = { 
                        file : me.attr("data-url"),
                        id : '<?php echo $id ?>'
                    };
                    
                    $.post(me.attr("href"), data, function(data, status)
                    {
                        if (data == "1")
                        {
                            warn("Creative deleted successfully", { type : "success", desc : ""});
                            set_ad_video_inputs({}, "");                   
                        }
                        else
                        {
                            warn("Failed to delete the creative, Please try again");
                        }
                    });                     
                }
            });
                
            
            return false;
        });
        
        set_ad_video_inputs({}, $("#CampaignAdName").val());
    });
    
    
    $("#detail-form").submit(function()
    {
        if ( $("#CampaignAdType").val() == "1" && !$("#CampaignAdName").val())
        {
            warn("Please uplaod the creative");
            return false;
        }
        
    })
    
    function toggle_upload()
    {
        if ($("#CampaignAdName").val())
        {
            $.fancybox.close();
            $("#play-section").show();
            $("#upload-section").hide();
        }
        else
        {
            $("#play-section").hide();
            $("#upload-section").show();
        }
    }
    
    function set_ad_video_inputs(data, file)
    {
        $("#CampaignAdName").val(file);
        $("#CampaignAdName").parents("#play-section").find(".ad-view").attr("data-url", '/<?php echo AD_CREATIVE ?>' + file);
        $("#CampaignAdName").parents("#play-section").find(".ad-delete").attr("data-url", '<?php echo AD_CREATIVE ?>' + file);
        
        if (data && Object.keys(data).length > 1)
        {
            $("#CampaignAdMime").val(data.type);
            $("#CampaignAdSize").val(data.size);
            $("#CampaignAdExt").val(data.ext);
            $("#CampaignAdLength").val(data.length);
        }
        
        toggle_upload();
    }
    
    function upload_update_callback(field_id, update_id, status, text, data)
    {
        $("#" + update_id).html(text);
        
        if (status == 1)
        {
            set_ad_video_inputs(data, data.file);
        }
    }
    
</script>
