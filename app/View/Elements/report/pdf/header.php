<?php
/**
 * Invoice PDF 
 * 
 * 
 * @created    09/05/2015
 * @package    ANPRMotion
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>
<html>
    <head>
        <style type='text/css'>
            @page { margin: 170px 60px; }

            #header { position: fixed; left: 0px; top: -150px; right: 0px; height: 150px; text-align: center; }
            #footer { position: fixed; left: 0px; bottom: -150px; right: 0px; height: 150px; }
            #footer .page:after { content: counter(page, upper-roman); }
            
            .summary-table{
                //border: 1px solid #375896;
            }
            
            .summary-table *{
                margin: 0;
                padding : 0;
            }
            
            .summary-table th{                
                font-weight: bold;
                padding : 7px 10px;                
                background-color: #1FA462;
                color : #fff;
                border-left: 1px solid #cccccc;
                border-top: 1px solid #cccccc;
                border-bottom: 1px solid #cccccc;
            }
            
            .summary-table th:last-child, .summary-table td:last-child{
                border-right: 1px solid #cccccc;
            }
                        
            .summary-table td{
                padding : 5px 10px;
                border-left: 1px solid #cccccc;                
                border-bottom: 1px solid #cccccc;
            }
                        
            .margin-5{
                margin : 5px;
            }
            
            .font-size-14{
                font-size: 14px;
            }
            
            .font-size-12{
                font-size: 12px;
            }
            
            .font-size-10{
                font-size: 10px;
            }
            
            .header-title{
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                padding : 5px 0;
            }
            
            .sub-title{
                font-size: 14px;
                font-weight: bold;                
                padding : 4px 0;
            }
            
            .label{
                font-weight: bold;
            }
            
            .blue{
                color : #375896;
            }
            
            #content{
                font-size: 11px;
            }
            
            .row{
                margin : 5px 2px;
            }      
            
            .table-row{
                width : 100%;
            }
            
            .table-row td{
                padding: 3px 5px;
            }          
            
            .td-center{
                text-align: center;
                vertical-align: middle;
            }
            
        </style>

    </head>
    
    <body>
        <div id="header">
            <div style="text-align: right; width: 100%;">
                <p class="margin-5">
                    <img src="img/frontend/logo-white.png" style="height: 38px; width : auto;">
                </p>
                
                <div class="font-size-10 margin-5">
                    <p class="margin-5">
                        4-5 Bonhill St, Shoreditch, London EC2A 4BX
                    </p>

                    <p class="margin-5">
                       Ph : (+39) 123-456-789 / (+39) 123-456-789
                    </p>

                    <p class="margin-5">
                       Email : info@anprmotion.com / support@anprmotion.com
                    </p>
                </div>
            </div>
        </div>