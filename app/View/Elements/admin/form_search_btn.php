<?php
/**
 * common search btns
 * 
 * 
 * @created    19/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */
?>


<div class="row">
    <div class="col-md-11">
        <div class="pull-right">
            <?php
                echo $this->Form->button("<i class='icon-magnifier'></i> Search", array("class" => "btn btn-circle green", "type" => "submit", 'label' => false, 'div' => false, 'escape' => false));
            ?>                                   
            <span class="space-10"></span>
            <?php
                echo $this->Html->link("Cancel", array("action" => $action), array("class" => "btn btn-circle default"));            
            ?>
        </div>
    </div>                                
</div>