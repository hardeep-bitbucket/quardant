<?php

/**
 * Date Utility
 * 
 * 
 * @created    24/03/2015
 * @package    ANPR
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

App::uses('CakeTime', 'Utility');

class DateUtility 
{
    static $DAYS = 3;
    static $HOURS = 2;
    static $MINTUES = 1;    
    static $SECONDS = 0;
    static $timezone = "";
    
    /**
     * set the time zone
     * @param String $timezone
     */
    public static function setClientTimeZone($timezone)
    {
        self::$timezone = $timezone;
    }

    /**
     * convert time string to standered date object 
     * @param String $date
     * @return DateTime object
     */
    public static function getDateObj($date)
    {
        return new DateTime(date(DEFAULT_SQL_DATETIME_FORMAT, CakeTime::fromString($date)));
    }
    
    public static function getClientDateObj($date)
    {
        $date->setTimezone(new DateTimeZone(self::$timezone));        
        return $date;
    }
    
    public static function getClientDateString($date, $format)
    {
        $date = self::getDateObj($date);           
        $date->setTimezone(new DateTimeZone(self::$timezone));        
        return self::getFormatDateFromObj($date, $format);
    }
    
    /**
     * return current date time object
     * @return Object
     */
    public static function getCurrentDateTimeObj()
    {
        return new DateTime(date(DEFAULT_SQL_DATETIME_FORMAT, time()));
    }
    
    /**
     * return current date time in String 
     * @param String $format
     * @return String
     */
    public static function getCurrentDateTimeString($format)
    {
        return self::getFormatDateFromString(self::getCurrentDateTimeObj(), $format);
    }
    
    /**
     * 
     * Convert String Date to Specfic String date Format
     * @param String $date
     * @param String $format
     * @return String date
     */
    public static function getFormatDateFromString($date, $format)
    {
        $dateObj = self::getDateObj($date);
        return self::getFormatDateFromObj($dateObj,$format);
    }
    
    /**
     * 
     * Convert DateTime Date obj to Specfic String date Format
     * @param DateTimeObj $date
     * @param String $format
     * @return String
     */
    public static function getFormatDateFromObj($date, $format)
    {
        return $date->format($format);
    }
    
    /**
     * 
     * compare dates and return the diffrence between them
     * 
     * if from_date is greater than to_date then return > 0
     * if not return < 0
     * 
     * @param DateTime $from_date
     * @param DateTime $to_date
     * @param int $type
     * @return int
     */
    public static function compareDateObjs($from_date, $to_date, $type = 0)
    {
        $end_seconds = date_format($to_date, "U");
        $start_seconds = date_format($from_date, "U");    
        
        switch($type)
        {
            case self::$MINTUES:
                $v = round(($end_seconds - $start_seconds)/60);
            break;
        
            case self::$HOURS:
                $v = round(($end_seconds - $start_seconds)/(60*60));
            break;
        
            case self::$DAYS:              
                $v = round(($end_seconds - $start_seconds)/(60*60*24));
            break;
        
            default :
                $v = ($end_seconds - $start_seconds);
            break;
        }
        
        return $v;
    }
    
    /**
     * 
     * compare dates and return the diffrence between them
     * 
     * if from_date is greater than to_date then return > 0
     * if not return < 0
     * 
     * @param String $from_date
     * @param String $to_date
     * @param int $type
     * @return int
     */
    public static function compareDateStrings($from_date, $to_date, $type = 0)
    {
        $from_date = self::getDateObj($from_date);
        $to_date = self::getDateObj($to_date);
        
        return self::compareDateObjs($from_date, $to_date);
    }
    
    /**
     * add days in date
     * @param string $datetime
     * @param int $days
     * @return date
     */
    public static function addDaysInDateObj($dateObj, $days)
    {        
        $date = self::getFormatDateFromObj($dateObj, DEFAULT_SQL_DATETIME_FORMAT);
        
        return self::getDateObj(self::addDaysInDateString($date, $days, DEFAULT_SQL_DATETIME_FORMAT));
    }
    
    /**
     * 
     * @param string $datetime
     * @param int $days
     * @param string $out_format
     * @return string
     */
    public static function addDaysInDateString($datetime, $days, $out_format)
    {
        if (!is_numeric($datetime))
        {
            $datetime = strtotime($datetime);
        }
        
        if ($days > 0) {
            $days = "+" . $days;
        }
        
        return date($out_format, strtotime($days. " day", $datetime));
    }
    
    /**
     * return list of days between two dates
     * @param object $start_date
     * @param object $end_date
     */
    public static function getDayListBetweenTwoDates($start_date, $end_date, $format)
    {
        $temp = $start_date;
        $list = array();
        while($temp <= $end_date)
        {
            $str = self::getFormatDateFromObj($temp, $format);
            $list[] = $str;
            $temp = self::addDaysInDateObj($temp, 1);
        }
        
        return $list;
    }
    
    /**
     * @param Object date 
     * reutrn true / false , check date is last date of month 
     */
    public static function isLastDayOfMonth($date)
    {
        $total_days = self::getFormatDateFromString($date, "t");
        
        $current_day = self::getFormatDateFromString($date, "d");
        
        return $total_days == $current_day;
    }
}