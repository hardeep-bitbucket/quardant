Dear Admin,<br/><br/>

You have received a enquiry from website. The Details are as follows:<br/><br/>

<table border=0 cellpadding = "5">
    <tr>
        <td>Name</td>
        <td><?php echo $this->request->data['name']; ?></td>
    </tr>

    <tr>
        <td>Phone</td>
        <td><?php echo $this->request->data['phone']; ?></td>
    </tr>

    <tr>
        <td>Email</td>
        <td><?php echo $this->request->data['email']; ?></td>
    </tr>

    <tr>
        <td>Comment</td>
        <td><?php echo $this->request->data['comment']; ?></td>
    </tr>
    <tr>
        <td>Industry</td>
        <td><?php echo $this->request->data["industry"]; ?></td>
    </tr>
</table>

<br/>
Kind Regards,<br/>
<?php echo SITE_NAME; ?><br/>