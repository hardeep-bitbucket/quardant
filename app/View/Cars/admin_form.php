<?php
/**
 * Add & edit Cars
 * 
 * 
 * @created    20/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep 
 */
echo $this->element("admin/form_header");
?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal'));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            ?>
            <div class="form-body"> 
                
                <h3 class="form-section">Car Info</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Manufacture <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('automobile_manufacture_id', array(
                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                    'options' => $manufacture_list,
                                    'empty' => 'Please Select',
                                    'class' => 'select2me form-control automobiles',
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Model <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('automobile_model_id', array(
                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                    'options' => $model_list,
                                    'empty' => 'Please Select',
                                    'class' => 'select2me form-control automobiles',
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">   
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Variant <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('automobile_variant_id', array(
                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                    'options' => $variant_list,
                                    'empty' => 'Please Select',
                                    'class' => 'select2me form-control',
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Color <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('color', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Reg. No. <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('reg_no', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => 'form-control',
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Reg. Date <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                    echo $this->Form->input('reg_date', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control datepicker'
                                    ));  
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Chase No. <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('chase_no', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => 'select2me form-control',
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                </div>                
                
                <h3 class="form-section">Personal Info</h3>
                
                <div class="row">
                    
                   <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Owner Name <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('owner_name', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                   </div>
                    
                   <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Country <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('country_id', array(
                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                    'options' => $countryList, "empty" => "Please Select",
                                    'class' => 'select2me form-control county_list'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">State <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('state_id', array(
                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                    'options' => $stateList, "empty" => "Please Select",
                                    'class' => 'select2me form-control',
                                    'id' => 'CarStateId'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Owner City </label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('owner_city', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                   </div>
                </div>
                
                
                <div class="row">
                    
                   <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Owner Zip <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('owner_zip', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Owner Phone <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('owner_phone', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="row">
                    
                   <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Owner Address <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('owner_address', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'rows' => '3', 'cols' => 3,
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                   </div>
                    
                   <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Owner Email <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('owner_email', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">                   
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Active</label>
                            <div class="col-md-1 form-checkbox">
                                <?php 
                                    echo $this->Form->input('is_active', array(
                                    'type' => 'checkbox', 'label' => false, 'div' => false, 'escape' => false,                                                                        
                                    'class' => "form-control form-checkbox")); 
                                ?>
                            </div>                                        
                        </div>
                    </div>
                </div>
                <!--/row-->
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
    $("select.county_list").on('change',function(){
       var v = $(this).val();
       if (v != '')
       {
           $.get('<?php echo $this->base; ?>/places/ajaxPlaceList/' + v, function(data){
               // $("#CarStateId").html(data);
                var obj= $.parseJSON(data);
                console.log(obj);
                $("#CarStateId option").remove();
                $("#CarStateId").append("<option value=''>Please Select</option>")
                $.each(obj,function(i,value){$("#CarStateId").append("<option value='" + i + "'>" + value + "</option>")});
            });
       }
   });
   
   function load_automobile_list(v, id)
   {
       $.get('<?php echo SITE_URL; ?>/automobiles/ajaxGetAutomobiles/' + v, function(data)
       {
           var data = JSON.parse(data);
           var change_id = 0;
            switch(id)
            {
                case "CarAutomobileManufactureId" :
                    change_id = "#CarAutomobileModelId";                    
                break;
                
                 case "CarAutomobileModelId" :
                    change_id = "#CarAutomobileVariantId";                                        
                break;
            }
            
            if (change_id)
            {
                $(change_id).children().remove();            
                $(change_id).append("<option value=''>Please Select</option>")
                $.each(data,function(i,value){$(change_id).append("<option value='" + i + "'>" + value + "</option>")});
            }
       });
   }
   
   $(".automobiles").change(function()
   {
       var v = $(this).val();
       
       if (v)
       {
           load_automobile_list(v, $(this).attr("id"));
       }
       
   });
});    
</script>   