<?php
/**
 * Campaign Controller
 * 
 * 
 * @created    21/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

echo $this->element("admin/summary_header");
$action_for_search = str_replace("admin_", '', $action);
?>

<div class="row">
    <div class="col-md-12">   
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">Search</div>  
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>

            </div>
            <div class="portlet-body">    
                
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->                    
                    <?php echo $this->Form->create($model, array('type' => 'GET' , 'class' => 'ajax-form form-horizontal', 'data-action' => $action_for_search)); ?>
                     
                        <div class="form-body">                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Name</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('CampaignSearch.name', array(
                                                'type' => "text", 'label' => false, 'div' => false, "escape" => false,
                                                'value' => $Campaignname, 
                                                'class' => 'form-control')); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>   
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Campaign No.</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('CampaignSearch.campaign_no', array(
                                                    'type' => "text", 'label' => false, 'div' => false, "escape" => false,                                                
                                                    'value' => $Campaigncampaign_no, 
                                                    'class' => 'form-control'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>   
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Start Date</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('CampaignSearch.start_date', array(
                                                'type' => "text", 'label' => false, 'div' => false, "escape" => false,
                                                'value' => $Campaignstart_date, 
                                                'class' => 'form-control datepicker')); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>   
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">End Date</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('CampaignSearch.end_date', array(
                                                'type' => "text", 'label' => false, 'div' => false, "escape" => false,                                                
                                                'value' => $Campaignend_date, 
                                                'class' => 'form-control datepicker')); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>   
                            </div>
                            <div class="row">
                                <?php if ($auth_user['group_id'] != ADVERTISER_GROUP_ID) : ?>
                                    <div class="col-md-6">
                                         <div class="form-group">
                                             <label class="control-label col-md-4">Advertiser</label>
                                             <div class="col-md-8">
                                                 <?php echo $this->Form->input('CampaignSearch.user_id', array(
                                                     'type' => "select", 'label' => false, 'div' => false, "escape" => false,
                                                     'options' => $advertiserList, 'empty' => "Please Select",
                                                     'value' => $Campaignuser_id, 
                                                     'class' => 'form-control')); 
                                                 ?> 
                                             </div>
                                         </div>
                                    </div>  
                                <?php endif; ?>
                               <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Status</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('CampaignSearch.status_type_id', array(
                                                'type' => "select", 'label' => false, 'div' => false, "escape" => false,
                                                'options' => $campaign_status_list, 'empty' => "Please Select",
                                                'value' => $Campaignstatus_type_id, 
                                                'class' => 'form-control')); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Country</label>
                                        <div class="col-md-8">
                                            <?php echo $this->Form->input('CampaignSearch.country_id', array(
                                                'type' => "select", 'label' => false, 'div' => false, "escape" => false,
                                                'options' => $country_list, 'empty' => "Please Select",
                                                'value' => $Campaigncountry_id, 
                                                'class' => 'form-control')); 
                                            ?> 
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                         <?php echo $this->element("admin/form_search_btn"); ?>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Summary</div>                        
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed  table-custom">
                    <thead>
                       <tr class="head tr-border-blue-madison" >
                            <td width="12%"><?php echo $this->Paginator->sort('campaign_no', 'Campaign No', array('class' => "ajax-page-link")); ?></td>
                            <td width="10%"><?php echo $this->Paginator->sort('name', 'Name'); ?></td>
                            <?php if ($auth_user['group_id'] != ADVERTISER_GROUP_ID) : ?>
                                <td width="10%"><?php echo $this->Paginator->sort('User.full_name', 'Advertiser'); ?></td>
                            <?php endif; ?>
                            <td width="9%"><?php echo $this->Paginator->sort('is_trial', 'Trial/Regular'); ?></td>
                            <td width="9%"><?php echo $this->Paginator->sort('CampaignType.value', 'Campaign Automobile Type'); ?></td>                                                        
                            <td width="9%"><?php echo $this->Paginator->sort('CampaignDuration.value', 'Duration Type'); ?></td>                            
                            <td width="9%" class="td-center"><?php echo $this->Paginator->sort('start_date', 'Start Date'); ?></td>
                            <td width="9%" class="td-center"><?php echo $this->Paginator->sort('end_date', 'End Date'); ?></td>                            
                            <td width="7%" class="td-center"><?php echo $this->Paginator->sort('CampaignStatus.value', 'Status'); ?></td>                                                        
                            <td width="7%" class="td-center">Payment Status</td>
                            <td width="10%" class="td-center">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $record) { ?>
                            <tr>
                                <td><?php echo $record[$model]['campaign_no']; ?></td>
                                <td> <?php echo $record[$model]['name']; ?></td>
                                <?php if ($auth_user['group_id'] != ADVERTISER_GROUP_ID) : ?>
                                    <td> <?php echo $record["User"]['full_name']; ?></td>
                                <?php endif; ?>
                                <td> <?php echo $record[$model]['is_trial'] ? "Trial" : "Regular"; ?></td>
                                <td> <?php echo $record['CampaignType']['value']; ?></td>                                
                                <td> <?php echo $record['CampaignDuration']['value']; ?></td>                                
                                <td class="td-center"> <?php echo $record[$model]['start_date']; ?></td>
                                <td class="td-center"> <?php echo $record['CampaignDuration']['id'] == CAMPAIGN_DURATION_FIXED_ID ? $record[$model]['end_date'] : ""; ?></td>                                
                                <td class="td-center"> <?php echo $this->TSHtml->campaignStatusLabel($record['CampaignStatus']['id'], $record['CampaignStatus']['value']); ?></td>
                                
                                <td class="td-center"> 
                                    <?php 
                                        if ($record[$model]['total_pending_invoice_count'] > 0)
                                        {
                                            echo $this->TSHtml->transactionStatusLabel(StaticArray::$invoice_status_unpaid); 
                                        }
                                        else if ($record[$model]['total_paid_invoice_count'] > 0)
                                        {
                                            echo $this->TSHtml->transactionStatusLabel(StaticArray::$invoice_status_paid); 
                                        }
                                    ?>
                                </td>
                                <td class="td-center">
                                    <div class="summary-action">  
                                        <?php 
                                            if ($record['CampaignStatus']['id'] != CAMPAIGN_CREATED_ID)
                                            {
                                                echo $this->Html->link(
                                                        $this->Html->image("admin/summary_view.png", array("class" => "summary-action-icon")), 
                                                        array('action' => 'admin_campaign_detail', $record[$model]['id']), 
                                                        array('escape' => false)
                                                    );
                                            }
                                        ?>
                                        <?php 
                                            echo $this->Html->link(
                                                $this->Html->image("admin/summary_edit.png", array("class" => "summary-action-icon")), 
                                                array('action' => 'admin_edit', $record[$model]['id']), 
                                                array('escape' => false, 'title' => EDIT_TITLE, "class" => 'ajax-page-link')
                                            );
                                        ?>
                                        <span> </span>
                                        <?php
                                            if ($record['CampaignStatus']['id'] == CAMPAIGN_CREATED_ID || $record['CampaignStatus']['id'] == CAMPAIGN_SUBMITTED_ID)
                                            {
                                                echo $this->Html->link(
                                                    "link", 
                                                    array('action' => 'admin_delete', $record[$model]['id']), 
                                                    array('escape' => false, 'title' => DELETE_TITLE, "class" => "summary-action-delete-link")
                                                );

                                                echo $this->Html->image("admin/summary_delete.png" , array(
                                                    "class" => "summary-action-icon summary-action-delete",
                                                    "data-confirm_text" => "Are you sure to delete record ?"
                                                ));
                                            }
                                       ?>
                                    </div>
                                </td>
                    </tr>
<?php } ?>
                    </tbody>                  
                </table>                    
                    <div>    
                      <?php echo $this->element("admin/pagination"); ?>
                    </div> 
            </div>
        </div>
    </div>
</div>