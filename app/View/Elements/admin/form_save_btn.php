<?php
/**
 * common save btns
 * 
 * 
 * @created    19/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<div class="row">
    <div class="col-md-11">
        <div class="pull-right">
            <?php 
                echo $this->Form->button("<i class='fa fa-check'></i> Submit", array("class" => "btn btn-circle green", "type" => "submit", 'label' => false, 'div' => false, 'escape' => false)); 
            ?>
            <span class="space-10"></span>
            <?php
                echo $this->Form->button("Cancel", array("class" => "btn btn-circle default", "type" => "reset", 'label' => false, 'div' => false,)); 
                echo $this->Form->end();
            ?>
        </div>
    </div>                                
</div>  
