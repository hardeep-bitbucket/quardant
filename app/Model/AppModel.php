<?php

/**
 * @package    TFQ
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Sonia
 */
App::uses('Model', 'Model');
App::uses('Sanitize', 'Utility');

class AppModel extends Model 
{

    public $sanitize = true;
    
    public $actsAs = array('Containable');

    /**
     * Save the created_on, created_by, modified_on, modified_by data
     * @param array $options
     */
    public function beforeSave($options = array()) {
        //Checks if created_on field is exists and it's not record edit mode
        if ($this->hasField('created_on') && empty($this->data[$this->alias][$this->primaryKey])) 
        {
            $this->data[$this->alias]['created_on'] = DateUtility::getCurrentDateTimeString(DEFAULT_SQL_DATETIME_FORMAT);
        }

        //Checks if modified_on field is exists
        if ($this->hasField('modified_on')) {
            $this->data[$this->alias]['modified_on'] = DateUtility::getCurrentDateTimeString(DEFAULT_SQL_DATETIME_FORMAT);
        }

        //Checks if created_by field is exists and it's not record edit mode
        if ($this->hasField('created_by')) {
            $this->data[$this->alias]['created_by'] = isset($_SESSION['Auth']['User']['id']) ? $_SESSION['Auth']['User']['id'] : 0;
        }

        //Checks if modified_by field is exists
        if ($this->hasField('modified_by')) {
            $this->data[$this->alias]['modified_by'] = isset($_SESSION['Auth']['User']['id']) ? $_SESSION['Auth']['User']['id'] : 0;
        }
        
        if ($this->sanitize)
        {
            $this->data = Sanitize::clean($this->data);
        }
        
        return true;
    }

    /**
     * Adds is_deleted condition
     * 
     * @param Array $query
     */
    function beforeFind($query) {
        // Force all finds to only find stuff which is not deleted        
        if ($this->hasField('is_deleted')) {
            $query['conditions'][$this->alias . '.is_deleted'] = 0;
        }
        return $query;
    }

    /*
     * Prevent Delete and Inactive if child data available
     * @param id
     * @return Boolean
     */

    public function preventDeleteAndInactive($id) {
        $flag = TRUE;
        if (isset($this->hasMany)) {
            foreach ($this->hasMany as $key => $value) {
                $this->{$key}->recursive = 0;
                $childRecords = $this->{$key}->find('count', array('conditions' => array($key . '.' . $value['foreignKey'] => $id)));
                if (isset($childRecords) && $childRecords > 0) {
                    $flag = FALSE;
                    break;
                }
            }
        }
        return $flag;
    }

    /**
     * Gets the list data which is active
     * 
     * @param Array $fields
     * @return Array
     */
    public function getActiveList($fields = array(), $conditions = array('is_active' => 1, 'is_deleted' => 0)) {
        $fields = $this->_getListDefaultFields($fields);

        return $this->find("list", array('fields' => $fields, 'conditions' => $conditions));
    }

    /**
     * Checks if fields are empty then adds default field list
     * 
     * @param Array $fields
     * @return Array
     */
    private function _getListDefaultFields($fields) {
        if (empty($fields)) {
            $fields = array('id', 'name');
        }
        return $fields;
    }

    /**
     * Get Tree List
     */
    public function getTreeList($parentField, $conditions = array()) {
        $rawData = $this->find('all', array(
            'fields' => array('id', $parentField, 'name'), 
            "conditions" => $conditions
        ));
        //debug($rawData);exit;
        $formatData = array();

        foreach ($rawData as $item) {
            //debug($item);exit;
            $formatData[$item[$this->alias]['id']] = array($parentField => $item[$this->alias][$parentField],
                'name' => $item[$this->alias]['name']);
        }
        //debug($formatData);exit;

        $resultData = array();

        foreach ($formatData as $key => $item) {

            $tempString = '';

            while (!empty($item[$parentField])) {
                $tempString = ' | ' . $item['name'] . $tempString;
                $item = $formatData[$item[$parentField]];
            }

            $tempString = $item['name'] . $tempString;
            $resultData[$key] = $tempString;
        }
        asort($resultData);
        return $resultData;
    }
    
    /**
     * get list of model which have parent field
     * for use that you have to set parent_field in model
     * @param array $parent_id
     * @return array
     */
    public function getChildList($parent_id = 0)
    {
        $parent_id = $parent_id ? $parent_id : NULL;
        
        $list = $this->find("list", array(
            "conditions" => array(
                $this->parent_field => $parent_id
            )
        ));
        
        return $list;
    }
    
    /**
     * get parent and grand parent from child id
     * @param int $id
     * @param array $fields     
     * @return type
     */
    public function getParentChildHierarchy ($id, $fields, $data = array())
    {
        if (!in_array($this->parent_field, $fields) && empty($data)) // check field exist in $fields. check condition only one time
        {
            $fields[] = $this->parent_field;
        }        
        
        $record = $this->find("first", array(
            "fields" => $fields,
            "conditions" => array("id" => $id),
            "recursive" => -1
        ));
        
        
        if ($record)
        {
            $data[] = $record;                 
            if (isset($record[$this->alias][$this->parent_field]) && $record[$this->alias][$this->parent_field])
            {
                return $this->getParentChildHierarchy($record[$this->alias][$this->parent_field], $fields, $data);
            }
        }
        
        $data = array_reverse($data);
        return $data;
    }
    
    
    /**
     * increament the value in field
     * @param int $id
     * @param string $field
     * @param float $add_value
     * @return dynamic
     */
    public function IncreaseFieldValue($id, $field, $add_value)
    {
        $this->recursive = -1;
        $data = $this->find("first", array(
            "fields" => array($field),
            "conditions" => array(
                "id" => $id
            )
        ));
        
        if (!$data)
        {
            return false;
        }
        
        $value = (int) $data[$this->alias][$field];
        $value += $add_value;
        
        $this->id = $id;
        
        $result = $this->saveField($field, $value, false);
        
        return $result ? $value : 0;
    }
    
    public function DecreaseFieldValue($id, $field, $deduct_value)
    {
        $this->recursive = -1;
        $data = $this->find("first", array(
            "fields" => array($field),
            "conditions" => array(
                "id" => $id
            )
        ));
        
        if (!$data)
        {
            return false;
        }
        
        $value = (int) $data[$this->alias][$field];
        $value -= $deduct_value;
        
        $this->id = $id;
        $result = $this->saveField($field, $value, false);
        
        return $result ? $value : 0;
    }
    
    /**
     * Validate you tube url and its length also
     * @param string $url
     */
    public function ValidateYouTubeURL($url)
    {
        $parse = parse_url($url);
        
        if ($parse["host"] != "www.youtube.com") 
        {
            return "Please enter YouTube Url Only";
        }
        
        $info = get_youtube_info_from_embed_url($url);
                
        if (!$info["duration"])
        {
            return "Invalid YouTube Url";
        }
        
        if ($info["duration"] > AD_URL_DURATION_LIMIT)
        {
            return AD_WEB_VIDEO_DURATION_VALIDATION_MSG;
        }
        
        return "";
    } 
}