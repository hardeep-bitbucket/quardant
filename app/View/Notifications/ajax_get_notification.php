<?php
/**
 * ajax get notification view
 * 
 * 
 * @created    05/04/2015
 * @package    anpr
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<?php if ($records) foreach ($records as $record):  ?>
    <li id="<?php echo $record[$model]['id'] ?>">
        <div class="col1">
            <div class="cont">
                <div class="cont-col1">
                    <?php 
                        switch ($record[$model]["type"])
                        {
                            case StaticArray::$notification_type_postive:                                
                                echo '<div class="label label-sm label-success">';
                                    echo '<i class="fa fa-plus"></i>';
                                echo '</div>';
                            break;
                        
                            case StaticArray::$notification_type_negtive:                                
                                echo '<div class="label label-sm label-warning">';
                                    echo '<i class="fa fa-times"></i>';
                                echo '</div>';
                            break;
                            
                            case StaticArray::$notification_type_alert:                                
                                echo '<div class="label label-sm label-info">';
                                    echo '<i class="fa fa-bell-o"></i>';
                                echo '</div>';
                            break;
                        
                            case StaticArray::$notification_type_alert_negtive:                                
                                echo '<div class="label label-sm label-danger">';
                                    echo '<i class="fa fa-bullhorn"></i>';
                                echo '</div>';
                            break;
                        
                            case StaticArray::$notification_type_alert_postive:                                
                                echo '<div class="label label-sm label-success">';
                                    echo '<i class="fa fa-bullhorn"></i>';
                                echo '</div>';
                            break;
                        
                            case StaticArray::$notification_type_warning:                                
                                echo '<div class="label label-sm label-warning">';
                                    echo '<i class="fa fa-warning"></i>';
                                echo '</div>';
                            break;
                                    
                            case StaticArray::$notification_type_error:                                
                                echo '<div class="label label-sm label-danger">';
                                    echo '<i class="fa fa-bolt"></i>';
                                echo '</div>';
                            break;
                        } 
                    ?>
                </div>
                <div class="cont-col2">
                    <div class="desc">
                         <?php echo $record[$model]['content']; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col2">
            <div class="date">
                 <?php echo $record[$model]['created_on']; ?>
            </div>
        </div>
    </li>
    
<?php   endforeach;   ?>
