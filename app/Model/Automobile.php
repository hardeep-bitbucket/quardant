<?php

/**
 * Automobile Model
 * 
 * 
 * @created    19/12/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Sonia
 */
class Automobile extends AppModel 
{
    public $parent_field = "automobile_id";
    /**
     * Behaviours
     */
    public $actsAs = array('Tree' => array(
        'parent' => 'automobile_id'
    ));

    /**
     * Associations
     */
    var $belongsTo = array(
        'ParentCategory' => array(
            'className' => 'Automobile',
            'foreignKey' => 'automobile_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
        'ChildCategory' => array(
            'className' => 'Automobile',
            'foreignKey' => 'automobile_id',
            'dependent' => false,
            'conditions' => '',
        ),
        "AutomobileLocation" => array(
            'className' => 'AutomobileLocation',
            'foreignKey' => 'variant_id',
            'dependent' => false,
            'conditions' => '',
        ),
        "AdPlayedLog" => array(
            'className' => 'AdPlayedLog',
            'foreignKey' => 'automobile_variant_id',
            'dependent' => false,
            'conditions' => '',
        ),
    );

    /**
     * Server Valiadtions
     */
    public $validate = array(
        'name' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => NAME_REQUIRED_MESSAGE)
        )
    );
    
    public function getAutomobilelist($parent_id = 0)
    {
        $parent_id = $parent_id ? $parent_id : NULL;
        
        $list = $this->find("list", array(
            "conditions" => array(
                $this->parent_field => $parent_id
            )
        ));
        
        return $list;
    }
}