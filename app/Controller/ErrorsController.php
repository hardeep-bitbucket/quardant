<?php
/**
 * Errors Controller
 * 
 * 
 * @created    17/03/2015
 * @package    ANPR
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class ErrorsController extends AppController 
{
    public $name = 'Errors';
    
    public function beforeFilter() {
        parent::beforeFilter();        
        $this->Auth->allow();
    }

    public function error404() {
		
        $this->layout = 'home';
        $back = "/";
        $this->set(compact('back'));
    }
    public function admin_error404() {
		
        $this->layout = 'admin_inner';
        $back = "/admin/pages";
        $this->set(compact('back'));
        //$this->render = false;
    }
    
    public function methodNotAllowed()
    {
        $this->layout = 'home';        
    }
    
    public function admin_methodNotAllowed()
    {
        $this->layout = "admin_inner";
    }
}
?>