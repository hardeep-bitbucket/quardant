<?php

/**
 * @package    anpr
 * 
 * 
 * @created    09/03/2014
 * @package    TFQ
 * @copyright  Copyright (anpr) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

$records = $this->request->data;

echo $this->element("admin/form_header");

?>

<div class="portlet light">   
    <div class="portlet-body">  
         <div class="row">
            <div class="col-md-6">
                           
            </div>
            <div class="col-md-6" style="text-align: right;">
                <?php
                    echo $this->Html->link("<i class='fa fa-file-pdf-o'></i> Invoice",                
                        array('action' => 'export_pdf', "admin" => true, $records[$model]["id"]), 
                        array('escape' => false, "class" => "btn btn-circle  btn-primary")
                    );
                ?>
            </div>
        </div>
        <div class="row margin-top-20">
            <div class="col-md-12">
                <div class="portlet green-meadow box">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-desktop"></i> Invoice Details
                        </div>    
                        <div class="tools">                           
                            <a href="javascript:;" class="collapse"></a>                             
                        </div>
                    </div>
                    <div class="portlet-body">                             
                        <div class="row static-info">
                            <div class="col-md-6">
                                <div class="col-md-5 name">Campaign</div>
                                <div class="col-md-7 value">
                                    <?php 
                                        echo $this->Html->link(
                                            $records['Campaign']['name'], 
                                            array("controller" => "Campaigns", 'action' => 'admin_campaign_detail', $records['Campaign']['id']), 
                                            array('escape' => false, 'title' => EDIT_TITLE, "class" => 'ajax-page-link')
                                        );
                                    ?>
                                </div>
                            </div> 
                            <?php if ($auth_user["group_id"] != ADVERTISER_GROUP_ID) : ?>
                                <div class="col-md-6">
                                   <div class="col-md-5 name">Advertiser</div>
                                   <div class="col-md-7 value"><?php echo $records['User']['full_name']; ?> ( $<?php echo $this->request->data["User"]["wallet_amount"]; ?> ) </div>
                               </div>
                            <?php endif; ?>
                        </div>
                        
                         <div class="row static-info">
                            <div class="col-md-6">
                                <div class="col-md-5 name">Start Date</div>
                                <div class="col-md-7 value"><?php echo $records[$model]['start_date']; ?></span></div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-5 name">End Date</div>
                                <div class="col-md-7 value"><?php echo $records[$model]['end_date']; ?></span></div>
                            </div>
                        </div>
                        
                        <div class="row static-info">
                            <div class="col-md-6">
                                <div class="col-md-5 name">Amount</div>
                                <div class="col-md-7 value"><?php echo CURRENCY_SYMBOL; ?><?php echo $records[$model]['amount']; ?></span></div>
                            </div>
                            <?php if ($records["DiscountTransaction"]['created_on']): ?>
                            <div class="col-md-6">
                                <div class="col-md-5 name">Discount</div>
                                <div class="col-md-7 value"><?php echo CURRENCY_SYMBOL; ?><?php echo $records['DiscountTransaction']['credit']; ?></div>
                            </div>
                            <?php endif; ?>
                        </div>
                        <?php if ($records['Transaction']["debit"]): ?>
                        <div class="row static-info">
                            <div class="col-md-6">
                                <div class="col-md-5 name">Paid Amount</div>
                                <div class="col-md-7 value"><?php echo CURRENCY_SYMBOL; ?><?php echo $records['Transaction']["debit"] - ($records['DiscountTransaction']['credit'] ? $records['DiscountTransaction']['credit'] : 0); ?></div>
                            </div>                            
                        </div>
                        <?php endif; ?>
                        <div class="row static-info">
                            <div class="col-md-6">
                                <div class="col-md-5 name">Status</div>
                                <div class="col-md-7 value"><?php echo $this->TSHtml->transactionStatusLabel($records[$model]['status']); ?></div>
                            </div>
                            <div class="col-md-6">
                                <?php if ($records['Transaction']['created_on']): ?>
                                    <div class="col-md-5 name">Payment Date</div>
                                    <div class="col-md-7 value"><?php echo DateUtility::getFormatDateFromString($records['Transaction']['created_on'], DEFAULT_DATETIME_FORMAT); ?></div>
                                <?php endif; ?>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class='col-md-12 form'>
                <div class="form-body">
                <?php
                    if ($auth_user['group_id'] != ADVERTISER_GROUP_ID && $records[$model]['status'] != StaticArray::$invoice_status_cancel) : 
                        echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal invoice-form'));
                        echo $this->Form->hidden('id', array('label' => false)); 
                        echo $this->Form->hidden('user_id', array('label' => false)); 
                        echo $this->Form->hidden('campaign_id', array('label' => false)); 
                        echo $this->Form->hidden('amount', array('label' => false)); 
                        echo $this->Form->hidden('start_date', array('label' => false)); 
                        echo $this->Form->hidden('start_date', array('label' => false)); 
                        echo $this->Form->hidden('end_date', array('label' => false));                         
                        echo $this->Form->hidden('User.wallet_amount', array('label' => false));
                        echo $this->Form->hidden('User.full_name', array('label' => false));
                ?>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label col-md-4">Status <span class="required">*</span></label>
                                <div class="col-md-8">                                            
                                    <?php
                                        switch($records[$model]['status'])
                                        {
                                            case StaticArray::$invoice_status_unpaid: 
                                                $options = array(
                                                    StaticArray::$invoice_status_paid => StaticArray::$invoice_status_type[StaticArray::$invoice_status_paid],                                                
                                                );
                                            break;
                                        }

                                        $options[StaticArray::$invoice_status_cancel] = StaticArray::$invoice_status_type[StaticArray::$invoice_status_cancel];

                                        echo $this->Form->input('status', array(
                                            'type' => 'select' ,'label' => false,'div' => false, 'escape' => false, 
                                            'options' => $options,
                                            'empty' => 'Please Select',
                                            'class' => 'form-control',
                                            'required'
                                        ));                                   
                                    ?>                                              
                                </div>
                            </div>
                        </div>    
                        <div class="col-md-5 transaction-paid">
                            <div class="form-group">
                                <label class="control-label col-md-4">Payment Mode <span class="required">*</span></label>
                                <div class="col-md-8">                                            
                                    <?php
                                        $options = StaticArray::$payment_mode;
                                        unset($options[StaticArray::$payment_mode_online]);
                                        
                                        echo $this->Form->input('TransactionDetail.payment_mode', array(
                                            'type' => 'select' ,'label' => false,'div' => false, 'escape' => false, 
                                            'options' => $options,
                                            'empty' => 'Please Select',
                                            'class' => 'form-control',
                                            'required'
                                        ));                                   
                                    ?>                                              
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-5">                       
                            <div class="form-group">
                                <label class="control-label col-md-4">Description <span class="required">*</span></label>
                                <div class="col-md-5">        
                                    <?php 
                                        echo $this->Form->input('description', array(
                                            'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,                                         
                                            'class' => 'form-control',
                                            "rows" => 2,  "col" =>10
                                            
                                        ));   
                                    ?>
                                </div>
                            </div>
                        </div>                            
                        <div class="col-md-5 transaction-paid">                       
                            <div class="form-group">
                                <label class="control-label col-md-4">Discount (%) <span class="required">*</span></label>
                                <div class="col-md-5">        
                                    <?php 
                                        echo $this->Form->input('discount', array(
                                            'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,                                         
                                            'class' => 'form-control percentage numeric',
                                            'required'
                                        ));   
                                    ?>
                                </div>
                            </div>
                        </div>        
                        <div class="col-md-5 transaction-paid">
                            <div class="form-group">
                                <label class="control-label col-md-4">Net Amount (<?php echo CURRENCY_SYMBOL; ?>)</label>
                                <div class="col-md-8">        
                                    <?php 
                                        echo $this->Form->input('net_amount', array(
                                            'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,                                         
                                            'class' => 'form-control', 'readOnly', "id" => "net-amount-input"
                                        ));                                      
                                    ?>
                                </div>
                            </div>
                        </div>
                </div>
                <?php echo $this->element("admin/form_save_btn"); ?>
                <?php elseif ($records[$model]['status'] == StaticArray::$invoice_status_unpaid) : 
                        echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal advertiser-invoice-form'));                        
                        echo $this->Form->hidden('id', array('label' => false)); 
                        echo $this->Form->hidden('user_id', array('label' => false)); 
                        echo $this->Form->hidden('campaign_id', array('label' => false)); 
                        echo $this->Form->hidden('amount', array('label' => false)); 
                        echo $this->Form->hidden('start_date', array('label' => false)); 
                        echo $this->Form->hidden('start_date', array('label' => false)); 
                        echo $this->Form->hidden('end_date', array('label' => false)); 
                        echo $this->Form->hidden('status', array('label' => false, "value" => StaticArray::$invoice_status_paid));
                        echo $this->Form->hidden('TransactionDetail.payment_mode', array('label' => false));
                        echo $this->Form->hidden('User.wallet_amount', array('label' => false));
                ?>
                    <div class="row static-info">
                        <div class="col-md-6">                       
                            <div class="form-group">
                                <label class="control-label col-md-4">Discount Coupon</label>
                                <div class="col-md-5">        
                                    <?php 
                                        echo $this->Form->input('discount_coupon', array(
                                            'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,                                         
                                            'class' => 'form-control', "id" => "discount-coupon",                                        
                                        ));   
                                    ?>                                
                                </div>
                                <div>
                                    <span id="discount-info" class="required"></span>
                                </div>
                            </div>
                        </div>  

                        <div class="col-md-6">                       
                            <div class="form-group">
                                <label class="control-label col-md-4">Discount (<?php echo CURRENCY_SYMBOL; ?>)</label>
                                <div class="col-md-5">        
                                    <?php 
                                        echo $this->Form->input('discount', array(
                                            'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,                                         
                                            'class' => 'form-control', 'disabled', "id" => "discount-amt"                                       
                                        ));   
                                    ?>
                                </div>
                            </div>
                        </div>  
                    </div>                    
                    <div class="row static-info">
                        <div class="col-md-6">                       
                            <div class="form-group">
                                <label class="control-label col-md-4">Net Amount (<?php echo CURRENCY_SYMBOL; ?>) <span class="required">*</span></label>
                                <div class="col-md-5">        
                                    <?php 
                                        echo $this->Form->input('net_amount', array(
                                            'type' => 'text' ,'label' => false,'div' => false, 'escape' => false,                                         
                                            'class' => 'form-control', "id" => "net-amount",
                                            'disabled'
                                        ));   
                                    ?>
                                </div>
                            </div>
                        </div>    
                    </div>   
                
                <div class="row static-info">
                    <div class="col-md-8"></div>
                    <div class="col-md-4">  
                        <?php 
                            echo $this->Form->button("<i class='fa fa-check'></i> Pay Now", array("class" => "btn green", "type" => "submit", 'label' => false, 'div' => false, 'escape' => false)); 
                            echo $this->Form->end();
                        ?>
                    </div>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
    
<script>
    $(document).ready(function ()
    {
        var amount = parseFloat('<?php echo $records[$model]['amount']; ?>');
        $("#InvoiceDiscount").blur(function()
        {
            var v = parseFloat($(this).val());
            var total = amount;
            if (v > 0)
            {
                if (v > 100)
                 v = 100;
                    
                total = (amount * v) / 100;
                total = amount - total;
            }
            else
            {
                v = 0;
                total = amount;
            }            
            
            $(this).val(v);
            $("#net-amount-input").val(total);
        });
                
        $("#InvoiceStatus").change(function()
        {
           var v = $(this).val();
           
           if (v == '<?php echo StaticArray::$invoice_status_paid ?>')
           {
               $(".transaction-paid input").removeAttr("disabled").attr("required", true);
               $(".transaction-paid").show();
           }
           else
           {
               $(".transaction-paid input").attr("disabled", true).removeAttr("required");
               $(".transaction-paid").hide();
           }
               
        });
        
        $(".invoice-form").submit(function(e, data)
        {
            if (typeof data == "undefined")
            {
                $("#InvoiceDiscount").trigger("blur");

                var net_amount = $("#net-amount-input").val();
                if (net_amount <  parseFloat('<?php echo $this->request->data["User"]['wallet_amount']; ?>'))
                {
                    $(".invoice-form").trigger("submit", { pass : 1});
                }
                else
                {
                    warn("Please recharge Advertiser's wallet");
                }
                return false;
            }
            return true;
        });
        
        function get_discount(coupon, fn)
        {
            $("#discount-info").html("");            
            $.get("<?php echo SITE_URL ?>discounts/ajaxGetDiscount/<?php echo $records[$model]['user_id']; ?>/" + <?php echo $records[$model]['campaign_id']; ?>  + "/" + coupon + "/" + amount, function(data, status)
            {
                data = parseFloat(data);                
                if (data < 0)
                {
                    data = 0;
                    $("#discount-info").html("Invalid Discount Coupon");
                }                
                
                $("#discount-amt, #discount_amount").val(data);
                $("#net-amount").val(amount - data);
                
                if (typeof fn != "undefined" && fn)
                {
                    fn();
                }
            });
        }
        
        $("#discount-coupon").blur(function()
        {
            var v = $(this).val().trim();
            
            if (v == "")
            {
                v = 0;
                $("#discount_code").val("");
            }
            else
            {
                $("#discount_code").val(v);
            }
            
            
            get_discount(v, null);
        });
        
        
        $("#InvoiceStatus").trigger("change");  
        
        $("#InvoiceDiscount").trigger("blur");
        
        $(".advertiser-invoice-form").submit(function(e, data)
        {
            if (typeof data == "undefined")
            {
                var v = $("#discount-coupon").val().trim();
            
                if (v == "")
                {
                    v = 0;
                }
            
                get_discount(v, function ()
                {
                    var net_amount = $("#net-amount").val();
                    if (net_amount == "")
                    {
                        net_amount = 0;
                    }
                    else
                    {
                        net_amount = parseFloat(net_amount);
                    }
                    
                    if (net_amount <  parseFloat('<?php echo $auth_user['wallet_amount']; ?>'))
                    {
                        $(".advertiser-invoice-form").trigger("submit", { pass : 1});
                    }
                    else
                    {
                        warn("Please recharge your wallet");
                    }
                });
                return false;
            }
        });
        
        
    });
</script>
