/**
 * Chart config 
 
 * @created    11/03/2015
 * @package    anpr
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

var chartConfig = {
    
    setPieDefault : function (obj)
    {
        obj.titleField = "title";
        obj.valueField = "value";
        obj.outlineColor = "#FFFFFF";
        obj.outlineAlpha = 0.8;
        obj.outlineThickness = 2;
        obj.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
        obj.depth3D = 15;
        obj.angle = 30;
        
        //return obj;
    },
    setSerialChartDefault : function (obj, opt)
    {
            obj.categoryField = "title";
            obj.startDuration = 1;

            // AXES
            // category
            var categoryAxis = obj.categoryAxis;
            categoryAxis.labelRotation = 20;
            categoryAxis.gridPosition = "start";            

            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.valueField = "value";
            graph.balloonText = "[[title]]: <b>[[value]]</b>";
            graph.type = "column";
            graph.lineAlpha = 0;
            graph.fillAlphas = 0.8;
            
            if (typeof opt != "undefined")
            {
                if (typeof opt != "undefined")
                {
                    graph.colorField = opt.colorField;
                }
            }
            
            obj.addGraph(graph);

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.zoomable = false;
            chartCursor.categoryBalloonEnabled = false;
            obj.addChartCursor(chartCursor);

            obj.creditsPosition = "top-right";
    },
    
    setDownloadLink : function(obj)
    {
        obj.exportConfig = {
            menuItems: [{
                icon: '/js/assets/global/plugins/amcharts/amcharts/images/export.png',
                format: 'png',
                onclick: function(a) {
                    var output = a.output({
                        format: 'png',
                        output: 'datastring'
                    }, function(data) {                        
                        $.post('/AdLocations/ajaxSaveTempImage/', {data : encodeURIComponent(data)}, function (data, status)
                        {
                            var win = window.open('/files/chart-image.png', '_blank');
                            if(win){                             
                                win.focus();
                            }
                        });
                    });
                }
            }]
        };
    },
    getColorArray : function (scheme)
    {
        switch(scheme)
        {
            case 1:
                return [
                    "#FF8433", "#793A93",
                    "#3F75A2", "#35918B",
                    "#FF0F00", "#FF6600", 
                    "#FF9E01", "#FCD202",
                    "#F8FF01", "#B0DE09",
                    "#04D215", "#0D8ECF",
                    "#0D52D1", "#2A0CD0",
                    "#8A0CCF", "#CD0D74",
                ];
            break;
            
            case 2:
                return [
                    "#35918B", "#3F75A2", 
                    "#793A93", "#FF8433", 
                    "#0D52D1", "#2A0CD0",
                    "#FF6600", "#FF0F00",
                    "#FCD202", "#FF9E01", 
                    "#B0DE09", "#F8FF01", 
                    "#04D215", "#0D8ECF",                    
                    "#8A0CCF", "#CD0D74",
                ];
            break;
            
            default : 
                return [
                    "#3F75A2", "#35918B",
                    "#FF9E01", "#FCD202",
                    "#FF8433", "#793A93",
                    "#F8FF01", "#B0DE09",
                    "#04D215", "#0D8ECF",
                    "#FF0F00", "#FF6600",
                    "#0D52D1", "#2A0CD0",
                    "#8A0CCF", "#CD0D74",
                ];
            break;
        }
    },
    
    getColor : function (i, scheme)
    {
        scheme = typeof scheme == "undefined" ? 0 : scheme;
        
        return chartConfig.getColorArray(scheme)[i];
    }
}
