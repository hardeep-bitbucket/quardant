<?php
/**
 * home Partner
 * 
 * 
 * @created    29/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

<div class="row">
    <div class="col-md-4">
        <div style="width: 100%;" class="profile-sidebar">
            <!-- PORTLET MAIN -->
            <div class="portlet light">                
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <span class="font-size-24">Hi, </span>
                    <span class="profile-usertitle-name">
                         <?php echo $auth_user["full_name"]; ?>
                    </span>                    
                </div>    
                
                <div class="margin-top-15">
                    <?php if ($auth_user["last_login"]): ?>
                    <div class="margin-5">
                        <span class="profile-desc-text"> Last Login : <b><?php echo DateUtility::getFormatDateFromString($auth_user["last_login"], DEFAULT_DATETIME_FORMAT); ?></b> </span>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="margin-top-15">
                    <div class="profile-userbuttons">                        
                        <?php
                            echo $this->Html->link("<b> Profile </b>", 
                                array("controller" => "partners", "action" => "admin_edit_profile", $auth_user['id']), 
                                array("escape" => false, "class" => "btn btn-circle green-soft btn-sm")
                            );
                        ?>
                        
                        <a href="mailto:<?php echo FROM_EMAIL; ?>">
                            <button class="btn btn-circle blue-soft btn-sm" type="button">Contact Admin</button>
                        </a>
                    </div>
                </div>        
            </div>
        </div>
    </div>
    
    <div class="col-md-8">
        <!-- BEGIN PORTLET -->
        <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">                    
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_1" data-toggle="tab">
                                <i class="icon-bell"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Notifications</span>
                                <span class="badge badge-default" id="notification-count"> 0 </span>  
                            </a>
                        </li>
                        <li>
                            <a href="#tab_1_2" data-toggle="tab">
                                <i class="fa fa-warning"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Location Alerts</span>
                                <span class="badge badge-default" id="location-alert-count"> 0 </span>  
                            </a>
                        </li>
                    </ul>                  
                </div>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1_1">
                        <div style="height : 208px; overflow-y: auto;">
                            <ul class="feeds" id="notification-list">                    
                            </ul>
                        </div>

                        <hr>
                        <div style="text-align: center;">
                            <a href="#">
                                <span id="load-notification">Load old Notifications</span>
                            </a>
                        </div>
                    </div>
                    
                     <div class="tab-pane" id="tab_1_2">
                         
                         <div style="height : 208px; overflow-y: auto;">
                            <ul class="feeds" id="location-alert-list">                    
                            </ul>
                        </div>

                        <hr>
                        <div style="text-align: center;">
                            <a href="#">
                                <span id="load-location-alert">Load old Alerts</span>
                            </a>
                        </div>
                     </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET -->
    </div>
</div>

<div class="row margin-top-10"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">Details</span>
                </div>
                <div class="pull-right">
                    <div class="caption caption-md">
                        <?php
                           echo $this->Form->input("country", array(
                                "type" => "select", "div" => false, "label" => false, "escape" => false,
                                "options" => $location_list, "empty" => "All",
                                "class" => "form-control"
                            ));
                        ?>
                    </div>
                </div>
            </div>
            <div class="portlet-body" id="campaign-block">
                
            </div>
         </div>
    </div>
</div>
<div id="temp-list" style="display: none;"></div>
<div id="temp-list2" style="display: none;"></div>

<script>
    $(document).ready(function()
    {
        $("#country").change(function()
        {
            var v = $(this).val();
            
            $("#campaign-block").load("<?php echo SITE_URL.$controller; ?>/ajaxHomeCampaigns/" + v, function (data, status)
            {
            });
        });
        
        $("#country").trigger('change');
        
        //notifications
        
        $("#load-notification").click(function()
        {
            var id = $("#notification-list li:last").attr("id");
            if (!id)
            {
                id = 0;
            }
            
            $("#temp-list").load('<?php echo SITE_URL ?>Notifications/ajaxGetNotification/' + id, function(data, status)
            {
                 $("#notification-list").append($("#temp-list").html());  
                 
                 $("#notification-count").html($("#notification-list li").length);
            });
        });     
        
        $("#load-location-alert").click(function()
        {
            var id = $("#location-alert-list li:last").attr("id");
            if (!id)
            {
                id = 0;
            }
            
            var partner_id = '<?php echo $auth_user["id"]; ?>';

            $("#temp-list2").load('<?php echo SITE_URL ?>LocationWebServiceAlerts/ajaxGetAlerts/' + id + "/" + partner_id, function(data, status)
            {
                 $("#location-alert-list").append($("#temp-list2").html());  

                 $("#location-alert-count").html($("#location-alert-list li").length);
            });
        });
        
        $("#load-notification, #load-location-alert").trigger("click");
    });
</script>