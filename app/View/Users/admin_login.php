<?php 
/**
 * Login Area
 * 
 * 
 * @created    19/02/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     HARDEEP
 */

echo $this->Form->create();

?>

<div style="margin: 10px 0;">
    <?php echo $this->Session->flash(); ?>
</div>

    <div class="form-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Username</label>			
        <?php
            echo $this->Form->hidden("timezone", array("label" => false, "div" => false, "id" => "timezone-input"));
            echo $this->Form->input('User.username', array(
                'label' => false, 'class' => 'form-control form-control-solid placeholder-no-fix',
                "placeholder" => "Username", "required"
            ));
        ?>
    </div>
    <div class="form-group">    
        <label class="control-label visible-ie8 visible-ie9">Password</label>			
        <?php
            echo $this->Form->input('User.password', array(
                'label' => false, 'type' => 'password',
                'class' => 'form-control form-control-solid placeholder-no-fix',
                "placeholder" => "Password", "required"
            ));
        ?>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">    
                    <div class="col-md-1 form-checkbox">
                        <?php
                            echo $this->Form->input('User.remember_me', array(
                                'label' => false, 'type' => 'checkbox',
                                'class' => 'form-control', "style" => " margin-left: -10px; margin-top: -10px;"
                            ));
                        ?>
                    </div>
                    <label class="control-label col-md-9" style="margin-top:9px;">Remember Me</label>	            
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-group">
                    <div class="pull-right">
                        <?php
                            echo $this->Form->button("Login <i class='m-icon-swapright m-icon-white'></i>", array(
                                "div" => false, 'type' => "submit",
                                "class" => "btn btn-success uppercase", 
                                "escape" => false
                            ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>		

<?php  echo $this->Form->end(); ?>

  <script type="text/javascript">
  $(document).ready(function () {
        var tz = jstz.determine();
        
        var response_text = 'No timezone found';
        
        if (typeof (tz) === 'undefined') {
            response_text = 'No timezone found';
        }
        else {
            response_text = tz.name(); 
        }
        
        $('#timezone-input').val(response_text);        
    });
  </script>

