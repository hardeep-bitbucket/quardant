<?php 

class TSHtmlHelper extends AppHelper 
{
	 var $helpers = array('Html');
     public $treeOptions = array(
            "id" => "id", 
            "name" => "name", 
            "dataField" => "",
            "class" => "",
            "levelClass" => "level"
     );
     
	 //Function imageOptions gives magnifier and delete icons for input file fields in edit mode
	 function imageOptions($image, $required = false){
		
			echo "&nbsp;&nbsp;&nbsp";
			echo $this->Html->link(
				'View', 
				'#', 
				array('class' => 'view_link preview_image', 'url' => $image)
			);
			if(!$required) {
				echo "&nbsp;&nbsp;&nbsp";
				echo $this->Html->link(
					'Delete', 
					'#', 
					array('class' => 'delete_link delete_image', 'url' => $image)
				);
			}
		   
	 }
     
     /**
      * 
      * @param type $url
      * @param type $options array
      * options keys are 
      * 1. url_type constants LOCAL, WEB
      * 2. delete boolean
      */
     function videoOptions($url, $options)
     {
			echo "&nbsp;&nbsp;&nbsp";
			echo $this->Html->link(
				'View', 
				'#', 
				array('class' => 'view_link preview_video', 'data-url' => $url, 'data-url_type' => $options["url_type"])
			);
            
			if (isset($options["delete"]) && $options["delete"]) 
            {
                echo "&nbsp;&nbsp;&nbsp";
                echo $this->Html->link(
                    'Delete', 
                    '#', 
                    array('class' => 'delete_link delete_image', 'url' => $image)
                );
			}
		   
	 }
             
     
     function loadTreeLevelUL($data, $selected_node_list = array(), $options = array(), $level = 0)
     {
         foreach ($this->treeOptions as $key => $value)
         {
             if (!isset($options[$key]))
             {
                 $options[$key] = $value;
             }
         }
         
         $class = $options["class"] . " " . $options["levelClass"] . "-" . $level;
         
         if (!empty($data))
         {
            echo "<ul class='" . $class . "'>";
            foreach ($data as $inner_data)
            {
                foreach ($inner_data as $key => $arr)
                {
                    if ($key == "children")
                    {
                        if (!empty($arr))
                        {
                            $this->loadTreeLevelUL($arr, $selected_node_list, $options, $level + 1);
                        }
                    }
                    else
                    {
                        $attr = "id='" . $arr[$options["id"]] . "'";
                        
                        if ($options["dataField"] && isset($arr[$options["dataField"]]) && $arr[$options["dataField"]])
                        {
                            $attr = $attr . " data-" . $options["dataField"] . "=" . $arr[$options["dataField"]];
                        }   
                        
                        if (isset($arr["selected"]) && $arr["selected"])
                        {
                            $attr = $attr . " class='jstree-checked'";
                        }
                        else if (!empty($selected_node_list))
                        {
                            if (in_array($arr[$options["id"]], $selected_node_list))
                            {
                                 $attr = $attr . " class='jstree-checked'";
                            }
                        }
                        
                        echo "<li $attr>" . $arr[$options["name"]];
                    }
                }
                echo "</li>";
            }
            echo "</ul>";
         }
     }
     
     /**
      * --- Project Specfic function
      */
     
     /**
      * Transaction status
      * @param int $value
      */
     function transactionStatusLabel($value)
     {
        switch($value)
        {
            case StaticArray::$invoice_status_unpaid:
                echo '<span class="label label-danger custom-label" style="font-weight: bold;">';
            break;

            case StaticArray::$invoice_status_paid:
                echo '<span class="label label-success custom-label" style="font-weight: bold;">';
            break;

            case StaticArray::$invoice_status_cancel:
                echo '<span class="label  label-default custom-label" style="font-weight: bold;">';
            break;
        }
        echo StaticArray::$invoice_status_type[$value];
        echo '</span>';
     }
     
     /**
      * Campaign status
      * @param int $value
      */
     function campaignStatusLabel($id, $value)
     {
        switch($id)
        {
            case CAMPAIGN_SUBMITTED_ID: 
                echo '<span class="label label-warning custom-label" style="font-weight: bold;">';
            break;
        
            case CAMPAIGN_APRROVED_ID:
                echo '<span class="label label-success custom-label" style="font-weight: bold;">';
            break;

            case CAMPAIGN_ACTIVE_ID:
                echo '<span class="label  label-primary custom-label" style="font-weight: bold;">';
            break;
        
            case CAMPAIGN_STOPPED_ID: case CAMPAIGN_COMPLETED_ID:
                echo '<span class="label  label-default custom-label" style="font-weight: bold;">';
            break;

            case CAMPAIGN_REJECTED_ID:
                echo '<span class="label label-danger custom-label" style="font-weight: bold;">';
            break;

            default :
                echo '<span class="label label-info custom-label" style="font-weight: bold;">';
            break;
        }
        echo $value;
        echo '</span>';
     }
 }
 
 ?>