// JavaScript Document
$(document).on("blur", ".numeric", function()
{
    if ($(this).val() != "")
    {
        var v = parseFloat($(this).val());
        if (!v)
        {
            v = 0;
        }
        $(this).val(v);
    }
});

$(document).on("blur", ".percentage", function()
{
    if ($(this).val() != "")
    {
        var v = parseFloat($(this).val());
        if (!v)
        {
            v = 0;
        }

        if (v > 100)
        {
            v = 100;
        }
        $(this).val(v);
    }
});

$(document).on("blur", ".postive", function()
{
    if ($(this).val() != "")
    {
        var v = parseFloat($(this).val());
        if (!v)
        {
            v = 0;
        }
        v =  Math.abs(v);
        
        $(this).val(v);
    }
});


/**
 * Sort object properties (only own properties will be sorted).
 * @param {object} obj object to sort properties
 * @param {bool} isNumericSort true - sort object properties as numeric value, false - sort as string value.
 * @returns {Array} array of items in [[key,value],[key,value],...] format.
 */
function sortJSON(obj, isNumericSort)
{
    isNumericSort=isNumericSort || false; // by default text sort
    var sortable=[];
    for(var key in obj)
    {
        if(obj.hasOwnProperty(key))
        {
            sortable.push({ key : key, value : obj[key]});
        }
    }
    
    if(isNumericSort)
    {
        sortable.sort(function(a, b)
        {
            return a[1]-b[1];
        });
    }
    else
    {
        sortable.sort(function(a, b)
        {
            var x = a.value.toLowerCase(),
                y = b.value.toLowerCase();
            return x<y ? -1 : x>y ? 1 : 0;
        });
    }
        
    return sortable; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
}