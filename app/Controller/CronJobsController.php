<?php
/**
 * Cron jobs Controller
 * 
 * 
 * @created    11/03/2015
 * @package    anpr
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

App::uses('CommanController', 'Controller');

class CronJobsController extends CommanController 
{
    public $desc;
    public $uses = "CronJobLog";
    public $campaign_creation_days = 30;
    public $campaign_terminate_days = 5;    
    
    public function index()
    {
        $status = true;
        $this->extra = array();
        try
        {
            // before cron job
            
            $this->loadModel("Campaign");
            /**
             * remove date behaviour
             */
            $this->Campaign->Behaviors->detach("DateFormat");        
            
            $hour = (int) DateUtility::getFormatDateFromObj(DateUtility::getCurrentDateTimeObj(), "H");
        
            $this->_createPartnerTransaction();
            if ($hour == 21)
            {
               $this->_createInvoice();
            }
//           else if ($hour == 22)
//            {
                $this->_campaignStatus();
//            }
            
            
            
            /// after cron job            
            $log = $this->{$this->modelClass}->getDataSource()->getLog(false, false);
            
            debug($log);
            
            $affected_rows = 0;
            $min_time = 1000;
            $query = array();
            foreach ($log['log'] as $arr)
            {
                $affected_rows += $arr['affected'];
                if ($arr['took'] > $min_time)
                {
                   $query[] =  $arr['query'] . " (" . $arr['took'] . " ms)" ;
                   $notification_data[] = array(
                       "type" => StaticArray::$notification_type_warning,
                       "content" => "Cron job queries taking lot of time . Response Time : " . $arr['took'] / 1000 . " seconds",
                       "group_id" => ADMIN_GROUP_ID
                   );
                }
            }
            
            if ($query)
            {
                $query = "<br/>" . implode(", <br/>", $query);
            }
            else 
            {
                $query = "";
            }
            
            $this->desc = "Total Query : " . $log['count'] . " Affected Rows :" . $affected_rows . " " . $query;
            $this->desc .= implode(".<br/>", $this->extra);
        }
        catch(Exception $ex)
        {   
            $notification_data[] = array(
                "type" => StaticArray::$notification_type_error,
                "content" => "Exception came in background Cron Job process",
                "group_id" => ADMIN_GROUP_ID
            );
            
            $this->desc = $ex->getMessage() . " : Error found at line no : " . $ex->getLine();
            $status = FALSE;
        }
        
        $data = array(
            "description" => $this->desc,
            "status" => $status
        );
        
        
        if (isset($notification_data))
        {
            debug($notification_data);
            $this->_saveNotification($notification_data);
        }
        
        $this->{$this->modelClass}->save($data);
        
        echo $this->desc;
        exit;
    }
        
    /**
     * change campaign status 
     */
    private function _campaignStatus()
    {
        $this->_activeCampaign();
        
        $this->_completeCampaign();  
        
        $this->_terminateCampaign();        
    }
    
    private function _activeCampaign()
    {
        $records = $this->Campaign->find("all", array(
            "fields" => array("id", "status_type_id"),
            "conditions" => array(
                "status_type_id" => CAMPAIGN_APRROVED_ID,
                "start_date <= " => date("Y-m-d")
            ),
            "recursive" => -1
        ));
                
        if ($records)
        {
            foreach ($records as $record)
            {
                $this->_changeCampaignStatus($record['Campaign']['id'], CAMPAIGN_ACTIVE_ID);
            }            
        }
    }
    
    private function _completeCampaign()
    {
        $records = $this->Campaign->find("all", array(
            "fields" => array("id", "status_type_id"),
            "conditions" => array(
                "status_type_id" => CAMPAIGN_ACTIVE_ID,
                "duration_type_id" => CAMPAIGN_DURATION_FIXED_ID,
                "end_date <" => date("Y-m-d")
            ),
            "recursive" => -1
        ));
        
        if ($records)
        {
            foreach ($records as $record)
            {
                $this->_changeCampaignStatus($record['Campaign']['id'], CAMPAIGN_COMPLETED_ID);
            }
        }
    }
    
    private function _terminateCampaign()
    {
        $records = $this->Campaign->find("all", array(
            "fields" => array("id", "status_type_id"),
            "conditions" => array(
                "status_type_id" => CAMPAIGN_COMPLETED_ID,                
                "end_date <" => DateUtility::getFormatDateFromObj(DateUtility::addDaysInDateObj(DateUtility::getCurrentDateTimeObj(), "-" . $this->campaign_terminate_days), DEFAULT_SQL_DATETIME_FORMAT)
            ),
            "recursive" => -1
        ));
        //debug($records); exit;
        if ($records)
        {
            foreach ($records as $record)
            {
                $this->_changeCampaignStatus($record['Campaign']['id'], CAMPAIGN_TERMINATE_ID);
            }
        }
    }
    
    /**
     * this function create invoice 
     * by calculating all invoice total and all ad played total     
     */
    private function _createInvoice()
    {
        $records = $this->Campaign->find("all", array(
            "fields" => array("id", "status_type_id", "start_date", "end_date", "user_id"),
            "conditions" => array(                   
                "not" => array(
                    "status_type_id" => array(CAMPAIGN_CREATED_ID, CAMPAIGN_APRROVED_ID, CAMPAIGN_SUBMITTED_ID, CAMPAIGN_TERMINATE_ID)
                )
            ),
            "recursive" => -1
        ));
                
        if (!$records)
        {
            return;
        }
        
        //debug($records); exit;
        $date = DateUtility::getCurrentDateTimeObj();
        
        foreach ($records as $key => $record)
        {
            $campaign_id = $record["Campaign"]["id"];
            
            $invoice_first = $this->Campaign->Invoice->find("first", array(
                "fields" => array("id", "end_date", "created_on"),
                "conditions" => array(
                    "campaign_id" => $record["Campaign"]["id"],  
                    "status" => array(StaticArray::$invoice_status_paid, StaticArray::$invoice_status_unpaid),
                ),
                "order" => array(
                    "id" => "desc"
                ),
                "recursive" => -1
            ));
            
            // getiting invoice count which are generated 1 days before
            if ($record["Campaign"]["status_type_id"] != CAMPAIGN_COMPLETED_ID)
            {
                // invoice is created with in 30 days
                if ($invoice_first && isset($invoice_first["Invoice"]["created_on"]))
                {
                    $created_date = DateUtility::getDateObj($invoice_first["Invoice"]["created_on"]);                
                    $days = DateUtility::compareDateObjs($created_date, $date, DateUtility::$DAYS);

                    if ($days < $this->campaign_creation_days)
                    {
                        continue;
                    }
                }
            }
            
            //getting invoice sum which are not cancelled
            $invoice_total = $this->{$this->modelClass}->query("CALL sp_invoice_get_sum(NULL, $campaign_id, NULL, " . StaticArray::$invoice_status_cancel . ");"); 
            $invoice_total = $invoice_total[0][0]["sum_amount"] ? $invoice_total[0][0]["sum_amount"] : 0;
            
            //getting ad played total sum
            $ad_played_price_total = $this->{$this->modelClass}->query("CALL sp_get_ad_total_spend($campaign_id, NULL, NULL, NULL);");
            $ad_played_price_total = $ad_played_price_total[0][0]["sum_amount"] ? $ad_played_price_total[0][0]["sum_amount"] : 0;
            
            $amount = $ad_played_price_total - $invoice_total;
            if ($amount > 0)
            {
                $start_date = $invoice_first && isset($invoice_first["Invoice"]['end_date']) ? DateUtility::addDaysInDateString( $invoice_first["Invoice"]['end_date'], 1, DEFAULT_SQL_DATE_FORMAT) : $record["Campaign"]['start_date'];                    

                $end_date = in_array($record['Campaign']['status_type_id'], array(CAMPAIGN_COMPLETED_ID, CAMPAIGN_STOPPED_ID)) ? $record['Campaign']["end_date"] : DateUtility::getCurrentDateTimeString(DEFAULT_SQL_DATE_FORMAT);

                if ($this->_generateInvoice($record['Campaign']['id'], $record['Campaign']['user_id'], $amount, $start_date, $end_date))
                {
                    $this->extra[] = "Invoice created of campaign : " . $record["Campaign"]["id"];
                }
                else
                {
                    throw new Exception("failed to create invoice", ERROR_SAVE);
                }

            }
            
        }
    }
    
    protected function _generateInvoice($campaign_id, $user_id, $amount, $start_date, $end_date)
    {
        $this->loadModel("Invoice");
        
        $data["Invoice"] = array(
            "user_id" => $user_id,
            "campaign_id" => $campaign_id,
            "amount" => $amount,
            "status" => StaticArray::$invoice_status_unpaid,
            "start_date" => $start_date,
            "end_date" => $end_date
        );
        
        $result = $this->Invoice->save($data);        
        
        if ($result)
        {
            $this->_invoiceSaveCallBack($this->Invoice->id, StaticArray::$invoice_status_unpaid);
        }
        return $result;
    }
    
    private function _createPartnerTransaction()
    {
        $is_last_day_of_month = DateUtility::isLastDayOfMonth(DateUtility::getCurrentDateTimeObj());
        
        if (!$is_last_day_of_month)
        {
            //return false;
        }
        
        $this->loadModel("User");
        $this->User->recursive = -1;
        
        $this->User->contain(array(
            "Location" => array(
                "fields" => array(
                    "id", "name", "price_per_impression", "partner_revenue_share_percentage"
                ),
            )
        ));
        
        $partners = $this->User->find("all", array(
            "fields" => array("id", "username", "name"),
            "conditions" => array(
                "group_id" => PARTNER_GROUP_ID
            )
        ));
            
        $start_date = DateUtility::getFormatDateFromObj(DateUtility::getCurrentDateTimeObj(), "Y-m-01");
        $end_date = DateUtility::getFormatDateFromObj(DateUtility::getCurrentDateTimeObj(), "Y-m-t");
        
        foreach ($partners as $partner)
        {
            $partner_id = $partner["User"]["id"];
                            
            $partner_transaction_sum = $this->{$this->modelClass}->query("CALL sp_get_partner_transaction_amount($partner_id, NULL , '$start_date', '$end_date');");
            if (!$partner_transaction_sum)
            {
                foreach ($partner['Location'] as $location)
                {
                    $location_id = $location['id'];
                    $location_total_spend = $this->{$this->modelClass}->query("CALL sp_get_ad_total_spend(NULL, $location_id , NULL, NULL);");

                    $pending_sum = $location_total_spend - $partner_transaction_sum;
                    
                    if ($pending_sum)
                    {
                        $partner_amount = ($pending_sum * $location["partner_revenue_share_percentage"]) / 100;

                        $data[] = array(
                            "partner_id" => $partner_id,
                            "location_id" => $location_id,
                            "start_date" => $start_date,
                            "end_date" => $end_date,
                            "amount" => $partner_amount
                        );
                    }
                }
            }
        }
        
       
        if (isset($data))
        {
             debug($data); exit;
        
            return $this->User->PartnerTransaction->saveMany($data);
        }
        
        return true;
    }
    
}
