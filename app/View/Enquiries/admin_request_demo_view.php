<?php
/**
 * Enquiry Controller
 * 
 * 
 * @created    24/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

?>

<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h3 class="page-title">
              <?php echo $title_for_layout; ?>
            </h3>
        </div>
    </div>
</div>

<div class="portlet light">   
    <div class="portlet-body">        
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet green-meadow box">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-desktop"></i><?php echo $heading; ?>
                        </div>    
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>                            
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row static-info">
                            <div class="col-md-5 name">Full Name</div>
                            <div class="col-md-7 value"><?php echo $record['Enquiry']['fullname']; ?>
                            </div>
                        </div>                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Company Name</div>
                            <div class="col-md-7 value"><?php echo $record['Enquiry']['company_name']; ?>
                            </div>
                        </div>                                                                 
                        <div class="row static-info">
                            <div class="col-md-5 name">Email Address</div>
                            <div class="col-md-7 value"><?php echo $record['Enquiry']['email']; ?>
                            </div>
                        </div>                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Phone No</div>
                            <div class="col-md-7 value"><?php echo $record['Enquiry']['phone']; ?>
                            </div>
                        </div>                           
                         <div class="row static-info">
                            <div class="col-md-5 name">Advertising Agency</div>
                            <div class="col-md-7 value">                                
                            <?php 
                                echo $record['Enquiry']['is_advertising_agency'] == 1 ? "Yes" : "No";
                            ?>
                            </div>
                        </div>                         
                         <div class="row static-info">
                            <div class="col-md-5 name">Automobile Dealers</div>
                            <div class="col-md-7 value">
                             <?php 
                                echo $record['Enquiry']['is_automobile_dealers'] == 1 ? "Yes" : "No";
                            ?>
                            </div>
                        </div>                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Drive through restaurants</div>
                            <div class="col-md-7 value">
                             <?php 
                                echo $record['Enquiry']['is_drive_through_restaurants'] == 1 ? "Yes" : "No";
                             ?>
                            </div>
                        </div>                        
                        <div class="row static-info">
                            <div class="col-md-5 name">Petrol stations</div>
                            <div class="col-md-7 value">
                             <?php 
                                echo $record['Enquiry']['is_petrol_stations'] == 1 ? "Yes" : "No";
                             ?>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Super markets</div>
                            <div class="col-md-7 value">
                             <?php 
                                echo $record['Enquiry']['is_super_markets'] == 1 ? "Yes" : "No";
                             ?>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Automobile service stations</div>
                            <div class="col-md-7 value">
                             <?php 
                                echo $record['Enquiry']['is_automobile_service_stations'] == 1 ? "Yes" : "No";
                             ?>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Electric car charging points</div>
                            <div class="col-md-7 value">
                             <?php 
                                echo $record['Enquiry']['is_electric_car_charging_points'] == 1 ? "Yes" : "No";
                             ?>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">Shopping mall / Airport car parking</div>
                            <div class="col-md-7 value">
                             <?php 
                                echo $record['Enquiry']['is_shopping_mall'] == 1 ? "Yes" : "No";
                             ?>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">General business looking for advertising options</div>
                            <div class="col-md-7 value">
                             <?php 
                                echo $record['Enquiry']['is_general_business'] == 1 ? "Yes" : "No";
                             ?>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-5 name">How hear about us</div>
                            <div class="col-md-7 value"><?php  echo $record['Enquiry']['how_did_hear']; ?></div>
                        </div>
                         <div class="row static-info">
                            <div class="col-md-5 name">Hope We can help</div>
                            <div class="col-md-7 value"><?php  echo $record['Enquiry']['how_do_you_hope']; ?></div>
                        </div>
                         
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>