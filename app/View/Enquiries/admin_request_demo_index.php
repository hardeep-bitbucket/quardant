<?php
/**
 * Enquiries Controller
 * 
 * 
 * @created    23/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

$action_for_search = str_replace("admin_", '', $action);
?>

<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h3 class="page-title">
              <?php echo $title_for_layout; ?>
            </h3>
        </div>
    </div>
</div>
<?php
    //echo $this->element("admin/breadcrumb"); 
    echo $this->Session->flash();
?>

<div class="row">
    <div class="col-md-12">   
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">Search</div>  
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>

            </div>
            <div class="portlet-body">    
                
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->                    
                    <?php echo $this->Form->create($model, array( 'type' => 'GET' , 'class' => 'ajax-form form-horizontal', 'data-action' => $action_for_search)); ?>
                     
                        <div class="form-body">                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">From Date</label>
                                        <div class="col-md-8">
                                            <?php 
                                                echo $this->Form->input('EnquirySearch.fromdate', array(
                                                    'type' => "text", 'label' => false, 'div' => false, "escape" => false,
                                                    'value' => $Enquiryfromdate, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>                                                            
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">To Date</label>
                                        <div class="col-md-8">
                                            <?php 
                                                echo $this->Form->input('EnquirySearch.todate', array(
                                                    'type' => "text", 'label' => false, 'div' => false, "escape" => false,
                                                    'value' => $Enquirytodate, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Full Name</label>
                                        <div class="col-md-8">
                                            <?php 
                                                echo $this->Form->input('EnquirySearch.fullname', array(
                                                    'type' => "text", 'label' => false, 'div' => false, "escape" => false,                                                        
                                                    'value' => $Enquiryfullname,
                                                    'class' => 'form-control'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>                                                            
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Mobile</label>
                                        <div class="col-md-8">
                                            <?php 
                                                echo $this->Form->input('EnquirySearch.phone', array(
                                                    'type' => "text", 'label' => false, 'div' => false, "escape" => false,
                                                    'value' => $Enquiryphone, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                    
                         <?php echo $this->element("admin/form_search_btn"); ?>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Summary</div>                        
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed table-custom">
                    <thead>
                       <tr class="head" >
                            <td width="5%" class="td-center"><?php echo $this->Paginator->sort('id', 'ID', array('class' => "ajax-page-link")); ?></td>                            
                            <td><?php echo $this->Paginator->sort('created_on', 'Date', array('class' => "ajax-page-link")); ?></td>
                            <td><?php echo $this->Paginator->sort('fullname', 'Name', array('class' => "ajax-page-link")); ?></td>
                            <td><?php echo $this->Paginator->sort('email', 'Email', array('class' => "ajax-page-link")); ?></td>
                            <td><?php echo $this->Paginator->sort('phone', 'Phone', array('class' => "ajax-page-link")); ?></td>                            
                            <td width="10%" class="td-center">Actions</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $record) { ?>
                            <tr>
                                <td class="td-center"><?php echo $record[$model]['id']; ?></td>
                                <td><?php echo date(DEFAULT_DATE_FORMAT, strtotime($record[$model]['created_on'])); ?></td>
                                <td><?php echo $record[$model]['fullname']; ?></td>
                                <td><?php echo $record[$model]['email']; ?></td>
                                <td><?php echo $record[$model]['phone']; ?></td>                                
                                <td class="td-center">
                                    <div class="summary-action">                                
                                        <?php 
                                            echo $this->Html->link(
                                                    $this->Html->image("admin/summary_view.png", array("class" => "summary-action-icon")), 
                                                    array('action' => 'admin_request_demo_view', $record[$model]['id']), 
                                                    array('escape' => false, 'title' => EDIT_TITLE, "class" => 'ajax-page-link')
                                                );
                                        ?>
                                        <span> </span>
                                        <?php
                                            echo $this->Html->link(
                                                    "link", 
                                                    array('action' => 'admin_delete', $record[$model]['id']), 
                                                    array('escape' => false, 'title' => DELETE_TITLE, "class" => "summary-action-delete-link")
                                                );

                                            echo $this->Html->image("admin/summary_delete.png" , array(
                                                "class" => "summary-action-icon summary-action-delete",
                                                "data-confirm_text" => "Are you sure to delete record ?"
                                            ));

                                       ?>
                                    </div>
                                </td>
                         </tr>
                    <?php } ?>
                    </tbody>                  
                </table>                    
                    <div>    
                      <?php echo $this->element("admin/pagination"); ?>
                    </div> 
            </div>
        </div>               

    </div>
</div>
 </div>
    