<!DOCTYPE html>
<html class="no-js">
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Home</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <?php
        echo $this->Html->meta('keywords', $meta_keywords);
        echo $this->Html->meta('description', $meta_description);
        ?>
        <link href="<?php echo SITE_URL; ?>/app/webroot/favicon.ico" type="image/x-icon" rel="icon" /><link href="<?php echo SITE_URL; ?>/app/webroot/favicon.ico" type="image/x-icon" rel="shortcut icon" />
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700%7COpen+Sans:300italic,400italic,600italic,700italic,800italic,300,400,600,700">
        <?php
        echo $this->Html->css(array(
            'frontend/vendor', 
            'frontend/bootstrap', 
            'frontend/main', 
            'frontend/theme_sangria',
            'frontend/custom', 
            'bootstrap-multiselect'
        ));        
        echo $this->Html->script(array(
            'jquery-1.7.2.min', "jstz.min", 'frontend/frontend_common', "common"                    
        ));        
        ?>
        <noscript>
        <link rel="stylesheet" href="/frontend/styleNoJs.css">
        </noscript>
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Remove Empty <p> Tags -->
        <script type="text/javascript">
            $(document).ready(function()
            {
                
                $('p').each(function()
                {
                    var $this = $(this);
                    if ($this.html().replace(/\s|&nbsp;/g, '').length == 0)
                        $this.remove();
                });
                $(".clearfix").html("");
                
                //$(".prev-video").fancybox();
            });
            
        </script>
    </head>

    <body>
       
        <div id="load"></div><!--[if lt IE 9]>
<p class="browsehappy">You are using an strong outdated browser. <br>Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
        <div class="page">
            <!-- Start Nav Section-->
            <?php echo $this->element("frontend/header"); ?>

            <!-- Start body Section-->
            <?php echo $this->fetch('content'); ?>    

            <!-- Start Footer section-->
            <?php echo $this->element("frontend/footer"); ?>
            <!-- End Footer section-->
            
            <!--End Here -->
            <div id="back_to_top"><a href="#" class="fa fa-arrow-up fa-lg"></a></div>
        </div>
        <!-- Load JS -->
        <?php
        echo $this->Html->script(array(
            'frontend/vendor/modernizr',
            'frontend/vendor/plugin', 
            'frontend/vendor/bootstrap',
            'frontend/vendor/bootstrap-extend', 
            'frontend/main', 
            'bootstrap-multiselect'            
            ));
        ?>
    </body>
    
    
<script>
    
    $(document).ready(function()
    {
        /// setting the height width of iframe;
        var w = window.innerWidth, h = window.innerHeight;
        
        w = w - (w / 20);
        h = h - (h / 8);
        
        $(".modal-dialog, .modal-body").css("width", w);
        $(".modal-dialog, .modal-body").css("height", h);
        
        h = h - 40;
        w = w - 25;
        $(".video-frame iframe").css("width", w);
        $(".video-frame iframe").css("height", h);
        
        //map 
        $("#ANPRMotion-map-model-a").click(function()
        {
            if ($("#ANPRMotion-map-model iframe").attr("src") == "")
            {
                $("#ANPRMotion-map-model iframe").attr("src", '/Locations/ajaxLocationLastAd');
            }
        });
        
        $(".ms-multi-select").multiselect();
    });
    
</script>
</html>
