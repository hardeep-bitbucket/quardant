<?php 
/**
 * pagination
 * 
 * 
 * @created    17/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

?>

<div>
    <div class="pull-left dataTables_info">
        <?php
        echo $this->Paginator->counter(
                'Pages {:page} of {:pages}, showing {:current} records out of
              {:count} total, starting on record {:start}, ending on {:end}'
        );
        ?>
    </div>
    <div class="pull-right">
        <div class="dataTables_paginate paging_bootstrap_full_number">
            <ul class="pagination" style="visibility: visible;">
                <li>
                    <?php
                        echo $this->Paginator->prev('<i class="fa fa-angle-double-left"></i>', array('separator' => '', 'class' => 'ajax-page-link', 'div' => false, 'escape' => false)
                    );
                    ?>
                </li>
                    <?php 
                        echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '', "currentClass" => "pagination-current", "class" => "ajax-page-link"));
                    ?>	
                <li>
                    <?php 
                        echo $this->Paginator->next('<i class="fa fa-angle-double-right"></i>', array('separator' => '', 'class' => 'ajax-page-link', 'div' => false, 'escape' => false)); 
                     ?> 
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
</div>