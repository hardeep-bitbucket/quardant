<?php
/**
 * Campaign Controller
 * 
 * 
 * @created    26/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */

?>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&sensor=false&libraries=geometry"></script>

<?php
echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal target_location', 'enctype' => "multipart/form-data"));
echo $this->Form->hidden('id', array('label' => false)); 
echo $this->Form->hidden('location_ids', array('label' => false, 'id' => "location_ids")); 
echo $this->Form->hidden('tab_name', array('label' => false)); 
echo $this->Form->hidden('country_id', array('label' => false, 'id' => "country_id")); 
?>

<h3 class="form-section">Google Map</h3>
<div class="row">
    <div class="col-md-6 pull-right">
        <div class="form-group">
            <label class="control-label col-md-4">Place <span class="required">*</span></label>
            <div class="col-md-8">                                            
                <?php
                    echo $this->Form->input('place_id', array(
                        'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                        'options' => $place_list,
                        "id" => "place_id",
                        'class' => 'select2me form-control',
                    ));
                ?>                                            
            </div>
        </div>
    </div>
</div>
<div class="row  margin-bottom-10">
    <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
        <div id="map-canvas" class="map-canvas margin-bottom-10"></div>                        
    </div>
    <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
        <div class="portlet box blue margin-bottom-10">
            <div class="portlet-title">
                <div class="caption">Selected Location</div>                        
            </div>
            <div class="portlet-body"  id="selected-location-info-div" style="overflow-y: auto; height: 357px;">
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div class="row margin-top-10">
    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
        <div class="margin-bottom-10">
            <span class="margin-5">
                <img src="" id="map-marker-non-select" />
            </span>                    
            <span class="margin-5">
                Non-Select Locations
            </span>

            <span class="margin-5">
                <img src="" id="map-marker-select" />
            </span>

            <span class="margin-5">
                Selected Locations
            </span>

            <div class="margin-5">
                <ul>
                    <li>
                        To Select location, Click on non-selected marker
                    </li>
                    <li>
                        To Deselect location, Click on selected marker 
                    </li>                
                </ul>
            </div>
        </div>
    </div> 
    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
        <div class="margin-bottom-10 margin-top-10">
            <?php echo $this->element("admin/tab_save_btn"); ?>
        </div>
    </div>
</div>



<script>
    
       var input_field = "#location_ids";       
       var select_locations = {};
       var location_points = JSON.parse('<?php echo $location_points; ?>');
    
       function setLocationVisible(place_id)
       {
            for (var i in markers)
            {
                if (place_id == markers[i].customInfo.data.country_id || place_id == markers[i].customInfo.data.state_id)
                {
                    markers[i].setVisible(true);
                }
                else
                {
                    markers[i].setVisible(false);
                }
            }
       }       
       
       function setInfo()
       {
           $("#map-marker-non-select").attr("src", mapConfig.marker.icon.default);
           $("#map-marker-select").attr("src", mapConfig.marker.icon.new);
       }       
       
       var map, latlngbounds, markers = [];
        function placeMarker(point, latlngset)
        {   
            var icon = typeof point.selected != "undefined" && point.selected == 1 ? mapConfig.marker.icon.selected : mapConfig.marker.icon.nonSelected;

            var data = typeof point.data != "undefined" ? point.data : {};
            if (point.selected == 1 || point.selected == '1')
            {
                select_locations[point['id']] = data;
            }

            var marker = new google.maps.Marker({
                map: map,
                position: latlngset,
                icon: icon,
                customInfo : {
                    selected : point.selected,
                    id : point['id'],
                    data : data
                }
            });
            
            markers.push(marker);

            if ( typeof point['content'] != "undefined" && mapConfig.marker.info_window)
            {                                
                var content = "<div class='map-marker-window'>" + point['content'] + "</div>" ;

                var infoWindow = new google.maps.InfoWindow();
                infoWindow.setContent(content);

                /**
                 * adding Listeners for inflate window
                 */                    
                google.maps.event.addListener(marker, 'mouseover', infoWindowOpen(infoWindow,marker));
                google.maps.event.addListener(marker, 'mouseout', infoWindowClose(infoWindow,marker));                    
            }

            google.maps.event.addListener(marker, 'click', function(e) 
            {   
                if (this.customInfo.selected == 0 || this.customInfo.selected == '0')
                {
                    this.customInfo.selected = 1;
                    this.setIcon(mapConfig.marker.icon.new);
                    select_locations[this.customInfo.id] = this.customInfo.data;
                }
                else
                {
                    this.customInfo.selected = 0;
                    this.setIcon(mapConfig.marker.icon.default);
                    delete select_locations[this.customInfo.id];
                }

                setInputField();
            });
        }
        
        function loadData()
        {
            select_locations = [];
            
            latlngbounds = new google.maps.LatLngBounds();
            if (markers.length > 0)
            {
                for(var i in  markers)
                {
                    markers[i].setMap(null);
                }
            }
            
            if (location_points.length > 0)
            {
                for (var i in location_points)
                {
                    var content = "<div style='width:250px;'> <b>Location</b> : " + location_points[i]["name"] + "<br/>";
                    content += "<b>Price Per Impression</b> : $" + location_points[i]["price_per_impression"] + "<br/>";                    
                    content += "<b>Total Campaigns</b> : " + location_points[i]["campaign_count"] + "<br/>";
                    content += "<b>Active Campaigns</b> : " + location_points[i]["active"] + "<br/>";
                    content += "<b>Add. : </b> : " + location_points[i]["address"] + "<br/>";
                    
                    if (typeof location_points[i]['is_exclusive'] != "undefined" && location_points[i]['is_exclusive'])
                    {
                        content += "<b><i>*This is Exclusive Location</i></b><br/>";
                    }
                    
                    content += "</div>";
                    
                    var latlngset =  new google.maps.LatLng(location_points[i]["latitude"], location_points[i]["longitude"]);
                    
                    placeMarker({ 
                        content : content, 
                        id : location_points[i]['id'], 
                        selected : location_points[i]['selected'],
                        data : {
                            country_id : location_points[i]['country_id'], 
                            state_id : location_points[i]['state_id'],
                            name : location_points[i]["name"],
                            country : location_points[i]['country'],
                            state : location_points[i]['state']
                        }
                    },
                    latlngset);
                    
                    latlngbounds.extend(latlngset); 
                }
                
                setInputField();
                                
                if (location_points.length > 1)
                {
                    map.fitBounds(latlngbounds);
                }
                else
                {
                    map.panTo(latlngset);
                }
            }
            else
            {
                warn("No Location found");
            }
        }
        
        function zoomMap()
        {
            
        }
        
        function infoWindowOpen(infoWindow, marker)
        {
            return function() {
                infoWindow.open(map, marker);
            };
        }
        function infoWindowClose(infoWindow, marker)
        {
            return function() {
                infoWindow.close(map, marker);
            };
        }
        
        function setInputField()
        {
            var ids = Object.keys(select_locations);                
            $(input_field).val(ids.join());     
            
            if (ids.length > 0)
            {
                var html = "";
                var a = 1;
                for ( var i in select_locations)
                {
                    html += "<div class='margin-bottom-5'>";
                        html += "<div class='col-md-3'>";
                        html += "<img src='" + mapConfig.marker.icon.selected + "'/>";
                        html += "</div>";
                        
                        html += "<div class='col-md-9'>";
                        html += "<div><b>" + select_locations[i].name  + "</b></div>";
                        html += "<div>State : " + select_locations[i].state  + "</div>";
                        html += "</div><div class='clearfix'></div>";
                    html += "</div>";
                }
                
                $("#selected-location-info-div").html(html);
            }
        }  
    
       function initialize()
       {
            mapConfig.marker.info_window = true;
            
            // call after setting all values
            setInfo();
            
            var home = new google.maps.LatLng('<?php echo MAP_HOME_LAT ?>', '<?php echo MAP_HOME_LNG ?>');
            
            var mapOptions = {
                zoom: mapConfig.map.map_options.zoom,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: home
            };

            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            
            loadData();
        }


       $(document).ready(function()
       {  
            google.maps.event.addDomListener(window, 'load', initialize);
           
            $("form.target_location").submit(function()
            {
                var location_ids = $(input_field).val().trim();
                
                if (location_ids == "")
                {
                    warn("Please select at least one location");
                    return false;
                }
                
                return true;
            });
            
            $("#place_id").change(function ()
            {
                if ($(this).val() != "")
                {
                    setLocationVisible($(this).val());
                }
            });    
       });
</script>