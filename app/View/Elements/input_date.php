<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$options = array_merge($options, array(
    "aria-invalid" => "true" , "readonly",
    "aria-invalid" => "false", "aria-describedby" => "datepicker-error"
));
?>


<div class="input-group date date-picker" readonly data-date-format='<?php echo DEFAULT_JS_DATE_FORMAT; ?>'>
    <?php
        echo $this->Form->input($title, $options);
    ?>
    <span class="input-group-btn">
    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
    </span>
</div>
