<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="section-title text-center">                 
                    <div class="custom-icon-content-request-demo">
                        <img src="img/frontend/icon-request-a-demo.png" alt="" style="height : 12em;">					
                    </div>
                <h2 class ="upper">Request a Demo</h2>
                <!--<p class="lead">
                    You’ve got questions, and we have answers. Just send us a message
                    and one of our knowledgeable support staff will be in contact with
                    you within 48hrs – even on weekends and holidays.
                </p>-->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="contact-form sep-top-lg">     
                <div style="display:none" class="successMessage alert alert-success text-center">
                        <p><i class="fa fa-check-circle fa-2x"></i></p>
                        <p>Thanks.</p>
                </div>
                <div  style="display:none" class="failureMessage alert alert-danger text-center">
                    <p><i class="fa fa-times-circle fa-2x"></i></p>
                    <p>There was a problem. Please, try again.</p>
                </div>
                <div style="display:none" class="incompleteMessage alert alert-warning text-center">
                    <p><i class="fa fa-exclamation-triangle fa-2x"></i></p>
                    <p>Please complete all the fields in the form before sending.</p>
                </div>
            <?php 
                echo $this->Form->create($model, array("action" => "request_demo_form", 'type' => 'POST', 'class' => 'validate-form'));
            ?>
                 
            <div class="row">
                <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label for="name">Full Name <i class="required-lbl">*</i></label>
                        <input type="text" class="form-control input-lg required" name="fullname" placeholder="Full Name" id="fullname" aria-required="true">
                        <input type="hidden" class="form-control input-lg required" name="type_id" value ="<?php echo REQUEST_DEMO_ENQUIRY_TYPE_ID;?>">
                    </div>
                </div>
                <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label for="email">Company Name <i class="required-lbl">*</i></label>
                        <input type="text" class="form-control input-lg required" name="company_name" placeholder="Company Name" id="company_name" aria-required="true">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label for="phone">Email Address <i class="required-lbl">*</i></label>
                        <input type="text" class="form-control input-lg required email" name="email" placeholder="Email Address" id="email" aria-required="true">
                    </div>
                </div>
                <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label for="company">Phone Number <i class="required-lbl">*</i></label>
                        <input type="text" class="form-control input-lg number" name="phone" placeholder="Phone Number" id="phone" aria-required="true" required = "false">         
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 sep-top-xs">
                    <div class="form-group">
                        <label for="company">Best time to call <i class="required-lbl">*</i></label>
                        <input type="text" class="form-control input-lg" name="best_time_call" placeholder="Best time to call " id="time" aria-required="true" required = "false">                       
                    </div>
                </div>
                <div class="col-md-6 sep-top-xs">
                     <div class="form-group">
                        <label for="indusry">Select your segment <i class="required-lbl">*</i></label>                         
                        <div>
                            <?php
                                echo $this->Form->input("advertisement_segments", array(
                                    'multiple' => 'multiple', 'type' => 'select',
                                    "options" => StaticArray::$advertisement_segments,
                                    "class" => "ms-multi-select form-control input-lg",
                                    "label" => false, "div" => false,
                                ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
             
            <div class="row">
                <div class="col-md-12 sep-top-xs">
                    <div class="form-group">
                        <label for="comment">How did you first hear about ANPRMotion? </label>
                        <textarea class="form-control input-lg" name="how_did_hear" rows="9" placeholder="How did you first hear about ANPRMotion?" id="spont" aria-required="true"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 sep-top-xs">
                    <div class="form-group">
                        <label for="comment">How do you hope ANPRMotion can help? </label>
                        <textarea class="form-control input-lg" name="how_do_you_hope" rows="9" placeholder="How do you hope ANPRMotion can help?" id="spont" aria-required="true"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 sep-top-xs">
                    <div class="form-group text-center">
                        <button class="btn backcolor-green" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Send message</button>
                    </div>
                </div>
            </div>
            <!--input#subject.form-control.input-lg.required(type='text', placeholder='Subject of your message', name='subject')
            -->
        </form>
                <div class="hidden"></div>
            </div>
        </div>
    </div>    
</div>