Dear Admin,<br/><br/>

You have received a enquiry from website. The Details are as follows:<br/><br/>

<table border=0>
    <tr>
        <th>Company Name</th>
        <th>Email Address</th>
        <th>Phone Number</th>
        <th>Address</th>
        <th>Best Time Call</th>
        <th>Advertising agency</th>
        <th>Automobile dealers</th>
        <th>Drive through restaurants</th>
        <th>Petrol stations</th>
        <th>Super markets</th>
        <th>Automobile service stations</th>
        <th>Electric car charging points</th>
        <th>Shopping mall / Airport car parking</th>
        <th>General business looking for advertising options</th>
        <th>How did you first hear about TFQ?</th>
        <th>How do you hope TFQ can help?</th>
    </tr>
    <tr>
        <td><?php echo $this->request->data['company_name']; ?></td>
        <td><?php echo $this->request->data['email']; ?></td>
        <td><?php echo $this->request->data['phone']; ?></td>
        <td><?php echo $this->request->data['address']; ?></td>
        <td><?php echo $this->request->data['best_time_call']; ?></td>
        <td><?php echo $this->request->data['how_did_hear']; ?></td>
        <td><?php echo $this->request->data['how_do_you_hope']; ?></td>
        <td>
            <?php 
                if(isset($this->request->data['is_advertising_agency']))
                {
                    echo "Yes";
                }
                else
                {
                    echo "No";
                }
            ?>
        </td>
        <td>
            <?php 
                if(isset($this->request->data['is_automobile_dealers']))
                {
                    echo "Yes";
                }
                else
                {
                    echo "No";
                }
            ?>
        </td>
        <td>
            <?php 
                if(isset($this->request->data['is_drive_through_restaurants']))
                {
                    echo "Yes";
                }
                else
                {
                    echo "No";
                }
            ?>
        </td>
        <td>
            <?php 
                if(isset($this->request->data['is_petrol_stations']))
                {
                    echo "Yes";
                }
                else
                {
                    echo "No";
                }
            ?>
        </td>
        <td>
            <?php 
                if(isset($this->request->data['is_super_markets']))
                {
                    echo "Yes";
                }
                else
                {
                    echo "No";
                }
            ?>
        </td>
        <td>
            <?php 
                if(isset($this->request->data['is_automobile_service_stations']))
                {
                    echo "Yes";
                }
                else
                {
                    echo "No";
                }
            ?>
        </td>
        <td>
            <?php 
                if(isset($this->request->data['is_electric_car_charging_points']))
                {
                    echo "Yes";
                }
                else
                {
                    echo "No";
                }
            ?>
        </td>
        <td>
            <?php 
                if(isset($this->request->data['is_shopping_mall']))
                {
                    echo "Yes";
                }
                else
                {
                    echo "No";
                }
            ?>
        </td>
        <td>
            <?php 
                if(isset($this->request->data['is_general_business']))
                {
                    echo "Yes";
                }
                else
                {
                    echo "No";
                }
            ?>
        </td>
        <td><?php echo $this->request->data['how_did_hear']; ?></td>
        <td><?php echo $this->request->data['how_do_you_hope']; ?></td>
    </tr>
</table>

<br/>
Kind Regards,<br/>
<?php echo SITE_NAME; ?><br/>