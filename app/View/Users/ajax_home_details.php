<?php
/**
 * home Admin
 * 
 * 
 * @created    15/04/2015
 * @package    ANPR
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>


<div class="row">   
    <div class="col-lg-3 col-sm-6 col-xs-12">
         <span class="dashboard-stat dashboard-stat-light purple-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-users"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $others['total_audiance']; ?></div>
                <div class="desc"><b>Total Audience</b></div>                
            </div>
        </span>
    </div>
    <div class="col-lg-3 col-sm-6 col-xs-12">
         <span class="dashboard-stat dashboard-stat-light blue-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-money"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo CURRENCY_SYMBOL; ?><?php echo $campaigns['admin_revenue']; ?> </div>
                <div class="desc"><b>Revenue</b></div>                
            </div>
        </span>
    </div>
    <div class="col-lg-3 col-sm-6 col-xs-12">
         <span class="dashboard-stat dashboard-stat-light red-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-money"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo CURRENCY_SYMBOL; ?><?php echo $campaigns['partner_revenue']; ?> </div>
                <div class="desc"><b>Partner's Revenue</b></div>                
            </div>
        </span>
    </div>
    <div class="col-lg-3 col-sm-6 col-xs-12">
         <span class="dashboard-stat dashboard-stat-light green-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-money"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo CURRENCY_SYMBOL; ?><?php echo $campaigns['total_budget'] - $campaigns['total_spend']; ?> </div>
                <div class="desc"><b>Total Budget yet to spend</b></div>                
            </div>
        </span>
    </div>
</div>

<div class="row margin-top-10">    
    <div class="col-lg-3 col-sm-6 col-xs-12">
         <span class="dashboard-stat dashboard-stat-light blue-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-users"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $others['new_advertisers']; ?> </div>
                <div class="desc"><b>New  Registrations</b></div>                
            </div>
        </span>
    </div>
    <div class="col-lg-3 col-sm-6 col-xs-12">
         <a href="<?php echo Router::url(array('controller'=>'Campaigns', 'action'=>'index', "admin" => true), false); ?>" 
            class="dashboard-stat dashboard-stat-light red-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-tasks"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $campaigns['total']; ?> </div>
                <div class="desc"><b>Total Campaigns</b></div>                
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-sm-6 col-xs-12">
         <a  href="<?php echo Router::url(array('controller'=>'Campaigns', 'action'=>'index', "admin" => true, "status_type_id" => CAMPAIGN_SUBMITTED_ID), false); ?>" class="dashboard-stat dashboard-stat-light purple-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-tasks"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $campaigns['submitted']; ?> </div>
                <div class="desc"><b>New Campaigns</b></div>                
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-sm-6 col-xs-12">
         <a  href="<?php echo Router::url(array('controller'=>'Campaigns', 'action'=>'index', "admin" => true, "status_type_id" => CAMPAIGN_ACTIVE_ID), false); ?>" class="dashboard-stat dashboard-stat-light blue-soft round-10 margin-bottom-10">
            <div class="visual">
                <i class="fa fa-tasks"></i>                
            </div>
            <div class="details">                
                <div class="number"><?php echo $campaigns['active']; ?> </div>
                <div class="desc"><b>Active Campaigns</b></div>                
            </div>
        </a>
    </div>
</div>
<div class="row top-news margin-top-20">     
    <?php if (isset($best_advertiser['campaign_spend']["id"])) : ?>
    <div class="col-lg-3 col-sm-6 col-xs-12">
        <a href="<?php echo Router::url(array('controller'=>'advertisers', 'action'=>'edit', "admin" => true, $best_advertiser['campaign_count']["id"]), false); ?>" class="btn purple round-10  margin-bottom-10">
            <span>Best Customer  </span>                               
            <span><b><?php echo  isset($best_advertiser['campaign_count']) ? $best_advertiser['campaign_count']['name'] . " " . $best_advertiser['campaign_count']['subname'] : ""; ?> </b></span>
            <em>on basis of Number of Campaigns </em>  
            <i class="fa fa-user top-news-icon"></i>
        </a>
    </div>
    <?php endif; ?>
    
    <?php if (isset($best_advertiser['campaign_spend']["id"])) : ?>
    <div class="col-lg-3 col-sm-6 col-xs-12">
        <a href="<?php echo Router::url(array('controller'=>'advertisers', 'action'=>'edit', "admin" => true, $best_advertiser['campaign_spend']["id"]), false); ?>" class="btn green round-10 margin-bottom-10">
            <span>Best Customer </span>                              
            <span><b><?php  echo isset($best_advertiser['campaign_spend']) ? $best_advertiser['campaign_spend']['name'] . " " . $best_advertiser['campaign_spend']['subname'] : ""; ?></b> </span>
            <em>on basis of Spend</em>     
            <i class="fa fa-user top-news-icon"></i>
        </a>
    </div>
    <?php endif; ?>
    
    <?php if (isset($best_location['Location']["id"])) : ?>
    <div class="col-lg-3 col-sm-6 col-xs-12">        
        <a href="<?php echo Router::url(array('controller'=>'locations', 'action'=>'edit', "admin" => true, $best_location['Location']["id"]), false); ?>" class="btn blue round-10 margin-bottom-10">
            <span>Best Location </span>            
            <span><b><?php echo $best_location ? $best_location['Location']['name']: ""; ?> </b></span>            
            <em>
                <?php 
                    if ($best_location):
                        echo $best_location['City']['name'] ?  $best_location['City']['name'] . ", " : "";
                        echo $best_location['State']['name'] . ", " . $best_location['Country']['name']; 
                    endif;
                ?>
            </em>            
            <i class="fa fa-delicious top-news-icon"></i>
        </a>
    </div>    
    <?php endif; ?>
    
    <?php if (isset($best_campaign["id"])) : ?>
        <div class="col-lg-3 col-sm-6 col-xs-12">
            <a href="<?php echo Router::url(array('controller'=>'campaigns', 'action'=>'campaign_detail', "admin" => true, $best_campaign["id"]), false); ?>" class="btn green round-10 margin-bottom-10" style="height : 100px;">
                <span>Best Campaign </span>            
                <span><b><?php  echo $best_campaign ? $best_campaign['name'] : ""; ?></b> </span>            
                <i class="fa fa-tasks top-news-icon"></i>
            </a>
        </div>
    <?php endif; ?>
</div>

