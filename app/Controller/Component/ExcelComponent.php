<?php
/* 
 * @author Hardeep
 * date : 14-02-2015
 */


App::uses('Component', 'Controller');

class ExcelComponent extends Component 
{
    public $components = array('CommonExport'); 
    
    public $Empty = "[EMPTY]";
    public $last_mem, $TotalRows = 0, $TotalCols = 0; 
    public $options;
          
    /**
     * Is called before the controller’s beforeFilter method.
     * @param Controller $controller
     */
    public function initialize(Controller $controller) 
    {
        $this->last_mem = memory_get_usage(TRUE);
    }
    
    /**
     * Is called after the controller's beforeFilter method but before the controller executes the current action handler.
     * @param Controller $controller
     */
    public function startup(Controller $controller) 
    {
        if (!class_exists('PHPExcel')) {
            throw new CakeException('Vendor class PHPExcel not found!');
        }
        
        $this->_setDefaults();
    }
    
    
    /**
     * General Purpose Export
     * @param array $data
     */
    public function export($data)
    {
        $this->objPHPExcel = new PHPExcel();
        
        $options = $data["options"];        
        $sheets_data = $data["data"];
                
        $this->objPHPExcel->getProperties()->setCreator($options["author"])                                    
                                    ->setTitle($options["title"])
                                    ->setSubject($options["subject"])
                                    ->setDescription($options["desc"]);
        
        $this->objPHPExcel->getDefaultStyle()->getFont()->setName($this->defaultFontFamily)
                                                        ->setSize($this->defaultFontSize);
        
        //loop for sheets
        for ($i = 0 ; $i < count($sheets_data); $i++)
        {
            $sheet_data = $sheets_data[$i];
            if ($i > 0)
            {
                $this->objPHPExcel->createSheet();
            }
            
            $this->objPHPExcel->setActiveSheetIndex($i);
            
            $this->sheet = $this->objPHPExcel->getActiveSheet();            
            $this->sheet->setTitle($sheet_data["sheet"]["name"]);            
            
            if (isset($sheet_data["style"]) && $sheet_data["style"])
            {
                $this->_setStyle($sheet_data["style"]);
            }
            
            $this->_writeData($sheet_data["records"]);
            
            
            for ($i = 1; $i <= $this->TotalCols; $i++)
            {
                $this->sheet->getColumnDimension(chr(64 + $i))->setAutoSize(true);
            }
        }
        
        //echo (memory_get_usage(TRUE) - $this->last_mem)/ 1024 ;
        $this->_output($options);
    }    
    
    private function _writeHeader($header)
    {
        if (empty($header))
        {
            return;
        }
        
        $sr = $this->TotalRows;
        foreach ($header as $inner_data)
        {                 
            $sc = chr(64 + $inner_data["sc"]);
            $ec = chr(64 + $inner_data["ec"]);
            $sr = $inner_data["sr"];
            $er = $inner_data["er"];
            
            $cells = "$sc$sr:$ec$er";
            
            if ($er - $sr > 0 || $inner_data["ec"] - $inner_data["sc"] > 0)
            {
                $this->sheet->mergeCells($cells);
            }
            
            if (isset($inner_data["style"]) && $inner_data["style"])
            {
                $this->sheet->getStyle($cells)->applyFromArray($inner_data["style"]);
            }
            
            $this->sheet->setCellValue($sc.$sr, $inner_data["text"]);
            
            if ($inner_data["ec"] > $this->TotalCols)
            {
                $this->TotalCols = $inner_data["ec"];
            }
            
            if ($er > $this->TotalRows)
            {
                $this->TotalRows = $er;
            }
        }
        
        $this->TotalRows += 1;
    }
    
    private function _writeData($records)
    {
        $data = $records["data"];
        $style = $records["style"];
        
        $sr = $this->TotalRows; 
        
        $i = 0; $a = 0; $data_row_start = 0;
        $row_count = count($data);
        $header_printed = FALSE;
        foreach ($data as $row_data)
        {
            if ($row_data != $this->Empty)
            {
                $c = 0;
                
                if ($a == 0)
                {
                    $total_cols = count($row_data);
                    $ec = chr(64 + $total_cols);
                    $this->sheet->getStyle("A$sr:$ec$sr")->applyFromArray($this->defaultTableHeaderStyle);
                    
                    foreach ($row_data as $col => $col_value)
                    {
                        $sc = chr(65 + $c);
                        $val = $col;
                        
                        if ($col_value != $this->Empty || !$col_value)
                        {
                            $this->sheet->setCellValue($sc.$sr, $val);
                            
                            if (isset($style["header"][$val]) && $style["header"][$val])
                            {
                                $this->sheet->getStyle("$sc$sr:$sc$sr")->applyFromArray($style["header"][$val]);
                            }
                        }
                        $c++;
                    }                      
                    $sr++;
                    $c = 0;
                }
                
                if ($data_row_start == 0)
                {
                    $data_row_start = $sr;
                }
                
                foreach ($row_data as $col => $col_value)
                {
                    $sc = chr(65 + $c);                    
                    if ($col_value != $this->Empty || !$col_value)
                    {
                        $this->sheet->setCellValue($sc.$sr, $col_value);
                    }
                    $c++;
                }                
                $a++;
            }            
            $sr++;
            $i++;
        }
        
        if (isset($style["data"][$val]) && $style["header"][$val])
        {
            
        }
        
        $this->TotalRows = $sr;
    }
    
    private function _output()
    {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $this->options["filename"] . '"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        //header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
    
    private function _setDefaults()
    {
        $this->defaultFontFamily = 'Arial';
        $this->defaultFontSize = 11;

        $this->defaultTableHeaderStyle = array(
            'font'  => array(
              'bold'  => true            
            )  
        );
        $this->defaultTableFooterStyle = array(
            'font'  => array(
              'bold'  => true,
                'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE
            )  
        );

        $this->defaultHeaderStyle = array(
            'font'  => array(
                'size' => 11,
                'bold'  => false,
                'color' => array('rgb' => '000000')            
            ),  
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color'	=> array('rgb' => 'B3D0E5')
            ),
            'alignment' => array(
                'wrap' => true,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $this->defaultFooterStyle = array(
            'font'  => array(
                'size' => 11,
                'bold'  => false,
                'color' => array('rgb' => '000000')            
            ),  
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color'	=> array('rgb' => 'B3D0E5')
            ),
            'alignment' => array(
                'wrap' => true,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        
        $this->options = array(
            "author" => "ANPR",
            "title" => "Excel",
            "subject" => "Excel",
            "desc" => "Excel",
            "save" => false,
            "filename" => "temp.xlsx",
            "filelocation" => ""
        );
    }
    
    private function _setStyle($style)
    {
        foreach ($style as $inner_data)
        {
            $sc = chr(64 + $inner_data["sc"]);
            $ec = chr(64 + $inner_data["ec"]);
            $sr = $inner_data["sr"];
            $er = $inner_data["er"];

            $cells = "$sc$sr:$ec$er";
            
            $this->sheet->getStyle($cells)->applyFromArray($inner_data["style"]);
        }
    }
    
    private function _Header()
    {
        $logo = new PHPExcel_Worksheet_HeaderFooterDrawing();
        $logo->setName('Logo');
        //debug(APP); exit;
        $logo->setPath("img\admin\logo-admin.png"); 
        $logo->setHeight(36); 
        $this->sheet->getHeaderFooter()->addImage($logo, PHPExcel_Worksheet_HeaderFooter::IMAGE_HEADER_LEFT);
        $this->sheet->getHeaderFooter()->setOddHeader('&L&G&R date: ' . date('d-M-Y') . "\n" . 'User: ' . $this->options["User"]["name"]);
        $this->sheet->getHeaderFooter()->setEvenFooter('&L&G&R date: ' . date('d-M-Y') . "\n" . 'User: ' . $this->options["User"]["name"]);

    }
    
    private function _Footer()
    {
        $this->sheet->getHeaderFooter()->setOddFooter('Page &P / &N');
        $this->sheet->getHeaderFooter()->setEvenFooter('Page &P / &N');
    }
    
    private function _loadDefaults()
    {
        $this->objPHPExcel = new PHPExcel();
        
        $this->objPHPExcel->getProperties()->setCreator($this->options["author"])                                    
                                    ->setTitle($this->options["title"])
                                    ->setSubject($this->options["subject"])
                                    ->setDescription($this->options["desc"]);
        
        $this->objPHPExcel->getDefaultStyle()->getFont()->setName($this->defaultFontFamily)
                                                        ->setSize($this->defaultFontSize);
    }
    
    
    public function exportCampaignDetails($data)
    {
        $record = $data["record"];
        
        $this->_loadDefaults();
        
        // writing sheet data
        
        $header_title_style = array(                                       
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '204C70'),
            ),
            'alignment' => array(
                'wrap' => true,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
        );
        
        $header_value_style = array(
            'alignment' => array(
                'wrap' => true,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        
        // wrting first sheet
        $this->sheet = $this->objPHPExcel->getActiveSheet();
        $this->sheet->setTitle("Campaign Details");
        
        // writing header
        $this->_Header();
        // writing Footer
        $this->_Footer();
                
            // wrting the general details
            $c = "A"; $c2 = "B";
            $er = 0; $sr = 1;
            $r = $sr;          
                $this->sheet->setCellValue($c.$r, "Campaign No."); 
                $this->sheet->setCellValue($c2.$r, $record["Campaign"]["campaign_no"]);
                $r++;
                
                
                $this->sheet->setCellValue($c.$r, "Campaign Name"); 
                $this->sheet->setCellValue($c2.$r, $record["Campaign"]["name"]);
                $r++;
                
                $this->sheet->setCellValue($c.$r, "Campaign Type"); 
                $this->sheet->setCellValue($c2.$r, $record["CampaignType"]["value"]);
                $r++;
                
                $this->sheet->setCellValue($c.$r, "Submit Date"); 
                $this->sheet->setCellValue($c2.$r, $record["Campaign"]["submit_datetime"]);
                $r++;
                
                $this->sheet->setCellValue($c.$r, "Approve Date"); 
                $this->sheet->setCellValue($c2.$r, $record["Campaign"]["approve_datetime"]);
                $r++;
                
                $this->sheet->setCellValue($c.$r, "Activation Date"); 
                $this->sheet->setCellValue($c2.$r, $record["Campaign"]["active_datetime"]);
                $r++;
                
                $this->sheet->setCellValue($c.$r, "Efficiency"); 
                $this->sheet->setCellValue($c2.$r, $data["efficiency"] . "%");
                $r++;
                
                $this->sheet->setCellValue($c.$r, "Purpose");
                $this->sheet->setCellValue($c2.$r, $record["Campaign"]["purpose"]);
                
            
            $this->sheet->getStyle("$c$sr:$c$r")->applyFromArray($header_title_style);            
            $this->sheet->getStyle("$c2$sr:$c2$r")->applyFromArray($header_value_style);

            $er = $r;
                
            ///
            $c = "D"; $c2 = "E";
            $r = $sr;   
                $this->sheet->setCellValue($c.$r, "Advertiser"); 
                $this->sheet->setCellValue($c2.$r,  trim($record["User"]["name"]) . " " . trim($record["User"]["subname"]));
                $r++;
                
                
                $this->sheet->setCellValue($c.$r, "Duration"); 
                $this->sheet->setCellValue($c2.$r, $record["CampaignDuration"]["value"]);
                $r++;
                
                $this->sheet->setCellValue($c.$r, "Start Date"); 
                $this->sheet->setCellValue($c2.$r, $record["Campaign"]["start_date"]);
                $r++;
                
                $this->sheet->setCellValue($c.$r, "End Date"); 
                $this->sheet->setCellValue($c2.$r, $record["Campaign"]["end_date"]);
                $r++;
                
                if ($this->options["User"]["group_id"] != ADVERTISER_GROUP_ID)
                {
                    $this->sheet->setCellValue($c.$r, "Setup Cost"); 
                    $this->sheet->setCellValue($c2.$r, CURRENCY_SYMBOL_CODE . $record["Campaign"]["setup_cost"]);
                }
                $r++;
                
                $this->sheet->setCellValue($c.$r, "Budget"); 
                $this->sheet->setCellValue($c2.$r, CURRENCY_SYMBOL_CODE . $record["Campaign"]["budget"]);
                $r++;
                
                $this->sheet->setCellValue($c.$r, "Spend"); 
                $this->sheet->setCellValue($c2.$r, CURRENCY_SYMBOL_CODE . $record["Campaign"]["spend"]);
                
            
           $this->sheet->getStyle("$c$sr:$c$r")->applyFromArray($header_title_style);            
           $this->sheet->getStyle("$c2$sr:$c2$r")->applyFromArray($header_value_style);
           
           if ($r > $er)
           {
               $er = $r;
           }
           
        $sr = $er + 2;
        
        $r = $sr;
        $a = 0;
        foreach ($record["AdLocation"] as $arr)
        {
            $c = 1;
            $a++;
            if ($a == 1)
            {
                $c1 = chr($c + 64);
                $this->sheet->setCellValue(chr($c + 64).$r, "Sr.");  $c++;
                $this->sheet->setCellValue(chr($c + 64).$r, "Location");  $c++;
                $this->sheet->setCellValue(chr($c + 64).$r, "Target Impressions");  $c++;
                $this->sheet->setCellValue(chr($c + 64).$r, "Reached Impressions");  $c++;
                $this->sheet->setCellValue(chr($c + 64).$r, "Spend(" . CURRENCY_SYMBOL_CODE . ")");
                $c2 = chr($c + 64);
                
                $this->sheet->getStyle("$c1$r:$c2$r")->applyFromArray($this->defaultTableHeaderStyle);
                $r++;
            }
            
            $c = 1;
            
            $this->sheet->setCellValue(chr($c + 64).$r, $a);  $c++;
            $this->sheet->setCellValue(chr($c + 64).$r, $arr["Location"]["name"]);  $c++;
            $this->sheet->setCellValue(chr($c + 64).$r, $arr["target_imperssions"]);  $c++;
            $this->sheet->setCellValue(chr($c + 64).$r, $arr["reached_imperssions"]);  $c++;
            $this->sheet->setCellValue(chr($c + 64).$r, $arr["reached_imperssions"] * $arr["Location"]["price_per_impression"]);
                        
            $r++;
        }
        
        $this->TotalCols = 5;
        
        $center_alignment_style = array(
            'alignment' => array(                
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        ); 
        
        $this->sheet->getStyle("A$sr:A$r")->applyFromArray($center_alignment_style);
        $this->sheet->getStyle("C$sr:C$r")->applyFromArray($center_alignment_style);
        $this->sheet->getStyle("D$sr:D$r")->applyFromArray($center_alignment_style);
        $this->sheet->getStyle("E$sr:E$r")->applyFromArray($center_alignment_style);
        
        for ($i = 1; $i <= $this->TotalCols; $i++)
        {
            $this->sheet->getColumnDimension(chr(64 + $i))->setAutoSize(true);
        }
        
        
        //////////////////////Sheet2 ///////////////////////////////////////
        
        $this->objPHPExcel->createSheet();
        $this->objPHPExcel->setActiveSheetIndex(1);
        
        $this->sheet = $this->objPHPExcel->getActiveSheet();
        $this->sheet->setTitle("Invoices");
        
        // writing header
        $this->_Header();
        // writing Footer
        $this->_Footer();
        
        $sr = $er = 1;
        
        $r = $sr;
        $a = 0;
        foreach ($record["Invoice"] as $arr)
        {
            $c = 1;
            $a++;
            if ($a == 1)
            {
                $c1 = chr($c + 64);
                $this->sheet->setCellValue(chr($c + 64).$r, "Sr.");  $c++;
                $this->sheet->setCellValue(chr($c + 64).$r, "Start Date");  $c++;
                $this->sheet->setCellValue(chr($c + 64).$r, "End Date");  $c++;
                $this->sheet->setCellValue(chr($c + 64).$r, "Amount");  $c++;
                $this->sheet->setCellValue(chr($c + 64).$r, "Status");
                $c2 = chr($c + 64);
                
                $this->sheet->getStyle("$c1$r:$c2$r")->applyFromArray($this->defaultTableHeaderStyle);
                $r++;
            }
            
            $c = 1;
            
            $this->sheet->setCellValue(chr($c + 64).$r, $a);  $c++;
            $this->sheet->setCellValue(chr($c + 64).$r, DateUtility::getFormatDateFromString($arr["start_date"], DEFAULT_DATE_FORMAT));  $c++;
            $this->sheet->setCellValue(chr($c + 64).$r, DateUtility::getFormatDateFromString($arr["end_date"], DEFAULT_DATE_FORMAT));  $c++;
            $this->sheet->setCellValue(chr($c + 64).$r, $arr["amount"]);  $c++;
            $this->sheet->setCellValue(chr($c + 64).$r, StaticArray::$invoice_status_type[$arr["status"]]);
                        
            $r++;
            
            $this->TotalCols = 5;
        
            $center_alignment_style = array(
                'alignment' => array(                
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            ); 

            $this->sheet->getStyle("A$sr:A$r")->applyFromArray($center_alignment_style);        
            $this->sheet->getStyle("D$sr:D$r")->applyFromArray($center_alignment_style);

            for ($i = 1; $i <= $this->TotalCols; $i++)
            {
                $this->sheet->getColumnDimension(chr(64 + $i))->setAutoSize(true);
            }
        }
        
        
        //////////////////////Sheet3 ///////////////////////////////////////
        
        $this->objPHPExcel->createSheet();
        $this->objPHPExcel->setActiveSheetIndex(2);
        
        $this->sheet = $this->objPHPExcel->getActiveSheet();
        $this->sheet->setTitle("Ad Played-Automobiles");
        
        // writing header
        $this->_Header();
        // writing Footer
        $this->_Footer();
        
        if ($data["automoble_ad_played_data"]["data"])
        {
            $sr = $er = 1;        
            $r = $sr;

            $c = 1;
            foreach ($data["automoble_ad_played_data"]["header"] as $value)
            {
                $this->sheet->setCellValue(chr($c + 64).$r, $value);  
                $c++;           
            }

            $this->TotalCols = $c - 1;
            $r++;

            foreach ($data["automoble_ad_played_data"]["data"] as $arr)
            {
                $c = 1;
                foreach ($arr as $value)
                {
                    $this->sheet->setCellValue(chr($c + 64).$r, $value);  
                    $c++;           
                }

                $r++;
            }

            $col = chr(64 + $this->TotalCols);

            $this->sheet->getStyle("A$sr:$col$sr")->applyFromArray($this->defaultTableHeaderStyle);        
            $this->sheet->getStyle("A$sr:A$r")->applyFromArray($this->defaultTableHeaderStyle);

            for ($i = 1; $i <= $this->TotalCols; $i++)
            {
                $this->sheet->getColumnDimension(chr(64 + $i))->setAutoSize(true);
            }
        }
        
         //////////////////////Sheet3 ///////////////////////////////////////
        
        $this->objPHPExcel->createSheet();
        $this->objPHPExcel->setActiveSheetIndex(3);
        
        $this->sheet = $this->objPHPExcel->getActiveSheet();
        $this->sheet->setTitle("Ad Played-Creatives");
        
        // writing header
        $this->_Header();
        // writing Footer
        $this->_Footer();
        
        $sr = $er = 1;        
        $r = $sr;
        
        if ($data["creative_ad_played_data"]["data"])
        {
            $c = 1;
            foreach ($data["creative_ad_played_data"]["header"] as $value)
            {
                $this->sheet->setCellValue(chr($c + 64).$r, $value);  
                $c++;           
            }

            $this->TotalCols = $c - 1;
            $r++;

            foreach ($data["creative_ad_played_data"]["data"] as $arr)
            {
                $c = 1;
                foreach ($arr as $value)
                {
                    $this->sheet->setCellValue(chr($c + 64).$r, $value);  
                    $c++;           
                }

                $r++;
            }

            $col = chr(64 + $this->TotalCols);

            $this->sheet->getStyle("A$sr:$col$sr")->applyFromArray($this->defaultTableHeaderStyle);        
            $this->sheet->getStyle("A$sr:A$r")->applyFromArray($this->defaultTableHeaderStyle);

            for ($i = 1; $i <= $this->TotalCols; $i++)
            {
                $this->sheet->getColumnDimension(chr(64 + $i))->setAutoSize(true);
            }
        }
        $this->_output();
    }
}