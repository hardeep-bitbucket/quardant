<?php
/**
 * Add & edit Pages
 * 
 * 
 * @created    24/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep 
 */
echo $this->element("admin/form_header");
?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal', "enctype" => "multipart/form-data"));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            ?>
            <div class="form-body"> 
                
                <h3 class="form-section">Page Details</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Parent Page <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('page_id', array(
                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                    'options' => $parentPageList,
                                    'empty' => 'ROOT',
                                    'class' => 'select2me form-control',
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Name <span class="required">*</span></label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('name', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php if($this->params["action"] == "admin_edit") : ?>
                    <div class="row">                
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Slug </label>
                                <div class="col-md-8">
                                    <?php
                                    echo $this->Form->input('slug', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                        'class' => 'form-control', 'readonly'
                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                
                <h3 class="form-section">SEO Details</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Meta Title <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('meta_title', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => 'form-control',
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Meta Keywords</label>
                            <div class="col-md-8">
                                <?php
                                echo $this->Form->input('meta_keywords', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                    'class' => 'form-control'
                                ));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Meta Description</label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('meta_description', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => 'form-control',
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div> 
                </div>
                
                
                 <h3 class="form-section">Content Details</h3>

                 <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">                            
                            <div class="col-md-12">                                            
                               <?php
                                if (!empty($this->data['Page']['contents']))
                                    $this->Fck->Value = $this->data['Page']['contents'];
                                if (!empty($this->validationErrors['Page']['contents']))
                                    $this->Fck->Error = $this->validationErrors['Page']['contents'];
                                echo $this->Fck->Create('Page/contents');
                                ?>                                          
                            </div>
                        </div>
                    </div>
                </div>
                 
                <h3 class="form-section">Other Details</h3>
                 
                <div class="row">
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Header Image</label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('header_image', array(
                                        'type' => 'file', 'label' => false, 'div' => false, 'escape' => false,                                    
                                        'class' => '',
                                    ));

                                    if (isset($this->data['Page']['header_image']) && !empty($this->data['Page']['header_image']) && is_string($this->data['Page']['header_image'])) 
                                    {
                                        echo $this->TSHtml->imageOptions(SITE_PATH . PAGE_IMAGES . "/" . $this->data['Page']['header_image'], array('required' => false));
                                        echo $this->Form->input('Page.header_image.remove', array('type' => 'hidden', 'value' => ''));
                                    }
                                ?>                                    
                                    &nbsp;&nbsp;&nbsp;(Type: jpeg/png)
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Header Menu</label>
                            <div class="col-md-8 form-checkbox">                                            
                                <?php
                                    echo $this->Form->input('header_menu', array(
                                        'type' => 'checkbox', 'label' => false, 'div' => false, 'escape' => false,                                    
                                        'class' => 'form-control form-checkbox',
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                </div>
                 
                <div class="row">                      
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Display Order</label>
                            <div class="col-md-8">                                            
                                <?php
                                    echo $this->Form->input('display_order', array(
                                        'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                        'class' => 'form-control',
                                    ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Active</label>
                            <div class="col-md-1 form-checkbox">
                                <?php echo $this->Form->input('is_active', array('label' => false, 'type' => 'checkbox', 'class' => "form-control form-checkbox")); ?>
                            </div>                                        
                        </div>
                    </div>
                </div>
                <!--/row-->
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>