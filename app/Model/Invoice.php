<?php

/**
 * Invoice Model
 * 
 * 
 * @created    21/03/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */
class Invoice extends AppModel 
{
    
    public $actsAs = array(                        
        'DateFormat' => array('start_date', 'end_date'),
        "DateTimeFormat" => array(
            "created_on" => array("out_format" => DEFAULT_DATETIME_FORMAT, "in_format" => "")
        )
    );
    
    
    /**
    *Associations
    */
    var $belongsTo = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        "User" => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    var $hasOne = array(
        'Transaction' => array(
            'className' => 'Transaction',
            'foreignKey' => 'invoice_id',
            'conditions' => array("Transaction.type" => 3),
            'fields' => '',
            'order' => ''
        ),
        'DiscountTransaction' => array(
            'className' => 'Transaction',
            'foreignKey' => 'invoice_id',
            'conditions' => array("DiscountTransaction.type" => 4),
            'fields' => '',
            'order' => ''
        )
    ); 
    
}