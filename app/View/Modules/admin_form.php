<?php
/**
 * Add & edit Modules
 * 
 * 
 * @created    24/02/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep 
 */
echo $this->element("admin/form_header");
?>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption"><?php echo $heading; ?></div>  
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>                            
        </div>

    </div>

    <div class="portlet-body">                        
        <div class="portlet-body form">
            <!-- BEGIN FORM-->       
            <?php
            echo $this->Form->create($model, array('type' => 'POST', 'class' => 'ajax-form form-horizontal', "enctype" => "multipart/form-data"));
            echo $this->Form->hidden('id', array('label' => false, 'div' => false, 'escape' => false));
            ?>
            <div class="form-body">  
                
                <h3 class="form-section">General Details</h3>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Title <span class="required">*</span></label>
                            <div class="col-md-8">                                            
                                <?php
                                echo $this->Form->input('title', array(
                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,                                    
                                    'class' => 'form-control',
                                ));
                                ?>                                            
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Active</label>
                            <div class="col-md-1 form-checkbox">
                                <?php echo $this->Form->input('is_active', array('label' => false, 'type' => 'checkbox', 'class' => "form-control form-checkbox")); ?>
                            </div>                                        
                        </div>
                    </div>
                </div>
                
                <h3 class="form-section">Content</h3>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">                            
                            <div class="col-md-12">                                            
                                <?php
                                    if (!empty($this->data['Module']['contents']))
                                        $this->Fck->Value = $this->data['Module']['contents'];
                                    if (!empty($this->validationErrors['Module']['contents']))
                                        $this->Fck->Error = $this->validationErrors['Module']['contents'];
                                    echo $this->Fck->Create('Module/contents');
                                ?>                                         
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
            </div>
            <?php echo $this->element("admin/form_save_btn"); ?>                  
            <!-- END FORM-->
        </div>
    </div>
</div>
