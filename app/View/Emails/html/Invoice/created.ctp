<?php
/**
 * Invoice Email template - created
 * 
 * @created    01/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

Dear <b> <?php echo $records['User']['name'] . " " . $records['User']['subname']; ?> </b>,<br><br>
    
Invoice of campaign <b> <?php echo $records['Campaign']['name'] ?> </b> is pending.
<br>
<?php
    if ($records["Invoice"]["start_date"] && $records["Invoice"]["end_date"])
    {
        echo "Start Date : " . $records["Invoice"]["start_date"];
        echo "<br><br>";
        echo "End Date : " . $records["Invoice"]["end_date"];
    }
?>
<br><br>
<b>Details</b>
<br>
<table border="1" cellpadding="3" cellspacing="3">
    <tr>
        <td>Amount</td>
        <td><?php echo CURRENCY_SYMBOL_CODE; ?><?php echo $records["Invoice"]["amount"] ?></td>
    </tr>    
</table>


<br/><br/>
<?php echo $content['footer']; ?>