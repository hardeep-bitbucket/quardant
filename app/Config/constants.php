<?php
define("DOMAIN", "http://" . $_SERVER["SERVER_NAME"]);
define("SITE_NAME", "ANPRMotion");
define("SITE_URL", DOMAIN . "/");
define("SITE_PATH", DOMAIN . "/");
define("COUNTRY", "India");

define("CURRENCY_SYMBOL", "$");
define("CURRENCY_SYMBOL_CODE", "$");
define("PAYPAL_CURRENCY", "USD");

define("FCK_PATH", SITE_PATH . "/app/webroot/libs/fckeditor/");

// Title for common action buttons
define("EDIT_TITLE", "Edit Record");
define("DELETE_TITLE", "Delete Record");
define("ADD_TITLE", "Add a Record");
define("VIEW_TITLE", "View a Record");
define("BACK_TITLE", "Back to Summary");
define("STATUS_TITLE", "Change Status");
define("PRINT_TITLE", "Print Record");

// Group IDs
define('ADMIN_GROUP_ID', '1');
define('ADVERTISER_GROUP_ID', '2');
define('SUB_ADMIN_GROUP_ID', '3');
define('PARTNER_GROUP_ID', 4);

// Hints constants
define('USERNAME_CHARACTER_LIMIT', 'Username should be more than characters.');

// Date Formats
define('DEFAULT_SQL_DATETIME_FORMAT', 'Y-m-d H:i:s');
define('DEFAULT_SQL_DATE_FORMAT', 'Y-m-d');
define('DEFAULT_DATE_FORMAT', 'd-M-y');
define('DEFAULT_JS_DATE_FORMAT', 'dd-M-yy');
define("DEFAULT_DATETIME_FORMAT", 'd-M-y H:i a');
define("DEFAULT_TIME_FORMAT", 'h:i a');
define("DEFAULT_SQL_TIME_FORMAT", 'H:i:s'); 
//define("DEFAULT_JS_DATETIME_FORMAT", '');


// Constants of Image Paths
define("PAGE_IMAGES", "img/pages");

define("GRAPHIC_IMAGES", "img/graphics/images");
define("RESIZE_GRAPHIC_IMAGES", "img/graphics/images/resize/");
define("ACTUAL_GRAPHIC_IMAGES", "graphics/images/");

define("GRAPHIC_LOGO_IMAGES", "img/graphics/logo");
define("RESIZE_GRAPHIC_LOGO_IMAGES", "img/graphics/logo/resize/");
define("ACTUAL_GRAPHIC_LOGO_IMAGES", "graphics/logo/");

define("AD_CREATIVE", "files/ads/");
define("AD_CREATIVE_HISTORY", "files/history_ads/");
define("AD_LOCAL_DURATION_LIMIT", 15);
define("AD_URL_DURATION_LIMIT", 120);

// Constants of Types
define("GRAPHIC_TYPES", "Graphic Types");
define("INDUSTRY_TYPES", "Industry Types");
define("HOME_HEADER_SLIDER_TYPE_ID", "1");

// Module IDs
define("HEADER_MODULE_ID", "1");
define("FOOTER_MODULE_ID", "2");
define("SOCIAL_ICONS_MODULE_ID", "3");

// Email IDs
define("FROM_EMAIL", "hardeep@techformation.co.in");
define("TO_EMAIL", "hardeep@techformation.co.in");

// Page Slugs
define("ABOUT_US_SLUG", "about-us");
define("AUTO_AD_SLUG", "auto-ad");
define("HOW_IT_WORKS_SLUG", "how-it-works");
define("LOCATION_SLUG", "locations");
define("SERVICES_SLUG", "services");
define("TRY_IT_OUT_DEMO_SLUG", "try-it-out");
define("REQUEST_DEMO_SLUG", "request-a-demo");

//Page IDs
define("FAQ_PAGE_ID", "9");
define("PARTNER_ENQUIRY_TYPE_ID", "12");
define("REQUEST_DEMO_ENQUIRY_TYPE_ID", "13");

// Web service Error Constants
define("ERROR_GENERAL", 100);
define("ERROR_WEB_SERVICE_LOG_SAVE", 101);
define("ERROR_AD_LOCATION_LOGS_SAVE", 102);
define("ERROR_BAD_REQUEST", 400);
define("ERROR_NON_AUTHORISED", 401);
define("ERROR_INSUFFICIENT_DATA", 206);
define("ERROR_SERVICE_NOT_FOUND", 302);
define("ERROR_SAVE", 102);

define("SUMMARY_ACTIVE_CLASS", "summary-active");
define("SUMMARY_INACTIVE_CLASS", "summary-in-active");

define("INDUSTRY_TYPE", "Industry Types");

define("CAMPAIGN_FIXED_DATE_RANGE", 14);
define("CAMPAIGN_TYPE", "Campaign Types");
define("CAMPAIGN_STATUS_TYPE", "Campaign Status");
define("CAMPAIGN_DURATION_TYPE", "Campaign Duration Types");

define("CAMPAIGN_AUTOMOBILE_MASS_ID", 14);
define("CAMPAIGN_DURATION_FIXED_ID", 17);
define("CAMPAIGN_CREATED_ID", 20);
define("CAMPAIGN_SUBMITTED_ID", 18);
define("CAMPAIGN_APRROVED_ID", 19);
define("CAMPAIGN_ACTIVE_ID", 22);
define("CAMPAIGN_COMPLETED_ID", 23);
define("CAMPAIGN_REJECTED_ID", 21);
define("CAMPAIGN_STOPPED_ID", 25);
define("CAMPAIGN_TERMINATE_ID", 28);
define("CAMPAIGN_VERSION_LIMIT", 4);

define("DISCOUNT_FOR_TYPE_FIRST_TIME_CUSTOMER", 8);
define("DISCOUNT_FOR_TYPE_REPEATED_CUSTOMER", 9);
define("DISCOUNT_TYPE_AMOUNT_ID", 10);
define("DISCOUNT_TYPE_PERCENTAGE_ID", 11);

define("IMPORT_TYPE", "Log Types");
define("IMPORT_CAR_TYPE_ID", 2);
define("IMPORT_Automobile_TYPE_ID", 29);

//google map
define("GOOGLE_API_KEY", "AIzaSyCBp5XCcpfhSJ-Og2b7CuGxDptV6D296-0");
define("MAP_HOME_LAT", "51.507354");
define("MAP_HOME_LNG", "-0.127715");

define("INDIA_HOME_LAT", 25.284437746983055);
define("INDIA_HOME_LNG", 78.16085815429688);

//Transaction Account 
define("PAYPAL_SELLER_ID", "hardeepvicky1-facilitator@gmail.com");
define("PAYPAL_URL", "https://www.sandbox.paypal.com/cgi-bin/webscr");
define("PAYPAL_API_USERNAME", "hardeepvicky1-facilitator_api1.gmail.com");
define("PAYPAL_API_PASSWORD", "1403581592");
define("PAYPAL_API_SIGNATURE", "AFcWxV21C7fd0v3bYYYRCpSSRl31ABnCq2F-g9uY-eslUJ2RmiBlHOKl");
define("PAYPAL_ENVIORMENT", "sandbox");

//others
define("CAR_IMPORT_SAMPLE_FILE", "/files/import/cars/sample.csv");
define("CAR_IMPORT_PATH", "files/import/cars");
define("AUTOMOBILE_IMPORT_SAMPLE_FILE", "/files/import/automobiles/sample.csv");
define("AUTOMOBILE_IMPORT_PATH", "files/import/automobiles");

//reports
define("PDF_SAVE_PATH", "files/temp");

// static array
class StaticArray 
{ 
   public static $WebServiceType = array( 
       "0" => "PingServer",
       "1" => "GetAdToDownload",
       "2" => "ConfirmAdDownload",
       "3" => "GetAdToDelete",
       "4" => "ConfirmAdDelete",
       "5" => "GetAdToPlay",
       "6" => "ConfirmAdPlay",
       "7" => "ActivateLocation",
       "8" => "Authentication", 
       "9" => "IsConfigChange",
       "10" => "GetConfig",
       "11" => "SendLocationAlert",
   );   
   
   /**
    * Fail = -1 , will be remain in named params, because 0 will exclude be cakephp
    * @var type 
    */
   public static $StatusType = array(
       1 => "Success", "-1" => "Fail"
   );
   
   public static $AdType = array(
       1 => "LOCAL", 2 => "WEB"
   );
   
   public static $AdType_details = array(
       1 => "mp4", 2 => "Web Url"
   );
   
   public static $Campaign_duration_types = array(
       "Fixed" => 17, "Ongoing" => 16
   );   
   
   public static $campaign_trial_types = array(
        0 => "Non-Trail", 1 => "Trial"
   );
   
   
   public static $YesNo = array(
       1 => "Yes", 0 => "No"
   );
   
   public static $campaign_days = array(
       "mon" => "Monday",
       "tue" => "Tuesday",
       "wed" => "Wednesday",       
       "thu" => "Thursday",
       "fri" => "Friday",
       "sat" => "Saturday",
       "sun" => "Sunday",       
   );   
   
   public static $transaction_status_types = array(
       1 => "Credit",       
       2 => "Debit",       
       3 => "Invoice",
       4 => "Discount"
   );
   
   public static $transaction_status_credit_transaction = 1;
   public static $transaction_status_debit_transaction = 2;   
   public static $transaction_status_invoice_transaction = 3;
   public static $transaction_status_discount_transaction = 4;
   
   public static $invoice_status_type = array(
       0 => "Un-Paid",
       1 => "Paid",
       2 => "Cancel"
   );
   
   public static $invoice_status_unpaid = 0;
   public static $invoice_status_paid = 1;
   public static $invoice_status_cancel = 2;
   
   public static $gateway = array(
       1 => "PayPal"
   );
   
   public static $gateway_papal = 1;
   
   public static $payment_mode = array(
       1 => "Cash",
       2 => "Cheque/Draft",
       3 => "Online"
   );
   
   public static $payment_mode_cash = 1;
   public static $payment_mode_cheque = 2;
   public static $payment_mode_online = 3;
   
   public static $advertisement_segments = array(
       "is_advertising_agency" => "Advertising agency",
       "is_automobile_dealers" => "Automobile dealers",
       "is_drive_through_restaurants" => "Drive through restaurants",
       "is_petrol_stations" => "Petrol stations",
       "is_super_markets" => "Super markets",
       "is_automobile_service_stations" => "Automobile service stations",
       "is_electric_car_charging_points" => "Electric car charging points",
       "is_shopping_mall" => "Shopping mall / Airport car parking",
       "is_general_business" => "General business looking for advertising options",       
   );
   
   public static $notification_type_postive = 1;
   public static $notification_type_negtive = 2;
   public static $notification_type_alert = 3;
   public static $notification_type_alert_postive = 4;
   public static $notification_type_alert_negtive = 5;
   public static $notification_type_warning = 6;
   public static $notification_type_error = 7;
   
   public static $notification_types = array(
       "1" => "Postive",
       "2" => "Negtive",
       "3" => "Alert",
       "4" => "Alert Postive",
       "5" => "Alert Negtive",
       "6" => "Warning",
       "7" => "Error"
   );      
   
   public static $automobile_type = array(
       1 => "Manufature",
       2 => "Model",
       3 => "Variant",
   );
   
   public static $months = array(
       1 => "January",
       2 => "February",
       3 => "March",
       4 => "April",
       5 => "May",
       6 => "June",
       7 => "July",
       8 => "August",
       9 => "September",
       10 => "October",
       11 => "November",
       12 => "December",
   );
}
