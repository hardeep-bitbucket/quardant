<?php
/**
 * Location Email template - Activation
 * 
 * @created    01/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

?>
Hello <b> <?php echo $record['User']['name'] . " " . $record['User']['subname']; ?> </b>,<br/><br/>
    
Extend You Reach! We have added a new location to our portfolio. As a valued customer, we are giving you priority information on this location before we advertise it to the general public. <br/><br/>
You can now add the following location to your existing campaign or create a new campaign:<br/><br/>

Location name : <b><?php echo $record['Location']['name']; ?></b><br/>
Location address : <b><?php echo $record['Location']['address']; ?></b>

<br/><br/>
<?php echo $content['footer']; ?>