<?php

App::uses('CommanController', 'Controller');
App::import('Vendor', 'phpexcel/PHPExcel');

class AppController extends CommanController {

    //Includes application-wide components
    public $components = array('Session', 'Acl', 'Auth', 'Redirect', 'Email', 'Cookie', 'Excel');
    //Changes the view extension name from .ctp to .php
    public $ext = '.php';
    public $helpers = array('Html', 'Form', 'Session', 'Fck', 'TSHtml');
    //Sets default pagination for all controllers
    public $paginate = array(
        'limit' => 10,
        'order' => array(
            'id' => 'DESC'
        )
    );

    public function beforeFilter() 
    {   
        $this->layout = "admin_inner";
        if ($this->request->is('ajax'))
        {
            $this->layout = 'ajax';
        }        
        
        $auth_user = $this->user = $this->Auth->user();   
        
        //updateing wallet amount if advertiser already login
        if (isset($this->user["group_id"]) && $this->user["group_id"] == ADVERTISER_GROUP_ID)
        {
            if ($this->modelClass != "User")
            {
                $this->loadModel("User");
            }
            
            $record = $this->User->find("first", array(
                "fields" => array("wallet_amount"),
                "conditions" => array("id" => $this->user["id"]),
                "recursive" => -1
            )) ; 
                    
            $this->Session->write('Auth.User.wallet_amount', $record["User"]["wallet_amount"]);
            
            $auth_user = $this->user = $this->Auth->user();   
        }      
        
        //debug($auth_user); exit;
        
        
        if (isset($auth_user["timezone"]) && $auth_user["timezone"])
        {
            DateUtility::setClientTimeZone($auth_user['timezone']);            
        }
        
        //Configure AuthComponent
        $this->Auth->authorize = 'Actions';
        $this->Auth->actionPath = 'Controllers/';
        $this->Auth->authError = 'You are not allowed to visit that url.';
        $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
        $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');        
        $this->Auth->loginRedirect = array('controller' => 'advertisers', 'action' => 'home', "admin" => true);
        $this->Auth->unauthorizedRedirect = array("controller" => "errors", "action" => "methodNotAllowed");        
       
        $this->Auth->allow(array(
            'getModule', 'getPages', 'getUpdatesForFooter',
            "ajaxSuccessMessage", "ajaxFailureMessage", "ajaxCaptcha"
        ));
        //$this->Auth->allow();
       
        $model = $this->modelClass;
        $controller = $this->params["controller"];
        $action = $this->params["action"];
        
        
        $adminSetting = $this->_getAdminSetting();
        $adminSetting = $adminSetting['AdminSetting'];
        $this->adminSetting = $adminSetting; 
        
        $this->{$this->modelClass}->recursive = -1;
        
        $this->set(compact("auth_user", 'model', 'controller', 'action', 'adminSetting'));
    }

    /**
     *  Common add record action
     */
    public function add() {  
       
        if ($this->request->is('post')) {
             
            $this->{$this->modelClass}->create();
            if ($this->{$this->modelClass}->save($this->request->data)) {
                $this->Session->setFlash('Saved successfully.', 'flash_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Unable to add.', 'flash_failure');
            }
        }
    }

    /**
     *  Common edit record action
     */
    public function edit($id, $redirect = array('action' => 'index') ) 
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid Request'));
        }
        
        $saved = false;

        $record = $this->{$this->modelClass}->findById($id);
        if (!$record) {
            throw new NotFoundException(__('Invalid Request'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->{$this->modelClass}->id = $id;            
            if ($this->{$this->modelClass}->save($this->request->data)) {                   
                $saved = true;
                $this->Session->setFlash('Updated successfully.', 'flash_success');
                if ($redirect)
                {
                    $this->redirect($redirect);
                }                
            } else {          
                $saved = false;
                $this->Session->setFlash('Unable to update.', 'flash_failure');
            }
        }
        if (!$this->request->data) {
            $this->request->data = $record;
        }
        return $saved;
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id) 
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid Request'));
        } else {
            $this->{$this->modelClass}->id = $id;
        }
        
        $flag = $this->{$this->modelClass}->preventDeleteAndInactive($id);
        if ($flag) 
        {
            if ($this->{$this->modelClass}->hasField("is_deleted"))
            {
                $this->{$this->modelClass}->saveField("is_deleted", 1, false);
            }
            else
            {
                $this->{$this->modelClass}->delete();
            }
            
            $this->Session->setFlash('The record deleted Successfully!', "flash_success");
        } else {            
            $this->Session->setFlash('The record cannot be deleted!', "flash_failure");            
        }
        $this->redirect($this->referer());
    }

    /*
     * @Search Conditions
     */
    public function getSearchConditions($inputs) {
        $conditions = array();
        $searchArray = array();

        //Looping the input data
        foreach ($inputs as $input) {
            //Setting value in local variables
            $model = $input['model'];
            $field = $input['field'];
            $type = $input['type'];
            $view_field = $input['view_field'];
            //Checking type conditions			
            if (isset($this->params['named'][$view_field]) && !empty($this->params['named'][$view_field])) {

                if ($type == 'string') {
                    $conditions[$model . "." . $field . " LIKE"] = "%" . $this->params['named'][$field] . "%";
                } else if ($type == 'integer') {
                    $conditions[$model . "." . $field] = $this->params['named'][$field];
                } else if ($type == 'date') {
                    $date = date("Y-m-d", strtotime($this->params['named'][$view_field]));
                    $conditions[$model . "." . $field] = $date;
                } else if ($type == 'from_date') {
                    $from = date("Y-m-d", strtotime($this->params['named'][$view_field]));
                    $conditions[$model . "." . $field . " >="] = $from;
                } else if ($type == 'to_date') {
                    $to = date("Y-m-d", strtotime($this->params['named'][$view_field]));
                    $conditions[$model . "." . $field . " <="] = $to;
                }
                $searchArray[$model . $view_field] = $this->params['named'][$view_field];
            } else {
                $searchArray[$model . $view_field] = "";
            }
        }

        $this->set($searchArray);
        return $conditions;
    }

    /**
     * Toggles the status of is_active field
     * 
     * @param Integer $id
     * @param Integer $status
     */
    public function admin_toggleStatus($id, $status) {
        $flag = $this->{$this->modelClass}->preventDeleteAndInactive($id);
        $result = false;
        if (is_numeric($id) && $flag) {
            $this->{$this->modelClass}->id = $id;
            $result = $this->{$this->modelClass}->saveField('is_active', !(int) $status, FALSE);
        }
        else {
            $this->Session->setFlash('The record cannot be inactive!');
        }
        
        echo (int) $result; exit;
    }

    /**
     * @Frontend Actions
     * @Get Module
     */
    protected function getModule() 
    {
        $this->loadModel("Module");
        $header_module = $this->Module->find("first", array("conditions" => array("Module.id" => HEADER_MODULE_ID, "Module.is_active" => 1)));
        $footer_module = $this->Module->find("first", array("conditions" => array("Module.id" => FOOTER_MODULE_ID, "Module.is_active" => 1)));
        $social_icons_module = $this->Module->find("first", array("conditions" => array("Module.id" => SOCIAL_ICONS_MODULE_ID, "Module.is_active" => 1)));
        
        $this->set(compact("footer_module", "header_module", "social_icons_module"));
    }

    /**
     * @Frontend Actions
     * @Get Top Pages
     */
    protected function getPages() 
    {
        // Top Links
        $pages = $this->Page->find("all", array(
            "fields" => array("id", "name", "slug"),
            "order" => "Page.display_order ASC",
            "conditions" => array("Page.is_active" => 1, "Page.header_menu" => 1)
        ));
        
        $this->set(compact("pages"));
    }

    /*
     * @breadCrumb
     */

    protected function bread_crumb() {
        $bread_crumb = "";
        if (isset($this->params['slug'])) {
            $slug = $this->params['slug'];
            $data = $this->{$this->modelClass}->findBySlug($slug);
            $bread_crumb = $this->{$this->modelClass}->getPath($data["{$this->modelClass}"]["id"]);

            $active_link = end($bread_crumb);
        }
        $this->set(compact("bread_crumb", "active_link"));
    }

    /*
     * @Frotend Get Meta Info
     */
    protected function getMetaInfo()
    {
        $meta_title = "";
        $meta_keywords = "";
        $meta_description = "";
        $slug = "home";
        $model = "Page";
        
        switch($this->params["controller"])
        {
            case "pages":            
                $slug = $this->params["slug"];
            break;

            case "locations":            
                $slug = LOCATION_SLUG;
            break;
        }
        
        if(!empty($slug))
        {
            $rec = $this->$model->find("first", array("recursive" => -1, "conditions" => array("slug" => $slug)));            
            $meta_title = $rec[$model]["meta_title"];
            $meta_keywords = $rec[$model]["meta_keywords"];
            $meta_description = $rec[$model]["meta_description"];
        }
        
		$this->set(compact('meta_title', 'meta_keywords', 'meta_description'));
    }
    
    /*
     * @get update for Home and Index Actions
     */
    protected function getUpdatesForFooter()
    {       
        // get Update content
        $this->loadModel("Update");
        $latest_updates = $this->Update->find("all", array("limit" => 3, "order" => "date DESC", "conditions" => array("Update.is_active" => 1)));
        $this->set(compact("latest_updates"));
    }
    
    /**
     * get Config from admin setting Model
     * @return array
     */
    protected function _getAdminSetting($fields = array())
    {
        $this->loadModel("AdminSetting");
        return $this->AdminSetting->find("first", array(
            "fields" => $fields,
        ));
    }
    
    /**
     * success message 
     */
    public function ajaxSuccessMessage($message = "")
    {
        $this->set(compact("message"));
        $this->render("/Elements/flash_success", null);
    }
    
     /**
     * success message 
     */
    public function ajaxFailureMessage($text = "Opps! Something went wrong")
    {
        $this->set(compact("messsge"));
        $this->render("/Elements/flash_failure", null);
    }
    
    public function ajaxCaptcha()
    {
        $random_alpha = md5(rand());
        $captcha_code = substr($random_alpha, 0, 6);
        
        $this->Session->write("captcha_code", $captcha_code);
        
        $target_layer = imagecreatetruecolor(130,30);
        $captcha_background = imagecolorallocate($target_layer, 96, 154, 53);
        imagefill($target_layer,0,0,$captcha_background);
        $captcha_text_color = imagecolorallocate($target_layer, 255, 255, 255);
        imagestring($target_layer, 30, 30, 8, $captcha_code, $captcha_text_color);
        header("Content-type: image/jpeg");
        imagejpeg($target_layer);
        exit;
    }
    
    protected function createPDF($view, $path, $filePrefix)
    {
		$this->autoRender = false; //Sets rendering false before getting output of view		
		$this->layout = null; //No layout is required
		
		$view_output = $this->render($view); //Gets view output to a variable
		//Includes the library file and configures the default settings
		require_once(APP . 'Vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php'); 
		spl_autoload_register('DOMPDF_autoload');
		$dompdf = new DOMPDF();
		$dompdf->set_paper = 'A4';
		
		$dompdf->load_html(utf8_decode($view_output), Configure::read('App.encoding')); //Loads html of view variable into memory
		$dompdf->render();

		$filename =  $filePrefix . "_" . date('d_m_Y_H_i') . ".pdf";
        
        $dompdf->stream($filename);
	}
}
