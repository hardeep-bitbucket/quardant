<?php
/**
 * Wallet
 * 
 * 
 * @created    28/04/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep 
 */
?>

<div class="row">   
    <div class="col-md-12">
        <div class="pull-left">
            <h3 class="page-title">
              <?php echo ucwords($model); ?> <small>Manager</small>
            </h3>
        </div>
        <div class="pull-right">
            
        </div>
    </div>
</div>
<?php
    //echo $this->element("admin/breadcrumb"); 
    echo $this->Session->flash();
?>

<div class="row margin-top-10"> 
    <div class="col-md-12">
         <div class="portlet light">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold uppercase">Campaigns</span>
                </div>
                <div class="pull-right">
                    <div class="caption caption-md">
                        <?php
                           echo $this->Form->input("country", array(
                                "type" => "select", "div" => false, "label" => false, "escape" => false,
                                "options" => $country_list, "empty" => "All",
                                "class" => "form-control"
                            ));
                        ?>
                    </div>
                </div>
            </div>
            <div class="portlet-body" id="campaign-block">
                
            </div>
         </div>
    </div>
</div>