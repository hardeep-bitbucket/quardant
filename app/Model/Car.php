<?php

/**
 * Page Model
 * 
 * 
 * @created    19/12/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Sonia
 */
class Car extends AppModel {

    /**
     * Behaviours
     */
    public $actsAs = array('DateFormat' => array('reg_date'));

    /**
     * Associations
     */
    var $belongsTo = array(
        'Manufacture' => array(
            'className' => 'Automobile',
            'foreignKey' => 'automobile_manufacture_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Model' => array(
            'className' => 'Automobile',
            'foreignKey' => 'automobile_model_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Variant' => array(
            'className' => 'Automobile',
            'foreignKey' => 'automobile_variant_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'State' => array(
            'className' => 'Place',
            'foreignKey' => 'state_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Country' => array(
            'className' => 'Place',
            'foreignKey' => 'country_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array();

    /**
     * Valiadtions
     */
    public $validate = array(
        'automobile_manufacture_id' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'automobile_model_id' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'automobile_variant_id' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'color' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => COLOR_REQUIRED_MESSAGE),
            'maxLength' => array(
                'rule' => array('maxLength', 255),
                'allowEmpty' => true,
                'message' => "Maximum charaters limit is 255",
            ),
        ),
        'reg_no' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REG_NO_REQUIRED_MESSAGE),
            "isUnique" => array("rule" => "isUnique", 'allowEmpty' => true, "message" => UNIQUE_FIELD),
            'maxLength' => array(
                'rule' => array('maxLength', 255),
                'allowEmpty' => true,
                'message' => "Maximum charaters limit is 255",
            ),
        ),
        'reg_date' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REG_DATE_REQUIRED_MESSAGE)
        ),
        'chase_no' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => CHASE_NO_REQUIRED_MESSAGE)
        ),
        'owner_name' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => OWNER_NAME_REQUIRED_MESSAGE),
            'maxLength' => array(
                'rule' => array('maxLength', 100),
                'allowEmpty' => true,
                'message' => "Maximum charaters limit is 100",
            ),
        ),
        'country_id' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'state_id' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD)
        ),
        'owner_address' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => OWNER_ADDRESS_REQUIRED_MESSAGE),
            'maxLength' => array(
                'rule' => array('maxLength', 255),
                'allowEmpty' => true,
                'message' => "Maximum charaters limit is 255",
            ),
        ),
        'owner_zip' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => OWNER_ZIP_REQUIRED_MESSAGE),
            'maxLength' => array(
                'rule' => array('maxLength', 10),
                'allowEmpty' => true,
                'message' => "Maximum charaters limit is 10",
            ),
            'numeric' => array(
                'rule' => array('numeric'),
                'allowEmpty' => true,
                'message' => NUMERIC_FIELD,
            ),
        ),
        'owner_email' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD),
            'maxLength' => array(
                'rule' => array('maxLength', 255),
                'required' => false,
                'message' => "Maximum charaters limit is 255",
            ),
        ),
        'owner_phone' => array(
            'notEmptyRule' => array('rule' => 'notEmpty', 'message' => REQUIRED_FIELD),
            'numeric' => array(
                'rule' => array('numeric'),
                'allowEmpty' => true,
                'message' => NUMERIC_FIELD,
            ),
            'maxLength' => array(
                'rule' => array('maxLength', 12),
                'allowEmpty' => true,
                'message' => "Maximum charaters limit is 12",
            ),
        ),
    );

    public function getTopCar($conditions = array()) 
    {
        return $this->find("first", array(
            "conditions" => $conditions,
            "order" => array(
                "total_count" => "DESC"
            ),
            "limit" => -1
        ));
    }

}

?>