<?php
/**
 * Invoice Email template - submitted
 * 
 * @created    01/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

?>

Advertiser : <b> <?php echo $records['User']['name'] . " " . $records['User']['subname']; ?> </b>,<br><br>
    
Invoice of campaign <b> <?php echo $records['Campaign']['name'] ?> </b> is Canceled.
<br>
<?php
    if ($records["Invoice"]["start_date"] && $records["Invoice"]["end_date"])
    {
        echo "Start Date : " . $records["Invoice"]["start_date"];
        echo "<br><br>";
        echo "End Date : " . $records["Invoice"]["end_date"];
    }
?>
<br><br>
Details
<br>
<table border="1" cellpadding="2">
    <tr>
        <td>Amount</td>
        <td><?php echo CURRENCY_SYMBOL_CODE; ?><?php echo $records["Invoice"]["amount"] ?></td>
    </tr>    
    <?php if ($records["Invoice"]["discount"]): ?>
    <tr>
        <td>Discount</td>
        <td><?php echo CURRENCY_SYMBOL_CODE; ?><?php echo $records["Invoice"]["discount"]; ?></td>
    </tr>       
    <?php endif; ?>
    <?php if ($records["Invoice"]["net_amount"]): ?>
    <tr>
        <td>Net Amount</td>
        <td><?php echo CURRENCY_SYMBOL_CODE; ?><?php echo $records["Invoice"]["net_amount"] ?></td>
    </tr>     
    <?php endif; ?>    
</table>

<br/><br/>
<?php echo $content['footer']; ?>