<?php
/**
 * Pages Controller
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */

?>
<style>
.you-tube ul li{
    margin: 10px;
}

</style>

<div class="you-tube">
    <h3>To Get Embed URL, Please follow the steps</h3>
    <ul style="list-style: decimal;">
        <li>
            Open any video at youtube.com
        </li>

        <li>
            <span>click on share</span>
            <br><br>
            <img src="/img/you-tube-help1.png">        
        </li>

        <li>
            <span>Copy embed URL</span>
            <br><br>
            <img src="/img/you-tube-help2.png">        
        </li>
    </ul>
</div>