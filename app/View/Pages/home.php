<?php
/**
 * Frontend Home Area
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */
?>
<!-- Start Home Section-->
<section id="home" class="demo-1">        
    <!--     Codrops top bar-->
    <?php echo $this->element("frontend/graphics"); ?>
    <!--     /slider-wrapper-->
</section>
<!-- End Home Section-->

<!-- Start About section-->
<?php if ($aboutus_content) : ?>
    <section id="about-us" class="sep-bottom-2x" style="padding-top:8em;">
        <?php echo $aboutus_content["Page"]["contents"]; ?>
    </section>
<?php endif; ?>
<!-- End About section-->

<!-- Start Auto AD section-->
<section id="auto-ad" class="sep-top-2x sep-bottom-sm">
    <?php echo $autoad_content["Page"]["contents"]; ?>
</section>
<!-- End Parallax section-->

<!-- Start How it works section-->
<section id="how-it-works" class="sep-top-3x sep-bottom-sm">
    <div style="margin-top: -15px;">
        <?php echo $howit_works_content["Page"]["contents"]; ?>
    </div>
</section>
<!-- End Portfolio section-->

<!-- Start Service section-->
<section id="services" class="sep-top-3x sep-bottom-sm">
    <?php echo $services_content["Page"]["contents"]; ?>
</section>
<!-- End Service section-->

<!-- Start Try it out section-->
<section id="try-it-out" class="sep-top-3x sep-bottom-sm">
    <?php echo $tryitout_content["Page"]["contents"]; ?>
</section>
<!-- End Service section-->

<!-- Start Service section-->
<section id="request-a-demo" class="sep-top-3x sep-bottom-sm" style="display : none;">
    <?php
    //$request_content = $request_demo_content["Page"]["contents"];
   // $request_content = str_replace("{{REQUEST_A_DEMO}}", $this->element("frontend/inner/request-demo-form"), $request_content);
    //echo $request_content;
    ?>
</section>
<!-- End Service section-->

<!-- Start Contact section-->
<section id="contact-us" class="sep-top-3x sep-bottom-2x">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="section-title text-center">
                    <div class="custom-icon-content-request-demo">
                        <img src="img/frontend/icon-contact-us.png" alt="" style="height : 9em;">					
                    </div>
                    <h2 class="upper">Contact Us</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 sep-top-lg">
                <!--Start icon box-->
                <div class="icon-box icon-xs sep-bottom-xs icon-gradient">
                    <div data-wow-delay=".5s" class="col-md-2 wow flipInX">                        
                        <div class="icon-cell-block" style="height: 65px;  width: auto;">
                            <img src="img/frontend/icon-address.png" alt="">
                        </div>
                    </div>
                    <div class="icon-box-content">
                        <h6 class="upper info-title">Address</h6>
                        <p>4-5 Bonhill St, Shoreditch, London EC2A 4BX</p>
                    </div>
                </div>
                <!--End icon box-->
                <!--Start icon box-->
                <div class="icon-box icon-xs sep-bottom-xs icon-gradient">
                    <div data-wow-delay=".5s" class="col-md-2 wow flipInX">                        
                        <div class="icon-cell-block" style="height: 65px;  width: auto;">
                            <img src="img/frontend/icon-contact-us.png" alt="">
                        </div>
                    </div>
                    <div class="icon-box-content">
                        <h6 class="upper info-title">Phone</h6>
                        <p>(+39) 123-456-789</p>
                    </div>
                </div>
                <!--End icon box-->
                <!--Start icon box-->
                <div class="icon-box icon-xs sep-bottom-xs icon-gradient">
                    <div data-wow-delay=".5s" class="col-md-2 wow flipInX">                        
                        <div class="icon-cell-block" style="height: 65px; width: auto;">
                            <img src="img/frontend/icon-email.png" alt="">
                        </div>
                    </div>
                    <div class="icon-box-content">
                        <h6 class="upper info-title">Email</h6>
                        <p>
                            <a href ="mailto:info@anprmotion.com">info@anprmotion.com</a> / <a href ="mailto:support@anprmotion.com">support@anprmotion.com</a></p>
                    </div>
                </div>
                <!--End icon box-->
                
            </div>
            <div class="col-md-6 sep-top-lg">                            
                <div class="contact-form">   
                    <div style="display:none" class="successMessage alert alert-success text-center">
                        <p><i class="fa fa-check-circle fa-2x"></i></p>
                        <p>Thanks for sending your message! We'll get back to you shortly.</p>
                    </div>
                    <div  style="display:none" class="failureMessage alert alert-danger text-center">
                        <p><i class="fa fa-times-circle fa-2x"></i></p>
                        <p>There was a problem sending your message. Please, try again.</p>
                    </div>
                    <div style="display:none" class="incompleteMessage alert alert-warning text-center">
                        <p><i class="fa fa-exclamation-triangle fa-2x"></i></p>
                        <p>Please complete all the fields in the form before sending.</p>
                    </div>  
                    <?php 
                        echo $this->Form->create($model, array("action" => "contact_form", 'type' => 'POST', 'class' => 'validate-form'));
                    ?>
                        
                        <div class="form-group sep-top-xs">
                            <label for="contactFormName" class="upper">Your Name <i class="required-lbl">*</i></label>
                            <input id="contactFormName" type="text" placeholder="Enter name" name="name" aria-required="true" required="true"  class="form-control input-lg required">
                        </div>
                        <div class="form-group sep-top-xs">
                            <label for="contactFormPhone" class="upper">Your Phone <i class="required-lbl">*</i></label>
                            <input id="contactFormPhone" type="text" placeholder="Enter phone" name="phone" aria-required="true" required="true" class="form-control input-lg required">
                        </div>
                        <div class="form-group sep-top-xs">
                            <label for="contactFormEmail" class="upper">Your Email <i class="required-lbl">*</i></label>
                            <input id="contactFormEmail" type="email" placeholder="Enter email" name="email" aria-required="true" required="true" class="form-control input-lg required">
                        </div>
                        
                        <div class="form-group sep-top-xs">
                            <label for="contactForm" class="upper">Select your Industry<i class="required-lbl">*</i></label>
                            <div style="width : 100%;">
                                <?php
                                    echo $this->Form->input("advertisement_segments", array(
                                        'multiple' => 'multiple', 'type' => 'select', "name" => "advertisement_segments",
                                        "options" => StaticArray::$advertisement_segments,
                                        "class" => "ms-multi-select form-control input-lg",
                                        "label" => false, "div" => false, "escape" => false,
                                        "aria-required" => true, "required" => true
                                    ));
                                ?>
                            </div>
                        </div>
                        <div class="form-group sep-top-xs">
                            <label for="contactFormComment" class="upper">Your comment <i class="required-lbl">*</i></label>
                            <textarea id="contactFormComment" placeholder="Enter comment" rows="9" name="comment" class="form-control input-lg required"></textarea>
                        </div>
                        <div class="form-group sep-top-xs">
                            <button type="submit" data-wow-delay=".5s" class="btn backcolor-green btn-lg wow bounceInRight"><i class="fa fa-paper-plane"></i> Send message</button>
                        </div>
                        <!--input#subject.form-control.input-lg.required(type='text', placeholder='Subject of your message', name='subject')
                        -->
                    </form>
                    <div class="hidden"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Contact section-->
<!-- Start map section-->
<section>
    <div id="map-canvas" style="height:500px">
        <script>
            var
                    lat = '<?php echo MAP_HOME_LAT; ?>',
                    lon ='<?php echo MAP_HOME_LNG; ?>',
                    infoText = [
                '<div style="white-space:nowrap">',
                '<h5>ANPRMotion</h5>',
                '4-5 Bonhill St,<br>',
                'Shoreditch, London EC2A 4BX<br>',
                '(+39) 123-456-789 /<br>',
                '(+39) 123-456-789<br>',
                'info@anprmotion.com /<br>',
                'support@anprmotion.com',
                '</div>'
            ],
                    mapOptions = {
                scrollwheel: false,
                markers: [
                    {latitude: lat, longitude: lon, html: infoText.join('')}
                ],
                icon: {
                    image: '/img/frontend/themes/meadow/marker.png',
                    iconsize: [72, 65],
                    iconanchor: [12, 65],
                },
                latitude: lat,
                longitude: lon,
                zoom: 11
            };
        </script>
    </div>
</section>
<!-- Start call to action section-->
<section class="call-to-action bg-primary sep-top-md sep-bottom-md">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h5 class="action-title upper light">We will be glad to hear from you</h5>
            </div>
            <div class="col-md-3 text-right"><a href="#contact-us" class="btn btn-light btn-bordered btn-lg">Contact Us</a></div>
        </div>
    </div>
    
</section>
<!--Model Videos popup-->
<?php echo $this->element('frontend/model_popup_videos'); ?>

<!-- End Model Videos popup -->


<!-- End call to action section-->