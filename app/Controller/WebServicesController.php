<?php
/**
 * WebService Controller
 * 
 * 
 * @created    04/02/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

/**
 * Exception Codes 
 * 100 = general error
 * 101 = web_service_log save error
 * 
 * 400 = bad request
 * 401 = Non-Authoritative Information
 * 206 = insufficient data in request
 * 302 = not found service
 */

App::uses('CommanController', 'Controller');
App::uses('Xml', 'Utility');

class WebServicesController extends CommanController 
{
    
    public $ext = '.php';
    public $layout = '';
    
    public $xml;
    public $xml_data;
    public $web_service_log_id; 
    public $service_type;
    public $service_id;
    public $response_data;
    public $error_code;
    public $uses = array('WebServiceLog'); 
    public $location_id, $location_record;
    
    /**
     * function index
     * all web Service will hit to this function first
     */
    public function index()
    {
        try
        {
            $last_time = DateUtility::getCurrentDateTimeObj();
            
            if (isset($this->request->data['WebService']["data"]))
            {
                $this->xml = $this->request->data['WebService']["data"];
            }
            else
            {
                $this->xml = $this->request->data;                
            }
            
            /**
             * converting xml string to object then array
             */
            $this->xml_data = convert_object_to_array(simplexml_load_string($this->xml));            
            
            /**
             * validate general parameters which are required by all type services
             */
            $this->_validateRequest();
                
            if ($this->service_type != StaticArray::$WebServiceType[0])
            {
                $this->_insertWebServiceLog();

                /**
                 * all Web Service will proceed to this function
                 */
                $this->_webService();             
            }
            else
            {
                //ping Server
                $this->response_data['status'] = 1;
            }
        }
        catch (Exception $ex)
        {   
            $this->error_code = $ex->getCode();
            $msg = "Error (" . $ex->getCode() . ") : " .$ex->getMessage() . " : Error found at line no : " . $ex->getLine() ;            
            
            $this->response_data['status'] = 0;
            $this->response_data['msg'] = $msg;  
            
            $notification_data[] = array(
                "type" => StaticArray::$notification_type_warning,
                "content" => "Error comes while processing web service # id :" . $this->web_service_log_id,
                "group_id" => ADMIN_GROUP_ID
            );
        }
        
        $xmlObj = Xml::fromArray(array("root" => $this->response_data));
        
        $this->response_data['xml'] = $xmlObj->asXML();
        
        if ($this->service_type != StaticArray::$WebServiceType[0])
        {
            if (! in_array($this->error_code, array(ERROR_WEB_SERVICE_LOG_SAVE, ERROR_SERVICE_NOT_FOUND, ERROR_INSUFFICIENT_DATA, ERROR_NON_AUTHORISED )))
            {
                $this->_updateWebServiceLog();     
            }
        }
        
        $current_time = DateUtility::getCurrentDateTimeObj();
        
        if (DateUtility::compareDateObjs($last_time, $current_time, DateUtility::$SECONDS) > 1)
        {
            $notification_data[] = array(
                "type" => StaticArray::$notification_type_warning,
                "content" => "Web services taking more than 1 second to response # id :" . $this->web_service_log_id,
                "group_id" => ADMIN_GROUP_ID
            );
        }
        
        if (isset($notification_data))
        {
            $this->_saveNotification($notification_data);
        }
        
        echo $this->response_data['xml']; exit;        
    }
    
    public function test()
    {
        $this->layout = "ajax";
    }
    
    
    private function _validateRequest() 
    {
        if (!isset($this->xml_data["service"]))
        {
            throw new Exception("Missing Service in request", ERROR_INSUFFICIENT_DATA);
        }
        
        $this->service_type = $this->xml_data["service"];
        
        if (!in_array($this->service_type, StaticArray::$WebServiceType))
        {
            throw new Exception("Wrong Service in request", ERROR_SERVICE_NOT_FOUND);
        }     
    }
    
    private function _insertWebServiceLog()
    {
         $arr = array_flip(StaticArray::$WebServiceType);
              
         $this->service_id = $arr[$this->service_type];
         
         $data = array( 
                $this->modelClass => array(                       
                       "service_id" => $this->service_id,
                       "request" => htmlspecialchars($this->xml)
            ));
         
         if ($this->{$this->modelClass}->save($data))
         {
             $this->web_service_log_id = $this->{$this->modelClass}->id;
         }
         else
         {
             throw new Exception("Failed to Save Web Service Logs", ERROR_WEB_SERVICE_LOG_SAVE);
         }
    }
    
    private function _updateWebServiceLog()
    {
        $data = array( 
            $this->modelClass => array(    
                "response" => htmlspecialchars($this->response_data['xml']),
                "status" => $this->response_data['status'],
                "location_id" => $this->location_id
        ));
        
        $this->{$this->modelClass}->id = $this->web_service_log_id;
        
        if (!$this->{$this->modelClass}->save($data))
        {
             throw new Exception("Failed to Save Web Service Logs", ERROR_WEB_SERVICE_LOG_SAVE);
        }        
    }
    
    /**
     * following function will called always whenever web service hits     
     */
    private function _webService()
    {
        switch ($this->service_id)
        {
            case 1 : //GetAdToDownload                
                $this->_GetAdToDownload();                            
            break;
        
            case 2 : //ConfirmAdDownload
                $this->_ConfirmAdDownload();
            break;    
        
            case 3 : //GetAdToDelete
                $this->_GetAdToDelete();
            break;
        
            case 4 : //ConfirmAdDelete
                $this->_ConfirmAdDelete();
            break;
        
            case 5 : //_GetAdToPlay
                $this->_GetAdToPlay();
            break;
        
            case 6 :
                $this->_ConfirmAdPlay();
            break;
        
            case 7 :
                $this->_ActivateLocation();
            break;    
        
            case 8 :
                $this->_Authentication();
            break;
        
            case 9: 
                $this->_IsConfigChange();
            break;
            
            case 10:
                $this->_GetConfig();
            break;
        
            case 11:
                $this->_SendLocationAlert();
            break;
        }
        
        $this->response_data['status'] = 1;
        $this->response_data['timestamp'] = date('Y-m-d H:i:s');
        $this->response_data['data']['request_id'] = $this->web_service_log_id;
        
        if (empty($this->response_data['msg']))
        {
            $this->response_data['msg'] = "success";
        }
    }
    
    /**     
     * Auth checks 
     * @throws Exception
     */
    private function _AuthRequest($check_user = false)
    {
        if (!isset($this->xml_data["auth"]["location_id"]))
        {
            throw  new Exception("Missing location_id in Auth Information", ERROR_INSUFFICIENT_DATA);
        }
        
        if (!isset($this->xml_data["auth"]["security_code"]))
        {
            throw  new Exception("Missing security_code in Auth Information", ERROR_INSUFFICIENT_DATA);
        }
        
        if ($check_user)
        {
            if (!isset($this->xml_data["auth"]["username"]))
            {
                throw  new Exception("Missing username in Auth Information", ERROR_INSUFFICIENT_DATA);
            }
            
            if (!isset($this->xml_data["auth"]["password"]))
            {
                throw  new Exception("Missing password in Auth Information", ERROR_INSUFFICIENT_DATA);
            }
            
            $conditions['username'] = trim($this->xml_data["auth"]['username']);
            $conditions['password'] = trim($this->xml_data["auth"]['password']);
        }
        
        $conditions['location_id'] = trim($this->xml_data["auth"]['location_id']);
        $conditions['security_code'] = trim($this->xml_data["auth"]['security_code']);
                
        $this->{$this->modelClass}->Location->recursive = -1;
        
        $location_record = $this->{$this->modelClass}->Location->find("first", array(
            "fields" => array(
                "id", "is_activated", "is_active", "ws_no_of_ads",
                "client_ad_download_path", "client_welcome_msg", "is_client_config_change"
            ),
            "conditions" => $conditions
        )); 
        
        //debug($location_record); exit;
        
        if (empty($location_record))
        {
            throw  new Exception("Wrong Auth Information", ERROR_NON_AUTHORISED);
        }
        else
        {
            $this->location_record = $location_record;
            $this->location_id = $location_record['Location']['id'];
            
            if (!$location_record['Location']["is_active"] || !$location_record['Location']["is_activated"])
            {
                throw  new Exception("location may not active", ERROR_GENERAL);
            }            
        }
    }
    
    
    /**
     * Web service to download list of ads
     */
    private function _GetAdToDownload()
    {
        $this->_AuthRequest();
        
        
        $this->loadModel("AdLocation");
        
        $this->AdLocation->contain(array(
            "Campaign" => array(
                "fields" => array("ad_name", "ad_url", "ad_type", "ad_ext")
            )
        ));
        
        $ad_log_records = $this->AdLocation->find("all", array(
                "fields" => array(
                    "id",  "use_default_creative",
                    "ad_name", "ad_url", "ad_type", "ad_ext"
                ),
                "conditions" => array(
                        "is_download" => 1,
                        "is_delete" => 0,
                        "AdLocation.location_id" => $this->location_id
                ),
        ));        

        
        $this->response_data['data'] = array();
        
        if (!empty($ad_log_records))
        {
            foreach ($ad_log_records as $inner_data)
            {   
                if ($inner_data['AdLocation']['use_default_creative'])
                {
                    $type = (int) $inner_data['Campaign']['ad_type'];
                    if ($type == 1)
                    {
                        $path = AD_CREATIVE .  $inner_data['Campaign']['ad_name'];
                    }
                    else
                    {
                        $path = $inner_data['Campaign']["ad_url"];
                    }
                }
                else
                {
                    $type = (int) $inner_data['AdLocation']['ad_type'];
                    if ($type == 1)
                    {
                        $path = AD_CREATIVE .  $inner_data['AdLocation']['ad_name'];
                    }
                    else
                    {
                        $path = $inner_data['AdLocation']["ad_url"];
                    }
                }
                
                $this->response_data['data']['ads'][] = array(
                    "ad_id" => $inner_data['AdLocation']['id'],
                    "campaign_id" => $inner_data['Campaign']['id'],
                    "ad_url" => $path,
                    "type" => StaticArray::$AdType[$type],
                );
                
                $data = array(
                    "AdLocation" => array(
                        'download_web_service_log_id' => $this->web_service_log_id,                        
                    )
                );
                
                $this->AdLocation->id = $inner_data['AdLocation']['id'];
                if (!$this->AdLocation->save($data, false))
                {
                    throw new Exception("failed to save record in ad_location", ERROR_AD_LOCATION_LOGS_SAVE);
                }
            }
        }  
        
        if ( empty($this->response_data['data']))
        {
            $this->response_data['msg'] = "No Ad to download";
        }
    }
    
    private function _GetAdToDelete()
    {
        $this->_AuthRequest();
        
        $this->loadModel("AdLocation");
        
        /**
         * get data from query not find method beacuse here we getting deleted records
         */
        $q = "SELECT id, campaign_id FROM ad_locations AS AdLocation WHERE is_delete = 1 AND AdLocation.location_id=" . $this->location_id;
        $ad_log_records = $this->{$this->modelClass}->query($q);
                
        $this->response_data['data'] = array();
        
        if (!empty($ad_log_records))
        {
            foreach ($ad_log_records as $inner_data)
            {   
                $this->response_data['data']['ads'][] = array(
                    "ad_id" => $inner_data['AdLocation']['id'],
                    "campaign_id" => $inner_data['AdLocation']['campaign_id']                    
                );
                
                $data = array(
                    "AdLocation" => array(
                        'delete_web_service_log_id' => $this->web_service_log_id,
                    )
                );
                
                $this->AdLocation->id = $inner_data['AdLocation']['id'];
                if (!$this->AdLocation->save($data, false))
                {
                    throw new Exception("failed to save record in ad_location", ERROR_AD_LOCATION_LOGS_SAVE);
                }
            }
        }  
        
        if ( empty($this->response_data['data']))
        {
            $this->response_data['msg'] = "No Ad to delete";
        }
    }    
    
    private function _GetAdToPlay()
    {
        if (!isset($this->xml_data["data"]["car_reg_no"]))
        {
            throw  new Exception("Missing car_reg_no", ERROR_INSUFFICIENT_DATA);            
        }  
        
        if (!is_string($this->xml_data["data"]["car_reg_no"]))
        {
            throw  new Exception("Missing car_reg_no", ERROR_INSUFFICIENT_DATA);            
        }  
        
        $this->xml_data["data"]["car_reg_no"] = trim($this->xml_data["data"]["car_reg_no"]);
        
        if (!$this->xml_data["data"]["car_reg_no"])
        {
            throw  new Exception("Missing car_reg_no", ERROR_INSUFFICIENT_DATA);
        }
        
        /**
         * authenticate request
         */
        $this->_AuthRequest();
        
                
        $this->loadModel("Car");
        $car_record = $this->Car->find("first", array(
            "conditions" => array("reg_no" => $this->xml_data["data"]["car_reg_no"]),
            "fields" => array("id", "automobile_manufacture_id", "automobile_model_id", "automobile_variant_id"),
            "recursive" => -1
        ));
        
        if (!$car_record)
        {
            $this->response_data['msg'] = "No Ad to Play";
            return;
        }
        
        $car_record = $car_record['Car'];
        
        $this->response_data['data']['car_id'] = $car_record['id'];
        
        $this->{$this->modelClass}->Location->AdLocation->contain(array(
            "Campaign" => array(
                "fields" => array("id", "campaign_type_id", "duration_type_id"),
            ),            
        ));
        
        $ad_location_records = $this->{$this->modelClass}->Location->AdLocation->find("all", array(
            "fields" => array("id", "campaign_id", "from_date", "to_date", "target_imperssions", "reached_imperssions", "actual_budget"),
            "conditions" => array(                        
                    "AdLocation.location_id" => $this->location_id,
                    "from_date <=" => DateUtility::getCurrentDateTimeString(DEFAULT_SQL_DATE_FORMAT),
                    "to_date >=" => DateUtility::getCurrentDateTimeString(DEFAULT_SQL_DATE_FORMAT),
                    //"start_time <=" => DateUtility::getCurrentDateTimeString(DEFAULT_SQL_TIME_FORMAT),
                    //"end_time >=" => DateUtility::getCurrentDateTimeString(DEFAULT_SQL_TIME_FORMAT),
                    //"is_download" => 2,
                    //"is_delete" => 0,
                    "Campaign.status_type_id" => CAMPAIGN_ACTIVE_ID,
            ),
        ));
        
//        /debug($ad_location_records); exit;
        
        if (!$ad_location_records)
        {
            $this->response_data['msg'] = "No Ad to Play";
            return;
        }        
        
        $setting_record = $this->_getAdminSetting();
        $setting_record = $setting_record['AdminSetting'];
       
        $date_2_day_before = DateUtility::getFormatDateFromObj(DateUtility::addDaysInDateObj(DateUtility::getCurrentDateTimeObj(), "-2"), DEFAULT_SQL_DATE_FORMAT);
        
        /**
         * filtering the ads
         */       
        foreach ($ad_location_records as $key => $record)
        {
            $automobile_list = array();
            $automobile_count = 0;
            
            if ($record["Campaign"]["campaign_type_id"] != CAMPAIGN_AUTOMOBILE_MASS_ID)
            {
                $automobile_list = $this->{$this->modelClass}->Location->AdLocation->Campaign->CampaignAutomobile->find("list", array(
                    "fields" => array("automobile_id", "automobile_id"),
                    "conditions" => array("campaign_id" => $record['AdLocation']['campaign_id']),
                    "recursive" => -1
                ));
                
                if ($automobile_list)
                {                
                    if (isset($automobile_list[$car_record['automobile_manufacture_id']]) || isset($automobile_list[$car_record['automobile_model_id']]) || isset($automobile_list[$car_record['automobile_variant_id']]))
                    {
                        $automobile_count = $this->_getCampaignAutomobileCount($automobile_list);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            else
            {
                $automobile_count = $this->{$this->modelClass}->query("CALL sp_get_variant_count");
                $automobile_count = $automobile_count[0][0]["variant_count"];
            }
            
            $location_count = $this->{$this->modelClass}->Location->AdLocation->find("count", array(
                "conditions" => array(
                    "AdLocation.campaign_id" => $record['AdLocation']['campaign_id'],                    
                ),                
                "recursive" => -1
            ));
            
            $inner_data = $record['AdLocation'];
            $weightage = 0;

            // computing weightange on Cost 
            $cost_value = 0;

            if ($inner_data['actual_budget'] < $setting_record['campaign_budget_medium_from'])
            {
                //Low Cost
                $cost_value = 1;
            }
            else if ($inner_data['actual_budget'] <= $setting_record['campaign_budget_medium_to'])
            {
                //Medium Cost
                $cost_value = 2;
            }
            else
            {
                //high Cost
                $cost_value = 3;
            }

            $weightage = $cost_value * 40;

            // computing weightage on remaining days; 
            $total_days = $remaining_days = 0;
            if ($record["Campaign"]["duration_type_id"] == CAMPAIGN_DURATION_FIXED_ID)
            {
                $total_days = DateUtility::compareDateStrings($inner_data['from_date'], $inner_data['to_date'], DateUtility::$DAYS);                
                $remaining_days = DateUtility::compareDateStrings(DateUtility::getCurrentDateTimeString(DEFAULT_SQL_DATE_FORMAT), $inner_data['to_date'], DateUtility::$DAYS);
            }

            $remaining_impressions = $inner_data['target_imperssions'] - $inner_data['reached_imperssions'];                    

            /**
             * calculating $weightage from $automobile_count
             */
            if ($automobile_count > 0)
            {
                $temp = (($inner_data['target_imperssions'] / $automobile_count) * 12.5);                
                $weightage += $temp;
            }
            
            $temp = (($inner_data['target_imperssions'] / $location_count) * 17.5);
            $weightage += $temp;
                
            if ($remaining_impressions > 0 && $remaining_days > 0)
            {
                $temp = ($total_days / $remaining_days) * $remaining_impressions;
                $temp = ($temp / ($total_days * $inner_data['target_imperssions'])) * 30;

                $weightage += $temp;

                //checking whether ad played in last 2 days or not
                
                $temp_count = $this->{$this->modelClass}->Location->AdPlayedLog->find("count", array(
                    "conditions" => array(
                        "ad_location_id" => $ad_location_records[$key]['AdLocation']["id"],
                        "confirm_web_service_log_id > " => 0,
                        "created_on >= " => $date_2_day_before
                    ),
                    "recursive" => -1
                ));
                
                if ($temp_count)
                {
                    $weightage -= $temp_count * 2;
                }
                
                $ad_location_records[$key]['AdLocation']['weightage'] = $weightage;  
                
                $ads[] = $ad_location_records[$key]['AdLocation'];
            }
        }
        
        if (isset($ads))
        {
            /**
            * sorting according to weightage
            */
            $count = count($ads);
            for ($i = 0; $i < $count; $i++)
            {
                for ($a = $i + 1; $a < $count; $a++ )
                {
                    if ($ads[$a]['weightage'] > $ads[$i]['weightage'])
                    {                    
                        $temp = $ads[$i];
                        $ads[$i] = $ads[$a];
                        $ads[$a] = $temp;
                    }
                }
            }
            
            $ad_played_log_data = array();
            $i = 0;
            $no_of_ads = $this->location_record["Location"]["ws_no_of_ads"] ? $this->location_record["Location"]["ws_no_of_ads"] : 3;
            
            
            foreach ($ads  as $ad)
            {
                if ($i < $no_of_ads)
                {
                    $this->response_data['data']['ads'][] = array(
                        "ad_id" => $ad['id'],
                        "campaign_id" => $ad['campaign_id'],
                        "weightage" => $ad['weightage']
                    );

                    $ad_played_log_data[] = array(
                        "ad_location_id" => $ad['id'],
                        "location_id" => $this->location_id,
                        "campaign_id" => $ad['campaign_id'],
                        "play_web_service_log_id" => $this->web_service_log_id
                    );
                }
                $i++;
            }
            
            if (!$this->{$this->modelClass}->Location->AdPlayedLog->saveMany($ad_played_log_data))
            {
                throw new Exception("failed to save record in ad_played_logs", ERROR_SAVE);
            }
            
            // update automobile_locations//
            $automobile_data = array(
                "location_id" => $this->location_id,
                "variant_id" => $car_record['automobile_variant_id'],
                "month_year" => DateUtility::getCurrentDateTimeString("Y-m-01"),
            );
            
            if (!$this->{$this->modelClass}->Location->AutomobileLocation->saveRecord($automobile_data))
            {
                throw new Exception("failed to save record in automobile_locations table", ERROR_SAVE);
            }
            
        }
        else 
        {
            $this->response_data['msg'] = "No Ad to Play";
        }       
    }
    
    private function _ConfirmAdDownload()
    {
        $this->_AuthRequest();
        
        $this->_validateRequestforConfirmRequest();
        
        $this->loadModel("AdLocation");
        
        $q = "UPDATE ad_locations SET confirm_download_web_service_log_id=" . $this->web_service_log_id .  ", is_download=2 WHERE download_web_service_log_id=" . $this->xml_data['data']['request_id'] . " AND id=" .$this->xml_data['data']['ad_id'];
        //debug($q); exit;
        
        $this->AdLocation->query($q);
    }
    
    private function _ConfirmAdDelete()
    {
        $this->_AuthRequest();
        
        $this->_validateRequestforConfirmRequest();
        
        $this->loadModel("AdLocation");
        
        $q = "UPDATE ad_locations SET confirm_delete_web_service_log_id=" . $this->web_service_log_id .  ", is_delete=2 WHERE delete_web_service_log_id=" . $this->xml_data['data']['request_id'] . " AND id=" .$this->xml_data['data']['ad_id'];
        
        $this->AdLocation->query($q);  
        
    }
    
    private function _ConfirmAdPlay()
    {
        $this->_AuthRequest();
        
        if (!isset($this->xml_data["data"]["car_id"]))
        {
            throw  new Exception("Missing car_id", ERROR_INSUFFICIENT_DATA);
        }
        
        if (!is_string($this->xml_data["data"]["car_id"]))
        {
            throw  new Exception("Missing car_reg_no", ERROR_INSUFFICIENT_DATA);            
        }  
        
        $this->xml_data["data"]["car_id"] = trim($this->xml_data["data"]["car_id"]);
        
        if (!$this->xml_data["data"]["car_id"])
        {
            throw  new Exception("Missing car_id", ERROR_INSUFFICIENT_DATA);
        }
        
        $this->_validateRequestforConfirmRequest();
        
        $this->loadModel("AdPlayedLog");
        
        $data = array (
            'confirm_web_service_log_id' => $this->web_service_log_id
        );
        
        $conditions = array(
            'play_web_service_log_id' => $this->xml_data['data']['request_id'],
            'ad_location_id' => $this->xml_data['data']['ad_id'],            
        );
        
        $this->loadModel("Car");
            
        $car_data = $this->Car->find("first", array(
            "fields" => array("automobile_variant_id"),
            "conditions" => array(
                "id" => $this->xml_data["data"]["car_id"]
            ),
            "recursive" => -1
        ));

        if (!$car_data)
        {
            throw new Exception("Invalid Car Id", ERROR_GENERAL);
        }
        
        $data["automobile_variant_id"] = $car_data["Car"]['automobile_variant_id'];
        $data["car_id"] = $this->xml_data["data"]["car_id"];
                
        if ($this->{$this->modelClass}->Location->AdLocation->IncreaseFieldValue($this->xml_data['data']['ad_id'], "reached_imperssions", 1))
        {
            if (!$this->Car->IncreaseFieldValue($this->xml_data["data"]["car_id"], "total_count", 1))
            {
                throw new Exception("failed to save record in cars table", ERROR_SAVE);
            }
            
            if (!$this->AdPlayedLog->updateAll($data, $conditions))
            {
                throw new Exception("failed to save record in ad_played_logs", ERROR_SAVE);
            }
        }
        else
        {
            throw new Exception("failed to save record in ad_location_logs", ERROR_AD_LOCATION_LOGS_SAVE);
        }
    }
    
    private function _ActivateLocation()
    {
        if (!isset($this->xml_data["auth"]["location_id"]))
        {
            throw  new Exception("Missing location_id in Auth Information", ERROR_INSUFFICIENT_DATA);
        }
        
        if (!isset($this->xml_data["data"]["activation_key"]))
        {
            throw  new Exception("Missing activation_key in data parameters", ERROR_INSUFFICIENT_DATA);
        }
        
        $conditions['location_id'] = trim($this->xml_data["auth"]['location_id']);
        $conditions['activation_key'] = trim($this->xml_data["data"]['activation_key']);
                
        $this->{$this->modelClass}->Location->recursive = -1;
        
        $location_record = $this->{$this->modelClass}->Location->find("first", array(
            "fields" => array("id", "name", "address", "security_code", "is_active", "is_exclusive", "is_activated", "country_id"),
            "conditions" => $conditions
        )); 
        
        if (empty($location_record))
        {
            throw  new Exception("Wrong Auth Information", ERROR_NON_AUTHORISED);
        }
        else
        {
            $this->location_id = $location_record['Location']['id'];
            
            if (!$location_record['Location']["is_active"])
            {
                throw  new Exception("location may not active", ERROR_GENERAL);
            }
        }
        
        $this->{$this->modelClass}->Location->id = $this->location_id;
        
        if (!$this->{$this->modelClass}->Location->saveField("is_activated", 1))
        {
            throw new Exception("Failed to save location table", ERROR_AD_LOCATION_LOGS_SAVE);
        }
        
        if ( !$location_record['Location']["is_activated"])
        {        
            if (!$location_record['Location']["is_exclusive"])
            {
                $this->{$this->modelClass}->Location->AdLocation->Campaign->contain(array(
                    "User" => array(
                        "fields" => array("id", "username", "name", "subname")
                    )
                ));

                $user_records = $this->{$this->modelClass}->Location->AdLocation->Campaign->find("all", array(                                
                    "fields" => array("id"),
                    "conditions" => array(
                        "Campaign.country_id" => $location_record['Location']['country_id'],
                        "Campaign.status_type_id" => array(CAMPAIGN_CREATED_ID, CAMPAIGN_SUBMITTED_ID, CAMPAIGN_APRROVED_ID, CAMPAIGN_ACTIVE_ID)
                    )
                ));

                $user_records = Hash::combine($user_records, "{n}.User.id", "{n}.User");

                $this->_callBackLocationActivation($location_record, $user_records);
            }
            else
            {
                $this->_callBackLocationActivation($location_record, array());
            }
        }
        
        $this->response_data["data"]["activated"] = 1;
        $this->response_data["data"]["security_code"] = $location_record['Location']["security_code"];
    }
    
    private function _Authentication()
    {   
        $this->_AuthRequest(true);
        $this->response_data["status"] = 1;
    }
    
    private function _IsConfigChange()
    {
        $this->_AuthRequest();
        $setting_record = $this->_getAdminSetting(array("is_client_config_change"));
        
        $this->response_data["data"]["is_config_change"] = (int) $this->location_record["Location"]["is_client_config_change"] + (int) $setting_record["AdminSetting"]["is_client_config_change"];
    }
    
    private function _GetConfig()
    {
        $this->_AuthRequest();
        
        $setting_record = $this->_getAdminSetting();
        
        $setting_record = $setting_record["AdminSetting"];
        
        $location = $this->location_record["Location"];
        
        $this->response_data["data"] = array(
            "web_service_url" => $setting_record["ws_url"],
            "ftp_host" => $setting_record["ws_ftp_host"],
            "ftp_username" => $setting_record["ws_ftp_username"],
            "ftp_password" => $setting_record["ws_ftp_password"],
            "ad_download_path" => $location["client_ad_download_path"],
            "welcome_msg" => $location["client_welcome_msg"]
        );
        
        
        $this->{$this->modelClass}->Location->id = $this->location_id;
        $this->{$this->modelClass}->Location->saveField("is_client_config_change", 0, false);
        
        $this->AdminSetting->id = $setting_record["id"];
        $this->AdminSetting->saveField("is_client_config_change", 0, false);
    }  
    
    private function _SendLocationAlert()
    {
        $this->_AuthRequest();
        
        if (!isset($this->xml_data["data"]["content"]))
        {
            throw  new Exception("Missing content in data Information", ERROR_INSUFFICIENT_DATA);
        }
        
        $this->loadModel("LocationWebServiceAlert");
        
        $data['LocationWebServiceAlert'] = array(
            "location_id" => $this->location_id,
            "content" => mysql_escape_string(htmlspecialchars($this->xml_data["data"]["content"]))
        );
        
        if (!$this->LocationWebServiceAlert->save($data))
        {
            throw  new Exception("Failed to save data in location_web_service_alerts table", ERROR_SAVE);
        }
    }
    
    /**
     * Comman Validtion function used by all confirm services 
     * @throws Exception
     */
    private function _validateRequestforConfirmRequest()
    {
        if (!isset($this->xml_data['data']['request_id']))
        {
            throw new Exception("Missing request_id", ERROR_INSUFFICIENT_DATA);
        }
        
        if (!isset($this->xml_data['data']['ad_id']))
        {
            throw new Exception("Missing ad_id", ERROR_INSUFFICIENT_DATA);
        }
    }    
}