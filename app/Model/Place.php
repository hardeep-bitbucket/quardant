<?php

/**
 * @package    Audatex
 * 
 * 
 * @created    19/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */
class Place extends AppModel {

    public $order = array("name ASC");
    public $parent_field = "place_id";
    var $actsAs = array(
        'Tree' => array(
            'parent' => 'place_id'
        )
    );
    
    var $belongsTo = array(
        'ParentPlace' => array(
            'className' => 'Place',
            'foreignKey' => 'place_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
        'ChildPlace' => array(
            'className' => 'Place',
            'foreignKey' => 'place_id',
            'dependent' => false,
            'conditions' => '',
        ),
        'LocationCity' => array(
            'className' => 'Location',
            'foreignKey' => 'city_id',
            'dependent' => false,
            'conditions' => '',
        ),
        'LocationState' => array(
            'className' => 'Location',
            'foreignKey' => 'state_id',
            'dependent' => false,
            'conditions' => '',
        ),
        'LocationCountry' => array(
            'className' => 'Location',
            'foreignKey' => 'country_id',
            'dependent' => false,
            'conditions' => '',
        ),
        'CarState' => array(
            'className' => 'Car',
            'foreignKey' => 'state_id',
            'dependent' => false,
            'conditions' => '',
        ),
        'CarCountry' => array(
            'className' => 'Car',
            'foreignKey' => 'country_id',
            'dependent' => false,
            'conditions' => '',
        )
    );
    var $validate = array(
        'name' => array(
            'notEmptyRule' => array(
                'rule' => 'notEmpty',
                'message' => NAME_REQUIRED_MESSAGE
            )
        )
    );

}
