<?php
/**
 * Add & edit Page
 * 
 * 
 * @created    02/01/2015
 * @package    TFQ
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Sonia
 */
$model = Inflector::classify( $this->params['controller']);
$controller = $this->params['controller'];
$action = $this->params['action'];
?>
<script type='text/javascript'>
	$(function(){
		$(".continue").click(function(){
			$("#saveContinue").val(1);
		})
	});
</script>
<aside class="body_rt"> 
  <!--Page head start-->
  <div class="page_head">
      <h1> <?php echo ucwords($model); ?> Manager</h1>
	<div class="back">
    	<?php echo $this->Html->link($this->Html->image('back.png'),array('action'=>'admin_index'),array('escape' => false, 'title' => BACK_TITLE)); ?>
         </div>
  </div>
  <!--Page head end--> 
  
  <!--Blog warp start-->
<section class="bolg_warp">
<div class="sub_head"><?php echo $heading;?></div>
<?php if ($this->params['action']=='admin_edit') {?>
    <ul class="form_tab">
      <li><?php echo $this->Html->link('Campaign Details',array('controller'=>'campaigns','action'=>'admin_edit', $this->request->data['Campaign']['id']),array('class'=>'active_tab'));?></li>
      <li><?php echo $this->Html->link('Type of Campaign',array('controller'=>'campaigns','action'=>'admin_campaign_type', $this->request->data['Campaign']['id']));?></li>
      <li><?php echo $this->Html->link('Duration & Budget',array('controller'=>'campaigns','action'=>'admin_campaign_duration', $this->request->data['Campaign']['id']));?></li>
      <li><?php echo $this->Html->link('Review',array('controller'=>'campaigns','action'=>'admin_payment', $this->request->data['Campaign']['id']));?></li>
    </ul>
<?php } ?>
<div class="three_column">
<?php echo $this->Form->create($model, array('onsubmit'=>'return validate();')); ?>
    <h1>General Details</h1>
      <ul>
        <li>
            <?php echo $this->Form->hidden('id', array('label' => false,'class'=>'text_field')); ?>
        </li>
        <li>
            <h2>Advertiser<span> *</span>:</h2>
            <h3>
                <?php 
                    echo $this->Form->input('user_id',
                         array(
                            'label' => false, 
                            'options' => $advertiserList, 
                            'escape' => false,
                             'class'=>'text_field notEmpty',
                             'empty' => 'Please Select'
                        )
                    );
                  ?>	
            </h3>
        </li>
        <li>
            <h2>Name <span> *</span>:</h2>
            <h3>
                <?php echo $this->Form->input('name', array('label' => false,'class'=>'text_field')); ?>
            </h3>
        </li>
      </ul>
    <h1>Content Details</h1>
    <ul>
        <li>
            <h2>Purpose of Advertisement:</h2>
            <h3>
                <?php echo $this->Form->input('purpose_of_advertisement', array('label' => false,'class'=>'text_field')); ?>
            </h3>
        </li>

        <li>
            <h2>Default Creative :</h2>
            <h3>
                <?php echo $this->Form->input('default_creative', array('type'=>'text','label' => false,'class'=>'text_field'));  ?>
            </h3>
        </li>
        
        <li>
            <h2>Duration Type :</h2>
            <h3>
                <?php echo $this->Form->input('duration_type_id', array('type'=>'text','label' => false,'class'=>'text_field'));  ?>
            </h3>
        </li>
    </ul>
    <h1>Other Details</h1>
    <ul>
        <li>
            <h2>Start Date:</h2>
            <h3>
                <?php echo $this->Form->input('start_date', array('type'=>'text','label' => false,'class'=>'text_field datepick'));  ?>
            </h3>
        </li>
        <li>
            <h2>End Date:</h2>
            <h3>
                <?php echo $this->Form->input('end_date', array('type'=>'text','label' => false,'class'=>'text_field datepick'));  ?>
            </h3>
        </li>
    </ul>
</div>
	<div class="centerbutton">
        <ul>
              <li>
                     <?php echo $this->Form->submit('save.png'); ?>
               </li>
               <li>
                    <?php echo $this->Form->hidden('continue',array('id'=>'saveContinue','value'=>''));
                        echo $this->Form->submit('save_continue.png', array('class'=>'continue btn btn-primary')); ?>
                </li>
               <li>
                   <?php echo $this->Html->link($this->Html->image('cancle.png'), 
                               array('controller'=>$controller,'action'=>$reset_action),array('escape'=>false)); ?>
              </li>
        </ul>
	</div>
    <?php echo $this->Form->end();?>
  </section>
  <!--Blog warp end--> 
  
</aside>