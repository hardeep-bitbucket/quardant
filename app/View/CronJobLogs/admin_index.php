<?php
/**
 * web service logs Controller
 * 
 * 
 * @created    22/02/2014
 * @package    TFQ
 * @copyright  Copyright (C) 2014
 * @license    Proprietary
 * @author     Hardeep
 */

$action_for_search = str_replace("admin_", '', $action);
?>


<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h3 class="page-title">
              <?php echo $title_for_layout; ?>
            </h3>
        </div>
    </div>
</div>
<?php
    //echo $this->element("admin/breadcrumb"); 
    echo $this->Session->flash();
?>

<div class="row">
    <div class="col-md-12">   
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">Search</div>  
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>                            
                </div>

            </div>
            <div class="portlet-body">    
                
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->                    
                    <?php echo $this->Form->create($model, array('type' => 'GET' , 'class' => 'ajax-form form-horizontal', 'data-action' => $action_for_search)); ?>
                     
                        <div class="form-body">                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">From Date</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('CronJobLogSearch.from_date',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $CronJobLogfrom_date, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">To Date</label>
                                        <div class="col-md-8">
                                             <?php 
                                                echo $this->Form->input('CronJobLogSearch.to_date',array(
                                                    'type' => 'text', 'label' => false, 'div' => false, 'escape' => false,
                                                    'value' => $CronJobLogto_date, 
                                                    'class' => 'form-control datepicker'
                                                )); 
                                            ?> 
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                             <div class="row">                               
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Status</label>
                                        <div class="col-md-8">                                            
                                             <?php
                                                echo $this->Form->input('CronJobLogSearch.status', array(
                                                    'type' => 'select', 'label' => false, 'div' => false, 'escape' => false,
                                                    'options' => StaticArray::$StatusType, 'empty' => "Please Select",                                                   
                                                    'value' => $CronJobLogstatus,
                                                    'class' => 'select2me form-control',
                                                ));
                                              ?>                                            
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            
                            <!--/row-->
                        </div>
                        <?php echo $this->element("admin/form_search_btn"); ?>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
        
        
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Summary</div>                        
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-striped table-condensed table-custom">
                    <thead>
                       <tr class="head" >                           
                            <td width="5%"  class="td-center"><?php echo $this->Paginator->sort('id', 'ID', array('class' => "ajax-page-link")); ?></td>
                            <td width="30%">Description</td>                            
                            <td width="5%" class="td-center"><?php echo $this->Paginator->sort('status', 'Status'); ?></td>
                            <td width="10%" class="td-center"><?php echo $this->Paginator->sort('created_on', 'Created'); ?></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($records as $record) { ?>
                            <tr>                                
                                <td class="td-center"><?php echo $record[$model]['id']; ?></td>                                
                                <td><?php echo $record[$model]['description']; ?></td>                                  
                                <td class="td-center">
                                    <?php                            
                                        $img = $record[$model]['status'] == 1 ? "admin/status_1.png" : "admin/status_0.png";
                                        echo $this->Html->image($img,array(
                                            "alt" => "Active",                                    
                                            "title" => $record[$model]['status'] == 1 ? "Active" : "InActive",
                                            "class" => "summary-action-icon",
                                            "escape" => false
                                        )
                                    );
                                    ?> 
                                </td>  
                                <td class="td-center"><?php echo $record[$model]['created_on']; ?></td> 
                            </tr>
                    <?php } ?>
                    </tbody>                  
                </table>                    
                    <div>    
                      <?php echo $this->element("admin/pagination"); ?>
                    </div> 
            </div>
        </div>               

    </div>
</div>
