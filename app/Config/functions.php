<?php

/**
 * Functions
 * 
 * this file  contains general functions 
 * 
 * All functions name should be be lower case
 * 
 * 
 * @created    04/02/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Hardeep
 */

/**
 * Following function convert any type of object to array
 * it can convert xml, json object to array
 * 
 * @param object $obj
 * @return array
 */
function convert_object_to_array($obj) {
    $arr = array();
    if (gettype($obj) == "object") {
        $arr = get_object_vars($obj);

        foreach ($arr as $k => $v) {
            $arr[$k] = convert_object_to_array($v);
        }
    } else {
        $arr = $obj;
    }

    return $arr;
}

function fetchCSV($filename, $header = true, $delimiter = ',') 
{
    if (!file_exists($filename) || !is_readable($filename)) 
    {
        return FALSE;
    }
    $data = array();
    $header_data = array();
    
    if (($handle = fopen($filename, 'r')) !== FALSE) 
    {
        $r = 0;
        while (($row = fgetcsv($handle, 0, $delimiter)) !== FALSE) 
        {
            if ($header && $r == 0) 
            {
                $header_data = $row;               
            } 
            else 
            {
                if ($header)
                {
                    for($i = 0; $i < count ($row); $i++)
                    {
                        $data[$r][$header_data[$i]] = $row[$i];
                    }
                }
                else
                {
                    $data[$r] = $row;
                }
            }
            $r++;
        }
        fclose($handle);
    }
    return $data;
}


function writeCSV($file, $data, $header = true, $delimeter = ',', $mode = "w") 
{
    $handle = fopen($file, $mode);
    if ($handle) 
    {
        if (!empty($data['header']) && isset($data['header']) && $mode != "a") 
        {
            fputcsv($handle, $data['header'], $delimeter);
        }

        if (!empty($data['data']) && isset($data['data'])) 
        {
            foreach ($data['data'] as $key => $line) 
            {
                fputcsv($handle, $line, $delimeter);
            }
        }
        fclose($handle);
    }
}

/**
 * sort array on basis char len
 * @param array $arr
 * @return array
 */
function sort_array_on_value_string_len($arr)
{
    $temp_list = array_flip($arr);    
    $arr = array_keys($temp_list);
    
    $n = count($arr);
    for($i = 0; $i < $n; $i++)
    {
        for($a = $i + 1; $a < $n; $a++)
        {
            if (strlen($arr[$a]) < strlen($arr[$i]))
            {
                $temp = $arr[$i];
                $arr[$i] = $arr[$a];
                $arr[$a] = $temp;
            }
        }
    }
    
    $ret = array();
    
    foreach ($arr as $v)
    {
        if (isset($temp_list[$v]))
        {
            $ret[$temp_list[$v]] = $v;
        }
    }
    return $ret;
}

/**
 * get rondom string in given char string
 * @param String $valid_chars
 * @param int $length
 * @return string
 */
function get_random_string($valid_chars, $length) 
{
    $random_string = "";
    $num_valid_chars = strlen($valid_chars);
    for ($i = 0; $i < $length; $i++) 
    {
        $random_pick = mt_rand(1, $num_valid_chars);
        $random_char = trim($valid_chars[$random_pick - 1]);
        
        if (!$random_char)
        {
            $i--;
        }
        else
        {
            $random_string .= $random_char;
        }
    }
    return $random_string;
}

/**
 * get meta information of you tube embedd
 * @param type $url
 * @return type
 */
function get_youtube_info_from_embed_url($url)
{
    $id = get_youtube_id_from_embedd_url($url);
    
    $info = file_get_contents("https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=$id&key=" . GOOGLE_API_KEY);

    $info = json_decode($info, true);
    
    
    $dur = null;
    if (isset($info["items"][0]))
    {
        $dur = $info["items"][0]["contentDetails"]["duration"];         
        $dur = get_duration_from_youtube_time($dur);
    }
     
    return array(
        "id" => $id,
        "duration" => $dur
    );    
}

function get_duration_from_youtube_time($dur)
{
    $temp = explode("T", $dur);
    $temp = $temp[1];
    
    $arr = explode("M", $temp);
    
    $seconds = 0;
    if (count ($arr) >= 2)
    {
        $seconds = $arr[0] * 60;
        $temp = explode("S", $arr[1]);
        $seconds += $temp[0];
    }
    else
    {
        $temp = explode("S", $arr[0]);
        $seconds += $temp[0];
    }
        
    return $seconds;
}

function get_youtube_main_url_from_embeded_url($url)
{
    return "https://www.youtube.com/watch?v=" . get_youtube_id_from_embedd_url($url);
}

function get_youtube_id_from_embedd_url($url)
{
    $temp = explode("/", $url);
    $id = $temp[(count($temp) - 1)];
    return $id;
}

function is_youtube_url($url)
{
    $pos = strpos($url, "watch?v");
    
    if ($pos)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// not in use
function get_youtube_info_from_url($url)
{
    $youtube = "http://www.youtube.com/oembed?url=" . $url . "&format=json";
    $json =  @file_get_contents($youtube);
    return json_decode($json);
}

function get_youtube_embed_url($url)
{
    $temp = explode("?v=", $url);
    
    if (count($temp) > 1)
    {
        return "https://www.youtube.com/embed/" . $temp[1];
    }
    else
    {
        return "";
    }
}

function _merge_array_rec($arr, $arr2, $i = 0)
{ 
    foreach ($arr2 as $key => $value)
    {
        if (!isset($arr[$key]))
        {
            $arr[$key] = $value;
        }
        else
        {
            if (is_array($value))
            {
                $arr[$key] = _merge_array_rec($arr[$key], $arr2[$key], $i + 1);
            }
            else
            {
                $arr[$key] = $value;
            }
        }
    }
    
    return $arr;
}