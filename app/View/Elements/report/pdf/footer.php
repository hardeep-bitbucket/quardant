<?php
/**
 * Invoice PDF 
 * 
 * 
 * @created    09/05/2015
 * @package    ANPRMotion
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

if ( isset($pdf) ) 
{
  $font = Font_Metrics::get_font("verdana", "bold");
  $pdf->page_text(20, 805, "Page {PAGE_NUM} of {PAGE_COUNT}", $font, 10, array(0,0,0));
}
?>


    <div id="footer">
        
    </div>
</body>
</html>