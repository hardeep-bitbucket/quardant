<?php

App::uses('Controller', 'Controller');

class CommanController extends Controller 
{    
    public $components = array('Email');   
    public $ext = '.php';

    public function beforeFilter() 
    { 
        //DateUtility::setClientTimeZone("Europe/London");
    }
    
    /**
     * Sends email
     * 
     * @param String $to_email
     * @param String $subject
     * @param String $template
     * @param Array $files
     * @param Array $from_email
     * @param Array $bcc
     * @return int
     */
    public function sendEmail($to_email, $from_email, $subject, $template, $viewData = array(), $files = array(), $bcc = array()) {
        try {
            $email = new CakeEmail();
            $email->from($from_email);
            $email->to($to_email);
            $email->returnPath($from_email);
            $email->bcc($bcc);
            $email->subject($subject);
            $email->emailFormat('html');
            $email->template($template);
            $email->viewVars($viewData);
            if (isset($files) && !empty($files)) {
                $email->attachments($files);
            }
            $email->send();
        } catch (Exception $e) {
            echo $e->getMessage();
            return 0;
        }
        return 1;
    }
    
    /**
     * get Config from admin setting Model
     * @return array
     */
    protected function _getAdminSetting($fields = array())
    {
        $this->loadModel("AdminSetting");
        return $this->AdminSetting->find("first", array(
            "fields" => $fields,
        ));
    }
    
    /**
     * function get automobile count on given locations in given time period
     * @param array location_list
     * @param string start_date
     * @param string end_date 
     */
    protected function _getAutomobileCountOnLocation($location_list = array(), $start_date = null, $end_date = null)
    {
        if ($location_list)
        {
            $conditions['location_id'] = $location_list;
        }
        
        if ($start_date)
        {
            $conditions['month_year >= '] = $start_date;
        }
        
        if ($end_date)
        {
            $conditions['month_year <= '] = $end_date;
        }
                
        $record = $this->AutomobileLocation->find("first", array(
            "fields" => array("SUM(total_count) AS total"),
            "conditions" => $conditions,
            "recursive" => -1
        ));
        
        return $record[0]["total"];
    }
    
    protected function _sendInvoiceEmail($invoice_id, $send_email = true, $send_notification = true)
    {
        $this->Invoice->contain(array(
            "User" => array(
                "fields" => array(
                    "id", "name", "subname", "username"
                )
            ),
            "Campaign" => array(
                "fields" => array(
                    "id", "name"
                )
            ),
            "Transaction" => array(
                "fields" => array("debit")
            ),
            "DiscountTransaction" => array(
                "fields" => array("credit")
            )
        ));
        
        $record = $this->Invoice->find("first", array(
            "conditions" => array("Invoice.id" => $invoice_id)
        ));
        
        $record["Invoice"]["discount"] = $record["DiscountTransaction"]["credit"] ? $record["DiscountTransaction"]["credit"] : 0;
        $record["Invoice"]["net_amount"] = $record["Invoice"]["amount"] - $record["Invoice"]["discount"];
        
        $campaign_title = '<a href="/admin/Campaigns/campaign_detail/' . $record['Campaign']['id'] . '">' . $record['Campaign']['name'] . "</a>";
        
        $notification_data = array();
        
        switch($record["Invoice"]["status"])
        {
            case StaticArray::$invoice_status_unpaid: //invoice created
                $template = "created";
                $subject = "Invoice Pending of Campaign " . $record['Campaign']['name'];
                
                $notification_data = array(
                    "type" => StaticArray::$notification_type_alert,
                    "content" => "Invoice #" . $invoice_id . " is Pending of Campaign " . $campaign_title
                );
            break;
        
            case StaticArray::$invoice_status_cancel: //invoice canceled
                $subject = "Invoice Canceled of Campaign " . $record['Campaign']['name'];
                $template = "cancel";
                
                $notification_data = array(
                    "type" => StaticArray::$notification_type_alert,
                    "content" => "Invoice #" . $invoice_id . " has Canceled of Campaign " . $campaign_title
                );
            break;
        
            case StaticArray::$invoice_status_paid: // invoice paid
                $subject = "Invoice Paid of Campaign " . $record['Campaign']['name'];
                $template = "paid";
                
                $notification_data = array(
                    "type" => StaticArray::$notification_type_postive,
                    "content" => "Invoice #" . $invoice_id . " has Paid of Campaign " . $campaign_title
                );
            break;            
        }
        
        if ($send_notification)
        {
            $data = array(
                array_merge($notification_data, array("group_id" => ADMIN_GROUP_ID)),                
                array_merge($notification_data, array("user_id" => $record['User']['id']))
            );

            $saved = $this->_saveNotification($data);
        }
        
        if ($send_email)
        {
            $from_email = FROM_EMAIL;
            $to_email = $record["User"]["username"];

            $records["records"] = $record;
            $records['content']['footer'] = $this->_getEmailFooterContent();

            $this->sendEmail($to_email, $from_email, $subject, "Invoice/" . $template, $records, $files = array(), $bcc = array());
            $this->sendEmail(TO_EMAIL, $from_email, $subject, "Invoice/admin/" . $template, $records, $files = array(), $bcc = array());
        }
    }
    
    /**
     * send email to admin,advertiser on campaign status change
     * @param int $campaign_id
     */
    protected  function _sendCampaignEmail($campaign_id, $send_email = true, $send_notification = true)
    {
        $this->Campaign->contain(array(
            "User" => array(
                "fields" => array("User.id", "User.name", "User.subname", "User.username")
            )
        ));
        
        $record = $this->Campaign->find("first", array(
            "fields" => array ("Campaign.id", "Campaign.name", "Campaign.status_type_id"),
            "conditions" => array(
                "Campaign.id" => $campaign_id
            ),
            "recursive" => -1
        ));
        
        $title = '<a href="/admin/Campaigns/campaign_detail/' . $campaign_id . '">' . $record['Campaign']['name'] . "</a>";
        
        $notification_data = array();
        switch($record["Campaign"]['status_type_id'])
        {
            case CAMPAIGN_SUBMITTED_ID:
                $subject = "Campaign " . $record['Campaign']['name'] . " Submitted";
                $template = "submitted";
                
                $notification_data = array(
                    "type" => StaticArray::$notification_type_postive,
                    "content" => "Campaign " . $title . " has Submitted"
                );
            break;
            
            case CAMPAIGN_APRROVED_ID:
                $subject = "Campaign " . $record['Campaign']['name'] . " Aprooved";
                $template = "approved";
                
                $notification_data = array(
                    "type" => StaticArray::$notification_type_alert_postive,
                    "content" => "Campaign " . $title . " has Aprooved"
                );
            break;
        
            case CAMPAIGN_REJECTED_ID:
                $subject = "Campaign " . $record['Campaign']['name'] . " Rejected";
                $template = "rejected";
                
                $notification_data = array(
                    "type" => StaticArray::$notification_type_alert_negtive,
                    "content" => "Campaign " . $title . " has Rejected"
                );
            break;
        
            case CAMPAIGN_ACTIVE_ID:
                $subject = "Campaign " . $record['Campaign']['name'] . " Activated";
                $template = "active";
                
                $notification_data = array(
                    "type" => StaticArray::$notification_type_alert_postive,
                    "content" => "Campaign " . $title . " has Activated"
                );
            break;
        
            case CAMPAIGN_STOPPED_ID:
                $subject = "Campaign " . $record['Campaign']['name'] . " Stopped";
                $template = "stopped";
                
                $notification_data = array(
                    "type" => StaticArray::$notification_type_alert,
                    "content" => "Campaign " . $title . " has Stopped"
                );
            break;
        
            case CAMPAIGN_COMPLETED_ID:
                $subject = "Campaign " . $record['Campaign']['name'] . " Completed";
                $template = "completed";
                
                $notification_data = array(
                    "type" => StaticArray::$notification_type_alert,
                    "content" => "Campaign " . $title . " has Completed"
                );
            break;
        
            case CAMPAIGN_TERMINATE_ID:
                $subject = "Campaign " . $record['Campaign']['name'] . " Terminate";
                $template = "terminated";
                
                $notification_data = array(
                    "type" => StaticArray::$notification_type_alert,
                    "content" => "Campaign " . $title . " has Terminate"
                );
            break;
        
            default:
                return;
        }
        
        if ($send_notification)
        {
            $data = array(
                array_merge($notification_data, array("group_id" => ADMIN_GROUP_ID)),                
                array_merge($notification_data, array("user_id" => $record['User']['id']))
            );
            
            if (isset($this->user) && $this->user['group_id'] != ADMIN_GROUP_ID && $this->user['group_id'] != ADVERTISER_GROUP_ID)
            {
                $data[] = array_merge($notification_data, array("user_id" => $this->user["id"]));
            }
            
            if (in_array($record["Campaign"]['status_type_id'] , array(CAMPAIGN_ACTIVE_ID, CAMPAIGN_STOPPED_ID, CAMPAIGN_COMPLETED_ID)))
            {
                $partner_list = $this->{$this->modelClass}->query("CALL sp_get_partners(NULL,$campaign_id, NULL, NULL);"); 
                
                if ($partner_list)
                {
                    $partner_list = Hash::combine($partner_list, "{n}.L.partner_id", "{n}.L.partner_id");
                    
                    foreach ($partner_list as $partner_id => $partner)
                    {
                        $data[] = array_merge($notification_data, array("user_id" => $partner_id));
                    }
                }
            }
            
            $this->_saveNotification($data);
        }
        
        
        if ($send_email)
        {
            $to_email = $record['User']['username'];
            $from_email = FROM_EMAIL;
            
            $records["records"] = $record;
            $records['content']['footer'] = $this->_getEmailFooterContent();

            $this->sendEmail($to_email, $from_email, $subject, "Campaign/". $template, $records, $files = array(), $bcc = array());
            $this->sendEmail($from_email, TO_EMAIL, $subject, "Campaign/admin/". $template, $records, $files = array(), $bcc = array());
            
        }
    }

    /**
     * campaign status function- use to change status bu campaign and cron job controller
     * @param int $id - campaign_id
     * @param int $status_type
     * @return boolean
     */
    protected function _changeCampaignStatus($id, $status_type, $send_email = true, $send_notification = true)
    {
        if (!$id)
        {
            return false;
        }
        
        $data = array(
            "status_type_id" => $status_type,
        );
        
        
        switch($status_type)
        {
            case CAMPAIGN_ACTIVE_ID:
                $q = "UPDATE `ad_locations` SET is_download = 1 WHERE campaign_id=" . $id;
                $data["active_datetime"] = DateUtility::getCurrentDateTimeString(DEFAULT_SQL_DATETIME_FORMAT);
            break;

            case  CAMPAIGN_COMPLETED_ID: 
            case  CAMPAIGN_STOPPED_ID:
                $data['end_date'] = DateUtility::getCurrentDateTimeString(DEFAULT_SQL_DATETIME_FORMAT);                
            break;
        
            case CAMPAIGN_TERMINATE_ID:                
                $q = "UPDATE `ad_locations` SET is_download = 0, is_delete = 1  WHERE campaign_id=" . $id;
            break;   
        
            case CAMPAIGN_SUBMITTED_ID:
                $data["submit_datetime"] = DateUtility::getCurrentDateTimeString(DEFAULT_SQL_DATETIME_FORMAT);
            break;
        
            case CAMPAIGN_APRROVED_ID:
                $data["approve_datetime"] = DateUtility::getCurrentDateTimeString(DEFAULT_SQL_DATETIME_FORMAT);
            break;
        }
        
        $this->Campaign->id = $id;
        
        $data["Campaign"] = $data;
        
        if ($this->Campaign->save($data))
        {
            if ($send_email || $send_notification)
            {
                $this->_sendCampaignEmail($id, $send_email, $send_notification);
            }
            
            if (isset($q) && $q)
            {
                $this->Campaign->query($q);
            }
        }
        else
        {
            return false;
        }
        
        return true;
    }
    
    /**
     * function called whenever invoice saved
     * @param int $invoice_id
     * @param int $status
     */
    protected function _invoiceSaveCallBack($invoice_id, $status)
    {
        $this->_sendInvoiceEmail($invoice_id);
    }
    
    /**
     * 
     */
    protected function _saveNotification($data)
    {
        if (!ClassRegistry::isKeySet("Notification"))
        {
            $this->loadModel('Notification');
        }
        
        return $this->Notification->saveMany($data);        
    }
    
    protected function _getCampaignStatusInfo($record)
    {
        $record["Campaign"]["spend"] = 0;
        $record["Campaign"]["admin_total"] = $record["Campaign"]["partner_total"] = 0;
        $record["Campaign"]["target_imperssions"] = $record["Campaign"]["reached_imperssions"] = 0;
        $record["Campaign"]["setup_cost"] = 0;
        foreach ($record["AdLocation"] as $inner_data)
        {
            $record["Campaign"]["target_imperssions"] += $inner_data["target_imperssions"];
            $record["Campaign"]["reached_imperssions"] += $inner_data["reached_imperssions"];

            $temp = ($inner_data["reached_imperssions"] * $inner_data["Location"]["price_per_impression"]);
            
            if ($inner_data["Location"]['is_shared'] && $inner_data["Location"]['partner_id'] && $inner_data["Location"]['partner_revenue_share_percentage'])
            {
                 if (!isset($record["Campaign"]["User"]["partner"]))
                 {
                     $record["Campaign"]["User"]["partner"][$inner_data["Location"]['partner_id']] = 0;
                 }
                 
                 $partner_amount = ($temp * $inner_data["Location"]['partner_revenue_share_percentage']) / 100;
                 
                 $record["Campaign"]["User"]["partner"][$inner_data["Location"]['partner_id']] += $partner_amount;
                 
                 $record["Campaign"]["admin_total"] += ($temp - $partner_amount);
                 $record["Campaign"]["partner_total"] += $partner_amount;
                 
                 $record["Campaign"]["setup_cost"] += $inner_data["Location"]['setup_cost'];
            }
            
            $record["Campaign"]["spend"] += $temp;
        }

        if ($record["Campaign"]["reached_imperssions"])
        {
            $record["Campaign"]["complete"] = round(($record["Campaign"]["reached_imperssions"] * 100 / $record["Campaign"]["target_imperssions"]), 2);
        }
        else
        {
            $record["Campaign"]["complete"] = 0;
        }
        
        return $record;
    }
    
    protected function _getEmailFooterContent()
    {
        $view = new View($this, false);
        $content = $view->element('email_footer_content');
        return $content;
    }
    
    protected function _callBackLocationActivation($location_record, $user_records, $email = true, $notification = true)
    {
        if ($notification)
        {
            $notification_data = array(
                array(
                    "type" => StaticArray::$notification_type_alert,                        
                    "group_id" => ADMIN_GROUP_ID,
                    "content" => "New Location <b> " . $location_record["Location"]['name'] . "</b> is activated now"
                ),
            );
            
            if ($user_records)
            {
                foreach ($user_records as $user)
                {
                    $notification_data[] = array(
                        "type" => StaticArray::$notification_type_alert,                        
                        "user_id" => $user["id"],
                        "content" => "Extend your campaign with New Location <b> " . $location_record["Location"]['name'] . "</b>"
                    );
                }
            }
            
            $saved = $this->_saveNotification($notification_data);
        }
        
        if ($email)
        {
            $footer_content = $this->_getEmailFooterContent();
            $from_email = FROM_EMAIL;
            $subject = "New Location " . $location_record['Location']['name'];
            
            if ($user_records)
            {
                foreach ($user_records as $user)
                {
                    $viewData = array(
                        "record" => array(
                            "User" => $user,
                            "Location" => $location_record["Location"]
                        ),
                        "content" => array(
                            "footer" => $footer_content
                        )
                    );

                    
                    $to_email = $user['username'];
                    $this->sendEmail($to_email, $from_email, $subject, "Location/location_activation", $viewData, $files = array(), $bcc = array());
                }
            }
            
            $viewData = array(
                "record" => array(                   
                    "Location" => $location_record["Location"]
                ),
                "content" => array(
                    "footer" => $footer_content
                )
            );
			
			//debug($viewData); exit;

            if (isset($viewData))
            {
                $to_email = TO_EMAIL;
                $this->sendEmail($to_email, $from_email, $subject, "Location/admin/location_activation", $viewData, $files = array(), $bcc = array());
            }
        }
        
    }

    public function getDiscount($user_id, $campaign_id, $code, $amount)
    {
        $record_count = $this->Campaign->find("count", array(
            "fields" => "id",
            "conditions" => array("user_id" => $user_id),
            "recursive" => -1
        ));
        
        $discount_for = $record_count == 0 ? DISCOUNT_FOR_TYPE_FIRST_TIME_CUSTOMER : DISCOUNT_FOR_TYPE_REPEATED_CUSTOMER;
        
        $record = $this->Campaign->find("first", array(
            "fields" => array("is_trial", "duration_type_id", "campaign_type_id"),
            "conditions" => array("id" => $campaign_id),
            "recursive" => -1
        ));
        
        //debug($record); 
        
        $record = $this->Discount->find("first", array(
            "fields" => array("discount_percentage", "discount_amount", "discount_type"),
            "conditions" => array(
                "discount_code" => $code,
                "discount_for" => $discount_for,
                "campaign_trial_type" => (int) $record["Campaign"]['is_trial'],
                "campaign_target_automobile_type" => (int) $record["Campaign"]['campaign_type_id'],
                "campaign_duration_type" => (int) $record["Campaign"]['duration_type_id'],
                "or" => array(
                    "min_order_amt <=" => $amount, 
                    "min_order_amt" => NULL
                ),
                "or" => array(
                    "min_order_amt >=" => $amount, 
                    "min_order_amt" => NULL
                ),
                "start_date <= " => DateUtility::getCurrentDateTimeString("Y-m-d"),
                "end_date >= " => DateUtility::getCurrentDateTimeString("Y-m-d"),
                "or" => array(
                    "no_of_usage" => NULL,
                    "no_of_usage <" => "actual_usage"
                )
            )
        ));
        
        //debug($record["Discount"]["discount_percentage"]); exit;
        
        if ($record)
        {
            if ($record["Discount"]["discount_percentage"] && $record['Discount']['discount_type'] == DISCOUNT_TYPE_PERCENTAGE_ID)
            {
                return ($amount * ((float) $record["Discount"]["discount_percentage"]/100));
            }
            else
            {
                return  $record["Discount"]["discount_amount"];
            }
        }
        else
        {
            return -1;
        }
    }
    
    protected function _WalletCallBack($user, $notification = true)
    {
        if ($user["id"] == $this->user["id"])
        {
            $this->Session->write('Auth.User.wallet_amount', $user["wallet_amount"]);
            
            if ($notification)
            {
                $notification_data = array(
                    array(
                        "type" => StaticArray::$notification_type_postive,                        
                        "group_id" => ADMIN_GROUP_ID
                    ),
                );
                
                if ($user["amount"] < 0)
                {
                    $notification_data[0]['content'] = "Advertiser " . $user["name"] . " has refund the wallet of amount $" . abs($user["amount"]) ;
                }
                else
                {
                    $notification_data[0]['content'] = "Advertiser " . $user["name"] . " has recharge the wallet of amount $" . abs($user["amount"]) ;
                }

                $this->_saveNotification($notification_data);
            }  
        }
        else
        {
            if ($notification)
            {
                $notification_data = array(
                    array(
                        "type" => StaticArray::$notification_type_postive,                        
                        "user_id" => $user["id"]
                    ),
                );
                
                if ($user["amount"] < 0)
                {
                    $notification_data[0]['content'] = "your wallet has <b> refunded </b> of amount $" . abs($user["amount"]) ;
                }
                else
                {
                    $notification_data[0]['content'] =  "your wallet has <b> recharged </b> of amount $" . abs($user["amount"]) ;
                }
                
                $this->_saveNotification($notification_data);
            }  
        }
         
    }
    
    protected  function _updateWalletAmount($user_id, $amount, $callback = true)
    {
        if ($amount == 0)
        {
            return true;
        }
        
        $this->loadModel("User");
        
        $this->User->recursive = -1;
        $data = $this->User->find("first", array(
            "fields" => array("id", "name", "wallet_amount"),
            "conditions" => array(
                "id" => $user_id
            )
        ));
        
        if (!$data)
        {
            return false;
        }
        
        
        $wallet_amount = (int) $data["User"]["wallet_amount"];        
        $wallet_amount += (int) $amount;
        
        
        $this->User->id = $user_id;
        $result = $this->User->saveField("wallet_amount", $wallet_amount, false);
        
        if ($result)
        {
            $user = $data["User"];
            $user["wallet_amount"] = $wallet_amount;
            $user["amount"] = $amount;
            $this->_WalletCallBack($user, $callback);
            return true;
        }
        else
        {
            return false;
        }
    }    
    
    
    protected function _getCampaignAutomobileCount($automobile_list)
    {
        $automobile_count = 0;
        if ($automobile_list)
        {                
            $automobile_list_str = implode(",", $automobile_list);

            $q = "select count(*) from automobiles Manufacture "
                . "left join automobiles Model on Manufacture.id = Model.automobile_id "
                . "left join automobiles Varient on Model.id = Varient.automobile_id"
                . " WHERE ( Manufacture.id in ($automobile_list_str) OR Model.id in ($automobile_list_str) or Varient.id in ($automobile_list_str))";

            $automobile_count = $this->{$this->modelClass}->query($q);

            $automobile_count = $automobile_count[0][0]['count(*)'];
        }
        
        return $automobile_count;
    }
    
    protected  function _getLocationGeoPoints($location_id, $place_id, $is_exclusive, $is_activated)
    {
        $location_id = $location_id ? $location_id : "NULL";
        $place_id = $place_id ? $place_id : "NULL";
        $is_exclusive = $is_exclusive == null ? "NULL" : $is_exclusive;
        $is_activated = $is_activated == null ? "NULL" : $is_activated;
        
        $data = $this->{$this->modelClass}->query("CALL sp_get_locations_geo_points( $location_id, $place_id, $is_exclusive, $is_activated);");
        
        $records = array();
        foreach ($data as $inner_data)
        {
            if (!isset($records[$inner_data["Location"]["id"]]))
            {
                $arr = $inner_data;
                unset($arr["AdLocation"]);
                unset($arr["Campaign"]);
                $records[$inner_data["Location"]["id"]] = $arr;
            }
            
            $arr = $inner_data["AdLocation"];
            $arr["Campaign"] = $inner_data["Campaign"];
            
            $records[$inner_data["Location"]["id"]]["AdLocation"][] = $arr;
        }
        
        return $records;
    }
}
