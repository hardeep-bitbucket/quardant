<?php
/**
 * Campaign Email template - submitted
 * 
 * @created    01/04/2015
 * @package    TFQ
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */
?>

Advertiser : <b> <?php echo $records['User']['name'] . " " . $records['User']['subname']; ?> </b>,<br><br>
    
campaign <b> <?php echo $records['Campaign']['name'] ?> </b> is Completed

<br/><br/>
<?php echo $content['footer']; ?>