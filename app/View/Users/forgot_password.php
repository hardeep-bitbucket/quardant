<?php
/**
 * Frontend Home Area
 * 
 * 
 * @created    15/12/2014
 * @package    TFQ
 * @copyright  Copyright 2014
 * @license    Proprietary
 * @author     Sonia
 */
?>
<!-- Start header Section-->
<section style="background-image: url('img/frontend/intro-home2.jpg');" class="header-section fading-title parallax banner-margin">
    <div class="section-shade sep-top-2x sep-bottom-2x">
        <!--<div class="section-title text-upper text-center upper light">
            <h2 class="small-space">The New Way to&nbsp;<span class="text-primary">Success</span></h2>
            <p class="lead">Lorem ipsum dolor sit amet, consec</p>
        </div>-->
    </div>
</section>
<!-- Start Skills section-->

<section class="sep-top-1x sep-bottom-lg">
    <div class="container">
        <div class="row">
            <h3 class="upper">Forgot Password</h3>
            <div class="col-md-10 col-md-offset-1">
                <div class="contact-form sep-top">
                    <?php if (($this->Session->check('Message.flash'))) { ?> 
                        <div id="successMessage" class="successMessage alert alert-success text-center">
                            <p><i class="fa fa-check-circle fa-2x"></i></p>
                            <p><?php echo $this->Session->flash(); ?></p>
                        </div>
                    <?php } ?> 
                    <form class="validate" method="post" action="users/forgot_password">
                        <div class="row">
                            <div class="col-md-6 sep-top-xs">
                                <div class="form-group">
                                    <label class="upper" for="name">Username / Email *</label>
                                    <input type="email" class="form-control input-lg" name="username" placeholder="Username / Email" id="username" aria-required="true" required>
                                    <input type="hidden" class="form-control input-lg" name="group_id" value ="<?php echo ADVERTISER_GROUP_ID; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 sep-top-xs">
                                <div class="form-group text-center">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane"></i>&nbsp;Send</button>
                                </div>
                            </div>
                        </div>
                        <!--input#subject.form-control.input-lg.required(type='text', placeholder='Subject of your message', name='subject')
                        -->
                    </form>
                    <div class="hidden"></div>
                </div>
            </div>
        </div>
    </div>
</section>

