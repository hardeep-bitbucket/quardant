<?php

/**
 * Automobiles Controller
 * 
 * 
 * @created    01/04/2014
 * @package    ANPR
 * @copyright  Copyright (C) 2015
 * @license    Proprietary
 * @author     Hardeep
 */

/**
 * @property Automobile $Automobile 
 */
class AutomobilesController extends AppController 
{   
    /**
     * sets $pageList 
     */
    public function beforeFilter() 
    {
        parent::beforeFilter();
        $this->Auth->allow(array(
            "ajaxGetAutomobiles", 
            "ajaxAutmobileLocationForm",
            "ajaxAutmobileLocationFormSave",
            "ajaxGetAutomobileCountOnLocation"
        ));
        
        $pagesList = $this->{$this->modelClass}->getTreeList('automobile_id');
        
        $this->set(compact("pagesList"));
    }

    /**
     * Shows Summary Data 
     */
    public function admin_index() 
    {   
        //Converts querystring to named parameter
        $this->Redirect->urlToNamed();

        // Sets Search Parameters
        $conditions = $this->getSearchConditions(array(
            array('model' => 'Automobile', 'field' => 'name', 'type' => 'string', 'view_field' => 'name'),
            array('model' => 'Automobile', 'field' => 'automobile_id', 'type' => 'integer', 'view_field' => 'automobile_id'),
        ));

//        //In case there is no condition then show only Root level records
        if (!isset($this->params['named']) || empty($this->params['named'])) {
            $conditions['Automobile.automobile_id'] = "";
        }

        if (empty($conditions)) {
            $conditions['Automobile.automobile_id'] = "";
        }
        
        $this->{$this->modelClass}->contain(array(
            "ChildCategory" => array(
                "fields" => array("id", "name")
            ),
            "AdPlayedLog" => array(
                "fields" => array("id")
            )
        ));

        $records = $this->paginate($this->modelClass, $conditions);

//        //Add child count value to data
        foreach ($records as $key => $record) {
            $records[$key]['Automobile']['child_count'] = $this->Automobile->childCount($record['Automobile']['id'], true);
            $records[$key]['Automobile']['relation_count'] = $records[$key]['Automobile']['child_count'];
            $records[$key]['Automobile']['relation_count'] += count($record["AdPlayedLog"]);
        }

        $this->set(compact('records', 'pagesList'));
    }

    /**
     * Adds New Record 
     */
    public function admin_add() {
        //calling function from app controller
        parent::add();
        
        $heading = 'Add ' . $this->modelClass;
        $this->set(compact('heading'));
        $this->render('admin_form');
    }

    /**
     * Updates Existing Record
     * @param type $id 
     */
    public function admin_edit($id) {
        //calling function from app controller            
        parent::edit($id);
        
        $heading = 'Edit ' . $this->modelClass;        
        $this->set(compact('heading'));
        $this->render('admin_form');
    }
    
    /**
     * function get automobile count on given locations in given time period
     * function called in campaign target automobile tab
     */
    public function ajaxGetAutomobileCountOnLocation()
    {
        $list = $this->request->data["ad_location_list"];
        $last_month = $this->request->data["month"];
        
        $date = DateUtility::getCurrentDateTimeString("Y-m-01");        
        $date = DateUtility::getFormatDateFromString(strtotime($date . " " . $last_month. " month"), DEFAULT_SQL_DATE_FORMAT);  
        
        $this->{$this->modelClass}->AutomobileLocation->Location->contain(array(
            "AutomobileLocation" => array(
                "fields" => array("variant_id", "total_count"),
                "conditions" => array(                    
                    "month_year >= " => $date,
                ),                
            )
        ));
                
        $records = $this->{$this->modelClass}->AutomobileLocation->Location->find("all", array(
            "fields" => array("id", "name"),            
            "conditions" => array(
                "Location.id" => $list,
            )
        ));
        
        $records = Hash::combine($records, "{n}.Location.id", "{n}");
        //debug($records); exit;
        
        echo json_encode($records);
        exit;
    }
    
    

    /**
     * get count of cars on particular location
     */
    public function admin_locationAutomobile()
    {
        $manufacture_list = $this->{$this->modelClass}->getAutomobilelist(null);
        
        $year = (int) DateUtility::getCurrentDateTimeString("Y");
        $current_month = (int) DateUtility::getCurrentDateTimeString("m");
        
        $temp = $year - 5;
        
        for ( $i = 0; $i <= 5; $i++)
        {
            $year_list[$year - $i] = ($year - $i);
        }        
        
        $this->set(compact('manufacture_list', "year_list", "current_month"));
    }

    public function ajaxGetAutomobiles($parent_id = 0)
    {
        $list = $this->{$this->modelClass}->getAutomobilelist($parent_id);
        
        echo json_encode($list); exit;
    }
    
    public function ajaxAutmobileLocationForm($variant_id, $month, $year)
    {
        if (!$variant_id)
        {
            echo "Please Select Variant";
            exit;
        }
        
        $date = "$year-$month-01";
        
        $records = $this->{$this->modelClass}->query("CALL sp_get_automobile_locations_count_location_wise('$date', $variant_id);");
           
        $this->set(compact('records', 'variant_id', "month", "year"));
    }
    
    /**
     * save data recieved from ajaxAutmobileLocationForm
     */
    public function ajaxAutmobileLocationFormSave()
    {
        $data = array();
        
        $db = $this->{$this->modelClass}->getdatasource();
        
        $db->begin();
        
        $result = true;
        
        $date = $this->request->data['year'] . "-" . $this->request->data['month'] . "-01";
       
        foreach($this->data["total_count"] as $location_id => $count)
        {
            $count = $count ? $count : 0;
            
            $data = array(
                "month_year" => $date,
                "variant_id" => $this->request->data['variant_id'],
                "location_id" => $location_id,
                "total_count" => $count
            );
            
            if (!$this->{$this->modelClass}->AutomobileLocation->saveRecord($data))
            {
                $result = false;
                break;
            }
        }
        
        if ($result)
        {
            $db->commit();
            echo "1";
        }
        else
        {
            $db->rollback();
            echo "0";
        }
        exit;
    }
    
    /**
     * import the data of automobile from csv file
     */
    public function admin_import()
    {
        $total_records = 0;  $accepted_records = 0;  $rejected_records = 0;
        $msg = "";
        
        if (!empty($this->request->data) && isset($this->request->data[$this->modelClass]["csv_file"]))
        {
            $upload = new FileUtility(1000000, array("csv"));
            $result = $upload->uploadFile($this->request->data[$this->modelClass]["csv_file"], AUTOMOBILE_IMPORT_PATH);
            
            if ($result)
            {   
                $data = fetchCSV($upload->file_path . $upload->full_filename);
                
                if (count($data) == 0)
                {
                    $msg = "No record";
                }
                if (!isset($data[1]["manufacturer"]))
                {
                    $msg = "Missing manufacturer column in csv";
                }
                else if (!isset($data[1]["model"]))
                {
                    $msg = "Missing model column in csv";
                }
                else if (!isset($data[1]["varient"]))
                {
                    $msg = "Missing varient column in csv";
                }
                else
                {
                    $temp = $this->_importCsvData($data);
                    
                    if ($temp)
                    {
                        $total_records = $temp['total'];
                        $rejected_records = $temp['rejected'];
                        $accepted_records = $total_records - $rejected_records;

                        $data["ImportLog"] = array(
                            "type_id" => IMPORT_Automobile_TYPE_ID,
                            "filename" => $upload->full_filename,
                            "total_records" => $total_records,
                            "accepted_records" => $accepted_records,
                            "rejected_records" => $rejected_records,
                        );

                        $this->loadModel("ImportLog");
                        $this->ImportLog->save($data);
                    }
                    else
                    {
                        $msg = "There is problem while saving data";
                    }
                }
            }
            else
            {
                $this->{$this->modelClass}->validationErrors['csv_file'] = $upload->errors;
            }
        }
        
        $heading = 'Import ' . $this->modelClass;        
        $this->set(compact('heading', 'total_records', 'accepted_records', 'rejected_records', 'msg'));
    }
    
    private function _importCsvData($data)
    {
        $this->{$this->modelClass}->recursive = -1;
        $records = $this->{$this->modelClass}->find("threaded");
        
        $insert_records = array();
        $rejected = 0;
        $total = count($data);
        
        $records_count = count ($records);
        
        foreach ($data as $key => $inner_data)
        {
            $inner_data["manufacturer"] =  ucwords(trim($inner_data["manufacturer"]));
            $inner_data["model"] =  ucwords(trim($inner_data["model"]));
            $inner_data["varient"] =  trim($inner_data["varient"]);
            
            if (empty($inner_data["manufacturer"]))
            {
                $rejected++; 
                break;
            }                      
            
            $manufacturer_id = 0;
            $model_id = 0;
            $varient_id = 0;
            $i = 0;
            
            while ( $i < $records_count)
            {   
                $manufacture = $records[$i];
                if (ucwords($manufacture[$this->modelClass]['name']) == $inner_data["manufacturer"])
                {
                    $manufacturer_id  = $manufacture[$this->modelClass]['id'];
                    if ($manufacture["children"])
                    {
                        foreach ($manufacture["children"] as $model)
                        {
                            if (ucwords($model[$this->modelClass]['name']) == $inner_data["model"])
                            {
                                $model_id = $model[$this->modelClass]['id'];
                                if ($model["children"])
                                {
                                    foreach ($model["children"] as $varient)
                                    {
                                        if (trim($varient[$this->modelClass]['name']) == $inner_data["varient"])
                                        {
                                            $varient_id = $varient[$this->modelClass]["id"];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $i++;
            }
            
            if (!$manufacturer_id)
            {
                if (!isset($insert_records[$inner_data["manufacturer"]]))
                {
                    $insert_records[$inner_data["manufacturer"]] = array(
                        "name" => $inner_data["manufacturer"],                        
                    );
                }
                
                if (!isset( $insert_records[$inner_data["manufacturer"]]["children"][$inner_data["model"]]))
                {
                    $insert_records[$inner_data["manufacturer"]]["children"][$inner_data["model"]] = array(
                        "name" => $inner_data["model"],
                    );
                }
                
                if (isset($inner_data["varient"]) && $inner_data["varient"])
                {
                    $insert_records[$inner_data["manufacturer"]]["children"][$inner_data["model"]]["children"][$inner_data["varient"]] = array(
                            "name" => $inner_data["varient"]
                    );
                }
                
                
            }
            else if (!$model_id)
            {
                if (!isset($insert_records[$inner_data["model"]]))
                {
                    $insert_records[$inner_data["model"]] = array(
                        "name" => $inner_data["model"],                         
                        "automobile_id" => $manufacturer_id
                    );
                }
                
                if (isset($inner_data["varient"]) && $inner_data["varient"])
                {
                    $insert_records[$inner_data["model"]]["children"][$inner_data["varient"]] = array(
                            "name" => $inner_data["varient"]
                    );                 
                }               
            }
            else if (!$varient_id && $inner_data["varient"])
            {
                $insert_records[$inner_data["varient"]] = array(
                    "name" => $inner_data["varient"],                   
                    "automobile_id" => $model_id
                );                                  
            }
            else
            {
                $rejected++;
            }
        }
        
        $db = $this->{$this->modelClass}->getdatasource();
        
        $db->begin();
        
        $result = $this->_saveTreeData($insert_records);        
        
        if ($result)
        {
            $this->Session->setFlash('Import successfully.', 'flash_success');
            $db->commit();
        }
        else
        {
            $this->Session->setFlash('Import Failed', 'flash_failure');
            $db->rollback();
        }
            
        if ($result)
        {
            return array(
                "total" => $total,
                "rejected" => $rejected
            );
        }
        else
        {
            return false;
        }
    }
    
    private function _saveTreeData($records, $parent_id = null)
    {
        //debug($records); exit;; 
        foreach ($records as $record)
        {
            $data[$this->modelClass] = array(
                "name" => $record["name"], 
                "is_active" => 1,
                "automobile_id" => isset($record["automobile_id"]) ? $record["automobile_id"] : $parent_id
            );

            $this->{$this->modelClass}->create();
            if ($this->{$this->modelClass}->save($data, false))
            {
                if (isset($record["children"]) && !empty($record["children"]))
                {
                    if (!$this->_saveTreeData($record["children"], $this->{$this->modelClass}->id))
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }
        
        return true;
    }

}
