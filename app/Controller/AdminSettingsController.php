<?php

/**
 * Admin Settings Controller
 * 
 * 
 * @created    12/03/2015
 * @package    anpr
 * @copyright  Copyright 2015
 * @license    Proprietary
 * @author     Hardeep
 */

class AdminSettingsController extends AppController 
{
    
    /*
     * @Edit Screen
     */

    public function admin_edit($id = null) 
    {
        if (!$id) 
        {
           $record = $this->{$this->modelClass}->find("first");
        }        

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->{$this->modelClass}->id = $record[$this->modelClass]['id'];                        
            if ($this->{$this->modelClass}->save($this->request->data)) {                   
                $this->Session->setFlash('Updated successfully.', 'flash_success');                                
            } else {                
                $this->Session->setFlash('Unable to update.', 'flash_failure');
            }
        }
        
        if (!$this->request->data && isset($record)) 
        {
            $this->request->data = $record;
        }
        
        $heading = 'Edit Advertiser';

        $this->set(compact('heading'));
        $this->render('admin_form');
    }
    
    
    public function admin_dashboard()
    {   
        $user = $this->Auth->User();
        
        if ($user['group_id'] != ADVERTISER_GROUP_ID)
        {
            $this->redirect(array("controller" => "users", "action" => "admin_dashboard", "admin" => true));
        }
        
        
        $this->{$this->modelClass}->Campaign->contain(array( 
                "MastTransaction"
            ));
        
        $records = $this->{$this->modelClass}->Campaign->find("all", array(
            "fields" => array("id", "name", "budget", "start_date", "end_date", "status_type_id", "transaction_status"),
            "conditions" => array(
                "user_id" => $user['id']
            )
        ));
        
        //debug($records); exit;
        
        $transactions["credit"] = 0; $transactions["debit"] = 0;
        $campaigns['paid'] = 0;
        $campaigns['unpaid'] = 0;
        $campaigns['total'] = 0;
        if ($records)
        {
            foreach ($records as $key => $record)
            {
                if ($record['MastTransaction'])
                {
                    foreach ($record['MastTransaction'] as $data)
                    {
                        $transactions["credit"] += $data['credit'];
                        $transactions["debit"] += $data['debit'];
                    }
                }

                if ($record['Campaign']['transaction_status'] == StaticArray::$transaction_status_paid)
                {
                    $campaigns['paid'] += 1;
                }
                else
                {
                    $campaigns['unpaid'] += 1;
                }                
                
                $campaigns['total'] += 1;
            }
        }
        
        $this->set(compact('campaigns', 'transactions', 'records'));
    }

    /*
     * @Get Industry Types
     */

    private function _getIndustryType() {
        $industry_types = $this->User->Type->find("list", array("conditions" => array("Type.type" => INDUSTRY_TYPES)));
        $this->set(compact("industry_types"));
    }

}
